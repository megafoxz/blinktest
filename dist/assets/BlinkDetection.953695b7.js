import{a4 as xc,a5 as bc,k as dt,a6 as Qo,a7 as Id,a8 as Zo,n as Ad,b as $,p as je,e as Ae,G as kt,Q as Io,S as wc,N as Et,K as Ce,y as nt,a9 as Dd,W as mr,_ as Fr,$ as Fi,F as Cc,I as pt,aa as Pi,J as Pr,m as ea,l as ta,V as kn,ab as Mi,X as $t,h as Td,ac as ou,O as Oi,ad as _c,v as Ec,a0 as Nd,q as Bi,ae as Fd,af as Sc,B as Li,z as kc,s as Pd,a1 as Rc,ag as Md,A as Ic,P as Od,t as Ac,ah as Bd,ai as Ld,aj as na,ak as Wd,al as Vd,am as zd,o as Ut,a2 as oo,w as at,a as ut,c as $n,d as Yn,an as tn,ao,ap as Ia,aq as Ud,ar as Gd}from"./index.f709815c.js";import{$ as Hd}from"./helper.0e8b7f95.js";import{_ as qd}from"./_plugin-vue_export-helper.cdc0426e.js";import{m as Rt,u as Qt}from"./resizeObserver.503e9f79.js";import{m as Wi,u as Vi,a as zi,b as Dc,c as jd,d as Xr,e as Tc,f as Nc,g as Fc,h as Pc,i as Kd,j as Xd,k as Mc,l as Ao,n as $d,o as Yd,p as Jd,q as Qd,r as Zd,s as ep,R as tp,t as np,v as rp,w as op,x as ap,y as ip,z as sp,L as up,A as au,B as Aa,C as Da,D as iu,E as su,F as cp,G as lp,H as hp,I as fp,V as io}from"./VBtn.f3472c96.js";import{u as dp}from"./layout.cd282da9.js";class ar{constructor(t){let{x:e,y:n,width:o,height:a}=t;this.x=e,this.y=n,this.width=o,this.height=a}get top(){return this.y}get bottom(){return this.y+this.height}get left(){return this.x}get right(){return this.x+this.width}}function uu(r,t){return{x:{before:Math.max(0,t.left-r.left),after:Math.max(0,r.right-t.right)},y:{before:Math.max(0,t.top-r.top),after:Math.max(0,r.bottom-t.bottom)}}}function Oc(r){const t=r.getBoundingClientRect(),e=getComputedStyle(r),n=e.transform;if(n){let o,a,i,s,u;if(n.startsWith("matrix3d("))o=n.slice(9,-1).split(/, /),a=+o[0],i=+o[5],s=+o[12],u=+o[13];else if(n.startsWith("matrix("))o=n.slice(7,-1).split(/, /),a=+o[0],i=+o[3],s=+o[4],u=+o[5];else return new ar(t);const c=e.transformOrigin,l=t.x-s-(1-a)*parseFloat(c),h=t.y-u-(1-i)*parseFloat(c.slice(c.indexOf(" ")+1)),f=a?t.width/a:r.offsetWidth+1,d=i?t.height/i:r.offsetHeight+1;return new ar({x:l,y:h,width:f,height:d})}else return new ar(t)}function Sr(r,t,e){if(typeof r.animate>"u")return{finished:Promise.resolve()};let n;try{n=r.animate(t,e)}catch{return{finished:Promise.resolve()}}return typeof n.finished>"u"&&(n.finished=new Promise(o=>{n.onfinish=()=>{o(n)}})),n}const xo=new WeakMap;function pp(r,t){Object.keys(t).forEach(e=>{if(xc(e)){const n=bc(e),o=xo.get(r);if(t[e]==null)o==null||o.forEach(a=>{const[i,s]=a;i===n&&(r.removeEventListener(n,s),o.delete(a))});else if(!o||![...o].some(a=>a[0]===n&&a[1]===t[e])){r.addEventListener(n,t[e]);const a=o||new Set;a.add([n,t[e]]),xo.has(r)||xo.set(r,a)}}else t[e]==null?r.removeAttribute(e):r.setAttribute(e,t[e])})}function vp(r,t){Object.keys(t).forEach(e=>{if(xc(e)){const n=bc(e),o=xo.get(r);o==null||o.forEach(a=>{const[i,s]=a;i===n&&(r.removeEventListener(n,s),o.delete(a))})}else r.removeAttribute(e)})}function Ui(r){let t=arguments.length>1&&arguments[1]!==void 0?arguments[1]:"div",e=arguments.length>2?arguments[2]:void 0;return dt()({name:e!=null?e:Qo(Id(r.replace(/__/g,"-"))),props:{tag:{type:String,default:t},...Rt()},setup(n,o){let{slots:a}=o;return()=>{var i;return Zo(n.tag,{class:[r,n.class],style:n.style},(i=a.default)==null?void 0:i.call(a))}}})}function Bc(r){if(typeof r.getRootNode!="function"){for(;r.parentNode;)r=r.parentNode;return r!==document?null:document}const t=r.getRootNode();return t!==document&&t.getRootNode({composed:!0})!==document?null:t}const ai="cubic-bezier(0.4, 0, 0.2, 1)",mp="cubic-bezier(0.0, 0, 0.2, 1)",gp="cubic-bezier(0.4, 0, 1, 1)";function yp(r){let t=arguments.length>1&&arguments[1]!==void 0?arguments[1]:!1;for(;r;){if(t?xp(r):Gi(r))return r;r=r.parentElement}return document.scrollingElement}function Do(r,t){const e=[];if(t&&r&&!t.contains(r))return e;for(;r&&(Gi(r)&&e.push(r),r!==t);)r=r.parentElement;return e}function Gi(r){if(!r||r.nodeType!==Node.ELEMENT_NODE)return!1;const t=window.getComputedStyle(r);return t.overflowY==="scroll"||t.overflowY==="auto"&&r.scrollHeight>r.clientHeight}function xp(r){if(!r||r.nodeType!==Node.ELEMENT_NODE)return!1;const t=window.getComputedStyle(r);return["scroll","auto"].includes(t.overflowY)}function bp(r){for(;r;){if(window.getComputedStyle(r).position==="fixed")return!0;r=r.offsetParent}return!1}/**
 * @license
 * Copyright 2020 Google LLC. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */var Lc=function(r,t){return(Lc=Object.setPrototypeOf||{__proto__:[]}instanceof Array&&function(e,n){e.__proto__=n}||function(e,n){for(var o in n)n.hasOwnProperty(o)&&(e[o]=n[o])})(r,t)};function Mt(r,t){function e(){this.constructor=r}Lc(r,t),r.prototype=t===null?Object.create(t):(e.prototype=t.prototype,new e)}function Q(r,t,e,n){return new(e||(e=Promise))(function(o,a){function i(c){try{u(n.next(c))}catch(l){a(l)}}function s(c){try{u(n.throw(c))}catch(l){a(l)}}function u(c){c.done?o(c.value):new e(function(l){l(c.value)}).then(i,s)}u((n=n.apply(r,t||[])).next())})}function Z(r,t){var e,n,o,a,i={label:0,sent:function(){if(1&o[0])throw o[1];return o[1]},trys:[],ops:[]};return a={next:s(0),throw:s(1),return:s(2)},typeof Symbol=="function"&&(a[Symbol.iterator]=function(){return this}),a;function s(u){return function(c){return function(l){if(e)throw new TypeError("Generator is already executing.");for(;i;)try{if(e=1,n&&(o=2&l[0]?n.return:l[0]?n.throw||((o=n.return)&&o.call(n),0):n.next)&&!(o=o.call(n,l[1])).done)return o;switch(n=0,o&&(l=[2&l[0],o.value]),l[0]){case 0:case 1:o=l;break;case 4:return i.label++,{value:l[1],done:!1};case 5:i.label++,n=l[1],l=[0];continue;case 7:l=i.ops.pop(),i.trys.pop();continue;default:if(!(o=(o=i.trys).length>0&&o[o.length-1])&&(l[0]===6||l[0]===2)){i=0;continue}if(l[0]===3&&(!o||l[1]>o[0]&&l[1]<o[3])){i.label=l[1];break}if(l[0]===6&&i.label<o[1]){i.label=o[1],o=l;break}if(o&&i.label<o[2]){i.label=o[2],i.ops.push(l);break}o[2]&&i.ops.pop(),i.trys.pop();continue}l=t.call(r,i)}catch(h){l=[6,h],n=0}finally{e=o=0}if(5&l[0])throw l[1];return{value:l[0]?l[1]:void 0,done:!0}}([u,c])}}}var wp=function(){function r(t){this.global=t,this.flags={},this.flagRegistry={},this.urlFlags={},this.populateURLFlags()}return r.prototype.setPlatform=function(t,e){this.platform!=null&&console.warn("Platform "+this.platformName+" has already been set. Overwriting the platform with "+e+"."),this.platformName=t,this.platform=e},r.prototype.registerFlag=function(t,e,n){if(this.flagRegistry[t]={evaluationFn:e,setHook:n},this.urlFlags[t]!=null){var o=this.urlFlags[t];console.warn("Setting feature override from URL "+t+": "+o+"."),this.set(t,o)}},r.prototype.get=function(t){return t in this.flags?this.flags[t]:(this.flags[t]=this.evaluateFlag(t),this.flags[t])},r.prototype.getNumber=function(t){return this.get(t)},r.prototype.getBool=function(t){return this.get(t)},r.prototype.getFlags=function(){return this.flags},Object.defineProperty(r.prototype,"features",{get:function(){return this.flags},enumerable:!0,configurable:!0}),r.prototype.set=function(t,e){if(this.flagRegistry[t]==null)throw new Error("Cannot set flag "+t+" as it has not been registered.");this.flags[t]=e,this.flagRegistry[t].setHook!=null&&this.flagRegistry[t].setHook(e)},r.prototype.evaluateFlag=function(t){if(this.flagRegistry[t]==null)throw new Error("Cannot evaluate flag '"+t+"': no evaluation function found.");return this.flagRegistry[t].evaluationFn()},r.prototype.setFlags=function(t){this.flags=Object.assign({},t)},r.prototype.reset=function(){this.flags={},this.urlFlags={},this.populateURLFlags()},r.prototype.populateURLFlags=function(){var t=this;if(this.global!==void 0&&this.global.location!==void 0&&this.global.location.search!==void 0){var e,n,o=(e=this.global.location.search,n={},e.replace(/[?&]([^=?&]+)(?:=([^&]*))?/g,function(a){for(var i=[],s=1;s<arguments.length;s++)i[s-1]=arguments[s];return Cp(n,i[0],i[1]),i.join("=")}),n);"tfjsflags"in o&&o.tfjsflags.split(",").forEach(function(a){var i=a.split(":"),s=i[0],u=i[1];t.urlFlags[s]=function(c,l){if((l=l.toLowerCase())==="true"||l==="false")return l==="true";if(""+ +l===l)return+l;throw new Error("Could not parse value flag value "+l+" for flag "+c+".")}(s,u)})}},r}();function Cp(r,t,e){r[decodeURIComponent(t)]=decodeURIComponent(e||"")}function V(){return Wc}var Wc=null,To=new Map,ii=new Map;function Vc(r,t){var e=Uc(r,t);return To.get(e)}function _p(r){return ii.get(r)}function cu(r){for(var t=To.entries(),e=[];;){var n=t.next(),o=n.done,a=n.value;if(o)break;var i=a[0],s=a[1];i.split("_")[0]===r&&e.push(s)}return e}function zc(r){var t=r.kernelName,e=r.backendName,n=Uc(t,e);if(To.has(n))throw new Error("The kernel '"+t+"' for backend '"+e+"' is already registered");To.set(n,r)}function Ep(r){var t=r.kernelName;ii.has(t)&&console.warn("Overriding the gradient for '"+t+"'"),ii.set(t,r)}function Uc(r,t){return t+"_"+r}function lu(r){for(var t=r.length,e=0,n=0;t>0;)n=Math.random()*t|0,e=r[--t],r[t]=r[n],r[n]=e}function No(r,t,e){return Math.max(r,Math.min(t,e))}function Hi(r){return r%2==0?r:r+1}function Gc(r){for(var t=0,e=0;e<r.length;e++)t+=r[e];return t}function R(r,t){if(!r)throw new Error(typeof t=="string"?t:t())}function be(r,t,e){e===void 0&&(e=""),R(Oe(r,t),function(){return e+" Shapes "+r+" and "+t+" must match"})}function Gn(r){R(r!=null,function(){return"The input to the tensor constructor must be a non-null value."})}function sn(r,t,e){if(t===void 0&&(t=[]),e===void 0&&(e=!1),t==null&&(t=[]),Array.isArray(r)||Ye(r)&&!e)for(var n=0;n<r.length;++n)sn(r[n],t,e);else t.push(r);return t}function ee(r){if(r.length===0)return 1;for(var t=r[0],e=1;e<r.length;e++)t*=r[e];return t}function Oe(r,t){if(r===t)return!0;if(r==null||t==null||r.length!==t.length)return!1;for(var e=0;e<r.length;e++)if(r[e]!==t[e])return!1;return!0}function Fe(r){return r%1==0}function Hc(r){if(Math.tanh!=null)return Math.tanh(r);if(r===1/0)return 1;if(r===-1/0)return-1;var t=Math.exp(2*r);return(t-1)/(t+1)}function Fo(r){var t=Math.ceil(Math.sqrt(r));return[t,Math.ceil(r/t)]}function Bn(r,t){return t<=r.length?r:r+" ".repeat(t-r.length)}function si(r,t,e){return t===void 0&&(t=function(n){return 0}),new Promise(function(n,o){var a=0,i=function(){if(r())n();else{a++;var s=t(a);e!=null&&a>=e?o():setTimeout(i,s)}};i()})}function qc(r,t){for(var e=1,n=-1,o=0;o<r.length;++o)if(r[o]>=0)e*=r[o];else if(r[o]===-1){if(n!==-1)throw Error("Shapes can only have 1 implicit size. Found -1 at dim "+n+" and dim "+o);n=o}else if(r[o]<0)throw Error("Shapes can not be < 0. Found "+r[o]+" at dim "+o);if(n===-1){if(t>0&&t!==e)throw Error("Size("+t+") must match the product of shape "+r);return r}if(e===0)throw Error("Cannot infer the missing size in ["+r+"] when there are 0 elements");if(t%e!=0)throw Error("The implicit shape can't be a fractional number. Got "+t+" / "+e);var a=r.slice();return a[n]=t/e,a}function Le(r,t){var e=t.length;return R((r=r==null?t.map(function(n,o){return o}):[].concat(r)).every(function(n){return n>=-e&&n<e}),function(){return"All values in axis param must be in range [-"+e+", "+e+") but got axis "+r}),R(r.every(function(n){return Fe(n)}),function(){return"All values in axis param must be integers but got axis "+r}),r.map(function(n){return n<0?e+n:n})}function gn(r,t){for(var e=[],n=[],o=t!=null&&Array.isArray(t)&&t.length===0,a=t==null||o?null:Le(t,r).sort(),i=0,s=0;s<r.length;++s){if(a!=null){if(a[i]===s&&r[s]!==1)throw new Error("Can't squeeze axis "+s+" since its dim '"+r[s]+"' is not 1");(a[i]==null||a[i]>s)&&r[s]===1&&(e.push(r[s]),n.push(s)),a[i]<=s&&i++}r[s]!==1&&(e.push(r[s]),n.push(s))}return{newShape:e,keptDims:n}}function fr(r,t){var e=null;if(r==null||r==="float32")e=new Float32Array(t);else if(r==="int32")e=new Int32Array(t);else{if(r!=="bool")throw new Error("Unknown data type "+r);e=new Uint8Array(t)}return e}function Mr(r,t){var e=null;if(r==null||r==="float32")e=new Float32Array(t);else if(r==="int32")e=new Int32Array(t);else if(r==="bool")e=new Uint8Array(t);else{if(r!=="string")throw new Error("Unknown data type "+r);e=new Array(t)}return e}function jc(r,t){for(var e=0;e<r.length;e++){var n=r[e];if(isNaN(n)||!isFinite(n))throw Error("A tensor of type "+t+" being uploaded contains "+n+".")}}function Kc(r){return r==="bool"||r==="complex64"||r==="float32"||r==="int32"||r==="string"}function Xc(r,t){return t!=="complex64"&&(t!=="float32"||r==="complex64")&&(t!=="int32"||r==="float32"||r==="complex64")&&(t!=="bool"||r!=="bool")}function Ye(r){return r instanceof Float32Array||r instanceof Int32Array||r instanceof Uint8Array}function qi(r){if(r==="float32"||r==="int32")return 4;if(r==="complex64")return 8;if(r==="bool")return 1;throw new Error("Unknown dtype "+r)}function $c(r){if(r==null)return 0;var t=0;return r.forEach(function(e){return t+=e.length}),t}function yn(r){return typeof r=="string"||r instanceof String}function Yc(r){return typeof r=="boolean"}function Jc(r){return typeof r=="number"}function gr(r){return Array.isArray(r)?gr(r[0]):r instanceof Float32Array?"float32":r instanceof Int32Array||r instanceof Uint8Array?"int32":Jc(r)?"float32":yn(r)?"string":Yc(r)?"bool":"float32"}function Po(r){return!!(r&&r.constructor&&r.call&&r.apply)}function Mo(r,t){for(var e=t;e<r;++e)if(r%e==0)return e;return r}function Ft(r){var t=r.length;if(t<2)return[];var e=new Array(t-1);e[t-2]=r[t-1];for(var n=t-3;n>=0;--n)e[n]=e[n+1]*r[n+1];return e}function ji(r,t,e){if(t==="string")throw new Error("Cannot convert a string[] to a TypedArray");if(Array.isArray(r)&&(r=sn(r)),e&&jc(r,t),function(a,i){return a instanceof Float32Array&&i==="float32"||a instanceof Int32Array&&i==="int32"||a instanceof Uint8Array&&i==="bool"}(r,t))return r;if(t==null||t==="float32"||t==="complex64")return new Float32Array(r);if(t==="int32")return new Int32Array(r);if(t==="bool"){for(var n=new Uint8Array(r.length),o=0;o<n.length;++o)Math.round(r[o])!==0&&(n[o]=1);return n}throw new Error("Unknown data type "+t)}function ui(r,t){if(r.length===0)return t[0];var e=r.reduce(function(n,o){return n*o});if(e===0)return[];if(e!==t.length)throw new Error("["+r+"] does not match the input size.");return function n(o,a,i){var s=new Array;if(a.length===1)for(var u=a[0],c=0;c<u;c++)s[c]=i[o+c];else{u=a[0];var l=a.slice(1),h=l.reduce(function(f,d){return f*d});for(c=0;c<u;c++)s[c]=n(o+c*h,l,i)}return s}(0,r,t)}function Ki(r,t){for(var e=yr(r,t),n=0;n<e.length;n++)e[n]=1;return e}function yr(r,t){if(t==null||t==="float32"||t==="complex64")return new Float32Array(r);if(t==="int32")return new Int32Array(r);if(t==="bool")return new Uint8Array(r);throw new Error("Unknown data type "+t)}function Dt(){return V().platform.now()}function Xi(r){r.forEach(function(t){R(Number.isInteger(t)&&t>=0,function(){return"Tensor must have a shape comprised of positive integers but got shape ["+r+"]."})})}function Qc(r,t){return t===void 0&&(t="utf-8"),t=t||"utf-8",V().platform.encode(r,t)}function Or(r,t){return t===void 0&&(t="utf-8"),t=t||"utf-8",V().platform.decode(r,t)}function ci(r,t,e){if(t===0)return 0;if(t===1)return r[0];for(var n=r[r.length-1],o=0;o<r.length-1;++o)n+=e[o]*r[o];return n}function Zc(r,t,e){if(t===0)return[];if(t===1)return[r];for(var n=new Array(t),o=0;o<n.length-1;++o)n[o]=Math.floor(r/e[o]),r-=n[o]*e[o];return n[n.length-1]=r,n}Object.freeze({shuffle:lu,clamp:No,nearestLargerEven:Hi,sum:Gc,randUniform:function(r,t){var e=Math.random();return t*e+(1-e)*r},distSquared:function(r,t){for(var e=0,n=0;n<r.length;n++){var o=Number(r[n])-Number(t[n]);e+=o*o}return e},assert:R,assertShapesMatch:be,assertNonNull:Gn,flatten:sn,sizeFromShape:ee,isScalarShape:function(r){return r.length===0},arraysEqual:Oe,isInt:Fe,tanh:Hc,sizeToSquarishShape:Fo,createShuffledIndices:function(r){for(var t=new Uint32Array(r),e=0;e<r;++e)t[e]=e;return lu(t),t},rightPad:Bn,repeatedTry:si,inferFromImplicitShape:qc,parseAxisParam:Le,squeezeShape:gn,getTypedArrayFromDType:fr,getArrayFromDType:Mr,checkConversionForErrors:jc,isValidDtype:Kc,hasEncodingLoss:Xc,isTypedArray:Ye,bytesPerElement:qi,bytesFromStringArray:$c,isString:yn,isBoolean:Yc,isNumber:Jc,inferDtype:gr,isFunction:Po,nearestDivisor:Mo,computeStrides:Ft,toTypedArray:ji,toNestedArray:ui,makeOnesTypedArray:Ki,makeZerosTypedArray:yr,now:Dt,assertNonNegativeIntegerDimensions:Xi,fetch:function(r,t){return V().platform.fetch(r,t)},encodeString:Qc,decodeString:Or,locToIndex:ci,indexToLoc:Zc});var Sp=function(){function r(t,e){this.backendTimer=t,this.logger=e,e==null&&(this.logger=new kp)}return r.prototype.profileKernel=function(t,e,n){var o,a=this,i=this.backendTimer.time(function(){o=n()});return o.forEach(function(s){s.data().then(function(u){(function(c,l,h){if(l!=="float32")return!1;for(var f=0;f<c.length;f++){var d=c[f];if(isNaN(d)||!isFinite(d))return console.warn("Found "+d+" in the result of '"+h+"'"),!0}})(u,s.dtype,t),i.then(function(c){var l="";c.getExtraProfileInfo!=null&&(l=c.getExtraProfileInfo()),a.logger.logKernelProfile(t,s,u,c.kernelMs,e,l)})})}),o},r}(),kp=function(){function r(){}return r.prototype.logKernelProfile=function(t,e,n,o,a,i){var s=typeof o=="number"?Bn(o+"ms",9):o.error,u=Bn(t,25),c=e.rank,l=e.size,h=Bn(e.shape.toString(),14),f="";for(var d in a){var p=a[d].shape||e.shape,v=p.length;f+=d+": "+v+"D "+(v>0?p:"")+" "}console.log("%c"+u+"	%c"+s+"	%c"+c+"D "+h+"	%c"+l+"	%c"+f+"	%c"+i,"font-weight:bold","color:red","color:blue","color: orange","color: green","color: steelblue")},r}(),hu=20,br=3,Ta=7;function Rp(r,t,e,n){var o=Ft(t),a=function(c,l,h,f){var d=ee(l),p=f[f.length-1],v=new Array(p).fill(0),m=l.length,g=h==="complex64"?Cr(c):c;if(m>1)for(var x=0;x<d/p;x++)for(var y=x*p,b=0;b<p;b++)v[b]=Math.max(v[b],wr(g[y+b],0,h).length);return v}(r,t,e,o),i=t.length,s=function c(l,h,f,d,p,v){v===void 0&&(v=!0);var m=f==="complex64"?2:1,g=h[0],x=h.length;if(x===0)return f==="complex64"?[wr(Cr(l)[0],0,f)]:f==="bool"?[el(l[0])]:[l[0].toString()];if(x===1){if(g>hu){var y=br*m,b=Array.from(l.slice(0,y)),w=Array.from(l.slice((g-br)*m,g*m));return f==="complex64"&&(b=Cr(b),w=Cr(w)),["["+b.map(function(B,U){return wr(B,p[U],f)}).join(", ")+", ..., "+w.map(function(B,U){return wr(B,p[g-br+U],f)}).join(", ")+"]"]}return["["+(f==="complex64"?Cr(l):Array.from(l)).map(function(B,U){return wr(B,p[U],f)}).join(", ")+"]"]}var C=h.slice(1),I=d.slice(1),E=d[0]*m,S=[];if(g>hu){for(var k=0;k<br;k++){var T=(A=k*E)+E;S.push.apply(S,c(l.slice(A,T),C,f,I,p,!1))}for(S.push("..."),k=g-br;k<g;k++)T=(A=k*E)+E,S.push.apply(S,c(l.slice(A,T),C,f,I,p,k===g-1))}else for(k=0;k<g;k++){var A;T=(A=k*E)+E,S.push.apply(S,c(l.slice(A,T),C,f,I,p,k===g-1))}var F=x===2?",":"";for(S[0]="["+S[0]+F,k=1;k<S.length-1;k++)S[k]=" "+S[k]+F;var P=`,
`;for(k=2;k<x;k++)P+=`
`;return S[S.length-1]=" "+S[S.length-1]+"]"+(v?"":P),S}(r,t,e,o,a),u=["Tensor"];return n&&(u.push("  dtype: "+e),u.push("  rank: "+i),u.push("  shape: ["+t+"]"),u.push("  values:")),u.push(s.map(function(c){return"    "+c}).join(`
`)),u.join(`
`)}function wr(r,t,e){return Bn(Array.isArray(r)?parseFloat(r[0].toFixed(Ta))+" + "+parseFloat(r[1].toFixed(Ta))+"j":yn(r)?"'"+r+"'":e==="bool"?el(r):parseFloat(r.toFixed(Ta)).toString(),t)}function el(r){return r===0?"false":"true"}function Cr(r){for(var t=[],e=0;e<r.length;e+=2)t.push([r[e],r[e+1]]);return t}var Br=function(){function r(t,e,n){var o=this;if(this.dtype=e,this.shape=t.slice(),this.size=ee(t),n!=null){var a=n.length;R(a===this.size,function(){return"Length of values '"+a+"' does not match the size inferred by the shape '"+o.size+"'."})}if(e==="complex64")throw new Error("complex64 dtype TensorBuffers are not supported. Please create a TensorBuffer for the real and imaginary parts separately and call tf.complex(real, imag).");this.values=n||Mr(e,this.size),this.strides=Ft(t)}return r.prototype.set=function(t){for(var e=this,n=[],o=1;o<arguments.length;o++)n[o-1]=arguments[o];n.length===0&&(n=[0]),R(n.length===this.rank,function(){return"The number of provided coordinates ("+n.length+") must match the rank ("+e.rank+")"});var a=this.locToIndex(n);this.values[a]=t},r.prototype.get=function(){for(var t=[],e=0;e<arguments.length;e++)t[e]=arguments[e];t.length===0&&(t=[0]);for(var n=0,o=0,a=t;o<a.length;o++){var i=a[o];if(i<0||i>=this.shape[n]){var s="Requested out of range element at "+t+".   Buffer shape="+this.shape;throw new Error(s)}n++}for(var u=t[t.length-1],c=0;c<t.length-1;++c)u+=this.strides[c]*t[c];return this.values[u]},r.prototype.locToIndex=function(t){if(this.rank===0)return 0;if(this.rank===1)return t[0];for(var e=t[t.length-1],n=0;n<t.length-1;++n)e+=this.strides[n]*t[n];return e},r.prototype.indexToLoc=function(t){if(this.rank===0)return[];if(this.rank===1)return[t];for(var e=new Array(this.shape.length),n=0;n<e.length-1;++n)e[n]=Math.floor(t/this.strides[n]),t-=e[n]*this.strides[n];return e[e.length-1]=t,e},Object.defineProperty(r.prototype,"rank",{get:function(){return this.shape.length},enumerable:!0,configurable:!0}),r.prototype.toTensor=function(){return qt().makeTensor(this.values,this.shape,this.dtype)},r}(),qt=null,O=null,tl=null,Te=function(){function r(t,e,n,o){this.kept=!1,this.isDisposedInternal=!1,this.shape=t.slice(),this.dtype=e||"float32",this.size=ee(t),this.strides=Ft(t),this.dataId=n,this.id=o,this.rankType=this.rank<5?this.rank.toString():"higher"}return r.prototype.flatten=function(){return this.throwIfDisposed(),this.as1D()},r.prototype.asScalar=function(){return this.throwIfDisposed(),R(this.size===1,function(){return"The array must have only 1 element."}),this.reshape([])},r.prototype.as1D=function(){return this.throwIfDisposed(),this.reshape([this.size])},r.prototype.as2D=function(t,e){return this.throwIfDisposed(),this.reshape([t,e])},r.prototype.as3D=function(t,e,n){return this.throwIfDisposed(),this.reshape([t,e,n])},r.prototype.as4D=function(t,e,n,o){return this.throwIfDisposed(),this.reshape([t,e,n,o])},r.prototype.as5D=function(t,e,n,o,a){return this.throwIfDisposed(),this.reshape([t,e,n,o,a])},r.prototype.asType=function(t){return this.throwIfDisposed(),O.cast(this,t)},Object.defineProperty(r.prototype,"rank",{get:function(){return this.shape.length},enumerable:!0,configurable:!0}),r.prototype.buffer=function(){return Q(this,void 0,void 0,function(){var t;return Z(this,function(e){switch(e.label){case 0:return[4,this.data()];case 1:return t=e.sent(),[2,O.buffer(this.shape,this.dtype,t)]}})})},r.prototype.bufferSync=function(){return O.buffer(this.shape,this.dtype,this.dataSync())},r.prototype.array=function(){return Q(this,void 0,void 0,function(){var t;return Z(this,function(e){switch(e.label){case 0:return[4,this.data()];case 1:return t=e.sent(),[2,ui(this.shape,t)]}})})},r.prototype.arraySync=function(){return ui(this.shape,this.dataSync())},r.prototype.data=function(){return Q(this,void 0,void 0,function(){var t,e;return Z(this,function(n){switch(n.label){case 0:return this.throwIfDisposed(),t=qt().read(this.dataId),this.dtype!=="string"?[3,2]:[4,t];case 1:e=n.sent();try{return[2,e.map(function(o){return Or(o)})]}catch{throw new Error("Failed to decode the string bytes into utf-8. To get the original bytes, call tensor.bytes().")}n.label=2;case 2:return[2,t]}})})},r.prototype.dataSync=function(){this.throwIfDisposed();var t=qt().readSync(this.dataId);if(this.dtype==="string")try{return t.map(function(e){return Or(e)})}catch{throw new Error("Failed to decode the string bytes into utf-8. To get the original bytes, call tensor.bytes().")}return t},r.prototype.bytes=function(){return Q(this,void 0,void 0,function(){var t;return Z(this,function(e){switch(e.label){case 0:return this.throwIfDisposed(),[4,qt().read(this.dataId)];case 1:return t=e.sent(),this.dtype==="string"?[2,t]:[2,new Uint8Array(t.buffer)]}})})},r.prototype.dispose=function(){this.isDisposed||(qt().disposeTensor(this),this.isDisposedInternal=!0)},Object.defineProperty(r.prototype,"isDisposed",{get:function(){return this.isDisposedInternal},enumerable:!0,configurable:!0}),r.prototype.throwIfDisposed=function(){if(this.isDisposed)throw new Error("Tensor is disposed.")},r.prototype.toFloat=function(){return this.asType("float32")},r.prototype.toInt=function(){return this.asType("int32")},r.prototype.toBool=function(){return this.asType("bool")},r.prototype.print=function(t){return t===void 0&&(t=!1),O.print(this,t)},r.prototype.reshape=function(t){return this.throwIfDisposed(),O.reshape(this,t)},r.prototype.reshapeAs=function(t){return this.throwIfDisposed(),this.reshape(t.shape)},r.prototype.expandDims=function(t){return t===void 0&&(t=0),O.expandDims(this,t)},r.prototype.cumsum=function(t,e,n){return t===void 0&&(t=0),e===void 0&&(e=!1),n===void 0&&(n=!1),O.cumsum(this,t,e,n)},r.prototype.squeeze=function(t){return this.throwIfDisposed(),O.squeeze(this,t)},r.prototype.clone=function(){return this.throwIfDisposed(),O.clone(this)},r.prototype.oneHot=function(t,e,n){return this.throwIfDisposed(),O.oneHot(this,t,e,n)},r.prototype.toString=function(t){return t===void 0&&(t=!1),Rp(this.dataSync(),this.shape,this.dtype,t)},r.prototype.tile=function(t){return this.throwIfDisposed(),O.tile(this,t)},r.prototype.gather=function(t,e){return e===void 0&&(e=0),this.throwIfDisposed(),O.gather(this,t,e)},r.prototype.matMul=function(t,e,n){return e===void 0&&(e=!1),n===void 0&&(n=!1),this.throwIfDisposed(),O.matMul(this,t,e,n)},r.prototype.dot=function(t){return this.throwIfDisposed(),O.dot(this,t)},r.prototype.norm=function(t,e,n){return t===void 0&&(t="euclidean"),e===void 0&&(e=null),n===void 0&&(n=!1),this.throwIfDisposed(),O.norm(this,t,e,n)},r.prototype.slice=function(t,e){return this.throwIfDisposed(),O.slice(this,t,e)},r.prototype.reverse=function(t){return this.throwIfDisposed(),O.reverse(this,t)},r.prototype.concat=function(t,e){return e===void 0&&(e=0),this.throwIfDisposed(),t instanceof r&&(t=[t]),O.concat([this].concat(t),e)},r.prototype.split=function(t,e){return e===void 0&&(e=0),this.throwIfDisposed(),O.split(this,t,e)},r.prototype.stack=function(t,e){return e===void 0&&(e=0),O.stack([this,t],e)},r.prototype.unstack=function(t){return t===void 0&&(t=0),O.unstack(this,t)},r.prototype.pad=function(t,e){return e===void 0&&(e=0),O.pad(this,t,e)},r.prototype.batchNormalization=function(t,e,n,o,a){return n===void 0&&(n=.001),tl("tf.batchNormalization() is going away. Use tf.batchNorm() instead, and note the positional argument change of scale, offset, and varianceEpsilon"),this.batchNorm(t,e,a,o,n)},r.prototype.batchNorm=function(t,e,n,o,a){return a===void 0&&(a=.001),this.throwIfDisposed(),O.batchNorm(this,t,e,n,o,a)},r.prototype.all=function(t,e){return t===void 0&&(t=null),e===void 0&&(e=!1),this.throwIfDisposed(),O.all(this,t,e)},r.prototype.any=function(t,e){return t===void 0&&(t=null),e===void 0&&(e=!1),this.throwIfDisposed(),O.any(this,t,e)},r.prototype.logSumExp=function(t,e){return t===void 0&&(t=null),e===void 0&&(e=!1),this.throwIfDisposed(),O.logSumExp(this,t,e)},r.prototype.sum=function(t,e){return t===void 0&&(t=null),e===void 0&&(e=!1),this.throwIfDisposed(),O.sum(this,t,e)},r.prototype.prod=function(t,e){return t===void 0&&(t=null),e===void 0&&(e=!1),this.throwIfDisposed(),O.prod(this,t,e)},r.prototype.mean=function(t,e){return t===void 0&&(t=null),e===void 0&&(e=!1),this.throwIfDisposed(),O.mean(this,t,e)},r.prototype.min=function(t,e){return t===void 0&&(t=null),e===void 0&&(e=!1),this.throwIfDisposed(),O.min(this,t,e)},r.prototype.max=function(t,e){return t===void 0&&(t=null),e===void 0&&(e=!1),this.throwIfDisposed(),O.max(this,t,e)},r.prototype.argMin=function(t){return t===void 0&&(t=null),this.throwIfDisposed(),O.argMin(this,t)},r.prototype.argMax=function(t){return t===void 0&&(t=null),this.throwIfDisposed(),O.argMax(this,t)},r.prototype.cast=function(t){return this.throwIfDisposed(),O.cast(this,t)},r.prototype.add=function(t){return this.throwIfDisposed(),O.add(this,t)},r.prototype.addStrict=function(t){return this.throwIfDisposed(),O.addStrict(this,t)},r.prototype.atan2=function(t){return this.throwIfDisposed(),O.atan2(this,t)},r.prototype.sub=function(t){return this.throwIfDisposed(),O.sub(this,t)},r.prototype.subStrict=function(t){return this.throwIfDisposed(),O.subStrict(this,t)},r.prototype.pow=function(t){return this.throwIfDisposed(),O.pow(this,t)},r.prototype.powStrict=function(t){return this.throwIfDisposed(),O.powStrict(this,t)},r.prototype.mul=function(t){return this.throwIfDisposed(),O.mul(this,t)},r.prototype.mulStrict=function(t){return this.throwIfDisposed(),O.mulStrict(this,t)},r.prototype.div=function(t){return this.throwIfDisposed(),O.div(this,t)},r.prototype.divNoNan=function(t){return this.throwIfDisposed(),O.divNoNan(this,t)},r.prototype.floorDiv=function(t){return this.throwIfDisposed(),O.floorDiv(this,t)},r.prototype.divStrict=function(t){return this.throwIfDisposed(),O.divStrict(this,t)},r.prototype.minimum=function(t){return this.throwIfDisposed(),O.minimum(this,t)},r.prototype.minimumStrict=function(t){return this.throwIfDisposed(),O.minimumStrict(this,t)},r.prototype.maximum=function(t){return this.throwIfDisposed(),O.maximum(this,t)},r.prototype.maximumStrict=function(t){return this.throwIfDisposed(),O.maximumStrict(this,t)},r.prototype.mod=function(t){return this.throwIfDisposed(),O.mod(this,t)},r.prototype.modStrict=function(t){return this.throwIfDisposed(),O.modStrict(this,t)},r.prototype.squaredDifferenceStrict=function(t){return this.throwIfDisposed(),O.squaredDifferenceStrict(this,t)},r.prototype.transpose=function(t){return this.throwIfDisposed(),O.transpose(this,t)},r.prototype.notEqual=function(t){return this.throwIfDisposed(),O.notEqual(this,t)},r.prototype.notEqualStrict=function(t){return this.throwIfDisposed(),O.notEqualStrict(this,t)},r.prototype.less=function(t){return this.throwIfDisposed(),O.less(this,t)},r.prototype.lessStrict=function(t){return this.throwIfDisposed(),O.lessStrict(this,t)},r.prototype.equal=function(t){return this.throwIfDisposed(),O.equal(this,t)},r.prototype.equalStrict=function(t){return this.throwIfDisposed(),O.equalStrict(this,t)},r.prototype.lessEqual=function(t){return this.throwIfDisposed(),O.lessEqual(this,t)},r.prototype.lessEqualStrict=function(t){return this.throwIfDisposed(),O.lessEqualStrict(this,t)},r.prototype.greater=function(t){return this.throwIfDisposed(),O.greater(this,t)},r.prototype.greaterStrict=function(t){return this.throwIfDisposed(),O.greaterStrict(this,t)},r.prototype.greaterEqual=function(t){return this.throwIfDisposed(),O.greaterEqual(this,t)},r.prototype.greaterEqualStrict=function(t){return this.throwIfDisposed(),O.greaterEqualStrict(this,t)},r.prototype.logicalAnd=function(t){return this.throwIfDisposed(),O.logicalAnd(this,t)},r.prototype.logicalOr=function(t){return this.throwIfDisposed(),O.logicalOr(this,t)},r.prototype.logicalNot=function(){return this.throwIfDisposed(),O.logicalNot(this)},r.prototype.logicalXor=function(t){return this.throwIfDisposed(),O.logicalXor(this,t)},r.prototype.where=function(t,e){return this.throwIfDisposed(),O.where(t,this,e)},r.prototype.neg=function(){return this.throwIfDisposed(),O.neg(this)},r.prototype.ceil=function(){return this.throwIfDisposed(),O.ceil(this)},r.prototype.floor=function(){return this.throwIfDisposed(),O.floor(this)},r.prototype.sign=function(){return this.throwIfDisposed(),O.sign(this)},r.prototype.isNaN=function(){return this.throwIfDisposed(),O.isNaN(this)},r.prototype.isInf=function(){return this.throwIfDisposed(),O.isInf(this)},r.prototype.isFinite=function(){return this.throwIfDisposed(),O.isFinite(this)},r.prototype.exp=function(){return this.throwIfDisposed(),O.exp(this)},r.prototype.expm1=function(){return this.throwIfDisposed(),O.expm1(this)},r.prototype.log=function(){return this.throwIfDisposed(),O.log(this)},r.prototype.log1p=function(){return this.throwIfDisposed(),O.log1p(this)},r.prototype.sqrt=function(){return this.throwIfDisposed(),O.sqrt(this)},r.prototype.rsqrt=function(){return this.throwIfDisposed(),O.rsqrt(this)},r.prototype.square=function(){return this.throwIfDisposed(),O.square(this)},r.prototype.reciprocal=function(){return this.throwIfDisposed(),O.reciprocal(this)},r.prototype.abs=function(){return this.throwIfDisposed(),O.abs(this)},r.prototype.clipByValue=function(t,e){return this.throwIfDisposed(),O.clipByValue(this,t,e)},r.prototype.relu=function(){return this.throwIfDisposed(),O.relu(this)},r.prototype.relu6=function(){return this.throwIfDisposed(),O.relu6(this)},r.prototype.elu=function(){return this.throwIfDisposed(),O.elu(this)},r.prototype.selu=function(){return this.throwIfDisposed(),O.selu(this)},r.prototype.leakyRelu=function(t){return t===void 0&&(t=.2),this.throwIfDisposed(),O.leakyRelu(this,t)},r.prototype.prelu=function(t){return this.throwIfDisposed(),O.prelu(this,t)},r.prototype.sigmoid=function(){return this.throwIfDisposed(),O.sigmoid(this)},r.prototype.logSigmoid=function(){return this.throwIfDisposed(),O.logSigmoid(this)},r.prototype.softplus=function(){return this.throwIfDisposed(),O.softplus(this)},r.prototype.zerosLike=function(){return this.throwIfDisposed(),O.zerosLike(this)},r.prototype.onesLike=function(){return this.throwIfDisposed(),O.onesLike(this)},r.prototype.sin=function(){return this.throwIfDisposed(),O.sin(this)},r.prototype.cos=function(){return this.throwIfDisposed(),O.cos(this)},r.prototype.tan=function(){return this.throwIfDisposed(),O.tan(this)},r.prototype.asin=function(){return this.throwIfDisposed(),O.asin(this)},r.prototype.acos=function(){return this.throwIfDisposed(),O.acos(this)},r.prototype.atan=function(){return this.throwIfDisposed(),O.atan(this)},r.prototype.sinh=function(){return this.throwIfDisposed(),O.sinh(this)},r.prototype.cosh=function(){return this.throwIfDisposed(),O.cosh(this)},r.prototype.tanh=function(){return this.throwIfDisposed(),O.tanh(this)},r.prototype.asinh=function(){return this.throwIfDisposed(),O.asinh(this)},r.prototype.acosh=function(){return this.throwIfDisposed(),O.acosh(this)},r.prototype.atanh=function(){return this.throwIfDisposed(),O.atanh(this)},r.prototype.erf=function(){return this.throwIfDisposed(),O.erf(this)},r.prototype.round=function(){return this.throwIfDisposed(),O.round(this)},r.prototype.step=function(t){return t===void 0&&(t=0),this.throwIfDisposed(),O.step(this,t)},r.prototype.softmax=function(t){return t===void 0&&(t=-1),this.throwIfDisposed(),O.softmax(this,t)},r.prototype.logSoftmax=function(t){return t===void 0&&(t=-1),this.throwIfDisposed(),O.logSoftmax(this,t)},r.prototype.resizeBilinear=function(t,e){return e===void 0&&(e=!1),this.throwIfDisposed(),O.image.resizeBilinear(this,t,e)},r.prototype.resizeNearestNeighbor=function(t,e){return e===void 0&&(e=!1),this.throwIfDisposed(),O.image.resizeNearestNeighbor(this,t,e)},r.prototype.conv1d=function(t,e,n,o,a,i){return o===void 0&&(o="NWC"),a===void 0&&(a=1),this.throwIfDisposed(),O.conv1d(this,t,e,n,o,a,i)},r.prototype.conv2d=function(t,e,n,o,a,i){return o===void 0&&(o="NHWC"),a===void 0&&(a=[1,1]),this.throwIfDisposed(),O.conv2d(this,t,e,n,o,a,i)},r.prototype.conv2dTranspose=function(t,e,n,o,a){return this.throwIfDisposed(),O.conv2dTranspose(this,t,e,n,o,a)},r.prototype.depthwiseConv2D=function(t,e,n,o,a,i){return o===void 0&&(o="NHWC"),a===void 0&&(a=[1,1]),this.throwIfDisposed(),O.depthwiseConv2d(this,t,e,n,o,a,i)},r.prototype.separableConv2d=function(t,e,n,o,a,i){return a===void 0&&(a=[1,1]),i===void 0&&(i="NHWC"),this.throwIfDisposed(),O.separableConv2d(this,t,e,n,o,a,i)},r.prototype.avgPool=function(t,e,n,o){return this.throwIfDisposed(),O.avgPool(this,t,e,n,o)},r.prototype.maxPool=function(t,e,n,o){return this.throwIfDisposed(),O.maxPool(this,t,e,n,o)},r.prototype.localResponseNormalization=function(t,e,n,o){return t===void 0&&(t=5),e===void 0&&(e=1),n===void 0&&(n=1),o===void 0&&(o=.5),O.localResponseNormalization(this,t,e,n,o)},r.prototype.pool=function(t,e,n,o,a){return this.throwIfDisposed(),O.pool(this,t,e,n,o,a)},r.prototype.variable=function(t,e,n){return t===void 0&&(t=!0),this.throwIfDisposed(),qt().makeVariable(this,t,e,n)},r.prototype.unsortedSegmentSum=function(t,e){return this.throwIfDisposed(),O.unsortedSegmentSum(this,t,e)},r.prototype.batchToSpaceND=function(t,e){return this.throwIfDisposed(),O.batchToSpaceND(this,t,e)},r.prototype.spaceToBatchND=function(t,e){return this.throwIfDisposed(),O.spaceToBatchND(this,t,e)},r.prototype.topk=function(t,e){return t===void 0&&(t=1),e===void 0&&(e=!0),this.throwIfDisposed(),O.topk(this,t,e)},r.prototype.stridedSlice=function(t,e,n,o,a,i,s,u){return o===void 0&&(o=0),a===void 0&&(a=0),i===void 0&&(i=0),s===void 0&&(s=0),u===void 0&&(u=0),this.throwIfDisposed(),O.stridedSlice(this,t,e,n,o,a,i,s,u)},r.prototype.depthToSpace=function(t,e){return this.throwIfDisposed(),O.depthToSpace(this,t,e)},r.prototype.fft=function(){return this.throwIfDisposed(),O.spectral.fft(this)},r.prototype.ifft=function(){return this.throwIfDisposed(),O.spectral.ifft(this)},r.prototype.rfft=function(){return this.throwIfDisposed(),O.spectral.rfft(this)},r.prototype.irfft=function(){return this.throwIfDisposed(),O.spectral.irfft(this)},r}();Object.defineProperty(Te,Symbol.hasInstance,{value:function(r){return!!r&&r.dataId!=null&&r.shape!=null&&r.dtype!=null}});var fu,li,hi,fi,di,dr=function(r){function t(e,n,o,a){var i=r.call(this,e.shape,e.dtype,e.dataId,a)||this;return i.trainable=n,i.name=o,i}return Mt(t,r),t.prototype.assign=function(e){if(e.dtype!==this.dtype)throw new Error("dtype of the new value ("+e.dtype+") and previous value ("+this.dtype+") must match");if(!Oe(e.shape,this.shape))throw new Error("shape of the new value ("+e.shape+") and previous value ("+this.shape+") must match");qt().disposeTensor(this),this.dataId=e.dataId,qt().incRef(this,null)},t.prototype.dispose=function(){qt().disposeVariable(this),this.isDisposedInternal=!0},t}(Te);Object.defineProperty(dr,Symbol.hasInstance,{value:function(r){return r instanceof Te&&r.assign!=null&&r.assign instanceof Function}}),function(r){r.R0="R0",r.R1="R1",r.R2="R2",r.R3="R3",r.R4="R4",r.R5="R5",r.R6="R6"}(fu||(fu={})),function(r){r.float32="float32",r.int32="int32",r.bool="int32",r.complex64="complex64"}(li||(li={})),function(r){r.float32="float32",r.int32="int32",r.bool="bool",r.complex64="complex64"}(hi||(hi={})),function(r){r.float32="float32",r.int32="float32",r.bool="float32",r.complex64="complex64"}(fi||(fi={})),function(r){r.float32="complex64",r.int32="complex64",r.bool="complex64",r.complex64="complex64"}(di||(di={}));var Ip={float32:fi,int32:li,bool:hi,complex64:di};function He(r,t){if(r==="string"||t==="string"){if(r==="string"&&t==="string")return"string";throw new Error("Can not upcast "+r+" with "+t)}return Ip[r][t]}function Na(r){return He(r,"int32")}function De(r,t){if(r.dtype===t.dtype)return[r,t];var e=He(r.dtype,t.dtype);return[r.cast(e),t.cast(e)]}function nl(r,t){R(r.dtype===t.dtype,function(){return"The dtypes of the first("+r.dtype+") and second("+t.dtype+") input must match"})}function $i(r){var t=[];return function e(n,o,a){if(n!=null){if(n instanceof Te)return void o.push(n);if(i=n,!(!Array.isArray(i)&&typeof i!="object")){var i,s=n;for(var u in s){var c=s[u];a.has(c)||(a.add(c),e(c,o,a))}}}}(r,t,new Set),t}var Fa;Object.freeze({makeTypesMatch:De,assertTypesMatch:nl,isTensorInList:function(r,t){return t.some(function(e){return e.id===r.id})},getTensorsInContainer:$i});var du=function(){function r(){this.registeredVariables={},this.nextTapeNodeId=0,this.numBytes=0,this.numTensors=0,this.numStringTensors=0,this.numDataBuffers=0,this.gradientDepth=0,this.kernelDepth=0,this.scopeStack=[],this.numDataMovesStack=[],this.nextScopeId=0,this.tensorInfo=new WeakMap,this.profiling=!1,this.activeProfile={newBytes:0,newTensors:0,peakBytes:0,kernels:[],result:null}}return r.prototype.dispose=function(){for(var t in this.registeredVariables)this.registeredVariables[t].dispose()},r}(),Ap=function(){function r(t){this.ENV=t,this.registry={},this.registryFactory={},this.pendingBackendInitId=0,this.state=new du}return r.prototype.ready=function(){return Q(this,void 0,void 0,function(){var t,e,n;return Z(this,function(o){switch(o.label){case 0:if(this.pendingBackendInit!=null)return[2,this.pendingBackendInit.then(function(){})];if(this.backendInstance!=null)return[2];t=this.getSortedBackends(),e=0,o.label=1;case 1:return e<t.length?(n=t[e],[4,this.initializeBackend(n).success]):[3,5];case 2:return o.sent()?[4,this.setBackend(n)]:[3,4];case 3:return o.sent(),[2];case 4:return e++,[3,1];case 5:throw new Error("Could not initialize any backends, all backend initializations failed.")}})})},Object.defineProperty(r.prototype,"backend",{get:function(){if(this.pendingBackendInit!=null)throw new Error("Backend '"+this.backendName+"' has not yet been initialized. Make sure to await tf.ready() or await tf.setBackend() before calling other methods");if(this.backendInstance==null){var t=this.initializeBackendsAndReturnBest(),e=t.name;if(t.asyncInit)throw new Error("The highest priority backend '"+e+"' has not yet been initialized. Make sure to await tf.ready() or await tf.setBackend() before calling other methods");this.setBackend(e)}return this.backendInstance},enumerable:!0,configurable:!0}),r.prototype.backendNames=function(){return Object.keys(this.registryFactory)},r.prototype.findBackend=function(t){return!(t in this.registry)&&(!(t in this.registryFactory)||this.initializeBackend(t).asyncInit)?null:this.registry[t]},r.prototype.findBackendFactory=function(t){return t in this.registryFactory?this.registryFactory[t].factory:null},r.prototype.registerBackend=function(t,e,n){return n===void 0&&(n=1),t in this.registryFactory?(console.warn(t+" backend was already registered. Reusing existing backend factory."),!1):(this.registryFactory[t]={factory:e,priority:n},!0)},r.prototype.setBackend=function(t){return Q(this,void 0,void 0,function(){var e,n,o;return Z(this,function(a){switch(a.label){case 0:if(this.registryFactory[t]==null)throw new Error("Backend name '"+t+"' not found in registry");return this.backendName=t,this.registry[t]!=null?[3,4]:(this.backendInstance=null,e=this.initializeBackend(t),n=e.success,e.asyncInit?[4,n]:[3,2]);case 1:return o=a.sent(),[3,3];case 2:o=n,a.label=3;case 3:if(!o)return[2,!1];a.label=4;case 4:return this.backendInstance=this.registry[t],this.setupRegisteredKernels(),this.profiler=new Sp(this.backendInstance),[2,!0]}})})},r.prototype.setupRegisteredKernels=function(){var t=this;cu(this.backendName).forEach(function(e){e.setupFunc!=null&&e.setupFunc(t.backendInstance)})},r.prototype.disposeRegisteredKernels=function(t){var e=this;cu(t).forEach(function(n){n.disposeFunc!=null&&n.disposeFunc(e.registry[t])})},r.prototype.initializeBackend=function(t){var e=this,n=this.registryFactory[t];if(n==null)throw new Error("Cannot initialize backend "+t+", no registration found.");try{var o=n.factory();if(Promise.resolve(o)===o){var a=++this.pendingBackendInitId,i=o.then(function(s){return!(a<e.pendingBackendInitId)&&(e.registry[t]=s,e.pendingBackendInit=null,!0)}).catch(function(s){return!(a<e.pendingBackendInitId)&&(e.pendingBackendInit=null,console.warn("Initialization of backend "+t+" failed"),console.warn(s.stack||s.message),!1)});return this.pendingBackendInit=i,{success:i,asyncInit:!0}}return this.registry[t]=o,{success:!0,asyncInit:!1}}catch(s){return console.warn("Initialization of backend "+t+" failed"),console.warn(s.stack||s.message),{success:!1,asyncInit:!1}}},r.prototype.removeBackend=function(t){if(!(t in this.registryFactory))throw new Error(t+" backend not found in registry");this.backendName===t&&this.pendingBackendInit!=null&&this.pendingBackendInitId++,t in this.registry&&(this.disposeRegisteredKernels(t),this.registry[t].dispose(),delete this.registry[t]),delete this.registryFactory[t],this.backendName===t&&(this.pendingBackendInit=null,this.backendName=null,this.backendInstance=null)},r.prototype.getSortedBackends=function(){var t=this;if(Object.keys(this.registryFactory).length===0)throw new Error("No backend found in registry.");return Object.keys(this.registryFactory).sort(function(e,n){return t.registryFactory[n].priority-t.registryFactory[e].priority})},r.prototype.initializeBackendsAndReturnBest=function(){for(var t=this.getSortedBackends(),e=0;e<t.length;e++){var n=t[e],o=this.initializeBackend(n),a=o.success,i=o.asyncInit;if(i||a)return{name:n,asyncInit:i}}throw new Error("Could not initialize any backends, all backend initializations failed.")},r.prototype.moveData=function(t,e){var n=this.state.tensorInfo.get(e),o=n.backend,a=this.readSync(e);o.disposeData(e),n.backend=t,t.move(e,a,n.shape,n.dtype),this.shouldCheckForMemLeaks()&&this.state.numDataMovesStack[this.state.numDataMovesStack.length-1]++},r.prototype.tidy=function(t,e){var n,o=this,a=null;if(e==null){if(typeof t!="function")throw new Error("Please provide a function to tidy()");e=t}else{if(typeof t!="string"&&!(t instanceof String))throw new Error("When calling with two arguments, the first argument to tidy() must be a string");if(typeof e!="function")throw new Error("When calling with two arguments, the 2nd argument to tidy() must be a function");a=t}return this.scopedRun(function(){return o.startScope(a)},function(){return o.endScope(n)},function(){return(n=e())instanceof Promise&&console.error("Cannot return a Promise inside of tidy."),n})},r.prototype.scopedRun=function(t,e,n){t();try{var o=n();return e(),o}catch(a){throw e(),a}},r.prototype.nextTensorId=function(){return r.nextTensorId++},r.prototype.nextVariableId=function(){return r.nextVariableId++},r.prototype.clone=function(t){var e=this.makeTensorFromDataId(t.dataId,t.shape,t.dtype),n={x:t};return this.addTapeNode(this.state.activeScope.name,n,[e],function(o){return{x:function(){return o.toFloat()}}},[]),e},r.prototype.runKernel=function(t,e,n,o,a){return this.runKernelFunc(null,e,null,t,n,o,a)},r.prototype.shouldCheckForMemLeaks=function(){return this.ENV.getBool("IS_TEST")},r.prototype.checkKernelForMemLeak=function(t,e,n){var o=this.backend.numDataIds(),a=0;n.forEach(function(u){a+=u.dtype==="complex64"?3:1});var i=this.state.numDataMovesStack[this.state.numDataMovesStack.length-1],s=o-e-a-i;if(s>0)throw new Error("Backend '"+this.backendName+"' has an internal memory leak ("+s+" data ids) after running '"+t+"'")},r.prototype.runKernelFunc=function(t,e,n,o,a,i,s){var u,c=this;i===void 0&&(i=[]),s===void 0&&(s=[]);var l=[],h=this.isTapeOn();o==null&&(o=this.state.activeScope!=null?this.state.activeScope.name:"");var f,d=function(x){h&&(l=x.map(function(y){return c.keep(c.clone(y))}))},p=this.state.numBytes,v=this.state.numTensors;this.shouldCheckForMemLeaks()&&this.state.numDataMovesStack.push(0);var m,g=Vc(o,this.backendName);return f=g!=null?function(){var x=c.backend.numDataIds();m=g.kernelFunc({inputs:e,attrs:a,backend:c.backend});var y=Array.isArray(m)?m:[m];c.shouldCheckForMemLeaks()&&c.checkKernelForMemLeak(o,x,y);var b=y.map(function(C){var I=C.dataId,E=C.shape,S=C.dtype;return c.makeTensorFromDataId(I,E,S)}),w=b.filter(function(C,I){return s[I]});return d((i||[]).slice().concat(w)),b}:function(){var x=c.backend.numDataIds();m=c.tidy(function(){return t(c.backend,d)});var y=Array.isArray(m)?m:[m];return c.shouldCheckForMemLeaks()&&c.checkKernelForMemLeak(o,x,y),y},this.scopedRun(function(){return c.state.kernelDepth++},function(){return c.state.kernelDepth--},function(){u=c.ENV.getBool("DEBUG")?c.profiler.profileKernel(o,e,function(){return f()}):f()}),h&&this.addTapeNode(o,e,u,n,l),this.state.profiling&&this.state.activeProfile.kernels.push({name:o,bytesAdded:this.state.numBytes-p,totalBytesSnapshot:this.state.numBytes,tensorsAdded:this.state.numTensors-v,totalTensorsSnapshot:this.state.numTensors,inputShapes:Object.keys(e).map(function(x){return e[x].shape}),outputShapes:u.map(function(x){return x.shape})}),Array.isArray(m)?u:u[0]},r.prototype.makeTensor=function(t,e,n,o){if(t==null)throw new Error("Values passed to engine.makeTensor() are null");n=n||"float32",o=o||this.backend;var a=t;n==="string"&&yn(t[0])&&(a=t.map(function(l){return Qc(l)}));var i=o.write(a,e,n),s=new Te(e,n,i,this.nextTensorId());if(this.incRef(s,o),n==="string"){var u=this.state.tensorInfo.get(i),c=$c(a);this.state.numBytes+=c-u.bytes,u.bytes=c}return s},r.prototype.makeTensorFromDataId=function(t,e,n,o){var a=new Te(e,n=n||"float32",t,this.nextTensorId());return this.incRef(a,o),a},r.prototype.makeVariable=function(t,e,n,o){e===void 0&&(e=!0),n=n||this.nextVariableId().toString(),o!=null&&o!==t.dtype&&(t=t.asType(o));var a=new dr(t,e,n,this.nextTensorId());if(this.state.registeredVariables[a.name]!=null)throw new Error("Variable with name "+a.name+" was already registered");return this.state.registeredVariables[a.name]=a,this.incRef(a,this.backend),a},r.prototype.incRef=function(t,e){var n=this.state.tensorInfo.has(t.dataId)?this.state.tensorInfo.get(t.dataId).refCount:0;if(this.state.numTensors++,t.dtype==="string"&&this.state.numStringTensors++,n===0){this.state.numDataBuffers++;var o=0;t.dtype!=="complex64"&&t.dtype!=="string"&&(o=t.size*qi(t.dtype)),this.state.tensorInfo.set(t.dataId,{backend:e||this.backend,dtype:t.dtype,shape:t.shape,bytes:o,refCount:0}),this.state.numBytes+=o}this.state.tensorInfo.get(t.dataId).refCount++,t instanceof dr||this.track(t)},r.prototype.disposeTensor=function(t){if(this.state.tensorInfo.has(t.dataId)){this.state.numTensors--,t.dtype==="string"&&this.state.numStringTensors--;var e=this.state.tensorInfo.get(t.dataId);e.refCount<=1?(t.dtype!=="complex64"&&(this.state.numBytes-=e.bytes),this.state.numDataBuffers--,e.backend.disposeData(t.dataId),this.state.tensorInfo.delete(t.dataId)):this.state.tensorInfo.get(t.dataId).refCount--}},r.prototype.disposeVariables=function(){for(var t in this.state.registeredVariables){var e=this.state.registeredVariables[t];this.disposeVariable(e)}},r.prototype.disposeVariable=function(t){this.disposeTensor(t),this.state.registeredVariables[t.name]!=null&&delete this.state.registeredVariables[t.name]},r.prototype.memory=function(){var t=this.backend.memory();return t.numTensors=this.state.numTensors,t.numDataBuffers=this.state.numDataBuffers,t.numBytes=this.state.numBytes,this.state.numStringTensors>0&&(t.unreliable=!0,t.reasons==null&&(t.reasons=[]),t.reasons.push("Memory usage by string tensors is approximate (2 bytes per character)")),t},r.prototype.profile=function(t){return Q(this,void 0,void 0,function(){var e,n;return Z(this,function(o){return this.state.profiling=!0,e=this.state.numBytes,n=this.state.numTensors,this.state.activeProfile.kernels=[],this.state.activeProfile.result=t(),this.state.profiling=!1,this.state.activeProfile.peakBytes=Math.max.apply(Math,this.state.activeProfile.kernels.map(function(a){return a.totalBytesSnapshot})),this.state.activeProfile.newBytes=this.state.numBytes-e,this.state.activeProfile.newTensors=this.state.numTensors-n,[2,this.state.activeProfile]})})},r.prototype.isTapeOn=function(){return this.state.gradientDepth>0&&this.state.kernelDepth===0},r.prototype.addTapeNode=function(t,e,n,o,a){var i=this,s={id:this.state.nextTapeNodeId++,kernelName:t,inputs:e,outputs:n,saved:a},u=_p(t);u!=null&&(o=u.gradFunc),o!=null&&(s.gradient=function(c){return c=c.map(function(l,h){if(l==null){var f=n[h],d=yr(f.size,f.dtype);return i.makeTensor(d,f.shape,f.dtype)}return l}),o(c.length>1?c:c[0],a)}),this.state.activeTape.push(s)},r.prototype.keep=function(t){return t.kept=!0,t},r.prototype.startTape=function(){this.state.gradientDepth===0&&(this.state.activeTape=[]),this.state.gradientDepth++},r.prototype.endTape=function(){this.state.gradientDepth--},r.prototype.startScope=function(t){var e={track:[],name:"unnamed scope",id:this.state.nextScopeId++};t&&(e.name=t),this.state.scopeStack.push(e),this.state.activeScope=e},r.prototype.endScope=function(t){for(var e=this,n=$i(t),o=new Set(n.map(function(u){return u.id})),a=0;a<this.state.activeScope.track.length;a++){var i=this.state.activeScope.track[a];i.kept||o.has(i.id)||i.dispose()}var s=this.state.scopeStack.pop();this.state.activeScope=this.state.scopeStack.length===0?null:this.state.scopeStack[this.state.scopeStack.length-1],n.forEach(function(u){u.kept||u.scopeId!==s.id||e.track(u)})},r.prototype.gradients=function(t,e,n,o){var a=this;if(o===void 0&&(o=!1),R(e.length>0,function(){return"gradients() received an empty list of xs."}),n!=null&&n.dtype!=="float32")throw new Error("dy must have 'float32' dtype, but has '"+n.dtype+"'");var i=this.scopedRun(function(){return a.startTape()},function(){return a.endTape()},function(){return a.tidy("forward",t)});R(i instanceof Te,function(){return"The result y returned by f() must be a tensor."});var s=function(u,c,l){for(var h={},f={},d=0;d<c.length;d++)h[c[d].id]=!0;for(d=0;d<u.length;d++){var p=(C=u[d]).inputs;for(var v in p){for(var m=p[v],g=!1,x=0;x<c.length;x++)if(h[m.id]){C.outputs.forEach(function(k){return h[k.id]=!0}),g=!0,f[C.id]=!0;break}if(g)break}}var y={};y[l.id]=!0;var b={};for(d=u.length-1;d>=0;d--)for(p=(C=u[d]).inputs,x=0;x<C.outputs.length;x++)if(y[C.outputs[x].id]){for(var v in p)y[p[v].id]=!0,b[C.id]=!0;break}var w=[];for(d=0;d<u.length;d++){var C;if(f[(C=u[d]).id]&&b[C.id]){var I={};for(var v in C.inputs){var E=C.inputs[v];h[E.id]&&(I[v]=E)}var S=Object.assign({},C);S.inputs=I,S.outputs=C.outputs,w.push(S)}}return w}(this.state.activeTape,e,i);if(!o&&s.length===0&&e.length>0)throw new Error("Cannot compute gradient of y=f(x) with respect to x. Make sure that the f you passed encloses all operations that lead from x to y.");return this.tidy("backward",function(){var u,c,l={};l[i.id]=n==null?(u=i.shape,c=Ki(ee(u),"float32"),N.makeTensor(c,u,"float32")):n,function(f,d,p){for(var v=function(g){var x=d[g],y=[];if(x.outputs.forEach(function(I){var E=f[I.id];E!=null?y.push(E):y.push(null)}),x.gradient==null)throw new Error("Cannot compute gradient: gradient function not found for "+x.kernelName+".");var b=x.gradient(y),w=function(I){if(!(I in b))throw new Error("Cannot backprop through input "+I+". Available gradients found: "+Object.keys(b)+".");var E=p(function(){return b[I]()});if(E.dtype!=="float32")throw new Error("Error in gradient for op "+x.kernelName+". The gradient of input "+I+" must have 'float32' dtype, but has '"+E.dtype+"'");var S=x.inputs[I];if(!Oe(E.shape,S.shape))throw new Error("Error in gradient for op "+x.kernelName+". The gradient of input '"+I+"' has shape '"+E.shape+"', which does not match the shape of the input '"+S.shape+"'");if(f[S.id]==null)f[S.id]=E;else{var k=f[S.id];f[S.id]=k.add(E),k.dispose()}};for(var C in x.inputs)w(C)},m=d.length-1;m>=0;m--)v(m)}(l,s,function(f){return a.tidy(f)});var h=e.map(function(f){return l[f.id]});return a.state.gradientDepth===0&&(a.state.activeTape.forEach(function(f){for(var d=0,p=f.saved;d<p.length;d++)p[d].dispose()}),a.state.activeTape=null),{value:i,grads:h}})},r.prototype.customGrad=function(t){var e=this;return R(Po(t),function(){return"The f passed in customGrad(f) must be a function."}),function(){for(var n,o=[],a=0;a<arguments.length;a++)o[a]=arguments[a];R(o.every(function(s){return s instanceof Te}),function(){return"The args passed in customGrad(f)(x1, x2,...) must all be tensors"});var i={};return o.forEach(function(s,u){i[u]=s}),e.runKernelFunc(function(s,u){return R((n=t.apply(void 0,o.concat([u]))).value instanceof Te,function(){return"The function f passed in customGrad(f) must return an object where `obj.value` is a tensor"}),R(Po(n.gradFunc),function(){return"The function f passed in customGrad(f) must return an object where `obj.gradFunc` is a function."}),n.value},i,function(s,u){var c=n.gradFunc(s,u),l=Array.isArray(c)?c:[c];R(l.length===o.length,function(){return"The function f passed in customGrad(f) must return an object where `obj.gradFunc` is a function that returns the same number of tensors as inputs passed to f(...)."}),R(l.every(function(f){return f instanceof Te}),function(){return"The function f passed in customGrad(f) must return an object where `obj.gradFunc` is a function that returns a list of only tensors."});var h={};return l.forEach(function(f,d){h[d]=function(){return f}}),h})}},r.prototype.readSync=function(t){return this.state.tensorInfo.get(t).backend.readSync(t)},r.prototype.read=function(t){return this.state.tensorInfo.get(t).backend.read(t)},r.prototype.time=function(t){return Q(this,void 0,void 0,function(){var e,n;return Z(this,function(o){switch(o.label){case 0:return e=Dt(),[4,this.backend.time(t)];case 1:return(n=o.sent()).wallMs=Dt()-e,[2,n]}})})},r.prototype.track=function(t){return this.state.activeScope!=null&&(t.scopeId=this.state.activeScope.id,this.state.activeScope.track.push(t)),t},Object.defineProperty(r.prototype,"registeredVariables",{get:function(){return this.state.registeredVariables},enumerable:!0,configurable:!0}),r.prototype.reset=function(){for(var t in this.pendingBackendInitId++,this.state.dispose(),this.ENV.reset(),this.state=new du,this.registry)this.disposeRegisteredKernels(t),this.registry[t].dispose(),delete this.registry[t];this.backendName=null,this.backendInstance=null,this.pendingBackendInit=null},r.nextTensorId=0,r.nextVariableId=0,r}(),N=function(){var r=function(){if(Fa==null){var e=void 0;if(typeof window<"u")e=window;else if(typeof global<"u")e=global;else if(typeof process<"u")e=process;else{if(typeof self>"u")throw new Error("Could not find a global object");e=self}Fa=e}return Fa}();if(r._tfengine==null){var t=new wp(r);r._tfengine=new Ap(t)}return function(e){Wc=e}(r._tfengine.ENV),qt=function(){return r._tfengine},r._tfengine}();function rl(){return typeof window<"u"&&window.document!=null||typeof WorkerGlobalScope<"u"}var nn=V();nn.registerFlag("DEBUG",function(){return!1},function(r){r&&console.warn("Debugging mode is ON. The output of every math call will be downloaded to CPU and checked for NaNs. This significantly impacts performance.")}),nn.registerFlag("IS_BROWSER",function(){return rl()}),nn.registerFlag("IS_NODE",function(){return typeof process<"u"&&process.versions!==void 0&&process.versions.node!==void 0}),nn.registerFlag("IS_CHROME",function(){return typeof navigator<"u"&&navigator!=null&&navigator.userAgent!=null&&/Chrome/.test(navigator.userAgent)&&/Google Inc/.test(navigator.vendor)}),nn.registerFlag("PROD",function(){return!1}),nn.registerFlag("TENSORLIKE_CHECK_SHAPE_CONSISTENCY",function(){return nn.getBool("DEBUG")}),nn.registerFlag("DEPRECATION_WARNINGS_ENABLED",function(){return!0}),nn.registerFlag("IS_TEST",function(){return!1});var Lr,mt,vt,Fn={},Pa={alpha:!1,antialias:!1,premultipliedAlpha:!1,preserveDrawingBuffer:!1,depth:!1,stencil:!1,failIfMajorPerformanceCaveat:!0};function ol(r,t){Fn[r]=t}function Zt(r){r in Fn||(Fn[r]=function(e){if(e!==1&&e!==2)throw new Error("Cannot get WebGL rendering context, WebGL is disabled.");var n=function(o){if(typeof OffscreenCanvas<"u"&&o===2)return new OffscreenCanvas(300,150);if(typeof document<"u")return document.createElement("canvas");throw new Error("Cannot create a canvas in this context")}(e);return n.addEventListener("webglcontextlost",function(o){o.preventDefault(),delete Fn[e]},!1),e===1?n.getContext("webgl",Pa)||n.getContext("experimental-webgl",Pa):n.getContext("webgl2",Pa)}(r));var t=Fn[r];return t.isContextLost()?(delete Fn[r],Zt(r)):(t.disable(t.DEPTH_TEST),t.disable(t.STENCIL_TEST),t.disable(t.BLEND),t.disable(t.DITHER),t.disable(t.POLYGON_OFFSET_FILL),t.disable(t.SAMPLE_COVERAGE),t.enable(t.SCISSOR_TEST),t.enable(t.CULL_FACE),t.cullFace(t.BACK),Fn[r])}function ra(r,t){return[t,r]}function Dr(r){var t=ee(r);return Fo(Math.ceil(t/4))}function $r(r,t){return[Math.max(1,Math.ceil(t/2)),Math.max(1,Math.ceil(r/2))]}function Yi(r,t){var e,n,o,a,i,s,u,c,l,h=r;return V().getNumber("WEBGL_VERSION")===2?(e=h.R32F,n=h.R16F,o=h.RGBA16F,a=h.RGBA32F,i=h.RED,s=4,u=1,c=h.HALF_FLOAT,l=h.FLOAT):(e=r.RGBA,n=r.RGBA,o=r.RGBA,a=h.RGBA,i=r.RGBA,s=4,u=4,c=t!=null?t.HALF_FLOAT_OES:null,l=r.FLOAT),{internalFormatFloat:e,internalFormatHalfFloat:n,internalFormatPackedHalfFloat:o,internalFormatPackedFloat:a,textureFormatFloat:i,downloadTextureFormat:r.RGBA,downloadUnpackNumChannels:s,defaultNumChannels:u,textureTypeHalfFloat:c,textureTypeFloat:l}}function J(r,t,e){var n=e();return t&&function(o){var a=o.getError();if(a!==o.NO_ERROR)throw new Error("WebGL Error: "+il(o,a))}(r),n}(function(r){r[r.DENSE=0]="DENSE",r[r.SHARED_BATCH=1]="SHARED_BATCH"})(Lr||(Lr={})),function(r){r[r.RENDER=0]="RENDER",r[r.UPLOAD=1]="UPLOAD",r[r.PIXELS=2]="PIXELS",r[r.DOWNLOAD=3]="DOWNLOAD"}(mt||(mt={})),function(r){r[r.UNPACKED_FLOAT16=0]="UNPACKED_FLOAT16",r[r.UNPACKED_FLOAT32=1]="UNPACKED_FLOAT32",r[r.PACKED_4X1_UNSIGNED_BYTE=2]="PACKED_4X1_UNSIGNED_BYTE",r[r.PACKED_2X2_FLOAT32=3]="PACKED_2X2_FLOAT32",r[r.PACKED_2X2_FLOAT16=4]="PACKED_2X2_FLOAT16"}(vt||(vt={}));var Dp=596e-10,Tp=65504;function al(r){return!!(V().getBool("WEBGL_RENDER_FLOAT32_ENABLED")||r===0||Dp<Math.abs(r)&&Math.abs(r)<Tp)}function il(r,t){switch(t){case r.NO_ERROR:return"NO_ERROR";case r.INVALID_ENUM:return"INVALID_ENUM";case r.INVALID_VALUE:return"INVALID_VALUE";case r.INVALID_OPERATION:return"INVALID_OPERATION";case r.INVALID_FRAMEBUFFER_OPERATION:return"INVALID_FRAMEBUFFER_OPERATION";case r.OUT_OF_MEMORY:return"OUT_OF_MEMORY";case r.CONTEXT_LOST_WEBGL:return"CONTEXT_LOST_WEBGL";default:return"Unknown error code "+t}}function kr(r,t,e){return un(r,t,function(){return r.getExtension(e)},'Extension "'+e+'" not supported on this browser.')}function sl(r,t,e){var n=un(r,t,function(){return r.createShader(r.VERTEX_SHADER)},"Unable to create vertex WebGLShader.");if(J(r,t,function(){return r.shaderSource(n,e)}),J(r,t,function(){return r.compileShader(n)}),r.getShaderParameter(n,r.COMPILE_STATUS)===!1)throw console.log(r.getShaderInfoLog(n)),new Error("Failed to compile vertex shader.");return n}function ul(r,t,e){var n=un(r,t,function(){return r.createShader(r.FRAGMENT_SHADER)},"Unable to create fragment WebGLShader.");if(J(r,t,function(){return r.shaderSource(n,e)}),J(r,t,function(){return r.compileShader(n)}),r.getShaderParameter(n,r.COMPILE_STATUS)===!1)throw function(o,a){var i=Np.exec(a);if(i==null)return console.log("Couldn't parse line number in error: "+a),void console.log(o);for(var s=+i[1],u=o.split(`
`),c=u.length.toString().length+2,l=u.map(function(m,g){return Bn((g+1).toString(),c)+m}),h=0,f=0;f<l.length;f++)h=Math.max(l[f].length,h);var d=l.slice(0,s-1),p=l.slice(s-1,s),v=l.slice(s);console.log(d.join(`
`)),console.log(a.split(`
`)[0]),console.log("%c "+Bn(p[0],h),"border:1px solid red; background-color:#e3d2d2; color:#a61717"),console.log(v.join(`
`))}(e,r.getShaderInfoLog(n)),new Error("Failed to compile fragment shader.");return n}var bo,wo,Np=/ERROR: [0-9]+:([0-9]+):/g;function cl(r,t){return un(r,t,function(){return r.createProgram()},"Unable to create WebGLProgram.")}function ll(r,t,e){if(J(r,t,function(){return r.linkProgram(e)}),r.getProgramParameter(e,r.LINK_STATUS)===!1)throw console.log(r.getProgramInfoLog(e)),new Error("Failed to link vertex and fragment shaders.")}function Co(r,t,e){if(J(r,t,function(){return r.validateProgram(e)}),r.getProgramParameter(e,r.VALIDATE_STATUS)===!1)throw console.log(r.getProgramInfoLog(e)),new Error("Shader program validation failed.")}function hl(r,t,e){var n=un(r,t,function(){return r.createBuffer()},"Unable to create WebGLBuffer");return J(r,t,function(){return r.bindBuffer(r.ARRAY_BUFFER,n)}),J(r,t,function(){return r.bufferData(r.ARRAY_BUFFER,e,r.STATIC_DRAW)}),n}function fl(r,t,e){var n=un(r,t,function(){return r.createBuffer()},"Unable to create WebGLBuffer");return J(r,t,function(){return r.bindBuffer(r.ELEMENT_ARRAY_BUFFER,n)}),J(r,t,function(){return r.bufferData(r.ELEMENT_ARRAY_BUFFER,e,r.STATIC_DRAW)}),n}function dl(r,t){return un(r,t,function(){return r.createTexture()},"Unable to create WebGLTexture.")}function pl(r,t){var e=V().getNumber("WEBGL_MAX_TEXTURE_SIZE");if(r<=0||t<=0){var n="["+r+"x"+t+"]";throw new Error("Requested texture size "+n+" is invalid.")}if(r>e||t>e)throw n="["+r+"x"+t+"]",new Error("Requested texture size "+n+" greater than WebGL maximum on this browser / GPU "+("["+e+"x"+e+"]")+".")}function vl(r,t){return un(r,t,function(){return r.createFramebuffer()},"Unable to create WebGLFramebuffer.")}function pi(r,t,e,n,o,a,i,s){var u=r.getAttribLocation(e,n);return u!==-1&&(J(r,t,function(){return r.bindBuffer(r.ARRAY_BUFFER,o)}),J(r,t,function(){return r.vertexAttribPointer(u,a,r.FLOAT,!1,i,s)}),J(r,t,function(){return r.enableVertexAttribArray(u)}),!0)}function ml(r,t,e,n){wl(r,n),J(r,t,function(){return r.activeTexture(r.TEXTURE0+n)}),J(r,t,function(){return r.bindTexture(r.TEXTURE_2D,e)})}function gl(r,t,e,n){return un(r,t,function(){return r.getUniformLocation(e,n)},'uniform "'+n+'" not present in program.')}function yl(r,t,e){return r.getUniformLocation(t,e)}function xl(r,t,e,n,o,a){J(r,t,function(){return ml(r,t,n,a)}),J(r,t,function(){return r.uniform1i(o,a)})}function _o(r,t,e,n){J(r,t,function(){return r.bindFramebuffer(r.FRAMEBUFFER,n)}),J(r,t,function(){return r.framebufferTexture2D(r.FRAMEBUFFER,r.COLOR_ATTACHMENT0,r.TEXTURE_2D,e,0)})}function vi(r,t,e){J(r,t,function(){return r.bindFramebuffer(r.FRAMEBUFFER,e)}),J(r,t,function(){return r.framebufferTexture2D(r.FRAMEBUFFER,r.COLOR_ATTACHMENT0,r.TEXTURE_2D,null,0)})}function Rr(r){var t=r.checkFramebufferStatus(r.FRAMEBUFFER);if(t!==r.FRAMEBUFFER_COMPLETE)throw new Error("Error binding framebuffer: "+bl(r,t))}function bl(r,t){switch(t){case r.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:return"FRAMEBUFFER_INCOMPLETE_ATTACHMENT";case r.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:return"FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";case r.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:return"FRAMEBUFFER_INCOMPLETE_DIMENSIONS";case r.FRAMEBUFFER_UNSUPPORTED:return"FRAMEBUFFER_UNSUPPORTED";default:return"unknown error "+t}}function un(r,t,e,n){var o=J(r,t,function(){return e()});if(o==null)throw new Error(n);return o}function wl(r,t){var e=r.MAX_COMBINED_TEXTURE_IMAGE_UNITS-1,n=t+r.TEXTURE0;if(n<r.TEXTURE0||n>e)throw new Error("textureUnit must be in "+("[gl.TEXTURE0, gl.TEXTURE"+e+"]")+".")}function Wr(r,t){return t===void 0&&(t=2),ee(r.slice(0,r.length-t))}function Vr(r){if(r.length===0)throw Error("Cannot get rows and columns of an empty shape array.");return[r.length>1?r[r.length-2]:1,r[r.length-1]]}function Eo(r){var t=[1,1,1];return r.length===0||r.length===1&&r[0]===1||(t=[Wr(r)].concat(Vr(r))),t}function Cl(r,t){var e;t===void 0&&(t=!1);var n=V().getNumber("WEBGL_MAX_TEXTURE_SIZE");if(t&&(n*=2,(r=r.map(function(c,l){return l>=r.length-2?Hi(r[l]):r[l]})).length===1&&(r=[2,r[0]])),r.length!==2){var o=gn(r);r=o.newShape}var a=ee(r);if(r.length<=1&&a<=n)return[1,a];if(r.length===2&&r[0]<=n&&r[1]<=n)return r;if(r.length===3&&r[0]*r[1]<=n&&r[2]<=n)return[r[0]*r[1],r[2]];if(r.length===3&&r[0]<=n&&r[1]*r[2]<=n)return[r[0],r[1]*r[2]];if(r.length===4&&r[0]*r[1]*r[2]<=n&&r[3]<=n)return[r[0]*r[1]*r[2],r[3]];if(r.length===4&&r[0]<=n&&r[1]*r[2]*r[3]<=n)return[r[0],r[1]*r[2]*r[3]];if(t){var i=Wr(r),s=2,u=2;return r.length&&(s=(e=Vr(r))[0],u=e[1]),Fo(a=i*(s/2)*(u/2)).map(function(c){return 2*c})}return Fo(a)}function so(r){return r%2==0}function Ir(r,t){if(Oe(r=r.slice(-2),t=t.slice(-2))||!r.length||!t.length||r[0]===0||r[1]===0||t[0]===0||t[1]===0)return!0;if(r.length!==t.length){var e=r.slice(-1)[0],n=t.slice(-1)[0];if(e===n||so(e)&&so(n)&&(r[0]===1||t[0]===1))return!0}return r[1]===t[1]&&so(r[0])&&so(t[0])}function _l(r){if(bo==null){var t=Zt(r);bo=t.getParameter(t.MAX_TEXTURE_SIZE)}return bo}function El(r){if(wo==null){var t=Zt(r);wo=t.getParameter(t.MAX_TEXTURE_IMAGE_UNITS)}return Math.min(16,wo)}function Sl(r){if(r===0)return 0;var t=Zt(r);return gt(t,"EXT_disjoint_timer_query_webgl2")&&r===2?2:gt(t,"EXT_disjoint_timer_query")?1:0}function gt(r,t){return r.getExtension(t)!=null}function mi(r){try{if(Zt(r)!=null)return!0}catch{return!1}return!1}function kl(r){if(r===0)return!1;var t=Zt(r);if(r===1){if(!gt(t,"OES_texture_float"))return!1}else if(!gt(t,"EXT_color_buffer_float"))return!1;return gi(t)}function Rl(r){if(r===0)return!1;var t=Zt(r);if(r!==1){if(gt(t,"EXT_color_buffer_float"))return gi(t);if(gt(t,"EXT_color_buffer_half_float")){var e=t.getExtension("EXT_color_buffer_half_float");return function(n,o){var a=Yi(n,o),i=n.createTexture();n.bindTexture(n.TEXTURE_2D,i),n.texImage2D(n.TEXTURE_2D,0,a.internalFormatHalfFloat,1,1,0,a.textureFormatFloat,a.textureTypeHalfFloat,null);var s=n.createFramebuffer();n.bindFramebuffer(n.FRAMEBUFFER,s),n.framebufferTexture2D(n.FRAMEBUFFER,n.COLOR_ATTACHMENT0,n.TEXTURE_2D,i,0);var u=n.checkFramebufferStatus(n.FRAMEBUFFER)===n.FRAMEBUFFER_COMPLETE;return n.bindTexture(n.TEXTURE_2D,null),n.bindFramebuffer(n.FRAMEBUFFER,null),n.deleteTexture(i),n.deleteFramebuffer(s),u}(t,e)}return!1}return!!gt(t,"OES_texture_float")&&!!gt(t,"WEBGL_color_buffer_float")&&gi(t)}function gi(r){var t=Yi(r),e=r.createTexture();r.bindTexture(r.TEXTURE_2D,e),r.texImage2D(r.TEXTURE_2D,0,t.internalFormatFloat,1,1,0,t.textureFormatFloat,t.textureTypeFloat,null);var n=r.createFramebuffer();r.bindFramebuffer(r.FRAMEBUFFER,n),r.framebufferTexture2D(r.FRAMEBUFFER,r.COLOR_ATTACHMENT0,r.TEXTURE_2D,e,0);var o=r.checkFramebufferStatus(r.FRAMEBUFFER)===r.FRAMEBUFFER_COMPLETE;return r.bindTexture(r.TEXTURE_2D,null),r.bindFramebuffer(r.FRAMEBUFFER,null),r.deleteTexture(e),r.deleteFramebuffer(n),o}function Il(r){return r===2&&Zt(r).fenceSync!=null}var Fp=Object.freeze({callAndCheck:J,canBeRepresented:al,getWebGLErrorMessage:il,getExtensionOrThrow:kr,createVertexShader:sl,createFragmentShader:ul,createProgram:cl,linkProgram:ll,validateProgram:Co,createStaticVertexBuffer:hl,createStaticIndexBuffer:fl,getNumChannels:function(){return V().getNumber("WEBGL_VERSION")===2?1:4},createTexture:dl,validateTextureSize:pl,createFramebuffer:vl,bindVertexBufferToProgramAttribute:pi,bindTextureUnit:ml,unbindTextureUnit:function(r,t,e){wl(r,e),J(r,t,function(){return r.activeTexture(r.TEXTURE0+e)}),J(r,t,function(){return r.bindTexture(r.TEXTURE_2D,null)})},getProgramUniformLocationOrThrow:gl,getProgramUniformLocation:yl,bindTextureToProgramUniformSampler:xl,bindCanvasToFramebuffer:function(r,t){J(r,t,function(){return r.bindFramebuffer(r.FRAMEBUFFER,null)}),J(r,t,function(){return r.viewport(0,0,r.canvas.width,r.canvas.height)}),J(r,t,function(){return r.scissor(0,0,r.canvas.width,r.canvas.height)})},bindColorTextureToFramebuffer:_o,unbindColorTextureFromFramebuffer:vi,validateFramebuffer:Rr,getFramebufferErrorMessage:bl,getBatchDim:Wr,getRowsCols:Vr,getShapeAs3D:Eo,getTextureShapeFromLogicalShape:Cl,isReshapeFree:Ir,getWebGLMaxTextureSize:_l,resetMaxTextureSize:function(){bo=null},resetMaxTexturesInShader:function(){wo=null},getMaxTexturesInShader:El,getWebGLDisjointQueryTimerVersion:Sl,hasExtension:gt,isWebGLVersionEnabled:mi,isCapableOfRenderingToFloatTexture:kl,isDownloadFloatTextureEnabled:Rl,isWebGLFenceEnabled:Il}),ae=V();function Al(r){V().getBool("DEPRECATION_WARNINGS_ENABLED")&&console.warn(r+" You can disable deprecation warnings with tf.disableDeprecationWarnings().")}function Y(r,t){return N.tidy(r,t)}function ft(r){$i(r).forEach(function(t){return t.dispose()})}function Pp(r){return N.keep(r)}function Oo(){for(var r=[],t=0;t<arguments.length;t++)r[t]=arguments[t];V().getBool("IS_TEST")||console.warn.apply(console,r)}function Yt(r,t){var e=r;if(Ye(r))return t==="string"?[]:[r.length];if(!Array.isArray(r))return[];for(var n=[];Array.isArray(e)||Ye(e)&&t!=="string";)n.push(e.length),e=e[0];return Array.isArray(r)&&V().getBool("TENSORLIKE_CHECK_SHAPE_CONSISTENCY")&&function o(a,i,s){if(s=s||[],!Array.isArray(a)&&!Ye(a))return void R(i.length===0,function(){return"Element arr["+s.join("][")+"] is a primitive, but should be an array/TypedArray of "+i[0]+" elements"});R(i.length>0,function(){return"Element arr["+s.join("][")+"] should be a primitive, but is an array of "+a.length+" elements"}),R(a.length===i[0],function(){return"Element arr["+s.join("][")+"] should have "+i[0]+" elements, but has "+a.length+" elements"});for(var u=i.slice(1),c=0;c<a.length;++c)o(a[c],u,s.concat(c))}(r,n,[]),n}function pu(r,t,e,n){if(r!=null&&(r!=="numeric"&&r!==t||r==="numeric"&&t==="string"))throw new Error("Argument '"+e+"' passed to '"+n+"' must be "+r+" tensor, but got "+t+" tensor")}function _(r,t,e,n){if(n===void 0&&(n="numeric"),r instanceof Te)return pu(n,r.dtype,t,e),r;var o=gr(r);if(o!=="string"&&["bool","int32","float32"].indexOf(n)>=0&&(o=n),pu(n,o,t,e),r==null||!Ye(r)&&!Array.isArray(r)&&typeof r!="number"&&typeof r!="boolean"&&typeof r!="string"){var a=r==null?"null":r.constructor.name;throw new Error("Argument '"+t+"' passed to '"+e+"' must be a Tensor or TensorLike, but got '"+a+"'")}var i=Yt(r,o);Ye(r)||Array.isArray(r)||(r=[r]);var s=o!=="string"?ji(r,o,V().getBool("DEBUG")):sn(r,[],!0);return N.makeTensor(s,i,o)}function Bo(r,t,e,n){if(n===void 0&&(n="numeric"),!Array.isArray(r))throw new Error("Argument "+t+" passed to "+e+" must be a `Tensor[]` or `TensorLike[]`");return r.map(function(o,a){return _(o,t+"["+a+"]",e)},n)}function Ji(r,t){for(var e=0;e<r.length;++e)if(r[r.length-e-1]!==t-1-e)return!1;return!0}function Dl(r,t,e){for(var n=r.length+t.length,o=[],a=0,i=0,s=0;s<n;s++)e.indexOf(s)===-1?o.push(r[a++]):o.push(t[i++]);return o}function Xe(r,t){for(var e=[],n=r.length,o=0;o<n;o++)t.indexOf(o)===-1&&e.push(r[o]);return[e,t.map(function(a){return r[a]})]}function it(r,t){return Dl(r,t.map(function(e){return 1}),t)}function ct(r,t,e){R(Ji(t,e),function(){return r+" supports only inner-most axes for now. Got axes "+t+" and rank-"+e+" input."})}function Ot(r,t){if(Ji(r,t))return null;for(var e=[],n=0;n<t;++n)r.indexOf(n)===-1&&e.push(n);return r.forEach(function(o){return e.push(o)}),e}function oa(r){return r.map(function(t,e){return[e,t]}).sort(function(t,e){return t[1]-e[1]}).map(function(t){return t[0]})}function Bt(r,t){for(var e=[],n=t-r;n<t;++n)e.push(n);return e}function Tl(r,t){var e=r[0].length;r.forEach(function(o,a){R(o.length===e,function(){return"Error in concat"+e+"D: rank of tensors["+a+"] must be the same as the rank of the rest ("+e+")"})}),R(t>=0&&t<e,function(){return"Error in concat"+e+"D: axis must be between 0 and "+(e-1)+"."});var n=r[0];r.forEach(function(o,a){for(var i=0;i<e;i++)R(i===t||o[i]===n[i],function(){return"Error in concat"+e+"D: Shape of tensors["+a+"] ("+o+") does not match the shape of the rest ("+n+") along the non-concatenated axis "+a+"."})})}function Vn(r,t){for(var e=r[0].slice(),n=1;n<r.length;n++)e[t]+=r[n][t];return e}function D(r){var t=Object.keys(r);if(t.length!==1)throw new Error("Please provide an object with a single key (operation name) mapping to a function. Got an object with "+t.length+" keys.");var e=t[0],n=r[e];e.endsWith("_")&&(e=e.substring(0,e.length-1));var o=function(){for(var a=[],i=0;i<arguments.length;i++)a[i]=arguments[i];N.startScope(e);try{var s=n.apply(void 0,a);return s instanceof Promise&&console.error("Cannot return a Promise inside of tidy."),N.endScope(s),s}catch(u){throw N.endScope(null),u}};return Object.defineProperty(o,"name",{value:e,configurable:!0}),o}ae.registerFlag("HAS_WEBGL",function(){return ae.getNumber("WEBGL_VERSION")>0}),ae.registerFlag("WEBGL_VERSION",function(){return mi(2)?2:mi(1)?1:0}),ae.registerFlag("WEBGL_BUFFER_SUPPORTED",function(){return ae.get("WEBGL_VERSION")===2}),ae.registerFlag("WEBGL_CPU_FORWARD",function(){return!0}),ae.registerFlag("WEBGL_FORCE_F16_TEXTURES",function(){return!1}),ae.registerFlag("WEBGL_PACK",function(){return ae.getBool("HAS_WEBGL")}),ae.registerFlag("WEBGL_PACK_NORMALIZATION",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_PACK_CLIP",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_PACK_DEPTHWISECONV",function(){return!1}),ae.registerFlag("WEBGL_PACK_BINARY_OPERATIONS",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_PACK_UNARY_OPERATIONS",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_PACK_ARRAY_OPERATIONS",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_PACK_IMAGE_OPERATIONS",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_PACK_REDUCE",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_LAZILY_UNPACK",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_CONV_IM2COL",function(){return ae.getBool("WEBGL_PACK")}),ae.registerFlag("WEBGL_MAX_TEXTURE_SIZE",function(){return _l(ae.getNumber("WEBGL_VERSION"))}),ae.registerFlag("WEBGL_MAX_TEXTURES_IN_SHADER",function(){return El(ae.getNumber("WEBGL_VERSION"))}),ae.registerFlag("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION",function(){var r=ae.getNumber("WEBGL_VERSION");return r===0?0:Sl(r)}),ae.registerFlag("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_RELIABLE",function(){return ae.getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION")>0&&(r=navigator.userAgent||navigator.vendor||window.opera,!(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(r)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(r.substr(0,4))));var r}),ae.registerFlag("WEBGL_RENDER_FLOAT32_CAPABLE",function(){return kl(ae.getNumber("WEBGL_VERSION"))}),ae.registerFlag("WEBGL_RENDER_FLOAT32_ENABLED",function(){return!ae.getBool("WEBGL_FORCE_F16_TEXTURES")&&ae.getBool("WEBGL_RENDER_FLOAT32_CAPABLE")}),ae.registerFlag("WEBGL_DOWNLOAD_FLOAT_ENABLED",function(){return Rl(ae.getNumber("WEBGL_VERSION"))}),ae.registerFlag("WEBGL_FENCE_API_ENABLED",function(){return Il(ae.getNumber("WEBGL_VERSION"))}),ae.registerFlag("WEBGL_SIZE_UPLOAD_UNIFORM",function(){return ae.getBool("WEBGL_RENDER_FLOAT32_ENABLED")?4:0}),tl=Al;var Ke=D({complex_:function(r,t){var e=_(r,"real","complex"),n=_(t,"imag","complex");return be(e.shape,n.shape,"real and imag shapes, "+e.shape+" and "+n.shape+", must match in call to tf.complex()."),N.runKernelFunc(function(o){return o.complex(e,n)},{$real:e,$imag:n})}}),St=D({real_:function(r){var t=_(r,"input","real");return N.runKernelFunc(function(e){return e.real(t)},{$input:t})}}),jt=D({imag_:function(r){var t=_(r,"input","imag");return N.runKernelFunc(function(e){return e.imag(t)},{$input:t})}});function $e(r,t,e){return Rn(r,t,Yt(r,e),e)}function Rn(r,t,e,n){if(n==null&&(n=gr(r)),n==="complex64")throw new Error("Cannot construct a complex64 tensor directly. Please use tf.complex(real, imag).");if(!Ye(r)&&!Array.isArray(r)&&typeof r!="number"&&typeof r!="boolean"&&typeof r!="string")throw new Error("values passed to tensor(values) must be a number/boolean/string or an array of numbers/booleans/strings, or a TypedArray");if(t!=null){Xi(t);var o=ee(t),a=ee(e);R(o===a,function(){return"Based on the provided shape, ["+t+"], the tensor should have "+o+" values but has "+a});for(var i=0;i<e.length;++i){var s=e[i],u=i!==e.length-1||s!==ee(t.slice(i));R(e[i]===t[i]||!u,function(){return"Error creating a new Tensor. Inferred shape ("+e+") does not match the provided shape ("+t+"). "})}}return Ye(r)||Array.isArray(r)||(r=[r]),t=t||e,r=n!=="string"?ji(r,n,V().getBool("DEBUG")):sn(r,[],!0),N.makeTensor(r,t,n)}function K(r,t){if((Ye(r)&&t!=="string"||Array.isArray(r))&&t!=="complex64")throw new Error("Error creating a new Scalar: value must be a primitive (number|boolean|string)");if(t==="string"&&Ye(r)&&!(r instanceof Uint8Array))throw new Error("When making a scalar from encoded string, the value must be `Uint8Array`.");return Rn(r,[],[],t)}function Me(r,t){Gn(r);var e=Yt(r,t);if(e.length!==1)throw new Error("tensor1d() requires values to be a flat/TypedArray");return Rn(r,null,e,t)}function bn(r,t,e){if(Gn(r),t!=null&&t.length!==2)throw new Error("tensor2d() requires shape to have two numbers");var n=Yt(r,e);if(n.length!==2&&n.length!==1)throw new Error("tensor2d() requires values to be number[][] or flat/TypedArray");if(n.length===1&&t==null)throw new Error("tensor2d() requires shape to be provided when `values` are a flat/TypedArray");return Rn(r,t,n,e)}function Qi(r,t,e){if(Gn(r),t!=null&&t.length!==3)throw new Error("tensor3d() requires shape to have three numbers");var n=Yt(r,e);if(n.length!==3&&n.length!==1)throw new Error("tensor3d() requires values to be number[][][] or flat/TypedArray");if(n.length===1&&t==null)throw new Error("tensor3d() requires shape to be provided when `values` are a flat array");return Rn(r,t,n,e)}function lt(r,t,e){if(Gn(r),t!=null&&t.length!==4)throw new Error("tensor4d() requires shape to have four numbers");var n=Yt(r,e);if(n.length!==4&&n.length!==1)throw new Error("tensor4d() requires values to be number[][][][] or flat/TypedArray");if(n.length===1&&t==null)throw new Error("tensor4d() requires shape to be provided when `values` are a flat array");return Rn(r,t,n,e)}function Mp(r,t,e){if(Gn(r),t!=null&&t.length!==5)throw new Error("tensor5d() requires shape to have five numbers");var n=Yt(r,e);if(n.length!==5&&n.length!==1)throw new Error("tensor5d() requires values to be number[][][][][] or flat/TypedArray");if(n.length===1&&t==null)throw new Error("tensor5d() requires shape to be provided when `values` are a flat array");return Rn(r,t,n,e)}function Op(r,t,e){if(Gn(r),t!=null&&t.length!==6)throw new Error("tensor6d() requires shape to have six numbers");var n=Yt(r,e);if(n.length!==6&&n.length!==1)throw new Error("tensor6d() requires values to be number[][][][][][] or flat/TypedArray");if(n.length===1&&t==null)throw new Error("tensor6d() requires shape to be provided when `values` are a flat array");return Rn(r,t=t||n,n,e)}function Bp(r,t,e,n){return t===void 0&&(t=!0),N.makeVariable(r,t,e,n)}function xr(r,t){if(t===void 0&&(t="float32"),t==="complex64"){var e=xr(r,"float32"),n=Ie(r,"float32");return Ke(e,n)}var o=Ki(ee(r),t);return N.makeTensor(o,r,t)}function Ie(r,t){if(t===void 0&&(t="float32"),t==="complex64"){var e=Ie(r,"float32"),n=Ie(r,"float32");return Ke(e,n)}var o=yr(ee(r),t);return N.makeTensor(o,r,t)}function Xt(r,t,e){return N.runKernelFunc(function(n){return n.fill(r,t,e)},{})}function Lp(r,t,e){if(e<=0)throw new Error("The number of values should be positive.");return N.runKernelFunc(function(n){return n.linspace(r,t,e)},{})}function Lo(r,t,e,n){if(e===void 0&&(e=1),n===void 0&&(n="float32"),e===0)throw new Error("Cannot have a step of zero");if(r===t||r<t&&e<0||t<r&&e>1)return Ie([0],n);var o=yr(Math.abs(Math.ceil((t-r)/e)),n);t<r&&e===1&&(e=-1),o[0]=r;for(var a=1;a<o.length;a++)o[a]=o[a-1]+e;return Me(o,n)}var Nl=D({onesLike_:function(r){var t=_(r,"x","onesLike");if(t.dtype==="complex64"){var e=Nl(St(t)),n=ye(jt(t));return Ke(e,n)}return N.runKernelFunc(function(o){return o.onesLike(t)},{$x:t},function(o,a){return{$x:function(){return ye(o)}}})}}),ye=D({zerosLike_:function(r){var t=_(r,"x","zerosLike");return N.runKernelFunc(function(e){return e.zerosLike(t)},{$x:t},function(e,n){return{$x:function(){return ye(e)}}})}}),Ve=D({concat_:function(r,t){t===void 0&&(t=0),R(r.length>=1,function(){return"Pass at least one tensor to concat"});var e=Bo(r,"tensors","concat");e[0].dtype==="complex64"&&e.forEach(function(s){if(s.dtype!=="complex64")throw new Error(`Cannot concatenate complex64 tensors with a tensor
          with dtype `+s.dtype+". ")}),t=Le(t,e[0].shape)[0];var n=Vn(e.map(function(s){return s.shape}),t);if(ee(n)===0)return $e([],n);if((e=e.filter(function(s){return s.size>0})).length===1)return e[0];var o=e.map(function(s){return s.shape});Tl(o,t);var a=e,i={axis:t};return N.runKernelFunc(function(s){return s.concat(e,t)},a,function(s){var u=o.map(function(c){return c[t]});return Zi(s,u,t).map(function(c){return function(){return c}})},"Concat",i)}}),Wp=D({concat1d_:function(r){return Ve(r,0)}}),Vp=D({concat2d_:function(r,t){return Ve(r,t)}}),zp=D({concat3d_:function(r,t){return Ve(r,t)}}),Up=D({concat4d_:function(r,t){return Ve(r,t)}}),Zi=D({split_:function(r,t,e){e===void 0&&(e=0);var n,o=_(r,"x","split");return e=Le(e,o.shape)[0],typeof t=="number"?(R(o.shape[e]%t==0,function(){return"Number of splits must evenly divide the axis."}),n=new Array(t).fill(o.shape[e]/t)):(R(o.shape[e]===t.reduce(function(a,i){return a+i}),function(){return"The sum of sizes must match the size of the axis dimension."}),n=t),N.runKernelFunc(function(a){return a.split(o,n,e)},{$x:o},function(a){return{$x:function(){return Ve(a,e)}}})}});function Hn(r,t){return r(t={exports:{}},t.exports),t.exports}var Gp=Hn(function(r){(function(t,e,n){function o(s){var u,c=this,l=(u=4022871197,function(h){h=h.toString();for(var f=0;f<h.length;f++){var d=.02519603282416938*(u+=h.charCodeAt(f));d-=u=d>>>0,u=(d*=u)>>>0,u+=4294967296*(d-=u)}return 23283064365386963e-26*(u>>>0)});c.next=function(){var h=2091639*c.s0+23283064365386963e-26*c.c;return c.s0=c.s1,c.s1=c.s2,c.s2=h-(c.c=0|h)},c.c=1,c.s0=l(" "),c.s1=l(" "),c.s2=l(" "),c.s0-=l(s),c.s0<0&&(c.s0+=1),c.s1-=l(s),c.s1<0&&(c.s1+=1),c.s2-=l(s),c.s2<0&&(c.s2+=1),l=null}function a(s,u){return u.c=s.c,u.s0=s.s0,u.s1=s.s1,u.s2=s.s2,u}function i(s,u){var c=new o(s),l=u&&u.state,h=c.next;return h.int32=function(){return 4294967296*c.next()|0},h.double=function(){return h()+11102230246251565e-32*(2097152*h()|0)},h.quick=h,l&&(typeof l=="object"&&a(l,c),h.state=function(){return a(c,{})}),h}e&&e.exports?e.exports=i:n&&n.amd?n(function(){return i}):this.alea=i})(0,r,!1)}),Hp=Hn(function(r){(function(t,e,n){function o(s){var u=this,c="";u.x=0,u.y=0,u.z=0,u.w=0,u.next=function(){var h=u.x^u.x<<11;return u.x=u.y,u.y=u.z,u.z=u.w,u.w^=u.w>>>19^h^h>>>8},s===(0|s)?u.x=s:c+=s;for(var l=0;l<c.length+64;l++)u.x^=0|c.charCodeAt(l),u.next()}function a(s,u){return u.x=s.x,u.y=s.y,u.z=s.z,u.w=s.w,u}function i(s,u){var c=new o(s),l=u&&u.state,h=function(){return(c.next()>>>0)/4294967296};return h.double=function(){do var f=((c.next()>>>11)+(c.next()>>>0)/4294967296)/2097152;while(f===0);return f},h.int32=c.next,h.quick=h,l&&(typeof l=="object"&&a(l,c),h.state=function(){return a(c,{})}),h}e&&e.exports?e.exports=i:n&&n.amd?n(function(){return i}):this.xor128=i})(0,r,!1)}),qp=Hn(function(r){(function(t,e,n){function o(s){var u=this,c="";u.next=function(){var h=u.x^u.x>>>2;return u.x=u.y,u.y=u.z,u.z=u.w,u.w=u.v,(u.d=u.d+362437|0)+(u.v=u.v^u.v<<4^h^h<<1)|0},u.x=0,u.y=0,u.z=0,u.w=0,u.v=0,s===(0|s)?u.x=s:c+=s;for(var l=0;l<c.length+64;l++)u.x^=0|c.charCodeAt(l),l==c.length&&(u.d=u.x<<10^u.x>>>4),u.next()}function a(s,u){return u.x=s.x,u.y=s.y,u.z=s.z,u.w=s.w,u.v=s.v,u.d=s.d,u}function i(s,u){var c=new o(s),l=u&&u.state,h=function(){return(c.next()>>>0)/4294967296};return h.double=function(){do var f=((c.next()>>>11)+(c.next()>>>0)/4294967296)/2097152;while(f===0);return f},h.int32=c.next,h.quick=h,l&&(typeof l=="object"&&a(l,c),h.state=function(){return a(c,{})}),h}e&&e.exports?e.exports=i:n&&n.amd?n(function(){return i}):this.xorwow=i})(0,r,!1)}),jp=Hn(function(r){(function(t,e,n){function o(s){var u=this;u.next=function(){var c,l,h=u.x,f=u.i;return c=h[f],l=(c^=c>>>7)^c<<24,l^=(c=h[f+1&7])^c>>>10,l^=(c=h[f+3&7])^c>>>3,l^=(c=h[f+4&7])^c<<7,c=h[f+7&7],l^=(c^=c<<13)^c<<9,h[f]=l,u.i=f+1&7,l},function(c,l){var h,f=[];if(l===(0|l))f[0]=l;else for(l=""+l,h=0;h<l.length;++h)f[7&h]=f[7&h]<<15^l.charCodeAt(h)+f[h+1&7]<<13;for(;f.length<8;)f.push(0);for(h=0;h<8&&f[h]===0;++h);for(h==8?f[7]=-1:f[h],c.x=f,c.i=0,h=256;h>0;--h)c.next()}(u,s)}function a(s,u){return u.x=s.x.slice(),u.i=s.i,u}function i(s,u){s==null&&(s=+new Date);var c=new o(s),l=u&&u.state,h=function(){return(c.next()>>>0)/4294967296};return h.double=function(){do var f=((c.next()>>>11)+(c.next()>>>0)/4294967296)/2097152;while(f===0);return f},h.int32=c.next,h.quick=h,l&&(l.x&&a(l,c),h.state=function(){return a(c,{})}),h}e&&e.exports?e.exports=i:n&&n.amd?n(function(){return i}):this.xorshift7=i})(0,r,!1)}),Kp=Hn(function(r){(function(t,e,n){function o(s){var u=this;u.next=function(){var c,l,h=u.w,f=u.X,d=u.i;return u.w=h=h+1640531527|0,l=f[d+34&127],c=f[d=d+1&127],l^=l<<13,c^=c<<17,l^=l>>>15,c^=c>>>12,l=f[d]=l^c,u.i=d,l+(h^h>>>16)|0},function(c,l){var h,f,d,p,v,m=[],g=128;for(l===(0|l)?(f=l,l=null):(l+="\0",f=0,g=Math.max(g,l.length)),d=0,p=-32;p<g;++p)l&&(f^=l.charCodeAt((p+32)%l.length)),p===0&&(v=f),f^=f<<10,f^=f>>>15,f^=f<<4,f^=f>>>13,p>=0&&(v=v+1640531527|0,d=(h=m[127&p]^=f+v)==0?d+1:0);for(d>=128&&(m[127&(l&&l.length||0)]=-1),d=127,p=512;p>0;--p)f=m[d+34&127],h=m[d=d+1&127],f^=f<<13,h^=h<<17,f^=f>>>15,h^=h>>>12,m[d]=f^h;c.w=v,c.X=m,c.i=d}(u,s)}function a(s,u){return u.i=s.i,u.w=s.w,u.X=s.X.slice(),u}function i(s,u){s==null&&(s=+new Date);var c=new o(s),l=u&&u.state,h=function(){return(c.next()>>>0)/4294967296};return h.double=function(){do var f=((c.next()>>>11)+(c.next()>>>0)/4294967296)/2097152;while(f===0);return f},h.int32=c.next,h.quick=h,l&&(l.X&&a(l,c),h.state=function(){return a(c,{})}),h}e&&e.exports?e.exports=i:n&&n.amd?n(function(){return i}):this.xor4096=i})(0,r,!1)}),Xp=Hn(function(r){(function(t,e,n){function o(s){var u=this,c="";u.next=function(){var h=u.b,f=u.c,d=u.d,p=u.a;return h=h<<25^h>>>7^f,f=f-d|0,d=d<<24^d>>>8^p,p=p-h|0,u.b=h=h<<20^h>>>12^f,u.c=f=f-d|0,u.d=d<<16^f>>>16^p,u.a=p-h|0},u.a=0,u.b=0,u.c=-1640531527,u.d=1367130551,s===Math.floor(s)?(u.a=s/4294967296|0,u.b=0|s):c+=s;for(var l=0;l<c.length+20;l++)u.b^=0|c.charCodeAt(l),u.next()}function a(s,u){return u.a=s.a,u.b=s.b,u.c=s.c,u.d=s.d,u}function i(s,u){var c=new o(s),l=u&&u.state,h=function(){return(c.next()>>>0)/4294967296};return h.double=function(){do var f=((c.next()>>>11)+(c.next()>>>0)/4294967296)/2097152;while(f===0);return f},h.int32=c.next,h.quick=h,l&&(typeof l=="object"&&a(l,c),h.state=function(){return a(c,{})}),h}e&&e.exports?e.exports=i:n&&n.amd?n(function(){return i}):this.tychei=i})(0,r,!1)}),Pn=Hn(function(r){(function(t,e){var n,o=this,a=256,i=6,s="random",u=e.pow(a,i),c=e.pow(2,52),l=2*c,h=a-1;function f(g,x,y){var b=[],w=v(function E(S,k){var T,A=[],F=typeof S;if(k&&F=="object")for(T in S)try{A.push(E(S[T],k-1))}catch{}return A.length?A:F=="string"?S:S+"\0"}((x=x==1?{entropy:!0}:x||{}).entropy?[g,m(t)]:g==null?function(){try{var E;return n&&(E=n.randomBytes)?E=E(a):(E=new Uint8Array(a),(o.crypto||o.msCrypto).getRandomValues(E)),m(E)}catch{var S=o.navigator,k=S&&S.plugins;return[+new Date,o,k,o.screen,m(t)]}}():g,3),b),C=new d(b),I=function(){for(var E=C.g(i),S=u,k=0;E<c;)E=(E+k)*a,S*=a,k=C.g(1);for(;E>=l;)E/=2,S/=2,k>>>=1;return(E+k)/S};return I.int32=function(){return 0|C.g(4)},I.quick=function(){return C.g(4)/4294967296},I.double=I,v(m(C.S),t),(x.pass||y||function(E,S,k,T){return T&&(T.S&&p(T,C),E.state=function(){return p(C,{})}),k?(e[s]=E,S):E})(I,w,"global"in x?x.global:this==e,x.state)}function d(g){var x,y=g.length,b=this,w=0,C=b.i=b.j=0,I=b.S=[];for(y||(g=[y++]);w<a;)I[w]=w++;for(w=0;w<a;w++)I[w]=I[C=h&C+g[w%y]+(x=I[w])],I[C]=x;(b.g=function(E){for(var S,k=0,T=b.i,A=b.j,F=b.S;E--;)S=F[T=h&T+1],k=k*a+F[h&(F[T]=F[A=h&A+S])+(F[A]=S)];return b.i=T,b.j=A,k})(a)}function p(g,x){return x.i=g.i,x.j=g.j,x.S=g.S.slice(),x}function v(g,x){for(var y,b=g+"",w=0;w<b.length;)x[h&w]=h&(y^=19*x[h&w])+b.charCodeAt(w++);return m(x)}function m(g){return String.fromCharCode.apply(0,g)}if(e["seed"+s]=f,v(e.random(),t),r.exports){r.exports=f;try{n=require("crypto")}catch{}}})([],Math)});Pn.alea=Gp,Pn.xor128=Hp,Pn.xorwow=qp,Pn.xorshift7=jp,Pn.xor4096=Kp,Pn.tychei=Xp;var aa=Pn.alea,es=function(){function r(t,e,n,o,a){this.mean=t,this.stdDev=e,this.dtype=n,this.nextVal=NaN,this.truncated=o,this.truncated&&(this.upper=this.mean+2*this.stdDev,this.lower=this.mean-2*this.stdDev);var i=a||Math.random();this.random=aa(i.toString())}return r.prototype.nextValue=function(){if(!isNaN(this.nextVal)){var t=this.nextVal;return this.nextVal=NaN,t}for(var e,n,o=!1;!o;){var a=void 0,i=void 0,s=void 0;do s=(a=2*this.random()-1)*a+(i=2*this.random()-1)*i;while(s>=1||s===0);var u=Math.sqrt(-2*Math.log(s)/s);e=this.mean+this.stdDev*a*u,n=this.mean+this.stdDev*i*u,this.truncated&&!this.isValidTruncated(e)||(o=!0)}return this.truncated&&!this.isValidTruncated(n)||(this.nextVal=this.convertValue(n)),this.convertValue(e)},r.prototype.convertValue=function(t){return this.dtype==null||this.dtype==="float32"?t:Math.round(t)},r.prototype.isValidTruncated=function(t){return t<=this.upper&&t>=this.lower},r}(),$p=function(){function r(t,e,n,o){this.alpha=t,this.beta=1/e,this.dtype=n;var a=o||Math.random();this.randu=aa(a.toString()),this.randn=new es(0,1,n,!1,this.randu()),this.d=t<1?t+2/3:t-1/3,this.c=1/Math.sqrt(9*this.d)}return r.prototype.nextValue=function(){for(var t,e,n,o,a,i;;){do o=this.randn.nextValue(),i=1+this.c*o;while(i<=0);if(i*=i*i,e=1-.331*(t=o*o)*t,n=.5*t+this.d*(1-i+Math.log(i)),(a=this.randu())<e||Math.log(a)<n)break}return i=1/this.beta*this.d*i,this.alpha<1&&(i*=Math.pow(this.randu(),1/this.alpha)),this.convertValue(i)},r.prototype.convertValue=function(t){return this.dtype==="float32"?t:Math.round(t)},r}(),Yp=function(){function r(t,e,n,o){var a=this;if(t===void 0&&(t=0),e===void 0&&(e=1),this.canReturnFloat=function(){return a.dtype==null||a.dtype==="float32"},this.min=t,this.range=e-t,this.dtype=n,o==null&&(o=Math.random()),typeof o=="number"&&(o=o.toString()),!this.canReturnFloat()&&this.range<=1)throw new Error("The difference between "+t+" - "+e+" <= 1 and dtype is not float");this.random=aa(o)}return r.prototype.convertValue=function(t){return this.canReturnFloat()?t:Math.round(t)},r.prototype.nextValue=function(){return this.convertValue(this.min+this.range*this.random())},r}();function ue(r,t,e){return t===void 0&&(t="float32"),t=t||"float32",Xi(r),new Br(r,t,e)}function Jp(r,t){t===void 0&&(t=!1),console.log(r.toString(t))}var Fl=D({batchToSpaceND_:function(r,t,e){var n=_(r,"x","batchToSpaceND"),o=t.reduce(function(a,i){return a*i});return R(n.rank>=1+t.length,function(){return"input rank is "+n.rank+" but should be > than blockShape.length "+t.length}),R(e.length===t.length,function(){return"crops.length is "+e.length+" but should be equal to blockShape.length  "+t.length}),R(n.shape[0]%o==0,function(){return"input tensor batch is "+n.shape[0]+" but is not divisible by the product of the elements of blockShape "+t.join(" * ")+" === "+o}),N.runKernelFunc(function(a){return a.batchToSpaceND(n,t,e)},{$x:n},function(a){return{$x:function(){return a.spaceToBatchND(t,e)}}})}}),Qp=D({broadcastTo_:function(r,t){var e=_(r,"broadcastTo","x"),n=e.shape;if(t.some(function(u){return!(u>0)||u%1!=0}))throw new Error("broadcastTo(): Invalid broadcast shape ["+t+"].");if(t.length<e.rank)throw new Error("broadcastTo(): shape.length="+t.length+" < input.rank="+e.rank+".");if(t.length>e.rank){for(var o=e.shape.slice();o.length<t.length;)o.unshift(1);e=e.reshape(o)}for(var a=Array.from(t),i=t.length-1;i>=0;i--)if(e.shape[i]===t[i])a[i]=1;else if(e.shape[i]!==1)throw new Error("broadcastTo(): ["+n+"] cannot be broadcast to ["+t+"].");var s=a.map(function(u,c){return u>1?c:-1}).filter(function(u){return u>=0});return s.length===0?e.clone():N.runKernelFunc(function(u){return u.tile(e,a)},{input:e},function(u){return{input:function(){return u.sum(s,!0)}}})}}),Zp=D({cast_:function(r,t){var e=_(r,"x","cast");if(!Kc(t))throw new Error("Failed to cast to unknown dtype "+t);if(t==="string"&&e.dtype!=="string"||t!=="string"&&e.dtype==="string")throw new Error("Only strings can be casted to strings");var n={dtype:t};return N.runKernelFunc(function(o){return o.cast(e,t)},{x:e},function(o){return{x:function(){return o.clone()}}},"Cast",n)}}),ev=D({clone_:function(r){var t=_(r,"x","clone",null);return N.runKernelFunc(function(){return N.makeTensorFromDataId(t.dataId,t.shape,t.dtype)},{$x:t},function(e){return{$x:function(){return e.toFloat()}}})}}),tv=D({cumsum_:function(r,t,e,n){t===void 0&&(t=0),e===void 0&&(e=!1),n===void 0&&(n=!1);var o=_(r,"x","cumsum"),a=Ot([t|=0],o.rank),i=o;a!=null&&(i=o.transpose(a));var s=Bt(1,o.rank)[0],u=N.runKernelFunc(function(c){return c.cumsum(i,s,e,n)},{permutedX:i},function(c){return{permutedX:function(){return c.cumsum(t,e,!n)}}});return a!=null&&(u=u.transpose(a)),u}}),nv=D({depthToSpace_:function(r,t,e){e===void 0&&(e="NHWC");var n=_(r,"x","depthToSpace"),o=e==="NHWC"?n.shape[1]:n.shape[2],a=e==="NHWC"?n.shape[2]:n.shape[3],i=e==="NHWC"?n.shape[3]:n.shape[1];return R(o*t>=0,function(){return`Negative dimension size caused by overflow when multiplying
      `+o+" and "+t+`  for depthToSpace with input shape
      `+n.shape}),R(a*t>=0,function(){return`Negative dimension size caused by overflow when multiplying
      `+a+" and "+t+` for depthToSpace with input shape
          `+n.shape}),R(i%(t*t)==0,function(){return"Dimension size must be evenly divisible by "+t*t+" but is "+i+" for depthToSpace with input shape "+n.shape}),N.runKernelFunc(function(s){return s.depthToSpace(n,t,e)},{$x:n})}}),_t=D({expandDims_:function(r,t){t===void 0&&(t=0);var e=_(r,"x","expandDims",null);R(t<=e.rank,function(){return"Axis must be <= rank of the tensor"});var n=e.shape.slice();return t<0&&(R(-(e.rank+1)<=t,function(){return"Axis must be in the interval ["+-(e.rank+1)+", "+e.rank+"]"}),t=e.rank+t+1),n.splice(t,0,1),Nt(e,n)}}),Pl=D({eye_:function(r,t,e,n){n===void 0&&(n="float32"),t==null&&(t=r);for(var o=ue([r,t],n),a=r<=t?r:t,i=0;i<a;++i)o.set(1,i,i);var s=o.toTensor().as2D(r,t);if(e==null)return s;if(e.length===1)return ir(_t(s,0),[e[0],1,1]);if(e.length===2)return ir(_t(_t(s,0),0),[e[0],e[1],1,1]);if(e.length===3)return ir(_t(_t(_t(s,0),0),0),[e[0],e[1],e[2],1,1]);throw new Error("eye() currently supports only 1D and 2D batchShapes, but received "+e.length+"D.")}}),rv=D({multinomial_:function(r,t,e,n){n===void 0&&(n=!1);var o=_(r,"logits","multinomial"),a=o.size,i=o.rank;if(a<2)throw new Error("Error in multinomial: you need at least 2 outcomes, but got "+a+".");if(i>2)throw new Error("Rank of probabilities must be 1 or 2, but is "+i);e=e||Math.random();var s=i===1?o.as2D(1,-1):o,u=N.runKernelFunc(function(c){return c.multinomial(s,n,t,e)},{logits2D:s});return i===1?u.as1D():u}}),yi=D({oneHot_:function(r,t,e,n){if(e===void 0&&(e=1),n===void 0&&(n=0),t<2)throw new Error("Error in oneHot: depth must be >=2, but it is "+t);var o=_(r,"indices","oneHot","int32"),a=o.shape.concat([t]);return o=o.flatten(),N.runKernelFunc(function(i){return i.oneHot(o,t,e,n)},{$indices:o},function(i){return{$indices:function(){return Ie(o.shape,"float32")}}}).reshape(a)}}),qn=D({pad_:function(r,t,e){e===void 0&&(e=0);var n=_(r,"x","pad");if(n.rank===0)throw new Error("pad(scalar) is not defined. Pass non-scalar to pad");var o={paddings:t,constantValue:e};return N.runKernelFunc(function(a){return a.pad(n,t,e)},{x:n},function(a){var i=t.map(function(s){return s[0]});return{x:function(){return a.slice(i,n.shape)}}},"PadV2",o)}}),ov=D({pad1d_:function(r,t,e){return e===void 0&&(e=0),R(t.length===2,function(){return"Invalid number of paddings. Must be length of 2."}),qn(r,[t],e)}}),av=D({pad2d_:function(r,t,e){return e===void 0&&(e=0),R(t.length===2&&t[0].length===2&&t[1].length===2,function(){return"Invalid number of paddings. Must be length of 2 each."}),qn(r,t,e)}}),iv=D({pad3d_:function(r,t,e){return e===void 0&&(e=0),R(t.length===3&&t[0].length===2&&t[1].length===2&&t[2].length===2,function(){return"Invalid number of paddings. Must be length of 2 each."}),qn(r,t,e)}}),sv=D({pad4d_:function(r,t,e){return e===void 0&&(e=0),R(t.length===4&&t[0].length===2&&t[1].length===2&&t[2].length===2&&t[3].length===2,function(){return"Invalid number of paddings. Must be length of 2 each."}),qn(r,t,e)}}),uv=D({rand_:function(r,t,e){var n=ee(r),o=null;if(e==null||e==="float32")o=new Float32Array(n);else if(e==="int32")o=new Int32Array(n);else{if(e!=="bool")throw new Error("Unknown data type "+e);o=new Uint8Array(n)}for(var a=0;a<n;a++)o[a]=t();return N.makeTensor(o,r,e)}}),cv=D({randomNormal_:function(r,t,e,n,o){if(t===void 0&&(t=0),e===void 0&&(e=1),n!=null&&n==="bool")throw new Error("Unsupported data type "+n);for(var a=new es(t,e,n,!1,o),i=ue(r,n),s=0;s<i.values.length;s++)i.values[s]=a.nextValue();return i.toTensor()}}),lv=D({randomGamma_:function(r,t,e,n,o){if(e===void 0&&(e=1),n===void 0&&(n="float32"),e==null&&(e=1),n==null&&(n="float32"),n!=="float32"&&n!=="int32")throw new Error("Unsupported data type "+n);for(var a=new $p(t,e,n,o),i=ue(r,n),s=0;s<i.values.length;s++)i.values[s]=a.nextValue();return i.toTensor()}}),Ml=D({randomUniform_:function(r,t,e,n,o){t===void 0&&(t=0),e===void 0&&(e=1),n===void 0&&(n="float32");for(var a=ue(r,n),i=new Yp(t,e,null,o),s=0;s<a.values.length;s++)a.values[s]=i.nextValue();return a.toTensor()}}),Nt=D({reshape_:function(r,t){var e=_(r,"x","reshape",null);t=qc(t,e.size),R(e.size===ee(t),function(){return"new shape and old shape must have the same number of elements."});var n={shape:t};return N.runKernelFunc(function(o){return o.reshape(e,t)},{x:e},function(o){return{x:function(){return o.reshape(e.shape)}}},"Reshape",n)}}),Ol=D({spaceToBatchND_:function(r,t,e){var n=_(r,"x","spaceToBatchND");return R(n.rank>=1+t.length,function(){return"input rank "+n.rank+" should be > than [blockShape] "+t.length}),R(e.length===t.length,function(){return"paddings.shape[0] "+e.length+" must be equal to [blockShape] "+t.length}),R(n.shape.reduce(function(o,a,i){return i>0&&i<=t.length?o&&(a+e[i-1][0]+e[i-1][1])%t[i-1]==0:o},!0),function(){return"input spatial dimensions "+n.shape.slice(1)+" with paddings "+e.toString()+" must be divisible by blockShapes "+t.toString()}),N.runKernelFunc(function(o){return o.spaceToBatchND(n,t,e)},{$x:n},function(o){return{$x:function(){return o.batchToSpaceND(t,e)}}})}}),Bl=D({squeeze_:function(r,t){var e=_(r,"x","squeeze");return Nt(e,gn(e.shape,t).newShape)}}),bt=D({stack_:function(r,t){t===void 0&&(t=0);var e=Bo(r,"tensors","stack");if(R(e.length>=1,function(){return"Pass at least one tensor to tf.stack"}),e.length===1)return e[0].expandDims(t);var n=e[0].rank,o=e[0].shape,a=e[0].dtype;R(t<=n,function(){return"Axis must be <= rank of the tensor"}),e.forEach(function(s){be(o,s.shape,"All tensors passed to stack must have matching shapes")}),e.forEach(function(s){R(a===s.dtype,function(){return"All tensors passed to stack must have matching dtypes"})});var i=e.map(function(s){return s.expandDims(t)});return Ve(i,t)}}),ir=D({tile_:function(r,t){var e=_(r,"x","tile",null);R(e.rank===t.length,function(){return"Error in transpose: rank of input "+e.rank+" must match length of reps "+t+"."});var n=[e],o={reps:t};return N.runKernelFunc(function(a,i){var s=a.tile(e,t);return i([e]),s},{x:e},function(a,i){var s=i[0];return{x:function(){var u=ye(s);if(s.rank===1)for(var c=0;c<t[0];++c)u=u.add(a.slice([c*s.shape[0]],[s.shape[0]]));else if(s.rank===2)for(c=0;c<t[0];++c)for(var l=0;l<t[1];++l)u=u.add(a.slice([c*s.shape[0],l*s.shape[1]],[s.shape[0],s.shape[1]]));else if(s.rank===3)for(c=0;c<t[0];++c)for(l=0;l<t[1];++l)for(var h=0;h<t[2];++h)u=u.add(a.slice([c*s.shape[0],l*s.shape[1],h*s.shape[2]],[s.shape[0],s.shape[1],s.shape[2]]));else{if(s.rank!==4)throw new Error("Gradient for tile operation is not implemented for rank-"+s.rank+" tensors yet.");for(c=0;c<t[0];++c)for(l=0;l<t[1];++l)for(h=0;h<t[2];++h)for(var f=0;f<t[3];++f)u=u.add(a.slice([c*s.shape[0],l*s.shape[1],h*s.shape[2],f*s.shape[3]],[s.shape[0],s.shape[1],s.shape[2],s.shape[3]]))}return u}}},"Tile",o,n)}}),hv=D({truncatedNormal_:function(r,t,e,n,o){if(t===void 0&&(t=0),e===void 0&&(e=1),n!=null&&n==="bool")throw new Error("Unsupported data type "+n);for(var a=new es(t,e,n,!0,o),i=ue(r,n),s=0;s<i.values.length;s++)i.values[s]=a.nextValue();return i.toTensor()}}),ze=D({unstack_:function(r,t){t===void 0&&(t=0),t=t||0;var e=_(r,"x","unstack");R(t>=-e.shape.length&&t<e.shape.length,function(){return"Axis = "+t+" is not in [-"+e.shape.length+", "+e.shape.length+")"}),t<0&&(t+=e.shape.length);var n={axis:t};return N.runKernelFunc(function(o){return o.unstack(e,t)},{x:e},function(o){return{x:function(){return bt(o,t)}}},"Unpack",n)}}),fv=function(r,t){return Q(this,void 0,void 0,function(){var e,n,o,a,i,s,u,c,l,h;return Z(this,function(f){switch(f.label){case 0:return e=_(r,"x","setdiff1d"),n=_(t,"y","setdiff1d"),R(e.dtype===n.dtype,function(){return"x and y should have the same dtype, but got x ("+e.dtype+") and y ("+n.dtype+")."}),R(e.rank===1,function(){return"x should be 1D tensor, but got x ("+e.shape+")."}),R(n.rank===1,function(){return"y should be 1D tensor, but got y ("+n.shape+")."}),[4,e.data()];case 1:return o=f.sent(),[4,n.data()];case 2:for(a=f.sent(),i=new Set(a),s=0,l=0;l<o.length;l++)i.has(o[l])||s++;for(u=new Br([s],e.dtype),c=new Br([s],"int32"),l=0,h=0;l<o.length;l++)i.has(o[l])||(u.values[h]=o[l],c.values[h]=l,h++);return[2,[u.toTensor(),c.toTensor()]]}})})};function Wo(r,t,e,n){n===void 0&&(n=!0);var o=[];if(n)(o=o.concat(t.slice(0))).push(r[0]/e),o=o.concat(r.slice(1));else{o=o.concat(r[0]);for(var a=t.length,i=0;i<a;++i)o=o.concat([r[i+1]/t[i],t[i]]);o=o.concat(r.slice(a+1))}return o}function Vo(r,t,e){e===void 0&&(e=!0);var n=[];if(e){n.push(t);for(var o=t+1;o<r;++o)o<=2*t?(n.push(o),n.push(o-(t+1))):n.push(o)}else{var a=[],i=[];for(o=1;o<r;++o)o>=2*t+1||o%2==1?i.push(o):a.push(o);n.push.apply(n,a),n.push(0),n.push.apply(n,i)}return n}function zo(r,t,e,n){n===void 0&&(n=!0);var o=[];n?o.push(r[0]/e):o.push(r[0]*e);for(var a=1;a<r.length;++a)a<=t.length?n?o.push(t[a-1]*r[a]):o.push(r[a]/t[a-1]):o.push(r[a]);return o}function Ll(r,t){for(var e=[0],n=0;n<t;++n)e.push(r[n][0]);return e}function Wl(r,t,e){for(var n=r.slice(0,1),o=0;o<e;++o)n.push(r[o+1]-t[o][0]-t[o][1]);return n}function ts(r,t){if(r.rank<1)throw new Error("tf.gatherND() expects the input to be rank 1 or higher, but the rank was "+r.rank+".");if(t.rank<1)throw new Error("tf.gatherND() expects the indices to be rank 1 or higher, but the rank was "+t.rank+".");if(t.dtype!=="int32")throw new Error("tf.gatherND() expects the indices to be int32 type, but the dtype was "+t.dtype+".");if(t.shape[t.rank-1]>r.rank)throw new Error("index innermost dimension length must be <= tensor rank; saw: "+t.shape[t.rank-1]+" vs. "+r.rank);if(r.size===0)throw new Error("Requested more than 0 entries, but input is empty. Input shape: "+r.shape+".");for(var e=t.shape,n=e[e.length-1],o=1,a=0;a<e.length-1;++a)o*=e[a];var i=r.shape,s=e.slice();s.pop();var u=1;for(a=n;a<r.rank;++a)u*=i[a],s.push(i[a]);var c=Ft(r.shape).map(function(l){return l/u}).concat([1]).slice(0,n);return[s,o,u,c]}Object.freeze({prepareAndValidate:ts});var ns=30;function So(r){return r<=ns?r:Mo(r,Math.floor(Math.sqrt(r)))}function Vl(r,t,e){var n=t.rank>1?t.shape[t.rank-1]:1,o=t.rank>1?t.rank-1:1,a="Must have updates.shape = indices.shape[:batchDim] + shape[sliceDim:], got updates.shape: "+e.shape+", indices.shape: "+t.shape+", shape: "+r+", sliceDim: "+n+", and batchDim: "+o+".";if(e.rank<o)throw new Error(a+" update.rank < "+o+". ");if(r.length<n+(e.rank-o))throw new Error(a+" Output shape length < "+(n+(e.rank-o)));if(e.rank!==o+r.length-n)throw new Error(a+" update.rank != "+(o+r.length-n));for(var i=0;i<o;++i)if(e.shape[i]!==t.shape[i])throw new Error(a+" updates.shape["+i+"] ("+e.shape[i]+") != indices.shape["+i+"] ("+t.shape[i]+").");for(i=0;i<e.rank-o;++i)if(e.shape[i+o]!==r[i+n])throw new Error(a+" updates.shape["+(i+o)+"] ("+e.shape[i+o]+") != shape["+(i+o)+"] ("+r[i+o]+")")}function zl(r,t,e){if(t.rank<1)throw new Error("tf.scatterND() expects the indices to be rank 1 or higher, but the rank was "+t.rank+".");if(r.rank<1)throw new Error("tf.scatterND() expects the updates to be rank 1 or higher, but the rank was "+r.rank+".");if(t.dtype!=="int32")throw new Error("The dtype of 'indices' should be int32, but got dtype: "+t.dtype);if(e.length<1)throw new Error("Output rank must be greater or equal to 1, but got shape: "+e);if(e.length===0){if(t.size===0)throw new Error("Indices specified for empty output. indices shape: "+t.shape);if(r.size===0)throw new Error("Updates specified for empty output. updates shape: "+r.shape)}Vl(e,t,r)}function zr(r,t,e){for(var n=t.shape.length,o=n>1?t.shape[n-1]:1,a=e.length,i=1,s=o;s<a;++s)i*=e[s];var u=o<1?1:o;return{sliceRank:o,numUpdates:ee(t.shape)/u,sliceSize:i,strides:Ft(e.slice(0,o)).concat([1]),outputSize:ee(e)}}Object.freeze({validateUpdateShape:Vl,validateInput:zl,calculateShapes:zr});function Ul(r,t,e){R(r.rank===t.length,function(){return"Error in slice"+r.rank+"D: Length of begin "+t+" must match the rank of the array ("+r.rank+")."}),R(r.rank===e.length,function(){return"Error in slice"+r.rank+"D: Length of size "+e+" must match the rank of the array ("+r.rank+")."});for(var n=function(a){R(t[a]+e[a]<=r.shape[a],function(){return"Error in slice"+r.rank+"D: begin["+a+"] + size["+a+"] ("+(t[a]+e[a])+") would overflow input.shape["+a+"] ("+r.shape[a]+")"})},o=0;o<r.rank;++o)n(o)}function xi(r){for(var t=[],e=0;r>0;)1&r&&t.push(e),r/=2,e++;return t}function ia(r,t,e){for(var n=[],o=0;o<r.length;o++)n[o]=Math.ceil((t[o]-r[o])/e[o]);return n}function Gl(r,t,e,n,o){var a=t[o],i=e[o]||1;(r&1<<o||a==null)&&(a=i>0?Number.MIN_SAFE_INTEGER:Number.MAX_SAFE_INTEGER);var s=n[o];return a<0&&(a+=s),a=No(0,a,s-1)}function Hl(r,t,e,n,o){var a=t[o],i=e[o]||1;(r&1<<o||a==null)&&(a=i>0?Number.MAX_SAFE_INTEGER:Number.MIN_SAFE_INTEGER);var s=n[o];return a<0&&(a+=s),a=i>0?No(0,a,s):No(-1,a,s-1)}function rs(r,t,e){for(var n=e.length,o=0;o<e.length;o++)if(e[o]>1){n=o;break}for(o=n+1;o<e.length;o++)if(t[o]>0||e[o]!==r[o])return!1;return!0}function os(r,t){for(var e=r.length>0?r[r.length-1]:1,n=0;n<r.length-1;n++)e+=r[n]*t[n];return e}Object.freeze({assertParamsValid:Ul,maskToAxes:xi,computeOutShape:ia,startForAxis:Gl,stopForAxis:Hl,isSliceContinous:rs,computeFlatOffset:os});function dv(r,t){R(Po(r),function(){return"The f passed in variableGrads(f) must be a function"}),R(t==null||Array.isArray(t)&&t.every(function(l){return l instanceof dr}),function(){return"The varList passed in variableGrads(f, varList) must be an array of variables"});var e=t!=null;if(!e)for(var n in t=[],N.registeredVariables)t.push(N.registeredVariables[n]);var o=e?t.filter(function(l){return!l.trainable}):null,a=t.length;R((t=t.filter(function(l){return l.trainable})).length>0,function(){return"variableGrads() expects at least one of the input variables to be trainable, but none of the "+a+" variables is trainable."});var i=N.gradients(r,t,null,!0),s=i.value,u=i.grads;R(u.some(function(l){return l!=null}),function(){return"Cannot find a connection between any variable and the result of the loss function y=f(x). Please make sure the operations that use variables are inside the function f passed to minimize()."}),R(s.rank===0,function(){return"The f passed in variableGrads(f) must return a scalar, but it returned a rank-"+s.rank+" tensor"});var c={};return t.forEach(function(l,h){u[h]!=null&&(c[l.name]=u[h])}),o!=null&&o.forEach(function(l){return c[l.name]=null}),{value:s,grads:c}}function sa(r){return N.customGrad(r)}var cn=D({softmax_:function(r,t){t===void 0&&(t=-1);var e=_(r,"logits","softmax","float32");if(t===-1&&(t=e.rank-1),t!==e.rank-1)throw Error("Softmax along a non-last dimension is not yet supported. Logits was rank "+e.rank+" and dim was "+t);return N.runKernelFunc(function(n,o){var a=n.softmax(e,t);return o([a]),a},{logits:e},function(n,o){var a=o[0],i=n.mul(a);return{logits:function(){return i.sub(i.sum([t],!0).mul(a))}}},"Softmax",{dim:t},[],[!0])}}),pv=D({logSoftmax_:function(r,t){t===void 0&&(t=-1);var e=_(r,"logits","logSoftmax");if(t===-1&&(t=e.rank-1),t!==e.rank-1)throw Error("Log Softmax along a non-last dimension is not yet supported. Logits was rank "+e.rank+" and axis was "+t);return sa(function(n,o){var a=n.max(t,!0),i=n.sub(a),s=i.toFloat().sub(i.exp().sum(t,!0).log());return o([s]),{value:s,gradFunc:function(u,c){var l=c[0].exp();return u.sub(u.sum(t,!0).mul(l))}}})(e)}}),ql=function(){function r(t,e){this.backend=t,this.dataMover=e,this.data=new WeakMap,this.dataIdsCount=0}return r.prototype.get=function(t){return this.data.has(t)||this.dataMover.moveData(this.backend,t),this.data.get(t)},r.prototype.set=function(t,e){this.dataIdsCount++,this.data.set(t,e)},r.prototype.has=function(t){return this.data.has(t)},r.prototype.delete=function(t){return this.dataIdsCount--,this.data.delete(t)},r.prototype.numDataIds=function(){return this.dataIdsCount},r}(),jl=function(){function r(){}return r.prototype.time=function(t){return M("time")},r.prototype.read=function(t){return M("read")},r.prototype.readSync=function(t){return M("readSync")},r.prototype.numDataIds=function(){return M("numDataIds")},r.prototype.disposeData=function(t){return M("disposeData")},r.prototype.write=function(t,e,n){return M("write")},r.prototype.move=function(t,e,n,o){return M("move")},r.prototype.memory=function(){return M("memory")},r.prototype.floatPrecision=function(){return M("floatPrecision")},r.prototype.epsilon=function(){return this.floatPrecision()===32?1e-7:1e-4},r.prototype.batchMatMul=function(t,e,n,o){return M("batchMatMul")},r.prototype.fusedBatchMatMul=function(t){return t.a,t.b,t.transposeA,t.transposeB,t.bias,t.activation,t.preluActivationWeights,M("fusedBatchMatMul")},r.prototype.slice=function(t,e,n){return M("slice")},r.prototype.stridedSlice=function(t,e,n,o){return M("stridedSlice")},r.prototype.unstack=function(t,e){return M("unstack")},r.prototype.reverse=function(t,e){return M("reverse")},r.prototype.concat=function(t,e){return M("concat")},r.prototype.neg=function(t){return M("neg")},r.prototype.add=function(t,e){return M("add")},r.prototype.addN=function(t){return M("addN")},r.prototype.subtract=function(t,e){return M("subtract")},r.prototype.multiply=function(t,e){return M("multiply")},r.prototype.realDivide=function(t,e){return M("realDivide")},r.prototype.floorDiv=function(t,e){return M("floorDiv")},r.prototype.sum=function(t,e){return M("sum")},r.prototype.prod=function(t,e){return M("prod")},r.prototype.unsortedSegmentSum=function(t,e,n){return M("unsortedSegmentSum")},r.prototype.argMin=function(t,e){return M("argMin")},r.prototype.argMax=function(t,e){return M("argMax")},r.prototype.equal=function(t,e){return M("equal")},r.prototype.notEqual=function(t,e){return M("notEqual")},r.prototype.less=function(t,e){return M("less")},r.prototype.lessEqual=function(t,e){return M("lessEqual")},r.prototype.greater=function(t,e){return M("greater")},r.prototype.greaterEqual=function(t,e){return M("greaterEqual")},r.prototype.logicalNot=function(t){return M("logicalNot")},r.prototype.logicalAnd=function(t,e){return M("logicalAnd")},r.prototype.logicalOr=function(t,e){return M("logicalOr")},r.prototype.where=function(t){return M("where")},r.prototype.select=function(t,e,n){return M("select")},r.prototype.topk=function(t,e,n){return M("topk")},r.prototype.min=function(t,e){return M("min")},r.prototype.minimum=function(t,e){return M("minimum")},r.prototype.mod=function(t,e){return M("mod")},r.prototype.max=function(t,e){return M("max")},r.prototype.maximum=function(t,e){return M("maximum")},r.prototype.all=function(t,e){return M("all")},r.prototype.any=function(t,e){return M("any")},r.prototype.squaredDifference=function(t,e){return M("squaredDifference")},r.prototype.ceil=function(t){return M("ceil")},r.prototype.floor=function(t){return M("floor")},r.prototype.round=function(t){return M("round")},r.prototype.sign=function(t){return M("sign")},r.prototype.isNaN=function(t){return M("isNaN")},r.prototype.isInf=function(t){return M("isInf")},r.prototype.isFinite=function(t){return M("isFinite")},r.prototype.pow=function(t,e){return M("pow")},r.prototype.exp=function(t){return M("exp")},r.prototype.expm1=function(t){return M("expm1")},r.prototype.softmax=function(t,e){return M("softmax")},r.prototype.log=function(t){return M("log")},r.prototype.log1p=function(t){return M("log1p")},r.prototype.sqrt=function(t){return M("sqrt")},r.prototype.rsqrt=function(t){return M("rsqrt")},r.prototype.square=function(t){return M("square")},r.prototype.reciprocal=function(t){return M("reciprocal")},r.prototype.relu=function(t){return M("relu")},r.prototype.relu6=function(t){return M("relu6")},r.prototype.prelu=function(t,e){return M("prelu")},r.prototype.elu=function(t){return M("elu")},r.prototype.eluDer=function(t,e){return M("eluDer")},r.prototype.selu=function(t){return M("selu")},r.prototype.int=function(t){return M("int")},r.prototype.clip=function(t,e,n){return M("clip")},r.prototype.abs=function(t){return M("abs")},r.prototype.complexAbs=function(t){return M("complexAbs")},r.prototype.sigmoid=function(t){return M("sigmoid")},r.prototype.softplus=function(t){return M("softplus")},r.prototype.sin=function(t){return M("sin")},r.prototype.cos=function(t){return M("cos")},r.prototype.tan=function(t){return M("tan")},r.prototype.asin=function(t){return M("asin")},r.prototype.acos=function(t){return M("acos")},r.prototype.atan=function(t){return M("atan")},r.prototype.atan2=function(t,e){return M("atan2")},r.prototype.sinh=function(t){return M("sinh")},r.prototype.cosh=function(t){return M("cosh")},r.prototype.tanh=function(t){return M("tanh")},r.prototype.asinh=function(t){return M("asinh")},r.prototype.acosh=function(t){return M("acosh")},r.prototype.atanh=function(t){return M("atanh")},r.prototype.erf=function(t){return M("erf")},r.prototype.step=function(t,e){return M("step")},r.prototype.fusedConv2d=function(t){return t.input,t.filter,t.convInfo,t.bias,t.activation,t.preluActivationWeights,M("fusedConv2d")},r.prototype.conv2d=function(t,e,n){return M("conv2d")},r.prototype.conv2dDerInput=function(t,e,n){return M("conv2dDerInput")},r.prototype.conv2dDerFilter=function(t,e,n){return M("conv2dDerFilter")},r.prototype.fusedDepthwiseConv2D=function(t){return t.input,t.filter,t.convInfo,t.bias,t.activation,t.preluActivationWeights,M("fusedDepthwiseConv2D")},r.prototype.depthwiseConv2D=function(t,e,n){return M("depthwiseConv2D")},r.prototype.depthwiseConv2DDerInput=function(t,e,n){return M("depthwiseConv2DDerInput")},r.prototype.depthwiseConv2DDerFilter=function(t,e,n){return M("depthwiseConv2DDerFilter")},r.prototype.conv3d=function(t,e,n){return M("conv3d")},r.prototype.conv3dDerInput=function(t,e,n){return M("conv3dDerInput")},r.prototype.conv3dDerFilter=function(t,e,n){return M("conv3dDerFilter")},r.prototype.maxPool=function(t,e){return M("maxPool")},r.prototype.maxPoolBackprop=function(t,e,n,o){return M("maxPoolBackprop")},r.prototype.avgPool=function(t,e){return M("avgPool")},r.prototype.avgPoolBackprop=function(t,e,n){return M("avgPoolBackprop")},r.prototype.avgPool3d=function(t,e){return M("avgPool3d")},r.prototype.avgPool3dBackprop=function(t,e,n){return M("avgPool3dBackprop")},r.prototype.maxPool3d=function(t,e){return M("maxPool3d")},r.prototype.maxPool3dBackprop=function(t,e,n,o){return M("maxPool3dBackprop")},r.prototype.reshape=function(t,e){return M("reshape")},r.prototype.cast=function(t,e){return M("cast")},r.prototype.tile=function(t,e){return M("tile")},r.prototype.pad=function(t,e,n){return M("pad")},r.prototype.transpose=function(t,e){return M("transpose")},r.prototype.gather=function(t,e,n){return M("gather")},r.prototype.gatherND=function(t,e){return M("gatherND")},r.prototype.scatterND=function(t,e,n){return M("scatterND")},r.prototype.batchToSpaceND=function(t,e,n){return M("batchToSpaceND")},r.prototype.spaceToBatchND=function(t,e,n){return M("spaceToBatchND")},r.prototype.resizeBilinear=function(t,e,n,o){return M("resizeBilinear")},r.prototype.resizeBilinearBackprop=function(t,e,n){return M("resizeBilinearBackprop")},r.prototype.resizeNearestNeighbor=function(t,e,n,o){return M("resizeNearestNeighbor")},r.prototype.resizeNearestNeighborBackprop=function(t,e,n){return M("resizeNearestNeighborBackprop")},r.prototype.batchNormalization=function(t,e,n,o,a,i){return M("batchNormalization")},r.prototype.localResponseNormalization4D=function(t,e,n,o,a){return M("localResponseNormalization4D")},r.prototype.LRNGrad=function(t,e,n,o,a,i,s){return M("LRNGrad")},r.prototype.multinomial=function(t,e,n,o){return M("multinomial")},r.prototype.oneHot=function(t,e,n,o){return M("oneHot")},r.prototype.cumsum=function(t,e,n,o){return M("cumsum")},r.prototype.nonMaxSuppression=function(t,e,n,o,a){return M("nonMaxSuppression")},r.prototype.fft=function(t){return M("fft")},r.prototype.ifft=function(t){return M("ifft")},r.prototype.complex=function(t,e){return M("complex")},r.prototype.real=function(t){return M("real")},r.prototype.imag=function(t){return M("imag")},r.prototype.cropAndResize=function(t,e,n,o,a,i){return M("cropAndResize")},r.prototype.depthToSpace=function(t,e,n){return M("depthToSpace")},r.prototype.split=function(t,e,n){return M("split")},r.prototype.sparseToDense=function(t,e,n,o){return M("sparseToDense")},r.prototype.diag=function(t){return M("diag")},r.prototype.fill=function(t,e,n){return M("fill")},r.prototype.onesLike=function(t){return M("onesLike")},r.prototype.zerosLike=function(t){return M("zerosLike")},r.prototype.linspace=function(t,e,n){return M("linspace")},r.prototype.dispose=function(){return M("dispose")},r}();function M(r){throw new Error("'"+r+"' not yet implemented or not found in the registry. Did you forget to import the kernel?")}function an(r,t){for(var e=r.length,n=[],o=0;o<e;o++){var a=e-1-o,i=r[a]||1;(t[t.length-1-o]||1)>1&&i===1&&n.unshift(a)}return n}function Be(r,t){for(var e=[],n=0;n<t.length;n++){var o=r[r.length-n-1],a=t.length-n-1,i=t[a];(o==null||o===1&&i>1)&&e.unshift(a)}return e}function ve(r,t){for(var e=[],n=Math.max(r.length,t.length),o=0;o<n;o++){var a=r[r.length-o-1];a==null&&(a=1);var i=t[t.length-o-1];if(i==null&&(i=1),a===1)e.unshift(i);else if(i===1)e.unshift(a);else{if(a!==i)throw Error("Operands could not be broadcast together with shapes "+r+" and "+t+".");e.unshift(a)}}return e}function pr(r,t,e,n,o,a,i){i===void 0&&(i="channelsLast");var s,u=Uo(t),c=u[0],l=u[1];if(i==="channelsLast")s=[c,l,r[3],r[3]];else{if(i!=="channelsFirst")throw new Error("Unknown dataFormat "+i);s=[c,l,r[1],r[1]]}return In(r,s,e,n,o,a,!1,i)}function Ur(r,t,e,n,o,a,i){i===void 0&&(i="NDHWC");var s,u,c=bi(t),l=c[0],h=c[1],f=c[2];if(i==="NDHWC")u="channelsLast",s=[l,h,f,r[4],r[4]];else{if(i!=="NCDHW")throw new Error("Unknown dataFormat "+i);u="channelsFirst",s=[l,h,f,r[1],r[1]]}return Gr(r,s,e,n,o,!1,u,a)}function In(r,t,e,n,o,a,i,s){i===void 0&&(i=!1),s===void 0&&(s="channelsLast");var u=[-1,-1,-1,-1],c=u[0],l=u[1],h=u[2],f=u[3];if(s==="channelsLast")c=r[0],l=r[1],h=r[2],f=r[3];else{if(s!=="channelsFirst")throw new Error("Unknown dataFormat "+s);c=r[0],f=r[1],l=r[2],h=r[3]}var d,p=t[0],v=t[1],m=t[3],g=Uo(e),x=g[0],y=g[1],b=Uo(n),w=b[0],C=b[1],I=sr(p,w),E=sr(v,C),S=function(P,B,U,z,L,W,G,j){var q,X,oe;if(typeof P=="number"){q={top:P,bottom:P,left:P,right:P,type:P===0?"VALID":"NUMBER"};var re=function(me,we,ge,Ee,ke){Ee==null&&(Ee=as(me,we,ge));var Re=me[0],wt=me[1],Ct=Tr((Re-we+2*Ee)/ge+1,ke);R(Fe(Ct),function(){return"The output # of rows ("+Ct+") must be an integer. Change the stride and/or zero pad parameters"});var ot=Tr((wt-we+2*Ee)/ge+1,ke);return R(Fe(ot),function(){return"The output # of columns ("+ot+") must be an integer. Change the stride and/or zero pad parameters"}),[Ct,ot]}([B,U],W,z,P,j);X=re[0],oe=re[1]}else if(P==="same"){X=Math.ceil(B/z),oe=Math.ceil(U/L);var ie=Math.max(0,(X-1)*z+W-B),ce=Math.max(0,(oe-1)*L+G-U),de=Math.floor(ie/2),le=ie-de,_e=Math.floor(ce/2);q={top:de,bottom:le,left:_e,right:ce-_e,type:"SAME"}}else{if(P!=="valid")throw Error("Unknown padding parameter: "+P);q={top:0,bottom:0,left:0,right:0,type:"VALID"},X=Math.ceil((B-W+1)/z),oe=Math.ceil((U-G+1)/L)}return{padInfo:q,outHeight:X,outWidth:oe}}(o,l,h,x,y,I,E,a),k=S.padInfo,T=S.outHeight,A=S.outWidth,F=i?m*f:m;return s==="channelsFirst"?d=[c,F,T,A]:s==="channelsLast"&&(d=[c,T,A,F]),{batchSize:c,dataFormat:s,inHeight:l,inWidth:h,inChannels:f,outHeight:T,outWidth:A,outChannels:F,padInfo:k,strideHeight:x,strideWidth:y,filterHeight:p,filterWidth:v,effectiveFilterHeight:I,effectiveFilterWidth:E,dilationHeight:w,dilationWidth:C,inShape:r,outShape:d,filterShape:t}}function Gr(r,t,e,n,o,a,i,s){a===void 0&&(a=!1),i===void 0&&(i="channelsLast");var u=[-1,-1,-1,-1,-1],c=u[0],l=u[1],h=u[2],f=u[3],d=u[4];if(i==="channelsLast")c=r[0],l=r[1],h=r[2],f=r[3],d=r[4];else{if(i!=="channelsFirst")throw new Error("Unknown dataFormat "+i);c=r[0],d=r[1],l=r[2],h=r[3],f=r[4]}var p,v=t[0],m=t[1],g=t[2],x=t[4],y=bi(e),b=y[0],w=y[1],C=y[2],I=bi(n),E=I[0],S=I[1],k=I[2],T=sr(v,E),A=sr(m,S),F=sr(g,k),P=function(G,j,q,X,oe,re,ie,ce,de,le,_e){var me,we,ge,Ee;if(typeof G=="number"){me={top:G,bottom:G,left:G,right:G,front:G,back:G,type:G===0?"VALID":"NUMBER"};var ke=function(Kn,en,_a,Xn,zt,Ea){zt==null&&(zt=as(Kn,en,Xn));var Sd=Kn[0],kd=Kn[1],Rd=Kn[2],Sa=Tr((Sd-en+2*zt)/Xn+1,Ea);R(Fe(Sa),function(){return"The output # of depths ("+Sa+") must be an integer. Change the stride and/or zero pad parameters"});var ka=Tr((kd-en+2*zt)/Xn+1,Ea);R(Fe(ka),function(){return"The output # of rows ("+ka+") must be an integer. Change the stride and/or zero pad parameters"});var Ra=Tr((Rd-en+2*zt)/Xn+1,Ea);return R(Fe(Ra),function(){return"The output # of columns ("+Ra+") must be an integer. Change the stride and/or zero pad parameters"}),[Sa,ka,Ra,_a]}([j,q,X,1],ce,1,oe,G,_e);we=ke[0],ge=ke[1],Ee=ke[2]}else if(G==="same"){we=Math.ceil(j/oe),ge=Math.ceil(q/re),Ee=Math.ceil(X/ie);var Re=(we-1)*oe+ce-j,wt=(ge-1)*re+de-q,Ct=(Ee-1)*ie+le-X,ot=Math.floor(Re/2),pn=Re-ot,At=Math.floor(wt/2),Vt=wt-At,he=Math.floor(Ct/2);me={top:At,bottom:Vt,left:he,right:Ct-he,front:ot,back:pn,type:"SAME"}}else{if(G!=="valid")throw Error("Unknown padding parameter: "+G);me={top:0,bottom:0,left:0,right:0,front:0,back:0,type:"VALID"},we=Math.ceil((j-ce+1)/oe),ge=Math.ceil((q-de+1)/re),Ee=Math.ceil((X-le+1)/ie)}return{padInfo:me,outDepth:we,outHeight:ge,outWidth:Ee}}(o,l,h,f,b,w,C,T,A,F,s),B=P.padInfo,U=P.outDepth,z=P.outHeight,L=P.outWidth,W=a?x*d:x;return i==="channelsFirst"?p=[c,W,U,z,L]:i==="channelsLast"&&(p=[c,U,z,L,W]),{batchSize:c,dataFormat:i,inDepth:l,inHeight:h,inWidth:f,inChannels:d,outDepth:U,outHeight:z,outWidth:L,outChannels:W,padInfo:B,strideDepth:b,strideHeight:w,strideWidth:C,filterDepth:v,filterHeight:m,filterWidth:g,effectiveFilterDepth:T,effectiveFilterHeight:A,effectiveFilterWidth:F,dilationDepth:E,dilationHeight:S,dilationWidth:k,inShape:r,outShape:p,filterShape:t}}function as(r,t,e,n){n===void 0&&(n=1);var o=sr(t,n);return Math.floor((r[0]*(e-1)-e+o)/2)}function Uo(r){return typeof r=="number"?[r,r,r]:r.length===2?[r[0],r[1],1]:r}function bi(r){return typeof r=="number"?[r,r,r]:r}function sr(r,t){return t<=1?r:r+(r-1)*(t-1)}function Tr(r,t){if(!t)return r;switch(t){case"round":return Math.round(r);case"ceil":return Math.ceil(r);case"floor":return Math.floor(r);default:throw new Error("Unknown roundingMode "+t)}}function zn(r){var t=Uo(r),e=t[0],n=t[1],o=t[2];return e===1&&n===1&&o===1}function st(r,t){return zn(r)||zn(t)}function ua(r){if(r==="NHWC")return"channelsLast";if(r==="NCHW")return"channelsFirst";throw new Error("Unknown dataFormat "+r)}function is(r,t,e){if(t==="complex64"){if(r.dtype==="complex64")return r.clone();var n=Ie(r.shape),o=r.toFloat(),a=e.complex(o,n);return n.dispose(),o.dispose(),a}if(!Xc(r.dtype,t))return N.makeTensorFromDataId(r.dataId,r.shape,t);if(r.dtype==="complex64"){var i=e.real(r);return a=i.cast(t),i.dispose(),a}if(t==="int32")return e.int(r);if(t==="bool"){var s=K(0,r.dtype);return a=e.notEqual(r,s),s.dispose(),a}throw new Error("Error in Cast: failed to cast "+r.dtype+" to "+t)}function Go(r,t){return N.makeTensorFromDataId(r.dataId,t,r.dtype)}function ss(r,t,e){var n=(t-r)/(e-1),o=yr(e,"float32");o[0]=r;for(var a=1;a<o.length;a++)o[a]=o[a-1]+n;return Me(o,"float32")}Object.freeze({castTensor:is,reshapeTensor:Go,linspaceImpl:ss,upcastType:He,axesAreInnerMostDims:Ji,combineLocations:Dl,computeOutAndReduceShapes:Xe,expandShapeToKeepDim:it,assertAxesAreInnerMostDims:ct,getAxesPermutation:Ot,getUndoAxesPermutation:oa,getInnerMostAxes:Bt,getBroadcastDims:an,getReductionAxes:Be,assertAndGetBroadcastShape:ve,assertParamsConsistent:Tl,computeOutShape:Vn,computePool2DInfo:pr,computePool3DInfo:Ur,computeConv2DInfo:In,computeConv3DInfo:Gr,computeDefaultPad:as,tupleValuesAreOne:zn,eitherStridesOrDilationsAreOne:st,convertConv2DDataFormat:ua,PARALLELIZE_THRESHOLD:ns,computeOptimalWindowSize:So});function wi(r,t){if(r.length!==t.length)throw new Error("Cannot merge real and imag arrays of different lengths. real:"+r.length+", imag: "+t.length+".");for(var e=new Float32Array(2*r.length),n=0;n<e.length;n+=2)e[n]=r[n/2],e[n+1]=t[n/2];return e}function vu(r,t){return{real:r[2*t],imag:r[2*t+1]}}function vv(r,t,e,n){r[2*n]=t,r[2*n+1]=e}function mv(r,t,e){var n=(e?2:-2)*Math.PI*(r/t);return{real:Math.cos(n),imag:Math.sin(n)}}function gv(r,t,e){var n=function(a,i,s){return function(u,c,l){for(var h=0,f=u.length,d=0,p=!1;h<f;){var v=l(c,u[d=h+(f-h>>>1)]);v>0?h=d+1:(f=d,p=!v)}return p?h:-h-1}(a,i,s||yv)}(r,t,e),o=n<0?-(n+1):n;r.splice(o,0,t)}function yv(r,t){return r>t?1:r<t?-1:0}function us(r,t,e,n,o){return Kl(r,t,e,n,o,0).selectedIndices}function cs(r,t,e,n,o,a){var i=Kl(r,t,e,n,o,a);return i.numValidOutputs.dispose(),{selectedIndices:i.selectedIndices,selectedScores:i.selectedScores}}function Kl(r,t,e,n,o,a,i,s){s===void 0&&(s=!1);for(var u=Array.from(t).map(function(b,w){return{score:b,boxIndex:w,suppressBeginIndex:0}}).filter(function(b){return b.score>o}).sort(mu),c=a>0?-.5/a:0,l=[],h=[];l.length<e&&u.length>0;){var f=u.pop(),d=f.score,p=f.boxIndex,v=f.suppressBeginIndex;if(d<o)break;for(var m=!1,g=l.length-1;g>=v;--g){var x=xv(r,p,l[g]);if(x>=n){m=!0;break}if(f.score=f.score*bv(n,c,x),f.score<=o)break}f.suppressBeginIndex=l.length,m||(f.score===d?(l.push(p),h.push(f.score)):f.score>o&&gv(u,f,mu))}var y=l.length;return s&&(l.fill(0,y),h.fill(0,y)),{selectedIndices:Me(l,"int32"),selectedScores:Me(h,"float32"),numValidOutputs:K(y,"int32")}}function xv(r,t,e){var n=r.subarray(4*t,4*t+4),o=r.subarray(4*e,4*e+4),a=Math.min(n[0],n[2]),i=Math.min(n[1],n[3]),s=Math.max(n[0],n[2]),u=Math.max(n[1],n[3]),c=Math.min(o[0],o[2]),l=Math.min(o[1],o[3]),h=Math.max(o[0],o[2]),f=Math.max(o[1],o[3]),d=(s-a)*(u-i),p=(h-c)*(f-l);if(d<=0||p<=0)return 0;var v=Math.max(a,c),m=Math.max(i,l),g=Math.min(s,h),x=Math.min(u,f),y=Math.max(g-v,0)*Math.max(x-m,0);return y/(d+p-y)}function bv(r,t,e){var n=Math.exp(t*e*e);return e<=r?n:0}function mu(r,t){return r.score-t.score||r.score===t.score&&t.boxIndex-r.boxIndex}function Xl(r,t,e){var n=new Array(r.rank).fill(0),o=r.shape.slice();return t.map(function(a){o[e]=a;var i=r.slice(n,o);return n[e]+=a,i})}function $l(r,t){for(var e=new Array(r.rank),n=0;n<e.length;n++)e[n]=r.shape[n]*t[n];var o=ue(e,r.dtype);for(n=0;n<o.values.length;++n){for(var a=o.indexToLoc(n),i=new Array(r.rank),s=0;s<i.length;s++)i[s]=a[s]%r.shape[s];var u=r.locToIndex(i);o.values[n]=r.values[u]}return o.toTensor()}function Yl(r,t,e,n,o){for(var a=t[t.length-1],i=[r.length/a,a],s=i[0],u=i[1],c=fr(e,s*n),l=fr("int32",s*n),h=0;h<s;h++){for(var f=h*u,d=r.subarray(f,f+u),p=[],v=0;v<d.length;v++)p.push({value:d[v],index:v});p.sort(function(b,w){return w.value-b.value});var m=h*n,g=c.subarray(m,m+n),x=l.subarray(m,m+n);for(v=0;v<n;v++)g[v]=p[v].value,x[v]=p[v].index}var y=t.slice();return y[y.length-1]=n,[$e(c,y,e),$e(l,y,"int32")]}function ls(r,t){for(var e=[],n=0;n<t.length;n++)t[n]&&e.push(n);var o=ue(r,"int32"),a=ue([e.length,r.length],"int32");for(n=0;n<e.length;n++){var i=o.indexToLoc(e[n]),s=n*r.length;a.values.set(i,s)}return a.toTensor()}var wv=function(r,t){this.outputShape=[],this.outputShape=r,this.variableNames=t.map(function(o,a){return"T"+a});var e=[];this.variableNames.forEach(function(o){e.push("float v"+o+" = get"+o+"AtOutCoords();")});var n=this.variableNames.map(function(o){return"v"+o}).join(" + ");this.userCode=`
      void main() {
        `+e.join(`
        `)+`

        float result = `+n+`;
        setOutput(result);
      }
    `},Cv=function(r,t){this.outputShape=[],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=r,this.variableNames=t.map(function(o,a){return"T"+a});var e=[];this.variableNames.forEach(function(o){e.push("vec4 v"+o+" = get"+o+"AtOutCoords();")});var n=this.variableNames.map(function(o){return"v"+o}).join(" + ");this.userCode=`
      void main() {
        `+e.join(`
        `)+`

        vec4 result = `+n+`;
        setOutput(result);
      }
    `},_v=function(r,t,e){this.variableNames=["A"];var n=r.windowSize,o=r.batchSize,a=r.inSize,i=Math.ceil(a/n);e||this.variableNames.push("bestIndicesA"),this.outputShape=[o,i];var s=t==="max"?">":"<",u=e?"inOffset + i;":"round(getBestIndicesA(batch, inOffset + i));";this.userCode=`
      void main() {
        ivec2 coords = getOutputCoords();
        int batch = coords[0];
        int outIdx = coords[1];
        int inOffset = outIdx * `+n+`;

        int bestIndex = inOffset;
        float bestValue = getA(batch, bestIndex);

        for (int i = 0; i < `+n+`; i++) {
          int inIdx = `+u+`;
          float candidate = getA(batch, inIdx);
          if (candidate `+s+` bestValue) {
            bestValue = candidate;
            bestIndex = inIdx;
          }
        }
        setOutput(float(bestIndex));
      }
    `};function Jl(r,t){return["x","y","z","w","u","v"].slice(0,t).map(function(e){return r+"."+e})}function ht(r,t){return t===1?[r]:Jl(r,t)}function Qe(){var r,t,e,n,o,a,i,s,u,c;return V().getNumber("WEBGL_VERSION")===2?(r="#version 300 es",t="in",e="out",n="in",o="texture",a="outputColor",i="out vec4 outputColor;",s=`
      bool isnan_custom(float val) {
        return (val > 0.0 || val < 0.0) ? false : val != 0.0;
      }

      bvec4 isnan_custom(vec4 val) {
        return bvec4(isnan_custom(val.x),
          isnan_custom(val.y), isnan_custom(val.z), isnan_custom(val.w));
      }

      #define isnan(value) isnan_custom(value)
    `,u="",c=`
      #define round(value) newRound(value)
      int newRound(float value) {
        return int(floor(value + 0.5));
      }

      ivec4 newRound(vec4 value) {
        return ivec4(floor(value + vec4(0.5)));
      }
    `):(r="",t="attribute",e="varying",n="varying",o="texture2D",a="gl_FragColor",i="",s=`
      #define isnan(value) isnan_custom(value)
      bool isnan_custom(float val) {
        return (val > 0. || val < 1. || val == 0.) ? false : true;
      }
      bvec4 isnan_custom(vec4 val) {
        return bvec4(isnan(val.x), isnan(val.y), isnan(val.z), isnan(val.w));
      }
    `,u=`
      uniform float INFINITY;

      bool isinf(float val) {
        return abs(val) == INFINITY;
      }
      bvec4 isinf(vec4 val) {
        return equal(abs(val), vec4(INFINITY));
      }
    `,c=`
      int round(float value) {
        return int(floor(value + 0.5));
      }

      ivec4 round(vec4 value) {
        return ivec4(floor(value + vec4(0.5)));
      }
    `),{version:r,attribute:t,varyingVs:e,varyingFs:n,texture2D:o,output:a,defineOutput:i,defineSpecialNaN:s,defineSpecialInf:u,defineRound:c}}function Mn(r,t,e){e===void 0&&(e="index");var n=Ft(t);return n.map(function(o,a){return"int "+r[a]+" = "+e+" / "+o+"; "+(a===n.length-1?"int "+r[a+1]+" = "+e+" - "+r[a]+" * "+o:"index -= "+r[a]+" * "+o)+";"}).join("")}function hs(r){var t=Ft(r).map(function(e){return e.toString()});return`
  int getFlatIndex(ivec3 coords) {
    return coords.x * `+t[0]+" + coords.y * "+t[1]+` + coords.z;
  }
`}var Ql=`
  const float FLOAT_MAX = 1.70141184e38;
  const float FLOAT_MIN = 1.17549435e-38;

  lowp vec4 encode_float(highp float v) {
    if (isnan(v)) {
      return vec4(255, 255, 255, 255);
    }

    highp float av = abs(v);

    if(av < FLOAT_MIN) {
      return vec4(0.0, 0.0, 0.0, 0.0);
    } else if(v > FLOAT_MAX) {
      return vec4(0.0, 0.0, 128.0, 127.0) / 255.0;
    } else if(v < -FLOAT_MAX) {
      return vec4(0.0, 0.0,  128.0, 255.0) / 255.0;
    }

    highp vec4 c = vec4(0,0,0,0);

    highp float e = floor(log2(av));
    highp float m = exp2(fract(log2(av))) - 1.0;

    c[2] = floor(128.0 * m);
    m -= c[2] / 128.0;
    c[1] = floor(32768.0 * m);
    m -= c[1] / 32768.0;
    c[0] = floor(8388608.0 * m);

    highp float ebias = e + 127.0;
    c[3] = floor(ebias / 2.0);
    ebias -= c[3] * 2.0;
    c[2] += floor(ebias) * 128.0;

    c[3] += 128.0 * step(0.0, -v);

    return c / 255.0;
  }
`;function Ev(r,t,e,n){var o=[];r.forEach(function(d){var p=ee(d.shapeInfo.logicalShape);d.shapeInfo.isUniform?o.push("uniform float "+d.name+(p>1?"["+p+"]":"")+";"):(o.push("uniform sampler2D "+d.name+";"),o.push("uniform int offset"+d.name+";"))});var a,i,s=o.join(`
`),u=r.map(function(d){return function(p,v,m){m===void 0&&(m=!1);var g="";g+=m?Zl(p):tr(p);var x=p.shapeInfo.logicalShape,y=v.logicalShape;return x.length<=y.length&&(g+=m?function(b,w){var C,I=b.name,E=I.charAt(0).toUpperCase()+I.slice(1),S="get"+E+"AtOutCoords",k=b.shapeInfo.logicalShape.length,T=w.logicalShape.length,A=an(b.shapeInfo.logicalShape,w.logicalShape),F=Se(T),P=T-k,B=["x","y","z","w","u","v"];C=k===0?"":T<2&&A.length>=1?"coords = 0;":A.map(function(q){return"coords."+B[q+P]+" = 0;"}).join(`
`);var U="";U=T<2&&k>0?"coords":b.shapeInfo.logicalShape.map(function(q,X){return"coords."+B[X+P]}).join(", ");var z="return outputValue;",L=ee(b.shapeInfo.logicalShape)===1,W=ee(w.logicalShape)===1;if(k!==1||L||W){if(L&&!W)z=T===1?`
        return vec4(outputValue.x, outputValue.x, 0., 0.);
      `:`
        return vec4(outputValue.x);
      `;else if(A.length){var G=k-2,j=k-1;A.indexOf(G)>-1&&A.indexOf(j)>-1?z="return vec4(outputValue.x);":A.indexOf(G)>-1?z="return vec4(outputValue.x, outputValue.y, outputValue.x, outputValue.y);":A.indexOf(j)>-1&&(z="return vec4(outputValue.xx, outputValue.zz);")}}else z=`
      return vec4(outputValue.xy, outputValue.xy);
    `;return`
    vec4 `+S+`() {
      `+F+` coords = getOutputCoords();
      `+C+`
      vec4 outputValue = get`+E+"("+U+`);
      `+z+`
    }
  `}(p,v):function(b,w){var C=b.name,I=C.charAt(0).toUpperCase()+C.slice(1),E="get"+I+"AtOutCoords",S=w.texShape,k=b.shapeInfo.texShape,T=b.shapeInfo.logicalShape.length,A=w.logicalShape.length;if(!b.shapeInfo.isUniform&&T===A&&b.shapeInfo.flatOffset==null&&Oe(k,S))return`
      float `+E+`() {
        return sampleTexture(`+C+`, resultUV);
      }
    `;var F,P=Se(A),B=an(b.shapeInfo.logicalShape,w.logicalShape),U=A-T,z=["x","y","z","w","u","v"];F=T===0?"":A<2&&B.length>=1?"coords = 0;":B.map(function(W){return"coords."+z[W+U]+" = 0;"}).join(`
`);var L="";return L=A<2&&T>0?"coords":b.shapeInfo.logicalShape.map(function(W,G){return"coords."+z[G+U]}).join(", "),`
    float `+E+`() {
      `+P+` coords = getOutputCoords();
      `+F+`
      return get`+I+"("+L+`);
    }
  `}(p,v)),g}(d,t,n)}).join(`
`),c=t.texShape,l=Qe(),h=function(d){return`
    float sampleTexture(sampler2D textureSampler, vec2 uv) {
      return `+d.texture2D+`(textureSampler, uv).r;
    }
  `}(l),f=function(d){return d.version+`
    precision highp float;
    precision highp int;
    precision highp sampler2D;
    `+d.varyingFs+` vec2 resultUV;
    `+d.defineOutput+`
    const vec2 halfCR = vec2(0.5, 0.5);

    struct ivec5
    {
      int x;
      int y;
      int z;
      int w;
      int u;
    };

    struct ivec6
    {
      int x;
      int y;
      int z;
      int w;
      int u;
      int v;
    };

    uniform float NAN;
    `+d.defineSpecialNaN+`
    `+d.defineSpecialInf+`
    `+d.defineRound+`

    int imod(int x, int y) {
      return x - y * (x / y);
    }

    int idiv(int a, int b, float sign) {
      int res = a / b;
      int mod = imod(a, b);
      if (sign < 0. && mod != 0) {
        res -= 1;
      }
      return res;
    }

    //Based on the work of Dave Hoskins
    //https://www.shadertoy.com/view/4djSRW
    #define HASHSCALE1 443.8975
    float random(float seed){
      vec2 p = resultUV * seed;
      vec3 p3  = fract(vec3(p.xyx) * HASHSCALE1);
      p3 += dot(p3, p3.yzx + 19.19);
      return fract((p3.x + p3.y) * p3.z);
    }

    `+Sv+`
    `+kv+`
    `+Rv+`
  `}(l);return t.isPacked?(a=function(d,p){switch(d.length){case 0:return`
    int getOutputCoords() {
      return 0;
    }
  `;case 1:return function(b,w){var C=[Math.ceil(w[0]/2),Math.ceil(w[1]/2)];return C[0]===1?`
      int getOutputCoords() {
        return 2 * int(resultUV.x * `+C[1]+`.0);
      }
    `:C[1]===1?`
      int getOutputCoords() {
        return 2 * int(resultUV.y * `+C[0]+`.0);
      }
    `:`
    int getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
                             vec2(`+C[0]+", "+C[1]+`));
      return 2 * (resTexRC.x * `+C[1]+` + resTexRC.y);
    }
  `}(0,p);case 2:return function(b,w){var C=[Math.ceil(w[0]/2),Math.ceil(w[1]/2)];if(Oe(b,w))return`
      ivec2 getOutputCoords() {
        return 2 * ivec2(resultUV.yx * vec2(`+C[0]+", "+C[1]+`));
      }
    `;var I=Math.ceil(b[1]/2);return`
    ivec2 getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
                             vec2(`+C[0]+", "+C[1]+`));

      int index = resTexRC.x * `+C[1]+` + resTexRC.y;
      int r = 2 * (index / `+I+`);
      int c = imod(index, `+I+`) * 2;

      return ivec2(r, c);
    }
  `}(d,p);case 3:return v=d,m=p,g=[Math.ceil(m[0]/2),Math.ceil(m[1]/2)],x=Math.ceil(v[2]/2),y=x*Math.ceil(v[1]/2),`
    ivec3 getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
                             vec2(`+g[0]+", "+g[1]+`));
      int index = resTexRC.x * `+g[1]+` + resTexRC.y;

      int b = index / `+y+`;
      index -= b * `+y+`;

      int r = 2 * (index / `+x+`);
      int c = imod(index, `+x+`) * 2;

      return ivec3(b, r, c);
    }
  `;default:return function(b,w){for(var C=[Math.ceil(w[0]/2),Math.ceil(w[1]/2)],I=Math.ceil(b[b.length-1]/2),E=I*Math.ceil(b[b.length-2]/2),S=E,k="",T="b, r, c",A=2;A<b.length-1;A++)S*=b[b.length-A-1],k=`
      int b`+A+" = index / "+S+`;
      index -= b`+A+" * "+S+`;
    `+k,T="b"+A+", "+T;return`
    ivec`+b.length+` getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
                             vec2(`+C[0]+", "+C[1]+`));
      int index = resTexRC.x * `+C[1]+` + resTexRC.y;

      `+k+`

      int b = index / `+E+`;
      index -= b * `+E+`;

      int r = 2 * (index / `+I+`);
      int c = imod(index, `+I+`) * 2;

      return ivec`+b.length+"("+T+`);
    }
  `}(d,p)}var v,m,g,x,y}(t.logicalShape,c),i=function(d){return`
    void setOutput(vec4 val) {
      `+d.output+` = val;
    }
  `}(l)):(a=function(d,p){switch(d.length){case 0:return`
    int getOutputCoords() {
      return 0;
    }
  `;case 1:return function(g,x){return x[0]===1?`
      int getOutputCoords() {
        return int(resultUV.x * `+x[1]+`.0);
      }
    `:x[1]===1?`
      int getOutputCoords() {
        return int(resultUV.y * `+x[0]+`.0);
      }
    `:`
    int getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
                             vec2(`+x[0]+", "+x[1]+`));
      return resTexRC.x * `+x[1]+` + resTexRC.y;
    }
  `}(0,p);case 2:return function(g,x){return Oe(g,x)?`
      ivec2 getOutputCoords() {
        return ivec2(resultUV.yx * vec2(`+x[0]+", "+x[1]+`));
      }
    `:g[1]===1?`
      ivec2 getOutputCoords() {
        ivec2 resTexRC = ivec2(resultUV.yx *
                               vec2(`+x[0]+", "+x[1]+`));
        int index = resTexRC.x * `+x[1]+` + resTexRC.y;
        return ivec2(index, 0);
      }
    `:g[0]===1?`
      ivec2 getOutputCoords() {
        ivec2 resTexRC = ivec2(resultUV.yx *
                               vec2(`+x[0]+", "+x[1]+`));
        int index = resTexRC.x * `+x[1]+` + resTexRC.y;
        return ivec2(0, index);
      }
    `:`
    ivec2 getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
                             vec2(`+x[0]+", "+x[1]+`));
      int index = resTexRC.x * `+x[1]+` + resTexRC.y;
      int r = index / `+g[1]+`;
      int c = index - r * `+g[1]+`;
      return ivec2(r, c);
    }
  `}(d,p);case 3:return v=p,m=Mn(["r","c","d"],d),`
    ivec3 getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
                             vec2(`+v[0]+", "+v[1]+`));
      int index = resTexRC.x * `+v[1]+` + resTexRC.y;
      `+m+`
      return ivec3(r, c, d);
    }
  `;case 4:return function(g,x){var y=Mn(["r","c","d","d2"],g);return`
    ivec4 getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
        vec2(`+x[0]+", "+x[1]+`));
      int index = resTexRC.x * `+x[1]+` + resTexRC.y;
      `+y+`
      return ivec4(r, c, d, d2);
    }
  `}(d,p);case 5:return function(g,x){var y=Mn(["r","c","d","d2","d3"],g);return`
    ivec5 getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx * vec2(`+x[0]+`,
                             `+x[1]+`));

      int index = resTexRC.x * `+x[1]+` + resTexRC.y;

      `+y+`

      ivec5 outShape = ivec5(r, c, d, d2, d3);
      return outShape;
    }
  `}(d,p);case 6:return function(g,x){var y=Mn(["r","c","d","d2","d3","d4"],g);return`
    ivec6 getOutputCoords() {
      ivec2 resTexRC = ivec2(resultUV.yx *
        vec2(`+x[0]+", "+x[1]+`));
      int index = resTexRC.x * `+x[1]+` + resTexRC.y;

      `+y+`

      ivec6 result = ivec6(r, c, d, d2, d3, d4);
      return result;
    }
  `}(d,p);default:throw new Error(d.length+"-D output sampling is not yet supported")}var v,m}(t.logicalShape,c),i=function(d){return`
    void setOutput(float val) {
      `+d.output+` = vec4(val, 0, 0, 0);
    }
  `}(l)),n&&(f+=Iv),[f,h,i,s,a,u,e].join(`
`)}function tr(r){var t=r.shapeInfo.logicalShape;switch(t.length){case 0:return function(e){var n=e.name,o="get"+n.charAt(0).toUpperCase()+n.slice(1);if(e.shapeInfo.isUniform)return"float "+o+"() {return "+n+";}";var a=e.shapeInfo.texShape,i=a[0],s=a[1];if(i===1&&s===1)return`
      float `+o+`() {
        return sampleTexture(`+n+`, halfCR);
      }
    `;var u=e.shapeInfo.texShape,c=u[0],l=u[1],h=Tn(n);return`
    float `+o+`() {
      vec2 uv = uvFromFlat(`+c+", "+l+", "+h+`);
      return sampleTexture(`+n+`, uv);
    }
  `}(r);case 1:return function(e){var n=e.name,o="get"+n.charAt(0).toUpperCase()+n.slice(1);if(e.shapeInfo.isUniform)return`
      float `+o+`(int index) {
        `+Jn(e)+`
      }
    `;var a=e.shapeInfo.texShape,i=a[0],s=a[1];if(s===1&&i===1)return`
      float `+o+`(int index) {
        return sampleTexture(`+n+`, halfCR);
      }
    `;var u=Tn(n);return s===1?`
      float `+o+`(int index) {
        vec2 uv = vec2(0.5, (float(index + `+u+") + 0.5) / "+i+`.0);
        return sampleTexture(`+n+`, uv);
      }
    `:i===1?`
      float `+o+`(int index) {
        vec2 uv = vec2((float(index + `+u+") + 0.5) / "+s+`.0, 0.5);
        return sampleTexture(`+n+`, uv);
      }
    `:`
    float `+o+`(int index) {
      vec2 uv = uvFromFlat(`+i+", "+s+", index + "+u+`);
      return sampleTexture(`+n+`, uv);
    }
  `}(r);case 2:return function(e){var n=e.shapeInfo.logicalShape,o=e.name,a="get"+o.charAt(0).toUpperCase()+o.slice(1),i=e.shapeInfo.texShape;if(i!=null&&Oe(n,i)){var s=i[0],u=i[1];return`
    float `+a+`(int row, int col) {
      vec2 uv = (vec2(col, row) + halfCR) / vec2(`+u+".0, "+s+`.0);
      return sampleTexture(`+o+`, uv);
    }
  `}var c=gn(n),l=c.newShape,h=c.keptDims,f=l;if(f.length<n.length){var d=nr(e,f);return`
      `+tr(d)+`
      float `+a+`(int row, int col) {
        return `+a+"("+rr(["row","col"],h)+`);
      }
    `}if(e.shapeInfo.isUniform)return`
      float `+a+`(int row, int col) {
        int index = round(dot(vec2(row, col), vec2(`+n[1]+`, 1)));
        `+Jn(e)+`
      }
    `;var p=i[0],v=i[1],m=Tn(o);return v===1?`
    float `+a+`(int row, int col) {
      float index = dot(vec3(row, col, `+m+"), vec3("+n[1]+`, 1, 1));
      vec2 uv = vec2(0.5, (index + 0.5) / `+p+`.0);
      return sampleTexture(`+o+`, uv);
    }
  `:p===1?`
    float `+a+`(int row, int col) {
      float index = dot(vec3(row, col, `+m+"), vec3("+n[1]+`, 1, 1));
      vec2 uv = vec2((index + 0.5) / `+v+`.0, 0.5);
      return sampleTexture(`+o+`, uv);
    }
  `:`
  float `+a+`(int row, int col) {
    // Explicitly use integer operations as dot() only works on floats.
    int index = row * `+n[1]+" + col + "+m+`;
    vec2 uv = uvFromFlat(`+p+", "+v+`, index);
    return sampleTexture(`+o+`, uv);
  }
`}(r);case 3:return function(e){var n=e.shapeInfo.logicalShape,o=e.name,a="get"+o.charAt(0).toUpperCase()+o.slice(1),i=n[1]*n[2],s=n[2],u=gn(n),c=u.newShape,l=u.keptDims,h=c;if(h.length<n.length){var f=nr(e,h);return`
        `+tr(f)+`
        float `+a+`(int row, int col, int depth) {
          return `+a+"("+rr(["row","col","depth"],l)+`);
        }
      `}if(e.shapeInfo.isUniform)return`
      float `+a+`(int row, int col, int depth) {
        int index = round(dot(vec3(row, col, depth),
                          vec3(`+i+", "+s+`, 1)));
        `+Jn(e)+`
      }
    `;var d=e.shapeInfo.texShape,p=d[0],v=d[1],m=e.shapeInfo.flatOffset;if(v===i&&m==null)return`
        float `+a+`(int row, int col, int depth) {
          float texR = float(row);
          float texC = dot(vec2(col, depth), vec2(`+s+`, 1));
          vec2 uv = (vec2(texC, texR) + halfCR) /
                     vec2(`+v+".0, "+p+`.0);
          return sampleTexture(`+o+`, uv);
        }
      `;if(v===s&&m==null)return`
    float `+a+`(int row, int col, int depth) {
      float texR = dot(vec2(row, col), vec2(`+n[1]+`, 1));
      float texC = float(depth);
      vec2 uv = (vec2(texC, texR) + halfCR) / vec2(`+v+".0, "+p+`.0);
      return sampleTexture(`+o+`, uv);
    }
  `;var g=Tn(o);return`
      float `+a+`(int row, int col, int depth) {
        // Explicitly use integer operations as dot() only works on floats.
        int index = row * `+i+" + col * "+s+" + depth + "+g+`;
        vec2 uv = uvFromFlat(`+p+", "+v+`, index);
        return sampleTexture(`+o+`, uv);
      }
  `}(r);case 4:return function(e){var n=e.shapeInfo.logicalShape,o=e.name,a="get"+o.charAt(0).toUpperCase()+o.slice(1),i=n[3],s=n[2]*i,u=n[1]*s,c=gn(n),l=c.newShape,h=c.keptDims;if(l.length<n.length){var f=nr(e,l);return`
      `+tr(f)+`
      float `+a+`(int row, int col, int depth, int depth2) {
        return `+a+"("+rr(["row","col","depth","depth2"],h)+`);
      }
    `}if(e.shapeInfo.isUniform)return`
      float `+a+`(int row, int col, int depth, int depth2) {
        int index = round(dot(vec4(row, col, depth, depth2),
                          vec4(`+u+", "+s+", "+i+`, 1)));
        `+Jn(e)+`
      }
    `;var d=e.shapeInfo.flatOffset,p=e.shapeInfo.texShape,v=p[0],m=p[1];if(m===u&&d==null)return`
      float `+a+`(int row, int col, int depth, int depth2) {
        float texR = float(row);
        float texC =
            dot(vec3(col, depth, depth2),
                vec3(`+s+", "+i+`, 1));
        vec2 uv = (vec2(texC, texR) + halfCR) /
                   vec2(`+m+".0, "+v+`.0);
        return sampleTexture(`+o+`, uv);
      }
    `;if(m===i&&d==null)return`
      float `+a+`(int row, int col, int depth, int depth2) {
        float texR = dot(vec3(row, col, depth),
                         vec3(`+n[1]*n[2]+", "+n[2]+`, 1));
        float texC = float(depth2);
        vec2 uv = (vec2(texC, texR) + halfCR) /
                  vec2(`+m+".0, "+v+`.0);
        return sampleTexture(`+o+`, uv);
      }
    `;var g=Tn(o);return`
    float `+a+`(int row, int col, int depth, int depth2) {
      // Explicitly use integer operations as dot() only works on floats.
      int index = row * `+u+" + col * "+s+` +
          depth * `+i+` + depth2;
      vec2 uv = uvFromFlat(`+v+", "+m+", index + "+g+`);
      return sampleTexture(`+o+`, uv);
    }
  `}(r);case 5:return function(e){var n=e.shapeInfo.logicalShape,o=e.name,a="get"+o.charAt(0).toUpperCase()+o.slice(1),i=n[4],s=n[3]*i,u=n[2]*s,c=n[1]*u,l=gn(n),h=l.newShape,f=l.keptDims;if(h.length<n.length){var d=nr(e,h);return`
      `+tr(d)+`
      float `+a+`(int row, int col, int depth, int depth2, int depth3) {
        return `+a+"("+rr(["row","col","depth","depth2","depth3"],f)+`);
      }
    `}if(e.shapeInfo.isUniform)return`
      float `+a+`(int row, int col, int depth, int depth2, int depth3) {
        float index = dot(
          vec4(row, col, depth, depth2),
          vec4(`+c+", "+u+", "+s+", "+i+`)) +
          depth3;
        `+Jn(e)+`
      }
    `;var p=e.shapeInfo.flatOffset,v=e.shapeInfo.texShape,m=v[0],g=v[1];if(g===c&&p==null)return`
      float `+a+`(int row, int col, int depth, int depth2, int depth3) {
        int texR = row;
        float texC = dot(vec4(col, depth, depth2, depth3),
                         vec4(`+u+", "+s+", "+i+`, 1));
        vec2 uv = (vec2(texC, texR) + halfCR) /
                   vec2(`+g+".0, "+m+`.0);
        return sampleTexture(`+o+`, uv);
      }
    `;if(g===i&&p==null)return`
      float `+a+`(int row, int col, int depth, int depth2, int depth3) {
        float texR = dot(
          vec4(row, col, depth, depth2),
          vec4(`+n[1]*n[2]*n[3]+`,
               `+n[2]*n[3]+", "+n[3]+`, 1));
        int texC = depth3;
        vec2 uv = (vec2(texC, texR) + halfCR) /
                  vec2(`+g+".0, "+m+`.0);
        return sampleTexture(`+o+`, uv);
      }
    `;var x=Tn(o);return`
    float `+a+`(int row, int col, int depth, int depth2, int depth3) {
      // Explicitly use integer operations as dot() only works on floats.
      int index = row * `+c+" + col * "+u+" + depth * "+s+` +
          depth2 * `+i+" + depth3 + "+x+`;
      vec2 uv = uvFromFlat(`+m+", "+g+`, index);
      return sampleTexture(`+o+`, uv);
    }
  `}(r);case 6:return function(e){var n=e.shapeInfo.logicalShape,o=e.name,a="get"+o.charAt(0).toUpperCase()+o.slice(1),i=gn(n),s=i.newShape,u=i.keptDims;if(s.length<n.length){var c=nr(e,s);return`
      `+tr(c)+`
      float `+a+`(int row, int col, int depth,
                    int depth2, int depth3, int depth4) {
        return `+a+"("+rr(["row","col","depth","depth2","depth3","depth4"],u)+`);
      }
    `}var l=n[5],h=n[4]*l,f=n[3]*h,d=n[2]*f,p=n[1]*d;if(e.shapeInfo.isUniform)return`
      float `+a+`(int row, int col, int depth,
                  int depth2, int depth3, int depth4) {
        int index = round(dot(
          vec4(row, col, depth, depth2),
          vec4(`+p+", "+d+", "+f+", "+h+`)) +
          dot(
            vec2(depth3, depth4),
            vec2(`+l+`, 1)));
        `+Jn(e)+`
      }
    `;var v=e.shapeInfo.flatOffset,m=e.shapeInfo.texShape,g=m[0],x=m[1];if(x===p&&v==null)return`
      float `+a+`(int row, int col, int depth,
                    int depth2, int depth3, int depth4) {
        int texR = row;
        float texC = dot(vec4(col, depth, depth2, depth3),
          vec4(`+d+", "+f+", "+h+", "+l+`)) +
               float(depth4);
        vec2 uv = (vec2(texC, texR) + halfCR) /
                   vec2(`+x+".0, "+g+`.0);
        return sampleTexture(`+o+`, uv);
      }
    `;if(x===l&&v==null)return`
      float `+a+`(int row, int col, int depth,
                    int depth2, int depth3, int depth4) {
        float texR = dot(vec4(row, col, depth, depth2),
          vec4(`+n[1]*n[2]*n[3]*n[4]+`,
               `+n[2]*n[3]*n[4]+`,
               `+n[3]*n[4]+`,
               `+n[4]+`)) + float(depth3);
        int texC = depth4;
        vec2 uv = (vec2(texC, texR) + halfCR) /
                  vec2(`+x+".0, "+g+`.0);
        return sampleTexture(`+o+`, uv);
      }
    `;var y=Tn(o);return`
    float `+a+`(int row, int col, int depth,
                  int depth2, int depth3, int depth4) {
      // Explicitly use integer operations as dot() only works on floats.
      int index = row * `+p+" + col * "+d+" + depth * "+f+` +
          depth2 * `+h+" + depth3 * "+l+" + depth4 + "+y+`;
      vec2 uv = uvFromFlat(`+g+", "+x+`, index);
      return sampleTexture(`+o+`, uv);
    }
  `}(r);default:throw new Error(t.length+"-D input sampling is not yet supported")}}function Zl(r){var t,e,n;switch(r.shapeInfo.logicalShape.length){case 0:return t=r.name,e="get"+t.charAt(0).toUpperCase()+t.slice(1),n=Qe(),`
    vec4 `+e+`() {
      return `+n.texture2D+"("+t+`, halfCR);
    }
  `;case 1:return function(o){var a=o.name,i="get"+a.charAt(0).toUpperCase()+a.slice(1),s=o.shapeInfo.texShape,u=[Math.ceil(s[0]/2),Math.ceil(s[1]/2)],c=Qe();return`
    vec4 `+i+`(int index) {
      vec2 uv = packedUVfrom1D(
        `+u[0]+", "+u[1]+`, index);
      return `+c.texture2D+"("+a+`, uv);
    }
  `}(r);case 2:return function(o){var a=o.shapeInfo.logicalShape,i=o.name,s="get"+i.charAt(0).toUpperCase()+i.slice(1),u=o.shapeInfo.texShape,c=u[0],l=u[1],h=Qe();if(u!=null&&Oe(a,u))return`
      vec4 `+s+`(int row, int col) {
        vec2 uv = (vec2(col, row) + halfCR) / vec2(`+l+".0, "+c+`.0);

        return `+h.texture2D+"("+i+`, uv);
      }
    `;var f=[Math.ceil(u[0]/2),Math.ceil(u[1]/2)],d=Math.ceil(a[1]/2);return`
    vec4 `+s+`(int row, int col) {
      vec2 uv = packedUVfrom2D(`+d+", "+f[0]+", "+f[1]+`, row, col);
      return `+h.texture2D+"("+i+`, uv);
    }
  `}(r);case 3:return function(o){var a=o.shapeInfo.logicalShape,i=o.name,s="get"+i.charAt(0).toUpperCase()+i.slice(1),u=o.shapeInfo.texShape,c=[Math.ceil(u[0]/2),Math.ceil(u[1]/2)];if(a[0]===1){var l=a.slice(1),h=nr(o,l);return`
        `+Zl(h)+`
        vec4 `+s+`(int b, int row, int col) {
          return `+s+"("+rr(["b","row","col"],[1,2])+`);
        }
      `}var f=c[0],d=c[1],p=Math.ceil(a[2]/2),v=p*Math.ceil(a[1]/2),m=Qe();return`
    vec4 `+s+`(int b, int row, int col) {
      vec2 uv = packedUVfrom3D(
        `+f+", "+d+", "+v+", "+p+`, b, row, col);
      return `+m.texture2D+"("+i+`, uv);
    }
  `}(r);default:return function(o){for(var a=o.shapeInfo.logicalShape,i=a.length,s=o.name,u="get"+s.charAt(0).toUpperCase()+s.slice(1),c=o.shapeInfo.texShape,l=[Math.ceil(c[0]/2),Math.ceil(c[1]/2)],h=l[0],f=l[1],d=Math.ceil(a[i-1]/2),p=d*Math.ceil(a[i-2]/2),v="int b, int row, int col",m="b * "+p+" + (row / 2) * "+d+" + (col / 2)",g=2;g<i-1;g++)v="int b"+g+", "+v,p*=a[i-g-1],m="b"+g+" * "+p+" + "+m;var x=Qe();return`
    vec4 `+u+"("+v+`) {
      int index = `+m+`;
      int texR = index / `+f+`;
      int texC = index - texR * `+f+`;
      vec2 uv = (vec2(texC, texR) + halfCR) / vec2(`+f+", "+h+`);
      return `+x.texture2D+"("+s+`, uv);
    }
  `}(r)}}var Sv=`
vec2 uvFromFlat(int texNumR, int texNumC, int index) {
  int texR = index / texNumC;
  int texC = index - texR * texNumC;
  return (vec2(texC, texR) + halfCR) / vec2(texNumC, texNumR);
}
vec2 packedUVfrom1D(int texNumR, int texNumC, int index) {
  int texelIndex = index / 2;
  int texR = texelIndex / texNumC;
  int texC = texelIndex - texR * texNumC;
  return (vec2(texC, texR) + halfCR) / vec2(texNumC, texNumR);
}
`,kv=`
vec2 packedUVfrom2D(int texelsInLogicalRow, int texNumR,
  int texNumC, int row, int col) {
  int texelIndex = (row / 2) * texelsInLogicalRow + (col / 2);
  int texR = texelIndex / texNumC;
  int texC = texelIndex - texR * texNumC;
  return (vec2(texC, texR) + halfCR) / vec2(texNumC, texNumR);
}
`,Rv=`
vec2 packedUVfrom3D(int texNumR, int texNumC,
    int texelsInBatch, int texelsInLogicalRow, int b,
    int row, int col) {
  int index = b * texelsInBatch + (row / 2) * texelsInLogicalRow + (col / 2);
  int texR = index / texNumC;
  int texC = index - texR * texNumC;
  return (vec2(texC, texR) + halfCR) / vec2(texNumC, texNumR);
}
`,Iv=`
  float getChannel(vec4 frag, vec2 innerDims) {
    vec2 modCoord = mod(innerDims, 2.);
    return modCoord.x == 0. ?
      (modCoord.y == 0. ? frag.r : frag.g) :
      (modCoord.y == 0. ? frag.b : frag.a);
  }
  float getChannel(vec4 frag, int dim) {
    float modCoord = mod(float(dim), 2.);
    return modCoord == 0. ? frag.r : frag.g;
  }
`;function Tn(r){return"offset"+r}function Jn(r){var t=r.name,e=ee(r.shapeInfo.logicalShape);return e<2?"return "+t+";":`
    for (int i = 0; i < `+e+`; i++) {
      if (i == index) {
        return `+t+`[i];
      }
    }
  `}function Se(r){if(r<=1)return"int";if(r===2)return"ivec2";if(r===3)return"ivec3";if(r===4)return"ivec4";if(r===5)return"ivec5";if(r===6)return"ivec6";throw Error("GPU for rank "+r+" is not yet supported")}function nr(r,t){var e=JSON.parse(JSON.stringify(r));return e.shapeInfo.logicalShape=t,e}function rr(r,t){return t.map(function(e){return r[e]}).join(", ")}var Av=function(r,t,e,n){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!0,R(r.length>2,function(){return"Packed arg"+(e.charAt(0).toUpperCase()+e.slice(1))+" supports only inputs with rank above 2."});var o=r[r.length-1],a=Math.ceil(o/t);this.outputShape=r.slice(0,-1),a>1&&this.outputShape.push(a),n||this.variableNames.push("bestIndicesA");var i,s,u=this.outputShape,c=u.length,l=Se(c),h=ht("coords",c);if(a===1){var f=Se(s=c+1);i=`
        `+f+" sourceLocR = "+f+"("+h.join()+`, 0);
        ++`+h[c-1]+`;
        `+f+" sourceLocG = "+f+"("+h.join()+`, 0);
        ++`+h[c-2]+`;
        `+f+" sourceLocA = "+f+"("+h.join()+`, 0);
        --`+h[c-1]+`;
        `+f+" sourceLocB = "+f+"("+h.join()+`, 0);
        --`+h[c-2]+";"}else s=c,i=`
        `+l+` sourceLocR = coords;
        ++`+h[c-1]+`;
        `+l+` sourceLocG = coords;
        ++`+h[c-2]+`;
        `+l+` sourceLocA = coords;
        --`+h[c-1]+`;
        `+l+` sourceLocB = coords;
        --`+h[c-2]+";";var d=["x","y","z","w","u","v"].slice(0,s),p="."+d[s-1],v=d.map(function(E){return"int "+E}),m=ht("sourceLocR",s-1).concat("inIdx.r"),g=ht("sourceLocG",s-1).concat("inIdx.g"),x=ht("sourceLocB",s-1).concat("inIdx.b"),y=ht("sourceLocA",s-1).concat("inIdx.a"),b=e==="max"?"greaterThan":"lessThan",w=n?"":`
          inIdx = round(vec4(getBestIndicesAChannel(`+m.join()+`),
                             getBestIndicesAChannel(`+g.join()+`),
                             getBestIndicesAChannel(`+x.join()+`),
                             getBestIndicesAChannel(`+y.join()+")));",C=`vec4(
            getAChannel(`+m.join()+`),
            hasNextCol ? getAChannel(`+g.join()+`) : 0.,
            hasNextRow ? getAChannel(`+x.join()+`) : 0.,
            hasNextRow && hasNextCol ? getAChannel(`+y.join()+") : 0.)",I=n?"":`
      float getBestIndicesAChannel(`+v.join()+`) {
        return getChannel(getBestIndicesA(`+d.join()+`),
                                          vec2(`+d.slice(-2).join()+`));
      }`;this.userCode=`
      float getAChannel(`+v.join()+`) {
        return getChannel(getA(`+d.join()+`),
                               vec2(`+d.slice(-2).join()+`));
      }
      `+I+`
      void main() {
        `+l+` coords = getOutputCoords();
        bool hasNextCol = `+h[c-1]+" < "+(u[c-1]-1)+`;
        bool hasNextRow = `+h[c-2]+" < "+(u[c-2]-1)+`;
        `+i+`
        ivec4 srcIdx = ivec4(sourceLocR`+p+", sourceLocG"+p+`,
          sourceLocB`+p+", sourceLocA"+p+") * "+t+`;
        ivec4 inIdx = srcIdx;
        vec4 bestIndex = vec4(inIdx);
        vec4 bestValue = `+C+`;

        for (int i = 0; i < `+t+`; i++) {
          inIdx = srcIdx;
          `+w+`
          vec4 candidate = `+C+`;
          bvec4 nan = isnan(candidate);
          bvec4 replace = bvec4(
            vec4(`+b+`(candidate, bestValue)) * (vec4(1.0) - vec4(nan)));

          bestValue = vec4(replace.x  ? candidate.x : bestValue.x,
                           replace.y  ? candidate.y : bestValue.y,
                           replace.z  ? candidate.z : bestValue.z,
                           replace.w  ? candidate.w : bestValue.w);
          bestIndex = mix(bestIndex, vec4(inIdx), vec4(replace));
          srcIdx++;
        }
        setOutput(bestIndex);
      }
    `},Dv=function(r){this.variableNames=["dy"],this.outputShape=r.inShape;var t=r.filterHeight,e=r.filterWidth,n=r.strideHeight,o=r.strideWidth,a=r.dilationHeight,i=r.dilationWidth,s=r.effectiveFilterHeight,u=r.effectiveFilterWidth,c=s-1-r.padInfo.top,l=u-1-r.padInfo.left,h=1/(t*e);this.userCode=`
      const ivec2 pads = ivec2(`+c+", "+l+`);
      const float avgMultiplier = float(`+h+`);

      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int d = coords[3];

        ivec2 dyRCCorner = coords.yz - pads;
        int dyRCorner = dyRCCorner.x;
        int dyCCorner = dyRCCorner.y;

        // Convolve dy(?, ?, d) with pos mask(:, :, d) to get dx(xR, xC, d).
        // ? = to be determined. : = across all values in that axis.
        float dotProd = 0.0;
        for (int wR = 0; wR < `+s+`;
            wR += `+a+`) {
          float dyR = float(dyRCorner + wR) / `+n+`.0;

          if (dyR < 0.0 || dyR >= `+r.outHeight+`.0 || fract(dyR) > 0.0) {
            continue;
          }
          int idyR = int(dyR);

          for (int wC = 0; wC < `+u+`;
            wC+= `+i+`) {
            float dyC = float(dyCCorner + wC) / `+o+`.0;

            if (dyC < 0.0 || dyC >= `+r.outWidth+`.0 ||
                fract(dyC) > 0.0) {
              continue;
            }
            int idyC = int(dyC);

            float dyValue = getDy(b, idyR, idyC, d);

            dotProd += dyValue * avgMultiplier;
          }
        }
        setOutput(dotProd);
      }
    `},Tv=function(r){this.variableNames=["dy"],this.outputShape=r.inShape;var t=r.filterDepth,e=r.filterHeight,n=r.filterWidth,o=r.strideDepth,a=r.strideHeight,i=r.strideWidth,s=r.dilationDepth,u=r.dilationHeight,c=r.dilationWidth,l=r.effectiveFilterDepth,h=r.effectiveFilterHeight,f=r.effectiveFilterWidth,d=l-1-r.padInfo.front,p=h-1-r.padInfo.top,v=f-1-r.padInfo.left,m=1/(t*e*n);this.userCode=`
      const ivec3 pads = ivec3(`+d+", "+p+", "+v+`);
      const float avgMultiplier = float(`+m+`);

      void main() {
        ivec5 coords = getOutputCoords();
        int batch = coords.x;
        int ch = coords.u;

        ivec3 dyCorner = ivec3(coords.y, coords.z, coords.w) - pads;
        int dyDCorner = dyCorner.x;
        int dyRCorner = dyCorner.y;
        int dyCCorner = dyCorner.z;

        // Convolve dy(?, ?, ?, d) with pos mask(:, :, :, ch) to get
        // dx(xD, xR, xC, ch).
        // ? = to be determined. : = across all values in that axis.
        float dotProd = 0.0;

        for (int wD = 0; wD < `+l+`;
            wD += `+s+`) {
          float dyD = float(dyDCorner + wD) / `+o+`.0;

          if (dyD < 0.0 || dyD >= `+r.outDepth+`.0 || fract(dyD) > 0.0) {
            continue;
          }
          int idyD = int(dyD);

          for (int wR = 0; wR < `+h+`;
              wR += `+u+`) {
            float dyR = float(dyRCorner + wR) / `+a+`.0;

            if (dyR < 0.0 || dyR >= `+r.outHeight+`.0 ||
                fract(dyR) > 0.0) {
              continue;
            }
            int idyR = int(dyR);

            for (int wC = 0; wC < `+f+`;
                wC += `+c+`) {
              float dyC = float(dyCCorner + wC) / `+i+`.0;

              if (dyC < 0.0 || dyC >= `+r.outWidth+`.0 ||
                  fract(dyC) > 0.0) {
                continue;
              }
              int idyC = int(dyC);

              float dyValue = getDy(batch, idyD, idyR, idyC, ch);

              dotProd += dyValue * avgMultiplier;
            }
          }
        }
        setOutput(dotProd);
      }
    `},Nv=function(r,t,e,n,o,a){this.outputShape=[],this.variableNames=["x","mean","variance"],ve(r,t),ve(r,e);var i="0.0";n!=null&&(ve(r,n),this.variableNames.push("offset"),i="getOffsetAtOutCoords()");var s="1.0";o!=null&&(ve(r,o),this.variableNames.push("scale"),s="getScaleAtOutCoords()"),this.outputShape=r,this.userCode=`
      void main() {
        float x = getXAtOutCoords();
        float mean = getMeanAtOutCoords();
        float variance = getVarianceAtOutCoords();
        float offset = `+i+`;
        float scale = `+s+`;
        float inv = scale * inversesqrt(variance + float(`+a+`));
        setOutput(dot(vec3(x, -mean, offset), vec3(inv, inv, 1)));
      }
    `},Fv=function(r,t,e,n,o,a){this.packedInputs=!0,this.packedOutput=!0,this.variableNames=["x","mean","variance"],ve(r,t),ve(r,e);var i="vec4(0.0)";n!=null&&(ve(r,n),this.variableNames.push("offset"),i="getOffsetAtOutCoords()");var s="vec4(1.0)";o!=null&&(ve(r,o),this.variableNames.push("scale"),s="getScaleAtOutCoords()"),this.outputShape=r,this.userCode=`
      void main() {
        vec4 offset = `+i+`;
        vec4 scale = `+s+`;

        vec4 x = getXAtOutCoords();
        vec4 mean = getMeanAtOutCoords();
        vec4 variance = getVarianceAtOutCoords();

        vec4 inv = scale * inversesqrt(variance + vec4(`+a+`));

        setOutput((x - mean) * inv + offset);
      }
    `},Pv="return areal * breal - aimag * bimag;",Mv="return areal * bimag + aimag * breal;",gu=function(r,t,e){this.variableNames=["AReal","AImag","BReal","BImag"],this.outputShape=ve(t,e),this.userCode=`
      float binaryOpComplex(
          float areal, float aimag, float breal, float bimag) {
        `+r+`
      }

      void main() {
        float areal = getARealAtOutCoords();
        float aimag = getAImagAtOutCoords();
        float breal = getBRealAtOutCoords();
        float bimag = getBImagAtOutCoords();
        setOutput(binaryOpComplex(areal, aimag, breal, bimag));
      }
    `},Ma="return a + b;",Oa="return a - b;",yu="return a * b;",eh="return (a < 0.) ? b * a : a;",Pe=function(r,t,e){this.variableNames=["A","B"],this.outputShape=ve(t,e),this.userCode=`
      float binaryOperation(float a, float b) {
        `+r+`
      }

      void main() {
        float a = getAAtOutCoords();
        float b = getBAtOutCoords();
        setOutput(binaryOperation(a, b));
      }
    `},th=`
  vec4 aLessThanZero = vec4(lessThan(a, vec4(0.)));
  return (aLessThanZero * (b * a)) + ((vec4(1.0) - aLessThanZero) * a);
`,rn=function(r,t,e,n){n===void 0&&(n=!1),this.variableNames=["A","B"],this.supportsBroadcasting=!0,this.packedInputs=!0,this.packedOutput=!0,this.outputShape=ve(t,e);var o=this.outputShape.length,a="";if(n)if(o===0||ee(this.outputShape)===1)a=`
          result.y = 0.;
          result.z = 0.;
          result.w = 0.;
        `;else if(a=`
          `+Se(o)+` coords = getOutputCoords();
        `,o===1)a+=`
            result.y = (coords + 1) >= `+this.outputShape[0]+` ? 0. : result.y;
            result.z = 0.;
            result.w = 0.;
          `;else{var i=ht("coords",o);a+=`
            bool nextRowOutOfBounds =
              (`+i[o-2]+" + 1) >= "+this.outputShape[o-2]+`;
            bool nextColOutOfBounds =
              (`+i[o-1]+" + 1) >= "+this.outputShape[o-1]+`;
            result.y = nextColOutOfBounds ? 0. : result.y;
            result.z = nextRowOutOfBounds ? 0. : result.z;
            result.w = nextColOutOfBounds || nextRowOutOfBounds ? 0. : result.w;
          `}this.userCode=`
      vec4 binaryOperation(vec4 a, vec4 b) {
        `+r+`
      }

      void main() {
        vec4 a = getAAtOutCoords();
        vec4 b = getBAtOutCoords();

        vec4 result = binaryOperation(a, b);
        `+a+`

        setOutput(result);
      }
    `},Ov=function(){function r(t){this.variableNames=["A"],this.outputShape=t,this.userCode=`
      uniform float minVal;
      uniform float maxVal;

      void main() {
        float value = getAAtOutCoords();
        if (isnan(value)) {
          setOutput(value);
          return;
        }

        setOutput(clamp(value, minVal, maxVal));
      }
    `}return r.prototype.getCustomSetupFunc=function(t,e){var n=this;return function(o,a){n.minLoc==null&&(n.minLoc=o.getUniformLocationNoThrow(a,"minVal"),n.maxLoc=o.getUniformLocationNoThrow(a,"maxVal")),o.gl.uniform1f(n.minLoc,t),o.gl.uniform1f(n.maxLoc,e)}},r}(),Bv=function(){function r(t){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=t,this.userCode=`
      uniform float minVal;
      uniform float maxVal;

      void main() {
        vec4 value = getAAtOutCoords();

        if (any(isnan(value))) {
          setOutput(value);
          return;
        }

        setOutput(clamp(value, vec4(minVal), vec4(maxVal)));
      }
    `}return r.prototype.getCustomSetupFunc=function(t,e){var n=this;return function(o,a){n.minLoc==null&&(n.minLoc=o.getUniformLocationNoThrow(a,"minVal"),n.maxLoc=o.getUniformLocationNoThrow(a,"maxVal")),o.gl.uniform1f(n.minLoc,t),o.gl.uniform1f(n.maxLoc,e)}},r}(),Lv=function(r){this.variableNames=["real","imag"],this.outputShape=r,this.userCode=`
      void main() {
        float re = abs(getRealAtOutCoords());
        float im = abs(getImagAtOutCoords());
        float mx = max(re, im);

        // sadly the length function in glsl is not underflow-safe
        // (at least not on Intel GPUs). So the safe solution is
        // to ensure underflow-safety in all cases.
        setOutput(
          mx == 0.0 ? 0.0 : mx * length(vec2(1, min(re, im)/mx))
        );
      }
    `},Wv=function(r){this.outputShape=[],this.outputShape=Vn(r,1),this.variableNames=r.map(function(s,u){return"T"+u});var t=new Array(r.length-1);t[0]=r[0][1];for(var e=1;e<t.length;e++)t[e]=t[e-1]+r[e][1];var n=["if (yC < "+t[0]+") setOutput(getT0(yR, yC));"];for(e=1;e<t.length;e++){var o=t[e-1];n.push("else if (yC < "+t[e]+") setOutput(getT"+e+"(yR, yC-"+o+"));")}var a=t.length,i=t[t.length-1];n.push("else setOutput(getT"+a+"(yR, yC-"+i+"));"),this.userCode=`
      void main() {
        ivec2 coords = getOutputCoords();
        int yR = coords.x;
        int yC = coords.y;

        `+n.join(`
        `)+`
      }
    `},Vv=function(r,t){this.packedInputs=!0,this.packedOutput=!0,this.outputShape=[],this.outputShape=Vn(r,t);var e=this.outputShape,n=e.length,o=Se(n),a=ht("coords",n),i=["x","y","z","w","u","v"].slice(0,n);this.variableNames=r.map(function(m,g){return"T"+g});var s=new Array(r.length-1);s[0]=r[0][t];for(var u=1;u<s.length;u++)s[u]=s[u-1]+r[u][t];var c=i[t],l=i.slice(-2),h=i.join(),f="if ("+c+" < "+s[0]+`) {
        return getChannel(
            getT0(`+h+"), vec2("+l.join()+`));
        }`;for(u=1;u<s.length;u++){var d=s[u-1];f+=`
        if (`+c+" < "+s[u]+"  && "+c+" >= "+s[u-1]+`) {
          return getChannel(
            getT`+u+"("+uo(i,c,d)+`),
            vec2(`+uo(l,c,d)+`));
        }`}var p=s.length,v=s[s.length-1];f+=`
        return getChannel(
          getT`+p+"("+uo(i,c,v)+`),
          vec2(`+uo(l,c,v)+"));",this.userCode=`
      float getValue(`+i.map(function(m){return"int "+m})+`) {
        `+f+`
      }

      void main() {
        `+o+` coords = getOutputCoords();
        vec4 result = vec4(getValue(`+a+`), 0., 0., 0.);

        `+a[n-1]+" = "+a[n-1]+` + 1;
        if (`+a[n-1]+" < "+e[n-1]+`) {
          result.g = getValue(`+a+`);
        }

        `+a[n-2]+" = "+a[n-2]+` + 1;
        if (`+a[n-2]+" < "+e[n-2]+`) {
          result.a = getValue(`+a+`);
        }

        `+a[n-1]+" = "+a[n-1]+` - 1;
        if (`+a[n-2]+" < "+e[n-2]+` &&
            `+a[n-1]+" < "+e[n-1]+`) {
          result.b = getValue(`+a+`);
        }
        setOutput(result);
      }
    `};function uo(r,t,e){var n=r.indexOf(t);return r.map(function(o,a){return a===n?o+" - "+e:o}).join()}var zv=function(r){this.variableNames=["x","dy"],this.outputShape=r.filterShape;var t=r.strideHeight,e=r.strideWidth,n=r.padInfo.top,o=r.padInfo.left,a=r.dataFormat==="channelsLast";this.userCode=`
      void main() {
        ivec4 coords = getOutputCoords();
        int wR = coords.x;
        int wC = coords.y;
        int d1 = coords.z;
        int d2 = coords.w;

        // Convolve x(?, ?, d1) with dy(:, :, d2) to get dw(wR, wC, d1, d2).
        // ? = to be determined. : = across all values in that axis.
        float dotProd = 0.0;

        for (int b = 0; b < `+r.batchSize+`; b++) {
          for (int yR = 0; yR < `+r.outHeight+`; yR++) {
            int xR = wR + yR * `+t+" - "+n+`;

            if (xR < 0 || xR >= `+r.inHeight+`) {
              continue;
            }

            for (int yC = 0; yC < `+r.outWidth+`; yC++) {
              int xC = wC + yC * `+e+" - "+o+`;

              if (xC < 0 || xC >= `+r.inWidth+`) {
                continue;
              }

              if (`+a+`) {
                float dyValue = getDy(b, yR, yC, d2);
                float xValue = getX(b, xR, xC, d1);
                dotProd += (xValue * dyValue);
              } else {
                float dyValue = getDy(b, d2, yR, yC);
                float xValue = getX(b, d1, xR, xC);
                dotProd += (xValue * dyValue);
              }

            }
          }
        }
        setOutput(dotProd);
      }
    `},Uv=function(r){this.variableNames=["dy","W"],this.outputShape=r.inShape;var t=r.filterHeight,e=r.filterWidth,n=r.strideHeight,o=r.strideWidth,a=r.dataFormat==="channelsLast",i=t-1-r.padInfo.top,s=e-1-r.padInfo.left,u=a?1:2,c=a?2:3,l=a?3:1;this.userCode=`
      const ivec2 pads = ivec2(`+i+", "+s+`);

      void main() {
        ivec4 coords = getOutputCoords();
        int batch = coords[0];
        int d1 = coords[`+l+`];

        ivec2 dyCorner = ivec2(coords[`+u+"], coords["+c+`]) - pads;
        int dyRCorner = dyCorner.x;
        int dyCCorner = dyCorner.y;

        // Convolve dy(?, ?, d2) with w(:, :, d1, d2) to compute dx(xR, xC, d1).
        // ? = to be determined. : = across all values in that axis.
        float dotProd = 0.0;
        for (int wR = 0; wR < `+t+`; wR++) {
          float dyR = float(dyRCorner + wR) / `+n+`.0;

          if (dyR < 0.0 || dyR >= `+r.outHeight+`.0 || fract(dyR) > 0.0) {
            continue;
          }
          int idyR = int(dyR);

          int wRPerm = `+t+` - 1 - wR;

          for (int wC = 0; wC < `+e+`; wC++) {
            float dyC = float(dyCCorner + wC) / `+o+`.0;

            if (dyC < 0.0 || dyC >= `+r.outWidth+`.0 ||
                fract(dyC) > 0.0) {
              continue;
            }
            int idyC = int(dyC);

            int wCPerm = `+e+` - 1 - wC;

            for (int d2 = 0; d2 < `+r.outChannels+`; d2++) {

              if (`+a+`) {
                float xValue = getDy(batch, idyR, idyC, d2);
                float wValue = getW(wRPerm, wCPerm, d1, d2);
                dotProd += xValue * wValue;
              } else {
                float xValue = getDy(batch, d2, idyR, idyC);
                float wValue = getW(wRPerm, wCPerm, d1, d2);
                dotProd += xValue * wValue;
              }

            }
          }
        }
        setOutput(dotProd);
      }
    `},Gv=function(r){this.variableNames=["x","dy"],this.outputShape=r.filterShape;var t=r.strideDepth,e=r.strideHeight,n=r.strideWidth,o=r.padInfo.front,a=r.padInfo.top,i=r.padInfo.left;this.userCode=`
      void main() {
        ivec5 coords = getOutputCoords();
        int wF = coords.x;
        int wR = coords.y;
        int wC = coords.z;
        int d1 = coords.w;
        int d2 = coords.u;

        float dotProd = 0.0;

        for (int b = 0; b < `+r.batchSize+`; b++) {
          for (int yF = 0; yF < `+r.outDepth+`; yF++) {
            int xF = wF + yF * `+t+" - "+o+`;

            if (xF < 0 || xF >= `+r.inDepth+`) {
              continue;
            }

            for (int yR = 0; yR < `+r.outHeight+`; yR++) {
              int xR = wR + yR * `+e+" - "+a+`;

              if (xR < 0 || xR >= `+r.inHeight+`) {
                continue;
              }

              for (int yC = 0; yC < `+r.outWidth+`; yC++) {
                int xC = wC + yC * `+n+" - "+i+`;

                if (xC < 0 || xC >= `+r.inWidth+`) {
                  continue;
                }

                float dyValue = getDy(b, yF, yR, yC, d2);
                float xValue = getX(b, xF, xR, xC, d1);
                dotProd += (xValue * dyValue);
              }
            }
          }
        }
        setOutput(dotProd);
      }
    `},Hv=function(r){this.variableNames=["dy","W"],this.outputShape=r.inShape;var t=r.filterDepth,e=r.filterHeight,n=r.filterWidth,o=r.strideDepth,a=r.strideHeight,i=r.strideWidth,s=t-1-r.padInfo.front,u=e-1-r.padInfo.top,c=n-1-r.padInfo.left;this.userCode=`
      const ivec3 pads = ivec3(`+s+", "+u+", "+c+`);

      void main() {
        ivec5 coords = getOutputCoords();
        int batch = coords.x;
        int d1 = coords.u;


        ivec3 dyCorner = ivec3(coords.y, coords.z, coords.w) - pads;
        int dyFCorner = dyCorner.x;
        int dyRCorner = dyCorner.y;
        int dyCCorner = dyCorner.z;

        float dotProd = 0.0;
        for (int wF = 0; wF < `+t+`; wF++) {
          float dyF = float(dyFCorner + wF) / `+o+`.0;

          if (dyF < 0.0 || dyF >= `+r.outDepth+`.0 || fract(dyF) > 0.0) {
            continue;
          }
          int idyF = int(dyF);

          int wFPerm = `+t+` - 1 - wF;

          for (int wR = 0; wR < `+e+`; wR++) {
            float dyR = float(dyRCorner + wR) / `+a+`.0;

            if (dyR < 0.0 || dyR >= `+r.outHeight+`.0 ||
              fract(dyR) > 0.0) {
              continue;
            }
            int idyR = int(dyR);

            int wRPerm = `+e+` - 1 - wR;

            for (int wC = 0; wC < `+n+`; wC++) {
              float dyC = float(dyCCorner + wC) / `+i+`.0;

              if (dyC < 0.0 || dyC >= `+r.outWidth+`.0 ||
                  fract(dyC) > 0.0) {
                continue;
              }
              int idyC = int(dyC);

              int wCPerm = `+n+` - 1 - wC;

              for (int d2 = 0; d2 < `+r.outChannels+`; d2++) {
                float xValue = getDy(batch, idyF, idyR, idyC, d2);
                float wValue = getW(wFPerm, wRPerm, wCPerm, d1, d2);
                dotProd += xValue * wValue;
              }
            }
          }
        }
        setOutput(dotProd);
      }
    `},qv=function(r){this.variableNames=["x","dy"],this.outputShape=r.filterShape;var t=r.strideHeight,e=r.strideWidth,n=r.padInfo.top,o=r.padInfo.left,a=r.outChannels/r.inChannels;this.userCode=`
      void main() {
        ivec4 coords = getOutputCoords();
        int wR = coords.x;
        int wC = coords.y;
        int d1 = coords.z;
        int dm = coords.w;
        int d2 = d1 * `+a+` + dm;

        float dotProd = 0.0;

        // TO DO: Vec4 over the batch size
        for (int b = 0; b < `+r.batchSize+`; b++) {
          for (int yR = 0; yR < `+r.outHeight+`; yR++) {
            int xR = wR + yR * `+t+" - "+n+`;

            if (xR < 0 || xR >= `+r.inHeight+`) {
              continue;
            }

            for (int yC = 0; yC < `+r.outWidth+`; yC++) {
              int xC = wC + yC * `+e+" - "+o+`;

              if (xC < 0 || xC >= `+r.inWidth+`) {
                continue;
              }

              float dyValue = getDy(b, yR, yC, d2);
              float xValue = getX(b, xR, xC, d1);
              dotProd += (xValue * dyValue);
            }
          }
        }
        setOutput(dotProd);
      }
    `},jv=function(r){this.variableNames=["dy","W"],this.outputShape=r.inShape;var t=r.filterHeight,e=r.filterWidth,n=r.strideHeight,o=r.strideWidth,a=t-1-r.padInfo.top,i=e-1-r.padInfo.left,s=r.outChannels/r.inChannels;this.userCode=`
      const ivec2 pads = ivec2(`+a+", "+i+`);

      void main() {
        ivec4 coords = getOutputCoords();
        int batch = coords[0];
        int d1 = coords[3];
        ivec2 dyCorner = coords.yz - pads;
        int dyRCorner = dyCorner.x;
        int dyCCorner = dyCorner.y;

        float dotProd = 0.0;

        for (int wR = 0; wR < `+t+`; wR++) {
          float dyR = float(dyRCorner + wR) / `+n+`.0;

          if (dyR < 0.0 || dyR >= `+r.outHeight+`.0 || fract(dyR) > 0.0) {
            continue;
          }
          int idyR = int(dyR);

          int wRPerm = `+t+` - 1 - wR;

          for (int wC = 0; wC < `+e+`; wC++) {
            float dyC = float(dyCCorner + wC) / `+o+`.0;

            if (dyC < 0.0 || dyC >= `+r.outWidth+`.0 ||
                fract(dyC) > 0.0) {
              continue;
            }
            int idyC = int(dyC);

            int wCPerm = `+e+` - 1 - wC;

            // TO DO: Vec4 over the channelMul
            for (int dm = 0; dm < `+s+`; dm++) {
              int d2 = d1 * `+s+` + dm;
              float xValue = getDy(batch, idyR, idyC, d2);
              float wValue = getW(wRPerm, wCPerm, d1, dm);
              dotProd += xValue * wValue;
            }
          }
        }
        setOutput(dotProd);
      }
    `},xu=function(r,t,e,n){t===void 0&&(t=!1),e===void 0&&(e=null),n===void 0&&(n=!1),this.variableNames=["x","W"],this.outputShape=r.outShape;var o=r.padInfo.top,a=r.padInfo.left,i=r.strideHeight,s=r.strideWidth,u=r.dilationHeight,c=r.dilationWidth,l=r.filterHeight,h=r.filterWidth,f=4*Math.floor(r.inChannels/4),d=r.inChannels%4,p=r.dataFormat==="channelsLast",v=p?1:2,m=p?2:3,g=p?3:1,x="",y="";e&&(x=n?`float activation(float a) {
          float b = getPreluActivationWeightsAtOutCoords();
          `+e+`
        }`:`
          float activation(float x) {
            `+e+`
          }
        `,y="result = activation(result);");var b=t?"result += getBiasAtOutCoords();":"";t&&this.variableNames.push("bias"),n&&this.variableNames.push("preluActivationWeights"),this.userCode=`
      `+x+`

      const ivec2 strides = ivec2(`+i+", "+s+`);
      const ivec2 pads = ivec2(`+o+", "+a+`);

      void main() {
        ivec4 coords = getOutputCoords();
        int batch = coords[0];
        int d2 = coords[`+g+`];

        ivec2 xRCCorner =
            ivec2(coords[`+v+"], coords["+m+`]) * strides - pads;
        int xRCorner = xRCCorner.x;
        int xCCorner = xRCCorner.y;

        // Convolve x(?, ?, d1) with w(:, :, d1, d2) to get y(yR, yC, d2).
        // ? = to be determined. : = across all values in that axis.
        float dotProd = 0.0;
        for (int wR = 0; wR < `+l+`; wR++) {
          int xR = xRCorner + wR * `+u+`;

          if (xR < 0 || xR >= `+r.inHeight+`) {
            continue;
          }

          for (int wC = 0; wC < `+h+`; wC++) {
            int xC = xCCorner + wC * `+c+`;

            if (xC < 0 || xC >= `+r.inWidth+`) {
              continue;
            }

            for (int d1 = 0; d1 < `+f+`; d1 += 4) {
              vec4 wValues = vec4(
                getW(wR, wC, d1, d2),
                getW(wR, wC, d1 + 1, d2),
                getW(wR, wC, d1 + 2, d2),
                getW(wR, wC, d1 + 3, d2)
              );

              if (`+p+`) {
                vec4 xValues = vec4(
                  getX(batch, xR, xC, d1),
                  getX(batch, xR, xC, d1 + 1),
                  getX(batch, xR, xC, d1 + 2),
                  getX(batch, xR, xC, d1 + 3)
                );
                dotProd += dot(xValues, wValues);
              } else {
                vec4 xValues = vec4(
                  getX(batch, d1, xR, xC),
                  getX(batch, d1 + 1, xR, xC),
                  getX(batch, d1 + 2, xR, xC),
                  getX(batch, d1 + 3, xR, xC)
                );
                dotProd += dot(xValues, wValues);
              }
            }

            if (`+(d===1)+`) {

              if (`+p+`) {
                dotProd +=
                    getX(batch, xR, xC, `+f+`) *
                    getW(wR, wC, `+f+`, d2);
              } else {
                dotProd +=
                    getX(batch, `+f+`, xR, xC) *
                    getW(wR, wC, `+f+`, d2);
              }

            } else if (`+(d===2)+`) {
              vec2 wValues = vec2(
                getW(wR, wC, `+f+`, d2),
                getW(wR, wC, `+f+` + 1, d2)
              );

              if (`+p+`) {
                vec2 xValues = vec2(
                  getX(batch, xR, xC, `+f+`),
                  getX(batch, xR, xC, `+f+` + 1)
                );
                dotProd += dot(xValues, wValues);
              } else {
                vec2 xValues = vec2(
                  getX(batch, `+f+`, xR, xC),
                  getX(batch, `+f+` + 1, xR, xC)
                );
                dotProd += dot(xValues, wValues);
              }

            } else if (`+(d===3)+`) {
              vec3 wValues = vec3(
                getW(wR, wC, `+f+`, d2),
                getW(wR, wC, `+f+` + 1, d2),
                getW(wR, wC, `+f+` + 2, d2)
              );

              if (`+p+`) {
                vec3 xValues = vec3(
                  getX(batch, xR, xC, `+f+`),
                  getX(batch, xR, xC, `+f+` + 1),
                  getX(batch, xR, xC, `+f+` + 2)
                );
                dotProd += dot(xValues, wValues);
              } else {
                vec3 xValues = vec3(
                  getX(batch, `+f+`, xR, xC),
                  getX(batch, `+f+` + 1, xR, xC),
                  getX(batch, `+f+` + 2, xR, xC)
                );
                dotProd += dot(xValues, wValues);
              }

            }
          }
        }

        float result = dotProd;
        `+b+`
        `+y+`
        setOutput(result);
      }
    `},Kv=function(r){this.variableNames=["x","W"],this.outputShape=r.outShape;var t=r.padInfo.front,e=r.padInfo.top,n=r.padInfo.left,o=r.strideDepth,a=r.strideHeight,i=r.strideWidth,s=r.dilationDepth,u=r.dilationHeight,c=r.dilationWidth,l=r.filterDepth,h=r.filterHeight,f=r.filterWidth,d=4*Math.floor(r.inChannels/4),p=r.inChannels%4;this.userCode=`
      const ivec3 strides = ivec3(`+o+", "+a+", "+i+`);
      const ivec3 pads = ivec3(`+t+", "+e+", "+n+`);

      void main() {
        ivec5 coords = getOutputCoords();
        int batch = coords.x;
        int d2 = coords.u;

        ivec3 xFRCCorner = ivec3(coords.y, coords.z, coords.w) * strides - pads;
        int xFCorner = xFRCCorner.x;
        int xRCorner = xFRCCorner.y;
        int xCCorner = xFRCCorner.z;

        // Convolve x(?, ?, ?, d1) with w(:, :, :, d1, d2) to get
        // y(yF, yR, yC, d2). ? = to be determined. : = across all
        // values in that axis.
        float dotProd = 0.0;
        for (int wF = 0; wF < `+l+`; wF++) {
          int xF = xFCorner + wF * `+s+`;

          if (xF < 0 || xF >= `+r.inDepth+`) {
            continue;
          }

          for (int wR = 0; wR < `+h+`; wR++) {
            int xR = xRCorner + wR * `+u+`;

            if (xR < 0 || xR >= `+r.inHeight+`) {
              continue;
            }

            for (int wC = 0; wC < `+f+`; wC++) {
              int xC = xCCorner + wC * `+c+`;

              if (xC < 0 || xC >= `+r.inWidth+`) {
                continue;
              }

              for (int d1 = 0; d1 < `+d+`; d1 += 4) {
                vec4 xValues = vec4(
                  getX(batch, xF, xR, xC, d1),
                  getX(batch, xF, xR, xC, d1 + 1),
                  getX(batch, xF, xR, xC, d1 + 2),
                  getX(batch, xF, xR, xC, d1 + 3)
                );
                vec4 wValues = vec4(
                  getW(wF, wR, wC, d1, d2),
                  getW(wF, wR, wC, d1 + 1, d2),
                  getW(wF, wR, wC, d1 + 2, d2),
                  getW(wF, wR, wC, d1 + 3, d2)
                );

                dotProd += dot(xValues, wValues);
              }

              if (`+(p===1)+`) {
                dotProd +=
                  getX(batch, xF, xR, xC, `+d+`) *
                  getW(wF, wR, wC, `+d+`, d2);
              } else if (`+(p===2)+`) {
                vec2 xValues = vec2(
                  getX(batch, xF, xR, xC, `+d+`),
                  getX(batch, xF, xR, xC, `+d+` + 1)
                );
                vec2 wValues = vec2(
                  getW(wF, wR, wC, `+d+`, d2),
                  getW(wF, wR, wC, `+d+` + 1, d2)
                );
                dotProd += dot(xValues, wValues);
              } else if (`+(p===3)+`) {
                vec3 xValues = vec3(
                  getX(batch, xF, xR, xC, `+d+`),
                  getX(batch, xF, xR, xC, `+d+` + 1),
                  getX(batch, xF, xR, xC, `+d+` + 2)
                );
                vec3 wValues = vec3(
                  getW(wF, wR, wC, `+d+`, d2),
                  getW(wF, wR, wC, `+d+` + 1, d2),
                  getW(wF, wR, wC, `+d+` + 2, d2)
                );
                dotProd += dot(xValues, wValues);
              }
            }
          }
        }
        setOutput(dotProd);
      }
    `},bu=function(r,t,e,n){t===void 0&&(t=!1),e===void 0&&(e=null),n===void 0&&(n=!1),this.variableNames=["x","W"],this.outputShape=r.outShape;var o=r.inHeight,a=r.inWidth,i=r.padInfo.top,s=r.padInfo.left,u=r.strideHeight,c=r.strideWidth,l=r.dilationHeight,h=r.dilationWidth,f=r.filterHeight,d=r.filterWidth,p=r.outChannels/r.inChannels,v="",m="";e&&(v=n?`float activation(float a) {
          float b = getPreluActivationWeightsAtOutCoords();
          `+e+`
        }`:`
          float activation(float x) {
            `+e+`
          }
        `,m="result = activation(result);");var g=t?"result += getBiasAtOutCoords();":"";t&&this.variableNames.push("bias"),n&&this.variableNames.push("preluActivationWeights"),this.userCode=`
      `+v+`

      const ivec2 strides = ivec2(`+u+", "+c+`);
      const ivec2 pads = ivec2(`+i+", "+s+`);

      void main() {
        ivec4 coords = getOutputCoords();
        int batch = coords.x;
        ivec2 xRCCorner = coords.yz * strides - pads;
        int d2 = coords.w;
        int d1 = d2 / `+p+`;
        int q = d2 - d1 * `+p+`;

        int xRCorner = xRCCorner.x;
        int xCCorner = xRCCorner.y;

        // Convolve x(?, ?, d1) with w(:, :, d1, q) to get y(yR, yC, d2).
        // ? = to be determined. : = across all values in that axis.
        float dotProd = 0.0;
        // TO DO(dsmilkov): Flatten the two for loops and vec4 the operations.
        for (int wR = 0; wR < `+f+`; wR++) {
          int xR = xRCorner + wR * `+l+`;

          if (xR < 0 || xR >= `+o+`) {
            continue;
          }

          for (int wC = 0; wC < `+d+`; wC++) {
            int xC = xCCorner + wC * `+h+`;

            if (xC < 0 || xC >= `+a+`) {
              continue;
            }

            float xVal = getX(batch, xR, xC, d1);
            float wVal = getW(wR, wC, d1, q);
            dotProd += xVal * wVal;
          }
        }

        float result = dotProd;
        `+g+`
        `+m+`
        setOutput(result);
      }
    `},wu=function(r,t,e,n){t===void 0&&(t=!1),e===void 0&&(e=null),n===void 0&&(n=!1),this.variableNames=["x","W"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=r.outShape;for(var o=r.inHeight,a=r.inWidth,i=r.padInfo.top,s=r.padInfo.left,u=r.strideHeight,c=r.strideWidth,l=r.dilationHeight,h=r.dilationWidth,f=r.filterHeight,d=r.filterWidth,p=d,v="int xR; int xC; int xCOffset;",m=0;m<f;m++)for(var g=0;g<d;g++)v+=`
          vec4 xTexelR`+m+"C"+2*g+` = vec4(0.);
          vec4 wR`+m+"C"+g+` = vec4(0.);
          vec4 xR`+m+"C"+g+" = vec4(0.);";for(m=0;m<f;m++)for(var x=0;x<p;x++){if(v+=`
          xR = xRCorner + `+m*l+`;
          xC = xCCorner + `+(g=2*x)*h+`;
        `,c===1){if(g<d&&(v+=s%2==1?`
                xCOffset = xC + 1;
                if(xR >= 0 && xR < `+o+" && xCOffset >= 0 && xCOffset < "+a+`) {
                  xTexelR`+m+"C"+g+` = getX(batch, xR, xCOffset, d1);

                  // Need to manually clear unused channels in case
                  // we're reading from recycled texture.
                  if(xCOffset + 1 >= `+a+`) {
                    xTexelR`+m+"C"+g+`.zw = vec2(0.);
                  }
                } else {
                  xTexelR`+m+"C"+g+` = vec4(0.);
                }

                xCOffset = xC + 1 - 2;
                if(xR >= 0 && xR < `+o+" && xCOffset >= 0 && xCOffset < "+a+`) {
                  vec4 previous = getX(batch, xR, xCOffset, d1);

                  // Need to manually clear unused channels in case
                  // we're reading from recycled texture.
                  if(xCOffset + 1 >= `+a+`) {
                    previous.zw = vec2(0.);
                  }

                  xR`+m+"C"+g+" = vec4(previous.zw, xTexelR"+m+"C"+g+`.xy);
                } else {
                  xR`+m+"C"+g+" = vec4(0, 0, xTexelR"+m+"C"+g+`.xy);
                }
              `:`
                if(xR >= 0 && xR < `+o+" && xC >= 0 && xC < "+a+`) {
                  xTexelR`+m+"C"+g+` = getX(batch, xR, xC, d1);
                } else {
                  xTexelR`+m+"C"+g+` = vec4(0.);
                }

                xR`+m+"C"+g+" = xTexelR"+m+"C"+g+`;
              `,g+1<d)){var y=s%2==0?Hi(h):h;h%2==0&&s%2==1||h%2!=0&&s%2!=1?(v+=`
                  xCOffset = xC + `+s%2+" + "+y+`;

                  if(xR >= 0 && xR < `+o+` &&
                    xCOffset >= 0 && xCOffset < `+a+`) {
                    xTexelR`+m+"C"+(g+2)+` = getX(batch, xR, xCOffset, d1);
                  }
                `,h>1&&(v+=`
                    xCOffset -= 2;
                    if(xR >= 0 && xR < `+o+` &&
                      xCOffset >= 0 && xCOffset < `+a+`) {
                      xTexelR`+m+"C"+g+` = getX(batch, xR, xCOffset, d1);
                    } else {
                      xTexelR`+m+"C"+g+` = vec4(0.);
                    }
                  `),v+=`
                  xR`+m+"C"+(g+1)+` = vec4(
                    xTexelR`+m+"C"+g+".zw, xTexelR"+m+"C"+(g+2)+`.xy);
                `):v+=`
                  xCOffset = xC + `+y+`;

                  if(xR >= 0 && xR < `+o+` &&
                    xCOffset >= 0 && xCOffset < `+a+`) {
                    xTexelR`+m+"C"+(g+2)+` = getX(batch, xR, xCOffset, d1);
                  }

                  xR`+m+"C"+(g+1)+" = xTexelR"+m+"C"+(g+2)+`;
                `}}else g<d&&(v+=`
              if(xR >= 0 && xR < `+o+`) {
            `,s%2==1?(v+=`
                xCOffset = xC + 1 - `+c+`;
                if(xCOffset >= 0 && xCOffset < `+a+`) {
                  xTexelR`+m+"C"+g+` = getX(batch, xR, xCOffset, d1);
                } else {
                  xTexelR`+m+"C"+g+` = vec4(0.);
                }

                if(xC + 1 >= 0 && xC + 1 < `+a+`) {
                  xTexelR`+m+"C"+(g+2)+` = getX(batch, xR, xC + 1, d1);
                } else {
                  xTexelR`+m+"C"+(g+2)+` = vec4(0.);
                }

                xR`+m+"C"+g+` = vec4(
                  xTexelR`+m+"C"+g+".zw, xTexelR"+m+"C"+(g+2)+`.zw);
              `,g+1<d&&(v+=`
                  vec4 final = vec4(0.);
                  xCOffset = xC + 1 + `+c+`;
                  if(xCOffset >= 0 && xCOffset < `+a+`) {
                    final = getX(batch, xR, xCOffset, d1);
                  }
                  xR`+m+"C"+(g+1)+" = vec4(xTexelR"+m+"C"+(g+2)+`.xy, final.xy);
                `)):(v+=`
                if(xC >= 0 && xC < `+a+`) {
                  xTexelR`+m+"C"+g+` = getX(batch, xR, xC, d1);
                } else {
                  xTexelR`+m+"C"+g+` = vec4(0.);
                }

                xCOffset = xC + `+c+`;
                if(xCOffset >= 0 && xCOffset < `+a+`) {
                  xTexelR`+m+"C"+(g+2)+` = getX(batch, xR, xCOffset, d1);
                } else {
                  xTexelR`+m+"C"+(g+2)+` = vec4(0.);
                }

                xR`+m+"C"+g+` = vec4(
                  xTexelR`+m+"C"+g+".xy, xTexelR"+m+"C"+(g+2)+`.xy);
              `,g+1<d&&(v+=`
                  xR`+m+"C"+(g+1)+` = vec4(
                    xTexelR`+m+"C"+g+".zw, xTexelR"+m+"C"+(g+2)+`.zw);
                `)),v+="}");g<d&&(v+=`
            vec4 wTexelR`+m+"C"+g+" = getW("+m+", "+g+`, d1, q);
            wR`+m+"C"+g+" = vec4(wTexelR"+m+"C"+g+".xz, wTexelR"+m+"C"+g+`.xz);
          `,g+1<d&&(v+=`
              vec4 wTexelR`+m+"C"+(g+1)+" = getW("+m+", "+(g+1)+`, d1, q);
              wR`+m+"C"+(g+1)+` =
                vec4(wTexelR`+m+"C"+(g+1)+".xz, wTexelR"+m+"C"+(g+1)+".xz);"))}for(m=0;m<f;m++)for(g=0;g<d;g++)v+="dotProd += xR"+m+"C"+g+" * wR"+m+"C"+g+";";var b="",w="";e&&(b=n?`vec4 activation(vec4 a) {
          vec4 b = getPreluActivationWeightsAtOutCoords();
          `+e+`
        }`:`vec4 activation(vec4 x) {
          `+e+`
        }`,w="result = activation(result);");var C=t?"result += getBiasAtOutCoords();":"";t&&this.variableNames.push("bias"),n&&this.variableNames.push("preluActivationWeights"),this.userCode=`
      `+b+`

      const ivec2 strides = ivec2(`+u+", "+c+`);
      const ivec2 pads = ivec2(`+i+", "+s+`);

      void main() {

        ivec4 coords = getOutputCoords();
        int batch = coords.x;
        ivec2 xRCCorner = coords.yz * strides - pads;
        int d2 = coords.w;
        int d1 = d2;
        int q = 0;
        int xRCorner = xRCCorner.x;
        int xCCorner = xRCCorner.y;

        vec4 dotProd = vec4(0.);

        `+v+`

        vec4 result = dotProd;
        `+C+`
        `+w+`
        setOutput(result);
      }
    `},Xv=function(r,t,e,n,o){this.variableNames=["Image","Boxes","BoxInd"],this.outputShape=[];var a=r[0],i=r[1],s=r[2],u=r[3],c=t[0],l=e[0],h=e[1];this.outputShape=[c,l,h,u];var f=n==="bilinear"?1:0,d=[i-1+".0",s-1+".0"],p=d[0],v=d[1],m=l>1?[""+(i-1)/(l-1),"(y2-y1) * height_ratio","y1*"+p+" + float(y)*(height_scale)"]:["0.0","0.0","0.5 * (y1+y2) * "+p],g=m[0],x=m[1],y=m[2],b=h>1?[""+(s-1)/(h-1),"(x2-x1) * width_ratio","x1*"+v+" + float(x)*(width_scale)"]:["0.0","0.0","0.5 * (x1+x2) * "+v],w=b[0],C=b[1],I=b[2];this.userCode=`
      const float height_ratio = float(`+g+`);
      const float width_ratio = float(`+w+`);
      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int y = coords[1];
        int x = coords[2];
        int d = coords[3];

        // get box vals
        float y1 = getBoxes(b,0);
        float x1 = getBoxes(b,1);
        float y2 = getBoxes(b,2);
        float x2 = getBoxes(b,3);

        // get image in batch index
        int bInd = round(getBoxInd(b));
        if(bInd < 0 || bInd >= `+a+`) {
          return;
        }

        float height_scale = `+x+`;
        float width_scale = `+C+`;

        float in_y = `+y+`;
        if( in_y < 0.0 || in_y > `+p+` ) {
          setOutput(float(`+o+`));
          return;
        }
        float in_x = `+I+`;
        if( in_x < 0.0 || in_x > `+v+` ) {
          setOutput(float(`+o+`));
          return;
        }

        vec2 sourceFracIndexCR = vec2(in_x,in_y);
        if(`+f+` == 1) {
          // Compute the four integer indices.
          ivec2 sourceFloorCR = ivec2(sourceFracIndexCR);
          ivec2 sourceCeilCR = ivec2(ceil(sourceFracIndexCR));

          float topLeft = getImage(b, sourceFloorCR.y, sourceFloorCR.x, d);
          float bottomLeft = getImage(b, sourceCeilCR.y, sourceFloorCR.x, d);
          float topRight = getImage(b, sourceFloorCR.y, sourceCeilCR.x, d);
          float bottomRight = getImage(b, sourceCeilCR.y, sourceCeilCR.x, d);

          vec2 fracCR = sourceFracIndexCR - vec2(sourceFloorCR);

          float top = topLeft + (topRight - topLeft) * fracCR.x;
          float bottom = bottomLeft + (bottomRight - bottomLeft) * fracCR.x;
          float newValue = top + (bottom - top) * fracCR.y;
          setOutput(newValue);
        } else {
          // Compute the coordinators of nearest neighbor point.
          ivec2 sourceNearestCR = ivec2(floor(
            sourceFracIndexCR + vec2(0.5,0.5)));
          float newValue = getImage(b, sourceNearestCR.y, sourceNearestCR.x, d);
          setOutput(newValue);
        }
      }
    `},$v=function(r,t,e){this.variableNames=["x"],this.outputShape=r;var n=r.length,o=r[r.length-1],a=e?"<":">";this.userCode=`
      int getIndex(int i) {
        `+(e?"return "+o+" -i - 1;":"return i;")+`
      }

      void main() {
        `+Se(n)+` coords = getOutputCoords();
        int end = `+Cu(n,"coords")+`;
        float val = 0.0;
        for (int i = `+o+` - 1; i >= 0; i -= 1) {
          int idx = getIndex(i);
          if (idx `+a+` end) {
            continue;
          }
          if (idx == end && `+t+`) {
            continue;
          }
          `+Cu(n,"coords")+` = idx;
          val += getX(`+function(i,s){if(i===1)return""+s;if(i===2)return s+".x, "+s+".y";if(i===3)return s+".x, "+s+".y, "+s+".z";if(i===4)return s+".x, "+s+".y, "+s+".z, "+s+".w";throw Error("Cumulative sum for rank "+i+" is not yet supported")}(n,"coords")+`);
        }
        setOutput(val);
      }
    `};function Cu(r,t){if(r===1)return""+t;if(r===2)return t+".y";if(r===3)return t+".z";if(r===4)return t+".w";throw Error("Cumulative sum for rank "+r+" is not yet supported")}var Yv=function(r){this.variableNames=["A"],this.packedInputs=!1,this.packedOutput=!0,this.outPackingScheme=Lr.DENSE;var t=Dr(r),e=Qe();this.outputShape=r,this.userCode=`
      ivec3 outCoordsFromFlatIndex(int index) {
        `+Mn(["r","c","d"],r)+`
        return ivec3(r, c, d);
      }

      void main() {
        ivec2 resTexRC = ivec2(resultUV.yx *
          vec2(`+t[0]+", "+t[1]+`));
        int index = 4 * (resTexRC.x * `+t[1]+` + resTexRC.y);

        vec4 result = vec4(0.);

        for (int i=0; i<4; i++) {
          int flatIndex = index + i;
          ivec3 rc = outCoordsFromFlatIndex(flatIndex);
          result[i] = getA(rc.x, rc.y, rc.z);
        }

        `+e.output+` = result;
      }
    `},Jv=function(r){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!0,this.outPackingScheme=Lr.DENSE;var t=Dr(r),e=Qe();this.outputShape=r,this.userCode=`
      ivec3 outCoordsFromFlatIndex(int index) {
        `+Mn(["r","c","d"],r)+`
        return ivec3(r, c, d);
      }

      void main() {
        ivec2 resTexRC = ivec2(resultUV.yx *
          vec2(`+t[0]+", "+t[1]+`));
        int index = 4 * (resTexRC.x * `+t[1]+` + resTexRC.y);

        vec4 result = vec4(0.);

        for (int i=0; i<4; i++) {
          int flatIndex = index + i;
          ivec3 rc = outCoordsFromFlatIndex(flatIndex);
          result[i] = getChannel(getA(rc.x, rc.y, rc.z), vec2(rc.y, rc.z));
        }

        `+e.output+` = result;
      }
    `},Qv=function(){function r(t,e,n){this.variableNames=["x"],this.outputShape=[],this.outputShape=t,this.blockSize=e,this.dataFormat=n,this.userCode=`
    void main() {
      ivec4 coords = getOutputCoords();
      int b = coords[0];
      int h = `+this.getHeightCoordString()+`;
      int w = `+this.getWidthCoordString()+`;
      int d = `+this.getDepthCoordString()+`;

      int in_h = h / `+e+`;
      int offset_h = imod(h, `+e+`);
      int in_w = w / `+e+`;
      int offset_w = imod(w, `+e+`);
      int offset_d = (offset_h * `+e+` + offset_w) *
        `+this.getOutputDepthSize()+`;
      int in_d = d + offset_d;

      float result = `+this.getInputSamplingString()+`;
      setOutput(result);
    }
  `}return r.prototype.getHeightCoordString=function(){return this.dataFormat==="NHWC"?"coords[1]":"coords[2]"},r.prototype.getWidthCoordString=function(){return this.dataFormat==="NHWC"?"coords[2]":"coords[3]"},r.prototype.getDepthCoordString=function(){return this.dataFormat==="NHWC"?"coords[3]":"coords[1]"},r.prototype.getOutputDepthSize=function(){return this.dataFormat==="NHWC"?this.outputShape[3]:this.outputShape[1]},r.prototype.getInputSamplingString=function(){return this.dataFormat==="NHWC"?"getX(b, in_h, in_w, in_d)":"getX(b, in_d, in_h, in_w)"},r}(),Zv=function(r){this.variableNames=["X"],this.outputShape=[r,r],this.userCode=`
      void main() {
          ivec2 coords = getOutputCoords();
          float val = coords[0] == coords[1] ? getX(coords[0]) : 0.0;
          setOutput(val);
      }
    `},em=function(r){this.variableNames=["A"],this.outTexUsage=mt.DOWNLOAD;var t=Qe();this.outputShape=r,this.userCode=`
      `+Ql+`

      void main() {
        float x = getAAtOutCoords();
        `+t.output+` = encode_float(x);
      }
    `},tm=function(r){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!1,this.outTexUsage=mt.DOWNLOAD;var t=Qe();this.outputShape=r,this.userCode=`
      `+Ql+`

      void main() {
        ivec3 coords = getOutputCoords();
        float x = getChannel(getAAtOutCoords(), vec2(coords.y, coords.z));
        `+t.output+` = encode_float(x);
      }
    `},nm=function(r,t,e){e===void 0&&(e=!1),this.variableNames=["A"];var n=Qe(),o=t[0],a=t[1];this.outputShape=r;var i="result";e&&(i="floor(result * 255. + 0.5)"),this.userCode=`
      `+hs(r)+`

      void main() {
        ivec3 coords = getOutputCoords();

        int flatIndex = getFlatIndex(coords);
        int offset = imod(flatIndex, 4);

        flatIndex = idiv(flatIndex, 4, 1.);
        
        int r = flatIndex / `+a+`;
        int c = imod(flatIndex, `+a+`);
        vec2 uv = (vec2(c, r) + halfCR) / vec2(`+a+".0, "+o+`.0);
        vec4 values = `+n.texture2D+`(A, uv);

        float result;

        if(offset == 0) {
          result = values[0];
        } else if(offset == 1) {
          result = values[1];
        } else if(offset == 2) {
          result = values[2];
        } else {
          result = values[3];
        }

        `+n.output+" = vec4("+i+`, 0., 0., 0.);
      }
    `},rm=function(r,t,e){e===void 0&&(e=!1),this.variableNames=["A"],this.packedInputs=!1,this.packedOutput=!0;var n=Qe(),o=t[0],a=t[1];this.outputShape=r;var i="",s="result";e&&(s="floor(result * 255. + 0.5)");for(var u=0;u<=1;u++)for(var c=0;c<=1;c++){var l=2*u+c;i+=`
          localCoords = coords;
          if(localCoords[2] + `+c+" < "+r[2]+`) {
            localCoords[2] += `+c+`;
            if(localCoords[1] + `+u+" < "+r[1]+`) {
              localCoords[1] += `+u+`;

              flatIndex = getFlatIndex(localCoords);
              offset = imod(flatIndex, 4);

              flatIndex = idiv(flatIndex, 4, 1.);

              r = flatIndex / `+a+`;
              c = imod(flatIndex, `+a+`);
              uv = (vec2(c, r) + halfCR) / vec2(`+a+".0, "+o+`.0);
              values = `+n.texture2D+`(A, uv);

              if(offset == 0) {
                result[`+l+`] = values[0];
              } else if(offset == 1) {
                result[`+l+`] = values[1];
              } else if(offset == 2) {
                result[`+l+`] = values[2];
              } else {
                result[`+l+`] = values[3];
              }
            }
          }
        `}this.userCode=`
      `+hs(r)+`

      void main() {
        ivec3 coords = getOutputCoords();

        vec4 result = vec4(0.);
        int flatIndex, r, c, offset;
        ivec3 localCoords;
        vec2 uv;
        vec4 values;

        `+i+`

        `+n.output+" = "+s+`;
      }
    `},om="return real * expR - imag * expI;",am="return real * expI + imag * expR;",_u=function(r,t,e){this.variableNames=["real","imag"];var n=t[1];this.outputShape=t;var o=e?"2.0 * "+Math.PI:"-2.0 * "+Math.PI,a=e?n+".0":"1.0";this.userCode=`
      const float exponentMultiplier = `+o+`;

      float unaryOpComplex(float real, float expR, float imag, float expI) {
        `+r+`
      }

      float mulMatDFT(int batch, int index) {
        float indexRatio = float(index) / float(`+n+`);
        float exponentMultiplierTimesIndexRatio =
            exponentMultiplier * indexRatio;

        float result = 0.0;

        for (int i = 0; i < `+n+`; i++) {
          // x = (-2|2 * PI / N) * index * i;
          float x = exponentMultiplierTimesIndexRatio * float(i);
          float expR = cos(x);
          float expI = sin(x);
          float real = getReal(batch, i);
          float imag = getImag(batch, i);

          result +=
              unaryOpComplex(real, expR, imag, expI) / `+a+`;
        }

        return result;
      }

      void main() {
        ivec2 coords = getOutputCoords();
        setOutput(mulMatDFT(coords[0], coords[1]));
      }
    `},im=function(){function r(t,e){this.outputShape=[],this.variableNames=["x"],this.outputShape=t,this.userCode=`
      uniform float value;
      void main() {
        // Input can be obtained from uniform value.
        setOutput(value);
      }
    `}return r.prototype.getCustomSetupFunc=function(t){var e=this;return function(n,o){e.valueLoc==null&&(e.valueLoc=n.getUniformLocationNoThrow(o,"value")),n.gl.uniform1f(e.valueLoc,t)}},r}(),sm=function(r,t,e){this.variableNames=["A","indices"];var n=r.slice();n[e]=t,this.outputShape=n,this.rank=n.length;var o=Se(this.rank),a=function(i,s){var u=i.length;if(u>4)throw Error("Gather for rank "+u+" is not yet supported");if(u===1)return"int(getIndices(resRC))";for(var c=["resRC.x","resRC.y","resRC.z","resRC.w"],l=[],h=0;h<i.length;h++)h===s?l.push("int(getIndices("+c[h]+"))"):l.push(""+c[h]);return l.join()}(r,e);this.userCode=`
      void main() {
        `+o+` resRC = getOutputCoords();
        setOutput(getA(`+a+`));
      }
    `},um=function(r,t,e){this.sliceDim=r,this.strides=t,this.variableNames=["x","indices"],this.outputShape=e;var n=Se(t.length),o=Se(e.length),a=this.sliceDim>1?"strides[j]":"strides";this.userCode=`
        `+n+" strides = "+n+"("+this.strides+`);
         void main() {
          `+o+` coords = getOutputCoords();
          int flattenIndex = 0;
          for (int j = 0; j < `+this.sliceDim+`; j++) {
            int index = round(getIndices(coords[0], j));
            flattenIndex += index * `+a+`;
          }
          setOutput(getX(flattenIndex, coords[1]));
        }
      `};function nh(r,t){var e=Qe();return sl(r,t,e.version+`
    precision highp float;
    `+e.attribute+` vec3 clipSpacePos;
    `+e.attribute+` vec2 uv;
    `+e.varyingVs+` vec2 resultUV;

    void main() {
      gl_Position = vec4(clipSpacePos, 1);
      resultUV = uv;
    }`)}function rh(r,t){return hl(r,t,new Float32Array([-1,1,0,0,1,-1,-1,0,0,0,1,1,0,1,1,1,-1,0,1,0]))}function oh(r,t){return fl(r,t,new Uint16Array([0,1,2,2,1,3]))}function Yr(r,t,e,n,o,a,i){pl(e,n);var s=dl(r,t),u=r.TEXTURE_2D;return J(r,t,function(){return r.bindTexture(u,s)}),J(r,t,function(){return r.texParameteri(u,r.TEXTURE_WRAP_S,r.CLAMP_TO_EDGE)}),J(r,t,function(){return r.texParameteri(u,r.TEXTURE_WRAP_T,r.CLAMP_TO_EDGE)}),J(r,t,function(){return r.texParameteri(u,r.TEXTURE_MIN_FILTER,r.NEAREST)}),J(r,t,function(){return r.texParameteri(u,r.TEXTURE_MAG_FILTER,r.NEAREST)}),J(r,t,function(){return r.texImage2D(u,0,o,e,n,0,a,i,null)}),J(r,t,function(){return r.bindTexture(r.TEXTURE_2D,null)}),s}function ah(r,t,e,n,o){var a=ra(e,n);return Yr(r,t,a[0],a[1],o.internalFormatFloat,o.textureFormatFloat,r.FLOAT)}function ih(r,t,e,n,o){var a=ra(e,n);return Yr(r,t,a[0],a[1],o.internalFormatHalfFloat,o.textureFormatFloat,o.textureTypeHalfFloat)}function sh(r,t,e,n,o){var a=ra(e,n);return Yr(r,t,a[0],a[1],r.RGBA,r.RGBA,r.UNSIGNED_BYTE)}function uh(r,t,e,n,o){var a=$r(e,n);return Yr(r,t,a[0],a[1],o.internalFormatPackedFloat,r.RGBA,r.FLOAT)}function ch(r,t,e,n,o){var a=$r(e,n);return Yr(r,t,a[0],a[1],o.internalFormatPackedHalfFloat,r.RGBA,o.textureTypeHalfFloat)}function lh(r,t,e,n){return J(r,t,function(){return r.bindBuffer(r.ARRAY_BUFFER,n)}),pi(r,t,e,"clipSpacePos",n,3,20,0)&&pi(r,t,e,"uv",n,2,20,12)}function hh(r,t,e,n,o,a,i){var s,u,c;J(r,t,function(){return r.bindTexture(r.TEXTURE_2D,e)}),a instanceof Uint8Array?(s=new Uint8Array(n*o*4),u=r.UNSIGNED_BYTE,c=r.RGBA):(s=new Float32Array(n*o*4),u=r.FLOAT,c=i.internalFormatPackedFloat),s.set(a),J(r,t,function(){return r.texImage2D(r.TEXTURE_2D,0,c,n,o,0,r.RGBA,u,s)}),J(r,t,function(){return r.bindTexture(r.TEXTURE_2D,null)})}function fh(r,t,e,n){J(r,t,function(){return r.bindTexture(r.TEXTURE_2D,e)}),n.data instanceof Uint8Array?J(r,t,function(){return r.texImage2D(r.TEXTURE_2D,0,r.RGBA,n.width,n.height,0,r.RGBA,r.UNSIGNED_BYTE,n.data)}):J(r,t,function(){return r.texImage2D(r.TEXTURE_2D,0,r.RGBA,r.RGBA,r.UNSIGNED_BYTE,n)}),J(r,t,function(){return r.bindTexture(r.TEXTURE_2D,null)})}function dh(r,t,e,n,o){var a=r.createBuffer();J(r,t,function(){return r.bindBuffer(r.PIXEL_PACK_BUFFER,a)});var i=16*e*n;return J(r,t,function(){return r.bufferData(r.PIXEL_PACK_BUFFER,i,r.STREAM_READ)}),J(r,t,function(){return r.readPixels(0,0,n,e,r.RGBA,r.FLOAT,0)}),J(r,t,function(){return r.bindBuffer(r.PIXEL_PACK_BUFFER,null)}),a}function ph(r,t,e){var n=r,o=new Float32Array(e);return n.bindBuffer(n.PIXEL_PACK_BUFFER,t),n.getBufferSubData(n.PIXEL_PACK_BUFFER,0,o),n.bindBuffer(n.PIXEL_PACK_BUFFER,null),o}function vh(r,t,e,n,o){var a=ra(e,n),i=a[0],s=a[1],u=new Uint8Array(e*n*4);return J(r,t,function(){return r.readPixels(0,0,i,s,o.downloadTextureFormat,r.UNSIGNED_BYTE,u)}),new Float32Array(u.buffer)}function mh(r,t,e,n,o,a,i,s){var u=r,c=new Float32Array(function(l,h){var f=$r(l,h);return f[0]*f[1]*4}(a,i));return u.bindBuffer(u.PIXEL_PACK_BUFFER,t),u.getBufferSubData(u.PIXEL_PACK_BUFFER,0,c),u.bindBuffer(u.PIXEL_PACK_BUFFER,null),c}function gh(r,t,e,n){var o=new Float32Array(e*n*4);return J(r,t,function(){return r.readPixels(0,0,n,e,r.RGBA,r.FLOAT,o)}),o}var cm=Object.freeze({createVertexShader:nh,createVertexBuffer:rh,createIndexBuffer:oh,createFloat32MatrixTexture:ah,createFloat16MatrixTexture:ih,createUnsignedBytesMatrixTexture:sh,createPackedMatrixTexture:uh,createFloat16PackedMatrixTexture:ch,bindVertexProgramAttributeStreams:lh,uploadDenseMatrixToTexture:hh,uploadPixelDataToTexture:fh,createBufferFromOutputTexture:dh,downloadFloat32MatrixFromBuffer:ph,downloadByteEncodedFloatMatrixFromOutputTexture:vh,downloadPackedMatrixFromBuffer:mh,downloadMatrixFromPackedOutputTexture:gh}),yh=function(){function r(t){this.outputTexture=null,this.program=null,this.disposed=!1,this.vertexAttrsAreBound=!1,this.itemsToPoll=[];var e=V().getNumber("WEBGL_VERSION");t!=null?(this.gl=t,ol(e,t)):this.gl=Zt(e);var n="WEBGL_color_buffer_float";if(V().getNumber("WEBGL_VERSION")===1){if(this.textureFloatExtension=kr(this.gl,this.debug,"OES_texture_float"),gt(this.gl,"OES_texture_half_float"))this.textureHalfFloatExtension=kr(this.gl,this.debug,"OES_texture_half_float");else if(V().get("WEBGL_FORCE_F16_TEXTURES"))throw new Error("GL context does not support half float textures, yet the environment flag WEBGL_FORCE_F16_TEXTURES is set to true.");if(this.colorBufferFloatExtension=this.gl.getExtension(n),gt(this.gl,"EXT_color_buffer_half_float"))this.colorBufferHalfFloatExtension=kr(this.gl,this.debug,"EXT_color_buffer_half_float");else if(V().get("WEBGL_FORCE_F16_TEXTURES"))throw new Error("GL context does not support color renderable half floats, yet the environment flag WEBGL_FORCE_F16_TEXTURES is set to true.")}else if(n="EXT_color_buffer_float",gt(this.gl,n))this.colorBufferFloatExtension=this.gl.getExtension(n);else{if(!gt(this.gl,"EXT_color_buffer_half_float"))throw new Error("GL context does not support color renderable floats");this.colorBufferHalfFloatExtension=this.gl.getExtension("EXT_color_buffer_half_float")}this.vertexBuffer=rh(this.gl,this.debug),this.indexBuffer=oh(this.gl,this.debug),this.framebuffer=vl(this.gl,this.debug),this.textureConfig=Yi(this.gl,this.textureHalfFloatExtension)}return Object.defineProperty(r.prototype,"debug",{get:function(){return V().getBool("DEBUG")},enumerable:!0,configurable:!0}),r.prototype.dispose=function(){var t=this;if(!this.disposed){this.program!=null&&console.warn("Disposing a GPGPUContext that still has a bound WebGLProgram. This is probably a resource leak, delete the program with GPGPUContext.deleteProgram before disposing."),this.outputTexture!=null&&console.warn("Disposing a GPGPUContext that still has a bound output matrix texture.  This is probably a resource leak, delete the output matrix texture with GPGPUContext.deleteMatrixTexture before disposing.");var e=this.gl;J(e,this.debug,function(){return e.finish()}),J(e,this.debug,function(){return e.bindFramebuffer(e.FRAMEBUFFER,null)}),J(e,this.debug,function(){return e.deleteFramebuffer(t.framebuffer)}),J(e,this.debug,function(){return e.bindBuffer(e.ARRAY_BUFFER,null)}),J(e,this.debug,function(){return e.bindBuffer(e.ELEMENT_ARRAY_BUFFER,null)}),J(e,this.debug,function(){return e.deleteBuffer(t.indexBuffer)}),this.disposed=!0}},r.prototype.createFloat32MatrixTexture=function(t,e){return this.throwIfDisposed(),ah(this.gl,this.debug,t,e,this.textureConfig)},r.prototype.createFloat16MatrixTexture=function(t,e){return this.throwIfDisposed(),ih(this.gl,this.debug,t,e,this.textureConfig)},r.prototype.createUnsignedBytesMatrixTexture=function(t,e){return this.throwIfDisposed(),sh(this.gl,this.debug,t,e,this.textureConfig)},r.prototype.uploadPixelDataToTexture=function(t,e){this.throwIfDisposed(),fh(this.gl,this.debug,t,e)},r.prototype.uploadDenseMatrixToTexture=function(t,e,n,o){this.throwIfDisposed(),hh(this.gl,this.debug,t,e,n,o,this.textureConfig)},r.prototype.createFloat16PackedMatrixTexture=function(t,e){return this.throwIfDisposed(),ch(this.gl,this.debug,t,e,this.textureConfig)},r.prototype.createPackedMatrixTexture=function(t,e){return this.throwIfDisposed(),uh(this.gl,this.debug,t,e,this.textureConfig)},r.prototype.deleteMatrixTexture=function(t){var e=this;this.throwIfDisposed(),this.outputTexture===t&&(vi(this.gl,this.debug,this.framebuffer),this.outputTexture=null),J(this.gl,this.debug,function(){return e.gl.deleteTexture(t)})},r.prototype.downloadByteEncodedFloatMatrixFromOutputTexture=function(t,e,n){var o=this;return this.downloadMatrixDriver(t,function(){return vh(o.gl,o.debug,e,n,o.textureConfig)})},r.prototype.downloadPackedMatrixFromBuffer=function(t,e,n,o,a,i){return mh(this.gl,t,0,0,0,a,i,this.textureConfig)},r.prototype.downloadFloat32MatrixFromBuffer=function(t,e){return ph(this.gl,t,e)},r.prototype.createBufferFromTexture=function(t,e,n){this.bindTextureToFrameBuffer(t);var o=dh(this.gl,this.debug,e,n,this.textureConfig);return this.unbindTextureToFrameBuffer(),o},r.prototype.createAndWaitForFence=function(){var t=this.createFence(this.gl);return this.pollFence(t)},r.prototype.createFence=function(t){var e,n,o=this;if(V().getBool("WEBGL_FENCE_API_ENABLED")){var a=t,i=a.fenceSync(a.SYNC_GPU_COMMANDS_COMPLETE,0);t.flush(),n=function(){var s=a.clientWaitSync(i,0,0);return s===a.ALREADY_SIGNALED||s===a.CONDITION_SATISFIED},e=i}else V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION")>0?(e=this.beginQuery(),this.endQuery(),n=function(){return o.isQueryAvailable(e,V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION"))}):n=function(){return!0};return{query:e,isFencePassed:n}},r.prototype.downloadMatrixFromPackedTexture=function(t,e,n){var o=this;return this.downloadMatrixDriver(t,function(){return gh(o.gl,o.debug,e,n)})},r.prototype.createProgram=function(t){this.throwIfDisposed();var e=this.gl,n=ul(e,this.debug,t),o=nh(e,this.debug),a=cl(e,this.debug);return J(e,this.debug,function(){return e.attachShader(a,o)}),J(e,this.debug,function(){return e.attachShader(a,n)}),ll(e,this.debug,a),this.debug&&Co(e,this.debug,a),this.vertexAttrsAreBound||(this.setProgram(a),this.vertexAttrsAreBound=lh(e,this.debug,this.program,this.vertexBuffer)),a},r.prototype.deleteProgram=function(t){var e=this;this.throwIfDisposed(),t===this.program&&(this.program=null),t!=null&&J(this.gl,this.debug,function(){return e.gl.deleteProgram(t)})},r.prototype.setProgram=function(t){var e=this;this.throwIfDisposed(),this.program=t,this.program!=null&&this.debug&&Co(this.gl,this.debug,this.program),J(this.gl,this.debug,function(){return e.gl.useProgram(t)})},r.prototype.getUniformLocation=function(t,e,n){return n===void 0&&(n=!0),this.throwIfDisposed(),n?gl(this.gl,this.debug,t,e):yl(this.gl,t,e)},r.prototype.getAttributeLocation=function(t,e){var n=this;return this.throwIfDisposed(),J(this.gl,this.debug,function(){return n.gl.getAttribLocation(t,e)})},r.prototype.getUniformLocationNoThrow=function(t,e){return this.throwIfDisposed(),this.gl.getUniformLocation(t,e)},r.prototype.setInputMatrixTexture=function(t,e,n){this.throwIfDisposed(),this.throwIfNoProgram(),xl(this.gl,this.debug,this.program,t,e,n)},r.prototype.setOutputMatrixTexture=function(t,e,n){this.setOutputMatrixTextureDriver(t,n,e)},r.prototype.setOutputPackedMatrixTexture=function(t,e,n){this.throwIfDisposed();var o=$r(e,n),a=o[0],i=o[1];this.setOutputMatrixTextureDriver(t,a,i)},r.prototype.setOutputMatrixWriteRegion=function(t,e,n,o){this.setOutputMatrixWriteRegionDriver(n,t,o,e)},r.prototype.setOutputPackedMatrixWriteRegion=function(t,e,n,o){throw new Error("setOutputPackedMatrixWriteRegion not implemented.")},r.prototype.debugValidate=function(){this.program!=null&&Co(this.gl,this.debug,this.program),Rr(this.gl)},r.prototype.executeProgram=function(){this.throwIfDisposed(),this.throwIfNoProgram();var t=this.gl;this.debug&&this.debugValidate(),J(t,this.debug,function(){return t.drawElements(t.TRIANGLES,6,t.UNSIGNED_SHORT,0)})},r.prototype.blockUntilAllProgramsCompleted=function(){var t=this;this.throwIfDisposed(),J(this.gl,this.debug,function(){return t.gl.finish()})},r.prototype.getQueryTimerExtension=function(){return this.disjointQueryTimerExtension==null&&(this.disjointQueryTimerExtension=kr(this.gl,this.debug,V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION")===2?"EXT_disjoint_timer_query_webgl2":"EXT_disjoint_timer_query")),this.disjointQueryTimerExtension},r.prototype.getQueryTimerExtensionWebGL2=function(){return this.getQueryTimerExtension()},r.prototype.getQueryTimerExtensionWebGL1=function(){return this.getQueryTimerExtension()},r.prototype.beginQuery=function(){if(V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION")===2){var t=this.gl,e=this.getQueryTimerExtensionWebGL2(),n=t.createQuery();return t.beginQuery(e.TIME_ELAPSED_EXT,n),n}var o=this.getQueryTimerExtensionWebGL1(),a=o.createQueryEXT();return o.beginQueryEXT(o.TIME_ELAPSED_EXT,a),a},r.prototype.endQuery=function(){if(V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION")!==2){var t=this.getQueryTimerExtensionWebGL1();t.endQueryEXT(t.TIME_ELAPSED_EXT)}else{var e=this.gl,n=this.getQueryTimerExtensionWebGL2();e.endQuery(n.TIME_ELAPSED_EXT)}},r.prototype.waitForQueryAndGetTime=function(t){return Q(this,void 0,void 0,function(){var e=this;return Z(this,function(n){switch(n.label){case 0:return[4,si(function(){return e.disposed||e.isQueryAvailable(t,V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION"))})];case 1:return n.sent(),[2,this.getQueryTime(t,V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_VERSION"))]}})})},r.prototype.getQueryTime=function(t,e){if(e===0)return null;if(e===2){var n=this.gl;return n.getQueryParameter(t,n.QUERY_RESULT)/1e6}var o=this.getQueryTimerExtensionWebGL1();return o.getQueryObjectEXT(t,o.QUERY_RESULT_EXT)/1e6},r.prototype.isQueryAvailable=function(t,e){if(e===0)return!0;if(e===2){var n=this.gl,o=this.getQueryTimerExtensionWebGL2(),a=n.getQueryParameter(t,n.QUERY_RESULT_AVAILABLE);return this.disjoint==null&&(this.disjoint=this.gl.getParameter(o.GPU_DISJOINT_EXT)),a&&!this.disjoint}return a=(o=this.getQueryTimerExtensionWebGL1()).getQueryObjectEXT(t,o.QUERY_RESULT_AVAILABLE_EXT),this.disjoint==null&&(this.disjoint=this.gl.getParameter(o.GPU_DISJOINT_EXT)),a&&!this.disjoint},r.prototype.pollFence=function(t){var e=this;return new Promise(function(n){e.addItemToPoll(function(){return t.isFencePassed()},function(){return n()})})},r.prototype.pollItems=function(){for(var t=function(n){for(var o=0;o<n.length&&n[o]();++o);return o-1}(this.itemsToPoll.map(function(n){return n.isDoneFn})),e=0;e<=t;++e)(0,this.itemsToPoll[e].resolveFn)();this.itemsToPoll=this.itemsToPoll.slice(t+1)},r.prototype.addItemToPoll=function(t,e){var n=this;this.itemsToPoll.push({isDoneFn:t,resolveFn:e}),this.itemsToPoll.length>1||si(function(){return n.pollItems(),n.itemsToPoll.length===0})},r.prototype.bindTextureToFrameBuffer=function(t){this.throwIfDisposed(),_o(this.gl,this.debug,t,this.framebuffer),this.debug&&Rr(this.gl)},r.prototype.unbindTextureToFrameBuffer=function(){this.outputTexture!=null?(_o(this.gl,this.debug,this.outputTexture,this.framebuffer),this.debug&&Rr(this.gl)):vi(this.gl,this.debug,this.framebuffer)},r.prototype.downloadMatrixDriver=function(t,e){this.bindTextureToFrameBuffer(t);var n=e();return this.unbindTextureToFrameBuffer(),n},r.prototype.setOutputMatrixTextureDriver=function(t,e,n){this.throwIfDisposed();var o=this.gl;_o(o,this.debug,t,this.framebuffer),this.debug&&Rr(o),this.outputTexture=t,J(o,this.debug,function(){return o.viewport(0,0,e,n)}),J(o,this.debug,function(){return o.scissor(0,0,e,n)})},r.prototype.setOutputMatrixWriteRegionDriver=function(t,e,n,o){var a=this;this.throwIfDisposed(),J(this.gl,this.debug,function(){return a.gl.scissor(t,e,n,o)})},r.prototype.throwIfDisposed=function(){if(this.disposed)throw new Error("Attempted to use disposed GPGPUContext.")},r.prototype.throwIfNoProgram=function(){if(this.program==null)throw new Error("No GPU program is currently set.")},r}();function Eu(r,t){if(r.length!==t.length)throw Error("Binary was compiled with "+r.length+" inputs, but was executed with "+t.length+" inputs");r.forEach(function(e,n){var o=e.logicalShape,a=t[n],i=a.shape;if(!Oe(o,i))throw Error("Binary was compiled with different shapes than the current args. Shapes "+o+" and "+i+" must match");if(!e.isUniform||!a.isUniform){var s=e.texShape,u=a.isUniform?null:a.texData.texShape;if(!Oe(s,u))throw Error("Binary was compiled with different texture shapes than the current args. Shape "+s+" and "+u+" must match")}})}var lm=function(r,t,e){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=r;for(var n=e.filterWidth,o=e.inChannels,a=e.strideWidth,i=e.strideHeight,s=e.padInfo,u=e.outWidth,c=e.dilationWidth,l=e.dilationHeight,h=e.dataFormat,f=s.left,d=s.top,p=o*n,v=Qe(),m=h==="channelsLast",g=m?0:1,x=m?1:2,y="",b=0;b<=1;b++)for(var w=0;w<=1;w++)y+=`
          blockIndex = rc.y + `+w+`;
          pos = rc.x + `+b+`;

          if(blockIndex < `+r[1]+" && pos < "+r[0]+`) {
            offsetY = int(blockIndex / (`+u+")) * "+i+" - "+d+`;
            d0 = offsetY + `+l+" * (pos / "+p+`);

            if(d0 < `+t[g]+` && d0 >= 0) {

              offsetX = int(mod(float(blockIndex), `+u+".) * "+a+". - "+f+`.);
              d1 = offsetX + `+c+" * (int(mod(float(pos), "+p+".) / "+o+`.));

              if(d1 < `+t[x]+` && d1 >= 0) {

                ch = int(mod(float(pos), `+o+`.));

                if (`+m+`) {
                  innerDims = vec2(d1, ch);
                  result[`+(2*b+w)+`] = getChannel(
                    getA(d0, int(innerDims.x),
                    int(innerDims.y)), innerDims);
                } else {
                  innerDims = vec2(d0, d1);
                  result[`+(2*b+w)+`] = getChannel(
                    getA(ch, int(innerDims.x),
                    int(innerDims.y)), innerDims);
                }
              }
            }
          }
        `;this.userCode=`
      void main() {
        ivec2 rc = getOutputCoords();

        vec4 result = vec4(0);

        int blockIndex, pos, offsetY, d0, offsetX, d1, ch;
        vec2 innerDims;

        `+y+`

        `+v.output+` = result;
      }
    `},hm=function(r,t,e,n,o){this.variableNames=["x"],this.outputShape=[];var a,i=t,s=r[3]-1;this.outputShape=r;var u="float("+e+") + float("+n+") * sum";a=o===.5?"inversesqrt("+u+")":o===1?"1.0/("+u+")":"exp(log("+u+") * float(-"+o+"));",this.userCode=`
      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int r = coords[1];
        int c = coords[2];
        int d = coords[3];
        float x = getX(b, r, c, d);
        float sum = 0.0;
        for (int j = -`+i+"; j <= "+i+`; j++) {
          int idx = d + j;
          if (idx >= 0 && idx <=  `+s+`) {
            float z = getX(b, r, c, idx);
            sum += z * z;
          }
        }
        float val = x * `+a+`;
        setOutput(val);
      }
    `},fm=function(r,t,e,n,o){this.variableNames=["inputImage","outputImage","dy"],this.outputShape=[],this.outputShape=r,this.depth=r[3],this.depthRadius=t,this.bias=e,this.alpha=n,this.beta=o,this.userCode=`
      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int r = coords[1];
        int c = coords[2];

        float result = 0.0;
        for (int d = 0; d < `+this.depth+`; ++d) {
          int depthBegin = int(max(0.0, float(d - `+t+`)));
          int depthEnd = int(min(float(`+this.depth+`),
              float(d + `+t+` + 1)));

          const int MIN_DEPTH_BEGIN = 0;
          const int MAX_DEPTH_END = `+this.depth+`;

          float norm = 0.0;
          for (int k = MIN_DEPTH_BEGIN; k < MAX_DEPTH_END; ++k) {
            if (k < depthBegin){
              continue;
            }
            else if (k >= depthBegin && k < depthEnd) {
              norm += getInputImage(b, r, c, k) * getInputImage(b, r, c, k);
            }
            else {
              break;
            }
          }

          norm = float(`+n+") * norm + float("+e+`);

          for(int k = MIN_DEPTH_BEGIN; k < MAX_DEPTH_END; ++k){
            if (k < depthBegin){
              continue;
            }
            else if (k >= depthBegin && k < depthEnd){
              float dyi = -2.0 * float(`+n+`)
                * float(`+o+`)
                * getInputImage(b ,r ,c, k) * getOutputImage(b, r, c, d)
                / norm;
              if (k == d) {
                dyi += pow(norm, -1.0 * `+o+`);
              }
              if (k == coords[3]) {
                dyi *= getDy(b, r, c, d);
                result += dyi;
              }
            }
            else {
              break;
            }
          }
      }
      setOutput(result);
      }
    `},dm=function(r,t,e,n,o){this.variableNames=["x"],this.outputShape=[],this.packedInputs=!0,this.packedOutput=!0;var a,i=t,s=r[3]-1;this.outputShape=r;var u="float("+e+") + float("+n+") * sum";a=o===.5?"inversesqrt("+u+")":o===1?"1.0/("+u+")":"exp(log("+u+") * float(-"+o+"));",this.userCode=`
      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords.x;
        int r = coords.y;
        int c = coords.z;
        int d = coords.w;

        bool hasNextCol = d < `+this.outputShape[3]+`;
        bool hasNextRow = c < `+this.outputShape[2]+`;

        vec4 sum = vec4(0.);
        vec4 xFragAtOutputCoords = getX(b, r, c, d);

        vec4 xAtOutputCoords = vec4(
          getChannel(xFragAtOutputCoords, vec2(c, d)),
          hasNextCol ?
            getChannel(xFragAtOutputCoords, vec2(c, d + 1)) : 0.0,
          hasNextRow ?
            getChannel(xFragAtOutputCoords , vec2(c + 1, d)) : 0.0,
          (hasNextRow && hasNextCol) ?
            getChannel(xFragAtOutputCoords, vec2(c + 1, d + 1)) : 0.0
        );

        int firstChannel = d - `+i+`;
        vec2 cache = vec2(0.);
        if(firstChannel >= 0){
          vec4 firstChannelFrag = getX(b, r, c, firstChannel);
          cache.x = getChannel(firstChannelFrag, vec2(c, firstChannel));
            if(hasNextRow){
              cache.y = getChannel(firstChannelFrag, vec2(c + 1, firstChannel));
            }
        }

        ivec2 depth = ivec2(d, d + 1);
        for (int j = - `+i+"; j <= "+i+`; j++) {
          ivec2 idx = depth + j;
          bvec2 aboveLowerBound = greaterThanEqual(idx, ivec2(0));
          bvec2 belowUpperBound = lessThanEqual(idx, ivec2(`+s+`));

          bool depthInRange = aboveLowerBound.x && belowUpperBound.x;
          bool depthPlusOneInRange = aboveLowerBound.y && belowUpperBound.y;

          if(depthInRange || depthPlusOneInRange){
            vec4 z = vec4(0.);
            vec4 xFragAtCurrentDepth;
            z.xz = cache.xy;
            if(depthPlusOneInRange && hasNextCol){
              xFragAtCurrentDepth = idx.y != d ?
                getX(b, r, c, idx.y) : xFragAtOutputCoords;
              z.y = getChannel(xFragAtCurrentDepth, vec2(c, idx.y));
              if(hasNextRow){
                z.w = getChannel(xFragAtCurrentDepth, vec2(c + 1, idx.y));
              }
            }
            cache.xy = z.yw;
            sum += z * z;
          }
        }
        vec4 result = xAtOutputCoords * `+a+`;
        setOutput(result);
      }
    `},pm=function(r){this.variableNames=["dy","maxPos"],this.outputShape=r.inShape;var t=r.strideHeight,e=r.strideWidth,n=r.dilationHeight,o=r.effectiveFilterHeight,a=r.effectiveFilterWidth,i=o-1-r.padInfo.top,s=a-1-r.padInfo.left,u=o*a-1;this.userCode=`
      const ivec2 pads = ivec2(`+i+", "+s+`);

      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int d = coords[3];

        ivec2 dyRCCorner = coords.yz - pads;
        int dyRCorner = dyRCCorner.x;
        int dyCCorner = dyRCCorner.y;

        // Convolve dy(?, ?, d) with pos mask(:, :, d) to get dx(xR, xC, d).
        // ? = to be determined. : = across all values in that axis.
        float dotProd = 0.0;
        for (int wR = 0; wR < `+o+`;
          wR += `+n+`) {
          float dyR = float(dyRCorner + wR) / `+t+`.0;

          if (dyR < 0.0 || dyR >= `+r.outHeight+`.0 || fract(dyR) > 0.0) {
            continue;
          }
          int idyR = int(dyR);

          for (int wC = 0; wC < `+a+`; wC++) {
            float dyC = float(dyCCorner + wC) / `+e+`.0;

            if (dyC < 0.0 || dyC >= `+r.outWidth+`.0 ||
                fract(dyC) > 0.0) {
              continue;
            }
            int idyC = int(dyC);

            float dyValue = getDy(b, idyR, idyC, d);
            int maxPosValue = `+u+` - int(getMaxPos(b, idyR, idyC, d));

            // Get the current value, check it against the value from the
            // position matrix.
            int curPosValue = wR * `+a+` + wC;
            float mask = float(maxPosValue == curPosValue ? 1.0 : 0.0);

            dotProd += dyValue * mask;
          }
        }
        setOutput(dotProd);
      }
    `},vm=function(r){this.variableNames=["dy","maxPos"],this.outputShape=r.inShape;var t=r.strideDepth,e=r.strideHeight,n=r.strideWidth,o=r.dilationDepth,a=r.dilationHeight,i=r.dilationWidth,s=r.effectiveFilterDepth,u=r.effectiveFilterHeight,c=r.effectiveFilterWidth,l=s-1-r.padInfo.front,h=u-1-r.padInfo.top,f=c-1-r.padInfo.left,d=s*u*c-1;this.userCode=`
      const ivec3 pads = ivec3(`+l+", "+h+", "+f+`);

      void main() {
        ivec5 coords = getOutputCoords();
        int batch = coords.x;
        int ch = coords.u;

        ivec3 dyCorner = ivec3(coords.y, coords.z, coords.w) - pads;
        int dyDCorner = dyCorner.x;
        int dyRCorner = dyCorner.y;
        int dyCCorner = dyCorner.z;

        // Convolve dy(?, ?, ?, ch) with pos mask(:, :, :, d) to get
        // dx(xD, xR, xC, ch).
        // ? = to be determined. : = across all values in that axis.
        float dotProd = 0.0;

        for (int wD = 0; wD < `+s+`;
           wD += `+o+`) {
          float dyD = float(dyDCorner + wD) / `+t+`.0;

          if (dyD < 0.0 || dyD >= `+r.outDepth+`.0 || fract(dyD) > 0.0) {
            continue;
          }
          int idyD = int(dyD);

          for (int wR = 0; wR < `+u+`;
              wR += `+a+`) {
            float dyR = float(dyRCorner + wR) / `+e+`.0;

            if (dyR < 0.0 || dyR >= `+r.outHeight+`.0 ||
                fract(dyR) > 0.0) {
              continue;
            }
            int idyR = int(dyR);

            for (int wC = 0; wC < `+c+`;
                wC += `+i+`) {
              float dyC = float(dyCCorner + wC) / `+n+`.0;

              if (dyC < 0.0 || dyC >= `+r.outWidth+`.0 ||
                  fract(dyC) > 0.0) {
                continue;
              }
              int idyC = int(dyC);

              float dyValue = getDy(batch, idyD, idyR, idyC, ch);
              int maxPosValue = `+d+` -
                  int(getMaxPos(batch, idyD, idyR, idyC, ch));

              // Get the current value, check it against the value from the
              // position matrix.
              int curPosValue =
                  wD * `+u+" * "+c+` +
                  wR * `+c+` + wC;
              float mask = float(maxPosValue == curPosValue ? 1.0 : 0.0);

              dotProd += dyValue * mask;
            }
          }
        }
        setOutput(dotProd);
      }
    `},Ba=function(r,t,e,n,o,a,i){e===void 0&&(e=!1),n===void 0&&(n=!1),o===void 0&&(o=!1),a===void 0&&(a=null),i===void 0&&(i=!1),this.variableNames=["matrixA","matrixB"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=t;var s=e?r[1]:r[2],u=Math.ceil(s/2),c=e?"i * 2, rc.y":"rc.y, i * 2",l=n?"rc.z, i * 2":"i * 2, rc.z",h=e?["a.xxyy","a.zzww"]:["a.xxzz","a.yyww"],f=n?["b.xzxz","b.ywyw"]:["b.xyxy","b.zwzw"],d="",p="";a&&(d=i?`vec4 activation(vec4 a) {
          vec4 b = getPreluActivationWeightsAtOutCoords();
          `+a+`
        }`:`vec4 activation(vec4 x) {
          `+a+`
        }`,p="result = activation(result);");var v=o?"result += getBiasAtOutCoords();":"";o&&this.variableNames.push("bias"),i&&this.variableNames.push("preluActivationWeights"),this.userCode=`
      `+d+`

      const float sharedDimension = `+u+`.0;

      vec4 dot2x2ARowBCol(ivec3 rc) {
        vec4 result = vec4(0);
        for (int i = 0; i < `+u+`; i++) {
          vec4 a = getMatrixA(rc.x, `+c+`);
          vec4 b = getMatrixB(rc.x, `+l+`);

          // These swizzled products need to be separately added.
          // See: https://github.com/tensorflow/tfjs/issues/1735
          result += (`+h[0]+" * "+f[0]+`);
          result += (`+h[1]+" * "+f[1]+`);
        }
        return result;
      }

      void main() {
        ivec3 rc = getOutputCoords();
        vec4 result = dot2x2ARowBCol(rc);

        `+v+`

        `+p+`

        setOutput(result);
      }
    `},mm=function(){function r(t,e,n){this.variableNames=["probs"],this.outputShape=[t,n],this.userCode=`
      uniform float seed;

      void main() {
        ivec2 coords = getOutputCoords();
        int batch = coords[0];

        float r = random(seed);
        float cdf = 0.0;

        for (int i = 0; i < `+(e-1)+`; i++) {
          cdf += getProbs(batch, i);

          if (r < cdf) {
            setOutput(float(i));
            return;
          }
        }

        // If no other event happened, last event happened.
        setOutput(float(`+(e-1)+`));
      }
    `}return r.prototype.getCustomSetupFunc=function(t){var e=this;return function(n,o){e.seedLoc==null&&(e.seedLoc=n.getUniformLocation(o,"seed")),n.gl.uniform1f(e.seedLoc,t)}},r}(),gm=function(r,t,e,n){this.variableNames=["indices"],this.outputShape=[r,t],this.userCode=`
      void main() {
        ivec2 coords = getOutputCoords();
        int index = round(getIndices(coords.x));
        setOutput(mix(float(`+n+"), float("+e+`),
                      float(index == coords.y)));
      }
    `},ym=function(r){this.variableNames=["A"],this.packedInputs=!1,this.packedOutput=!0,this.outputShape=r;var t=r.length;if(t===0)this.userCode=`
        void main() {
          setOutput(vec4(getA(), 0., 0., 0.));
        }
      `;else{var e=ht("rc",t),n=Se(t),o=function(s,u,c){if(s===1)return"rc > "+u[0];for(var l="",h=s-2;h<s;h++)l+=c[h]+" >= "+u[h],h<s-1&&(l+="||");return l}(t,r,e),a=function(s,u,c,l){if(s===1)return"";var h=l.slice(-2);return`
    int r = `+h[0]+`;
    int c = `+h[1]+`;
    int rp1 = r + 1;
    int cp1 = c + 1;

    bool cEdge = cp1 >= `+u+`;
    bool rEdge = rp1 >= `+c+`;
  `}(t,r[r.length-1],r[r.length-2],e),i=function(s,u){var c=s.length,l=function(h,f){for(var d=[],p=0;p<=1;p++)for(var v=0;v<=1;v++){for(var m=(p===0?"r":"rp1")+", "+(v===0?"c":"cp1"),g=2;g<h;g++)m=f[f.length-1-g]+","+m;d.push(m)}return d}(c,u);return c===1?`getA(rc),
            rc + 1 >= `+s[0]+` ? 0. : getA(rc + 1),
            0, 0`:"getA("+l[0]+`),
          cEdge ? 0. : getA(`+l[1]+`),
          rEdge ? 0. : getA(`+l[2]+`),
          rEdge || cEdge ? 0. : getA(`+l[3]+")"}(r,e);this.userCode=`
        void main() {
          `+n+` rc = getOutputCoords();

          if(`+o+`) {
            setOutput(vec4(0));
          } else {
            `+a+`

            setOutput(vec4(`+i+`));
          }
        }
      `}},xm=function(r,t,e){this.variableNames=["x"],this.outputShape=t.map(function(u,c){return u[0]+r[c]+u[1]});var n=r.length,o=Se(n),a=t.map(function(u){return u[0]}).join(","),i=t.map(function(u,c){return u[0]+r[c]}).join(","),s=["coords[0]","coords[1]","coords[2]","coords[3]"].slice(0,n);this.userCode=n!==1?`
      `+o+" start = "+o+"("+a+`);
      `+o+" end = "+o+"("+i+`);

      void main() {
        `+o+` outC = getOutputCoords();
        if (any(lessThan(outC, start)) || any(greaterThanEqual(outC, end))) {
          setOutput(float(`+e+`));
        } else {
          `+o+` coords = outC - start;
          setOutput(getX(`+s+`));
        }
      }
    `:`
        int start = `+a+`;
        int end = `+i+`;

        void main() {
          int outC = getOutputCoords();
          if (outC < start || outC >= end) {
            setOutput(float(`+e+`));
          } else {
            setOutput(getX(outC - start));
          }
        }
      `},bm=function(r,t,e){this.variableNames=["x"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=t.map(function(m,g){return m[0]+r[g]+m[1]});for(var n=r.length,o=Se(n),a=t.map(function(m){return m[0]}).join(","),i=t.map(function(m,g){return m[0]+r[g]}).join(","),s=ht("rc",n),u=ht("source",n),c=s[n-1]+" < "+this.outputShape[n-1],l=n===1?"source":"vec2("+u.slice(-2).join()+")",h=[o+" rc = outputLoc;",s[n-1]+` += 1;
       if(`+c+`) {
      `,n===1?"":`}
       rc = outputLoc;
       `+s[n-2]+` += 1;
       if(`+s[n-2]+" < "+this.outputShape[n-2]+") {",n===1?"":"  "+s[n-1]+` += 1;
         if(`+c+") {"],f=n===1?"rc < start || rc >= end":"any(lessThan(rc, start)) || any(greaterThanEqual(rc, end))",d="",p=0,v=n===1?2:4;p<v;p++)d+=`
        `+h[p]+`
        if (`+f+`) {
          result[`+p+"] = float("+e+`);
        } else {
          `+o+` source = rc - start;
          result[`+p+"] = getChannel(getX("+u.join()+"), "+l+`);
        }
      `;d+=n===1?"} ":"}}",this.userCode=`
      const `+o+" start = "+o+"("+a+`);
      const `+o+" end = "+o+"("+i+`);

      void main() {
        `+o+` outputLoc = getOutputCoords();
        vec4 result = vec4(0.);
        `+d+`
        setOutput(result);
      }
    `},La=function(r,t,e){if(this.variableNames=["x"],t==="avg"&&e)throw new Error("Cannot compute positions for average pool.");var n=r.filterWidth,o=r.strideHeight,a=r.strideWidth,i=r.dilationHeight,s=r.dilationWidth,u=r.effectiveFilterHeight,c=r.effectiveFilterWidth,l=r.padInfo.top,h=r.padInfo.left;this.outputShape=r.outShape;var f=t==="avg",d="0.0";if(f||(d="-1.0 / 1e-20"),e)this.userCode=`
        const ivec2 strides = ivec2(`+o+", "+a+`);
        const ivec2 pads = ivec2(`+l+", "+h+`);

        void main() {
          ivec4 coords = getOutputCoords();
          int batch = coords[0];
          int d = coords[3];

          ivec2 xRCCorner = coords.yz * strides - pads;
          int xRCorner = xRCCorner.x;
          int xCCorner = xRCCorner.y;

          // max/min x(?, ?, d) to get y(yR, yC, d).
          // ? = to be determined
          float minMaxValue = 0.0;
          float minMaxValueFound = 0.0;
          int minMaxPosition = 0;
          float avgValue = 0.0;

          for (int wR = 0; wR < `+u+`;
              wR += `+i+`) {
            int xR = xRCorner + wR;

            if (xR < 0 || xR >= `+r.inHeight+`) {
              continue;
            }

            for (int wC = 0; wC < `+c+`;
                wC += `+s+`) {
              int xC = xCCorner + wC;

              if (xC < 0 || xC >= `+r.inWidth+`) {
                continue;
              }

              float value = getX(batch, xR, xC, d);

              // If a min / max value has already been found, use it. If not,
              // use the current value.
              float currMinMaxValue = mix(
                  value, minMaxValue, minMaxValueFound);
              if (value >= currMinMaxValue) {
                minMaxValue = value;
                minMaxValueFound = 1.0;
                minMaxPosition = wR * `+c+` + wC;
              }
            }
          }
          setOutput(float(minMaxPosition));
        }
      `;else{var p=t+"("+t+"("+t+"(minMaxValue[0], minMaxValue[1]), minMaxValue[2]), minMaxValue[3])";t==="avg"&&(p="avgValue / count");var v=4*Math.floor(n/4),m=n%4,g=`
      if (`+f+`) {
        avgValue += dot(values, ones);
      } else {
        minMaxValue = max(values, minMaxValue);
      }
    `;this.userCode=`
      const ivec2 strides = ivec2(`+o+", "+a+`);
      const ivec2 pads = ivec2(`+l+", "+h+`);
      const float initializationValue = `+d+`;
      const vec4 ones = vec4(1.0, 1.0, 1.0, 1.0);

      float count = 0.0;

      float getValue(int batch, int xR, int xC, int d) {
        if (xC < 0 || xC >= `+r.inWidth+`) {
          return initializationValue;
        }
        count += 1.0;
        return getX(batch, xR, xC, d);
      }

      void main() {
        ivec4 coords = getOutputCoords();
        int batch = coords[0];
        int d = coords[3];

        ivec2 xRCCorner = coords.yz * strides - pads;
        int xRCorner = xRCCorner.x;
        int xCCorner = xRCCorner.y;

        // max/min x(?, ?, d) to get y(yR, yC, d).
        // ? = to be determined
        vec4 minMaxValue = vec4(`+d+`);
        float avgValue = 0.0;
        count = 0.0;

        for (int wR = 0; wR < `+u+`;
            wR += `+i+`) {
          int xR = xRCorner + wR;

          if (xR < 0 || xR >= `+r.inHeight+`) {
            continue;
          }

          for (int wC = 0; wC < `+v+`; wC += 4) {
            int xC = xCCorner + wC * `+s+`;

            vec4 values = vec4(
              getValue(batch, xR, xC, d),
              getValue(batch, xR, xC + `+s+`, d),
              getValue(batch, xR, xC + 2 * `+s+`, d),
              getValue(batch, xR, xC + 3 * `+s+`, d)
            );

            `+g+`
          }

          int xC = xCCorner + `+v+`;
          if (`+(m===1)+`) {
            vec4 values = vec4(
              getValue(batch, xR, xC, d),
              initializationValue,
              initializationValue,
              initializationValue
            );

            `+g+`
          } else if (`+(m===2)+`) {
            vec4 values = vec4(
              getValue(batch, xR, xC, d),
              getValue(batch, xR, xC + `+s+`, d),
              initializationValue,
              initializationValue
            );

            `+g+`
          } else if (`+(m===3)+`) {
            vec4 values = vec4(
              getValue(batch, xR, xC, d),
              getValue(batch, xR, xC + `+s+`, d),
              getValue(batch, xR, xC + 2 * `+s+`, d),
              initializationValue
            );

            `+g+`
          }
        }
        setOutput(`+p+`);
      }
    `}},Wa=function(r,t,e){if(this.variableNames=["x"],t==="avg"&&e)throw new Error("Cannot compute positions for average pool.");var n=r.filterWidth,o=r.strideDepth,a=r.strideHeight,i=r.strideWidth,s=r.dilationDepth,u=r.dilationHeight,c=r.dilationWidth,l=r.effectiveFilterDepth,h=r.effectiveFilterHeight,f=r.effectiveFilterWidth,d=r.padInfo.front,p=r.padInfo.top,v=r.padInfo.left;this.outputShape=r.outShape;var m=t==="avg",g="0.0";if(m||(g="-1.0 / 1e-20"),e)this.userCode=`
        const ivec3 strides =
            ivec3(`+o+", "+a+", "+i+`);
        const ivec3 pads = ivec3(`+d+", "+p+", "+v+`);

        void main() {
          ivec5 coords = getOutputCoords();
          int batch = coords.x;
          int ch = coords.u;

          ivec3 xCorner = ivec3(coords.y, coords.z, coords.w) * strides - pads;
          int xDCorner = xCorner.x;
          int xRCorner = xCorner.y;
          int xCCorner = xCorner.z;

          // max/min x(?, ?, ?, ch) to get y(yD, yR, yC, ch).
          // ? = to be determined
          float minMaxValue = 0.0;
          float minMaxValueFound = 0.0;
          int minMaxPosition = 0;

          for (int wD = 0; wD < `+l+`;
              wD += `+s+`) {
            int xD = xDCorner + wD;

            if (xD < 0 || xD >= `+r.inDepth+`) {
              continue;
            }

            for (int wR = 0; wR < `+h+`;
                wR += `+u+`) {
              int xR = xRCorner + wR;

              if (xR < 0 || xR >= `+r.inHeight+`) {
                continue;
              }

              for (int wC = 0; wC < `+f+`;
                  wC += `+c+`) {
                int xC = xCCorner + wC;

                if (xC < 0 || xC >= `+r.inWidth+`) {
                  continue;
                }

                float value = getX(batch, xD, xR, xC, ch);

                // If a min / max value has already been found, use it. If not,
                // use the current value.
                float currMinMaxValue = mix(
                    value, minMaxValue, minMaxValueFound);
                if (value >= currMinMaxValue) {
                  minMaxValue = value;
                  minMaxValueFound = 1.0;
                  minMaxPosition =
                      wD * `+h+" * "+f+` +
                      wR * `+f+` + wC;;
                }
              }
            }
          }
          setOutput(float(minMaxPosition));
        }
      `;else{var x=t+"("+t+"("+t+"(minMaxValue[0], minMaxValue[1]), minMaxValue[2]), minMaxValue[3])";t==="avg"&&(x="avgValue / count");var y=4*Math.floor(n/4),b=n%4,w=`
      if (`+m+`) {
        avgValue += dot(values, ones);
      } else {
        minMaxValue = max(values, minMaxValue);
      }
    `;this.userCode=`
      const ivec3 strides =
        ivec3(`+o+", "+a+", "+i+`);
      const ivec3 pads = ivec3(`+d+", "+p+", "+v+`);
      const float initializationValue = `+g+`;
      const vec4 ones = vec4(1.0, 1.0, 1.0, 1.0);

      float count = 0.0;

      float getValue(int batch, int xD, int xR, int xC, int ch) {
        if (xC < 0 || xC >= `+r.inWidth+`) {
          return initializationValue;
        }
        count += 1.0;
        return getX(batch, xD, xR, xC, ch);
      }

      void main() {
        ivec5 coords = getOutputCoords();
        int batch = coords.x;
        int ch = coords.u;

        ivec3 xCorner = ivec3(coords.y, coords.z, coords.w) * strides - pads;
        int xDCorner = xCorner.x;
        int xRCorner = xCorner.y;
        int xCCorner = xCorner.z;

        // max/min x(?, ?, ?, d) to get y(yD, yR, yC, ch).
        // ? = to be determined
        vec4 minMaxValue = vec4(`+g+`);
        float avgValue = 0.0;
        count = 0.0;

        for (int wD = 0; wD < `+l+`;
            wD += `+s+`) {
          int xD = xDCorner + wD;

          if (xD < 0 || xD >= `+r.inDepth+`) {
            continue;
          }

          for (int wR = 0; wR < `+h+`;
            wR += `+u+`) {
            int xR = xRCorner + wR;

            if (xR < 0 || xR >= `+r.inHeight+`) {
              continue;
            }

            for (int wC = 0; wC < `+y+`; wC += 4) {
              int xC = xCCorner + wC * `+c+`;

              vec4 values = vec4(
                getValue(batch, xD, xR, xC, ch),
                getValue(batch, xD, xR, xC + `+c+`, ch),
                getValue(batch, xD, xR, xC + 2 * `+c+`, ch),
                getValue(batch, xD, xR, xC + 3 * `+c+`, ch)
              );

              `+w+`
            }

            int xC = xCCorner + `+y+`;
            if (`+(b===1)+`) {
              vec4 values = vec4(
                getValue(batch, xD, xR, xC, ch),
                initializationValue,
                initializationValue,
                initializationValue
              );

              `+w+`
            } else if (`+(b===2)+`) {
              vec4 values = vec4(
                getValue(batch, xD, xR, xC, ch),
                getValue(batch, xD, xR, xC + `+c+`, ch),
                initializationValue,
                initializationValue
              );

              `+w+`
            } else if (`+(b===3)+`) {
              vec4 values = vec4(
                getValue(batch, xD, xR, xC, ch),
                getValue(batch, xD, xR, xC + `+c+`, ch),
                getValue(batch, xD, xR, xC + 2 * `+c+`, ch),
                initializationValue
              );

              `+w+`
            }
          }
          setOutput(`+x+`);
        }
      }
    `}},wm=function(r,t){this.variableNames=["x"];var e=r.windowSize,n=r.batchSize,o=r.inSize,a=Math.ceil(o/e);this.outputShape=[n,a];var i="0.0",s="";t==="prod"?i="1.0":t==="min"?(i="1.0 / 1e-20",s="min"):t==="max"&&(i="-1.0 / 1e-20",s="max");var u=t+"("+t+"("+t+"(minMaxValue[0], minMaxValue[1]), minMaxValue[2]), minMaxValue[3])";t==="sum"?u="sumValue":t==="prod"?u="prodValue":t==="all"?u="allValue":t==="any"&&(u="anyValue");var c=4*Math.floor(e/4),l=e%4,h=`
      if (`+(t==="sum")+`) {
        sumValue += dot(values, ones);
      } else if (`+(t==="prod")+`) {
        vec2 tmp = vec2(values[0], values[1]) * vec2(values[2], values[3]);
        prodValue *= tmp[0] * tmp[1];
      } else {
        minMaxValue = `+s+`(values, minMaxValue);
      }
    `,f="vec4";t==="all"?(i="1.0",h=`
        bool reducedAllValue = all(values);
        float floatedReducedAllValue = float(reducedAllValue);
        allValue = float(allValue >= 1.0 && floatedReducedAllValue >= 1.0);
      `,f="bvec4"):t==="any"&&(i="0.0",h=`
        bool reducedAnyValue = any(values);
        float floatedReducedAnyValue = float(reducedAnyValue);
        anyValue = float(anyValue >= 1.0 || floatedReducedAnyValue >= 1.0);
      `,f="bvec4");var d="";o%e>0&&(d=`
        if (inIdx < 0 || inIdx >= `+o+`) {
          return initializationValue;
        }
      `),this.userCode=`
      const float initializationValue = `+i+`;
      const vec4 ones = vec4(1.0, 1.0, 1.0, 1.0);

      float getValue(int batch, int inIdx) {
        `+d+`
        return getX(batch, inIdx);
      }

      void main() {
        ivec2 coords = getOutputCoords();
        int batch = coords[0];
        int outIdx = coords[1];
        int inOffset = outIdx * `+e+`;

        vec4 minMaxValue = vec4(`+i+`);
        float prodValue = 1.0;
        float sumValue = 0.0;
        float allValue = 1.0;
        float anyValue = 0.0;

        for (int i = 0; i < `+c+`; i += 4) {
          int inIdx = inOffset + i;
          `+f+" values = "+f+`(
            getValue(batch, inIdx),
            getValue(batch, inIdx + 1),
            getValue(batch, inIdx + 2),
            getValue(batch, inIdx + 3)
          );

          `+h+`
        }

        int inIdx = inOffset + `+c+`;
        if (`+(l===1)+`) {
          `+f+" values = "+f+`(
            getValue(batch, inIdx),
            initializationValue,
            initializationValue,
            initializationValue
          );

          `+h+`
        } else if (`+(l===2)+`) {
          `+f+" values = "+f+`(
            getValue(batch, inIdx),
            getValue(batch, inIdx + 1),
            initializationValue,
            initializationValue
          );

          `+h+`
        } else if (`+(l===3)+`) {
          `+f+" values = "+f+`(
            getValue(batch, inIdx),
            getValue(batch, inIdx + 1),
            getValue(batch, inIdx + 2),
            initializationValue
          );

          `+h+`
        }
        setOutput(`+u+`);
      }
    `},Cm=function(r,t){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=r;for(var e="",n=0;n<4;n++){var o="thisRC = rc;";n%2==1&&(o+="thisRC.z += 1;"),n>1&&(o+="thisRC.y += 1;"),e+=`
        `+o+`
        `+(n>0?"if(thisRC.y < rows && thisRC.z < cols){":"")+`
          int flatIndex = getFlatIndex(thisRC);

          ivec3 inputRC = inputCoordsFromReshapedOutCoords(flatIndex);
          vec2 inputRCInnerDims = vec2(float(inputRC.y),float(inputRC.z));

          result[`+n+`] =
            getChannel(getA(inputRC.x, inputRC.y, inputRC.z), inputRCInnerDims);
        `+(n>0?"}":"")+`
      `}this.userCode=`
      
    ivec3 inputCoordsFromReshapedOutCoords(int index) {
      `+Mn(["r","c","d"],t)+`
      return ivec3(r, c, d);
    }
  
      `+hs(r)+`

      void main() {
        ivec3 rc = getOutputCoords();

        vec4 result = vec4(0.);

        ivec3 thisRC;
        int rows = `+r[1]+`;
        int cols = `+r[2]+`;

        `+e+`

        setOutput(result);
      }
    `},_m=function(r,t,e){this.variableNames=["dy"],this.outputShape=[],this.outputShape=t.shape;var n=t.shape,o=n[1],a=n[2],i=r.shape,s=i[1],u=i[2],c=[e&&s>1?o-1:o,e&&u>1?a-1:a],l=[e&&s>1?s-1:s,e&&u>1?u-1:u],h=c[0]/l[0],f=c[1]/l[1],d=1/h,p=1/f,v=2*Math.ceil(d)+2,m=2*Math.ceil(p)+2;this.userCode=`
      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int d = coords[3];
        int r = coords[1];
        int c = coords[2];

        float accumulator = 0.0;

        const float heightScale = float(`+h+`);
        const float widthScale = float(`+f+`);

        const float invHeightScale = float(`+d+`);
        const float invWidthScale = float(`+p+`);

        const int winHeight = int(`+v+`);
        const int winWidth = int(`+m+`);

        // Compute bounds for where in dy we will look
        float startRLerp = floor(float(r) * invHeightScale);
        int startDyR = int(startRLerp - float(winHeight / 2));

        float startCLerp = floor(float(c) * invWidthScale);
        int startDyC = int(startCLerp - float(winWidth / 2));

        // Loop over dy
        for (int dyROffset = 0; dyROffset < winHeight; dyROffset++) {
          int dyR = dyROffset + startDyR;

          // Guard against the window exceeding the bounds of dy
          if (dyR < 0 || dyR >= `+s+`) {
            continue;
          }

          for (int dyCOffset = 0; dyCOffset < winWidth; dyCOffset++) {
            int dyC = dyCOffset + startDyC;

            // Guard against the window exceeding the bounds of dy
            if (dyC < 0 || dyC >= `+u+`) {
              continue;
            }

            float dxR = float(dyR) * heightScale;
            int topDxRIndex = int(floor(dxR));
            int bottomDxRIndex = int(min(ceil(dxR), `+(o-1)+`.0));
            float dxRLerp = dxR - float(topDxRIndex);
            float inverseDxRLerp = 1.0 - dxRLerp;

            float dxC = float(dyC) * widthScale;
            int leftDxCIndex = int(floor(dxC));
            int rightDxCIndex = int(min(ceil(dxC), `+(a-1)+`.0));
            float dxCLerp = dxC - float(leftDxCIndex);
            float inverseDxCLerp = 1.0 - dxCLerp;

            if (r == topDxRIndex && c == leftDxCIndex) {
              // topLeft
              accumulator +=
                getDy(b, dyR, dyC, d) * inverseDxRLerp * inverseDxCLerp;
            }

            if (r == topDxRIndex && c == rightDxCIndex) {
              // topRight
              accumulator += getDy(b, dyR, dyC, d) * inverseDxRLerp * dxCLerp;
            }

            if (r == bottomDxRIndex && c == leftDxCIndex) {
              // bottomLeft
              accumulator += getDy(b, dyR, dyC, d) * dxRLerp * inverseDxCLerp;
            }

            if (r == bottomDxRIndex && c == rightDxCIndex) {
              // bottomRight
              accumulator += getDy(b, dyR, dyC, d) * dxRLerp * dxCLerp;
            }
          }
        }
        // End loop over dy

        setOutput(accumulator);
      }
    `},Em=function(r,t,e,n){this.variableNames=["A"],this.outputShape=[];var o=r[0],a=r[1],i=r[2],s=r[3];this.outputShape=[o,t,e,s];var u=[n&&t>1?a-1:a,n&&e>1?i-1:i],c=[n&&t>1?t-1:t,n&&e>1?e-1:e];this.userCode=`
      const vec2 effectiveInputOverOutputRatioRC = vec2(
          `+u[0]/c[0]+`,
          `+u[1]/c[1]+`);
      const vec2 inputShapeRC = vec2(`+a+".0, "+i+`.0);

      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int d = coords[3];
        ivec2 yRC = coords.yz;

        // Fractional source index.
        vec2 sourceFracIndexRC = vec2(yRC) * effectiveInputOverOutputRatioRC;

        // Compute the four integer indices.
        ivec2 sourceFloorRC = ivec2(sourceFracIndexRC);
        ivec2 sourceCeilRC = ivec2(
          min(inputShapeRC - 1.0, ceil(sourceFracIndexRC)));

        float topLeft = getA(b, sourceFloorRC.x, sourceFloorRC.y, d);
        float bottomLeft = getA(b, sourceCeilRC.x, sourceFloorRC.y, d);
        float topRight = getA(b, sourceFloorRC.x, sourceCeilRC.y, d);
        float bottomRight = getA(b, sourceCeilRC.x, sourceCeilRC.y, d);

        vec2 fracRC = sourceFracIndexRC - vec2(sourceFloorRC);

        float top = topLeft + (topRight - topLeft) * fracRC.y;
        float bottom = bottomLeft + (bottomRight - bottomLeft) * fracRC.y;
        float newValue = top + (bottom - top) * fracRC.x;

        setOutput(newValue);
      }
    `},Sm=function(r,t,e,n){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=[];var o=r[0],a=r[1],i=r[2],s=r[3];this.outputShape=[o,t,e,s];var u=[n&&t>1?a-1:a,n&&e>1?i-1:i],c=[n&&t>1?t-1:t,n&&e>1?e-1:e];this.userCode=`
      const vec3 effectiveInputOverOutputRatioRC = vec3(
          `+u[0]/c[0]+`,
          `+u[1]/c[1]+`,
          `+u[1]/c[1]+`);
      const vec3 inputShapeRC = vec3(`+a+".0, "+i+`.0,
                                     `+i+`.0);

      float getAValue(int b, int r, int c, int d) {
        return getChannel(getA(b, r, c, d), vec2(c, d));
      }

      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int d = coords[3];
        // Calculate values for next column in yRC.z.
        ivec3 yRC = coords.yzz + ivec3(0, 0, 1);

        // Fractional source index.
        vec3 sourceFracIndexRC = vec3(yRC) * effectiveInputOverOutputRatioRC;

        // Compute the four integer indices.
        ivec3 sourceFloorRC = ivec3(sourceFracIndexRC);
        ivec3 sourceCeilRC = ivec3(
          min(inputShapeRC - 1.0, ceil(sourceFracIndexRC)));

        // Should we calculate next column and row elements in 2x2 packed cell.
        bool hasNextCol = d < `+(s-1)+`;
        bool hasNextRow = coords.z < `+(e-1)+`;

        // In parallel, construct four corners for all four components in
        // packed 2x2 cell.
        vec4 topLeft = vec4(
          getAValue(b, sourceFloorRC.x, sourceFloorRC.y, d),
          hasNextCol ? getAValue(b, sourceFloorRC.x, sourceFloorRC.y, d + 1)
                     : 0.0,
          hasNextRow ? getAValue(b, sourceFloorRC.x, sourceFloorRC.z, d)
                     : 0.0,
          (hasNextRow && hasNextCol) ?
            getAValue(b, sourceFloorRC.x, sourceFloorRC.z, d + 1) : 0.0);

        vec4 bottomLeft = vec4(
          getAValue(b, sourceCeilRC.x, sourceFloorRC.y, d),
          hasNextCol ? getAValue(b, sourceCeilRC.x, sourceFloorRC.y, d + 1)
                     : 0.0,
          hasNextRow ? getAValue(b, sourceCeilRC.x, sourceFloorRC.z, d)
                     : 0.0,
          (hasNextRow && hasNextCol) ?
            getAValue(b, sourceCeilRC.x, sourceFloorRC.z, d + 1) : 0.0);

        vec4 topRight = vec4(
          getAValue(b, sourceFloorRC.x, sourceCeilRC.y, d),
          hasNextCol ? getAValue(b, sourceFloorRC.x, sourceCeilRC.y, d + 1)
                     : 0.0,
          hasNextRow ? getAValue(b, sourceFloorRC.x, sourceCeilRC.z, d)
                     : 0.0,
          (hasNextRow && hasNextCol) ?
            getAValue(b, sourceFloorRC.x, sourceCeilRC.z, d + 1) : 0.0);

        vec4 bottomRight = vec4(
          getAValue(b, sourceCeilRC.x, sourceCeilRC.y, d),
          hasNextCol ? getAValue(b, sourceCeilRC.x, sourceCeilRC.y, d + 1)
                     : 0.0,
          hasNextRow ? getAValue(b, sourceCeilRC.x, sourceCeilRC.z, d)
                     : 0.0,
          (hasNextRow && hasNextCol) ?
            getAValue(b, sourceCeilRC.x, sourceCeilRC.z, d + 1) : 0.0);

        vec3 fracRC = sourceFracIndexRC - vec3(sourceFloorRC);

        vec4 top = mix(topLeft, topRight, fracRC.yyzz);
        vec4 bottom = mix(bottomLeft, bottomRight, fracRC.yyzz);
        vec4 newValue = mix(top, bottom, fracRC.x);

        setOutput(newValue);
      }
    `},km=function(r,t,e){this.variableNames=["dy"],this.outputShape=[],this.outputShape=t.shape;var n=t.shape,o=n[1],a=n[2],i=r.shape,s=i[1],u=i[2],c=[e&&s>1?o-1:o,e&&u>1?a-1:a],l=[e&&s>1?s-1:s,e&&u>1?u-1:u],h=c[0]/l[0],f=c[1]/l[1],d=1/h,p=1/f,v=2*Math.ceil(d)+2,m=2*Math.ceil(p)+2;this.userCode=`
      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int d = coords[3];
        int r = coords[1];
        int c = coords[2];

        float accumulator = 0.0;

        const float heightScale = float(`+h+`);
        const float widthScale = float(`+f+`);

        const float invHeightScale = float(`+d+`);
        const float invWidthScale = float(`+p+`);

        const int winHeight = int(`+v+`);
        const int winWidth = int(`+m+`);

        // Compute bounds for where in dy we will look
        float startRLerp = floor(float(r) * invHeightScale);
        int startDyR = int(floor(startRLerp - float(winHeight / 2)));

        float startCLerp = floor(float(c) * invWidthScale);
        int startDyC = int(floor(startCLerp - float(winWidth / 2)));

        // Loop over dy
        for (int dyROffset = 0; dyROffset < winHeight; dyROffset++) {
          int dyR = dyROffset + startDyR;

          // Guard against the window exceeding the bounds of dy
          if (dyR < 0 || dyR >= `+s+`) {
            continue;
          }

          for (int dyCOffset = 0; dyCOffset < winWidth; dyCOffset++) {
            int dyC = dyCOffset + startDyC;

            // Guard against the window exceeding the bounds of dy
            if (dyC < 0 || dyC >= `+u+`) {
              continue;
            }

            float sourceFracRow =
              float(`+c[0]+`) *
                (float(dyR) / float(`+l[0]+`));

            float sourceFracCol =
                float(`+c[1]+`) *
                  (float(dyC) / float(`+l[1]+`));

            int sourceNearestRow = int(min(
                float(int(`+o+`) - 1),
                `+e+` ? float(round(sourceFracRow)) :
                                  float(floor(sourceFracRow))));

            int sourceNearestCol = int(min(
                float(int(`+a+`) - 1),
                `+e+` ? float(round(sourceFracCol)) :
                                  float(floor(sourceFracCol))));

            if (r == sourceNearestRow && c == sourceNearestCol) {
              accumulator += getDy(b, dyR, dyC, d);
            }
          }
        }
        // End loop over dy

        setOutput(accumulator);
      }
    `},Rm=function(r,t,e,n){this.variableNames=["A"],this.outputShape=[];var o=r[0],a=r[1],i=r[2],s=r[3];this.outputShape=[o,t,e,s];var u=[n&&t>1?a-1:a,n&&e>1?i-1:i],c=[n&&t>1?t-1:t,n&&e>1?e-1:e],l=n?"0.5":"0.0";this.userCode=`
      const vec2 effectiveInputOverOutputRatioRC = vec2(
          `+u[0]/c[0]+`,
          `+u[1]/c[1]+`);
      const vec2 inputShapeRC = vec2(`+a+".0, "+i+`.0);

      void main() {
        ivec4 coords = getOutputCoords();
        int b = coords[0];
        int d = coords[3];
        ivec2 yRC = coords.yz;

        // Fractional source index.
        vec2 sourceFracIndexRC = vec2(yRC) * effectiveInputOverOutputRatioRC;

        // Compute the coordinators of nearest neighbor point.
        ivec2 sourceNearestRC = ivec2(
          min(inputShapeRC - 1.0, floor(sourceFracIndexRC + `+l+`)));

        float newValue = getA(b, sourceNearestRC.x, sourceNearestRC.y, d);

        setOutput(newValue);
      }
    `},Im=function(r,t){this.variableNames=["x"];var e=r.length;if(e>4)throw new Error("WebGL backend: Reverse of rank-"+e+" tensor is not yet supported");if(this.outputShape=r,e!==1){var n=r.map(function(a,i){return function(s){return t.indexOf(s)!==-1&&r[s]!==1?r[s]+" - coords["+s+"] - 1":"coords["+s+"]"}(i)}).join(","),o=Se(e);this.userCode=`
      void main() {
        `+o+` coords = getOutputCoords();
        setOutput(getX(`+n+`));
      }
    `}else this.userCode=`
        void main() {
          int coord = getOutputCoords();
          setOutput(getX(`+r[0]+` - coord - 1));
        }
      `},Am=function(r,t){this.variableNames=["x"],this.packedInputs=!0,this.packedOutput=!0;var e=r.length;if(e>4)throw new Error("WebGL backend: Reverse of rank-"+e+" tensor is not yet supported");this.outputShape=r;var n=ht("rc",e),o=n[e-1]+" + 1 < "+this.outputShape[e-1],a=n[e-2]+" + 1 < "+this.outputShape[e-2],i=Se(e);function s(u){var c=r.map(function(l,h){return function(f,d){return t.indexOf(f)!==-1&&r[f]!==1?r[f]+" - "+d[f]+" - 1":""+d[f]}(h,u)});return"getChannel(getX("+c.join(",")+"), vec2("+c.slice(-2).join(",")+"))"}this.userCode=e===1?`
        void main(){
          int rc = getOutputCoords();
          vec4 result = vec4(0.);
          result.r = getChannel(getX(`+r[0]+` - rc - 1),
            `+r[0]+` - rc - 1);
          if(`+o+`){
              result.g = getChannel(getX(`+r[0]+` - (rc  + 1) - 1),
                `+r[0]+` - (rc  + 1) - 1);
          }
          setOutput(result);
        }
      `:`
        void main() {
          `+i+` rc = getOutputCoords();
          vec4 result = vec4(0.);
          result.r = `+function(u){return s(u)}(n.slice())+`;
          if(`+o+`){
            result.g = `+function(u){return u[e-1]="("+u[e-1]+" + 1)",s(u)}(n.slice())+`;
          }
          if(`+a+`) {
            result.b = `+function(u){return u[e-2]="("+u[e-2]+" + 1)",s(u)}(n.slice())+`;
            if(`+o+`) {
              result.a = `+function(u){return u[e-1]="("+u[e-1]+" + 1)",u[e-2]="("+u[e-2]+" + 1)",s(u)}(n.slice())+`;
            }
          }
          setOutput(result);
        }
    `},Su=function(r,t,e,n,o,a,i){this.variableNames=["updates","indices","defaultValue"],this.outputShape=a;var s=Se(o.length),u=Se(a.length),c="";e===1?c="i":e===2&&(c="i, j");var l="getIndices("+c+")",h="";n===1?h="i":n===2&&(h="i, coords[1]");var f="getUpdates("+h+")",d=t>1?"strides[j]":"strides";this.userCode=`
        `+s+" strides = "+s+"("+o+`);

        void main() {
          `+u+` coords = getOutputCoords();
          float sum = 0.0;
          bool found = false;
          for (int i = 0; i < `+r+`; i++) {
            int flattenedIndex = 0;
            for (int j = 0; j < `+t+`; j++) {
              int index = round(`+l+`);
              flattenedIndex += index * `+d+`;
            }
            if (flattenedIndex == coords[0]) {
              sum += `+f+`;
              found = true;
            }
          }
          setOutput(mix(getDefaultValue(), sum, float(found)));
        }
      `},Dm=function(r,t){this.variableNames=["x","segmentIds"];var e=r.windowSize,n=r.batchSize,o=r.inSize,a=r.numSegments,i=a*Math.ceil(o/e);this.outputShape=[n,i];var s=4*Math.floor(e/4),u=e%4,c=`
        sumValue += dot(values, segFilter);
    `,l="";o%e>0&&(l=`
        if (inIdx < 0 || inIdx >= `+o+`) {
          return initializationValue;
        }
      `);var h="";o%e>0&&(h=`
        if (inIdx < 0 || inIdx >= `+o+`) {
          return -1.0;
        }
      `),this.userCode=`
      const float initializationValue = 0.0;

      float getValue(int batch, int inIdx) {
        `+l+`
        return getX(batch, inIdx);
      }

      float getSegmentIdAtIndex(int inIdx) {
        `+h+`
        return getSegmentIds(inIdx);
      }

      void main() {
        ivec2 coords = getOutputCoords();
        int batch = coords[0];
        int outIdx = coords[1];
        int inOffset = int(floor(float(outIdx) / float(
          `+a+")) * float("+e+`));
        int currentSeg = int(mod(float(outIdx), float(`+a+`)));

        float sumValue = 0.0;

        for (int i = 0; i < `+s+`; i += 4) {
          int inIdx = inOffset + i;
          vec4 values = vec4(
            getValue(batch, inIdx),
            getValue(batch, inIdx + 1),
            getValue(batch, inIdx + 2),
            getValue(batch, inIdx + 3)
          );

          vec4 segFilter = vec4(
            int(getSegmentIdAtIndex(inIdx)) == currentSeg ? 1 : 0,
            int(getSegmentIdAtIndex(inIdx + 1)) == currentSeg ? 1 : 0,
            int(getSegmentIdAtIndex(inIdx + 2)) == currentSeg ? 1 : 0,
            int(getSegmentIdAtIndex(inIdx + 3)) == currentSeg ? 1 : 0
          );

          `+c+`
        }

        int inIdx = inOffset + `+s+`;
        if (`+(u===1)+`) {
          vec4 values = vec4(
            getValue(batch, inIdx),
            initializationValue,
            initializationValue,
            initializationValue
          );

          int inIdxSeg = int(getSegmentIdAtIndex(inIdx));

          vec4 segFilter = vec4(
            int(getSegmentIdAtIndex(inIdx)) == currentSeg ? 1 : 0,
            0,
            0,
            0
          );

          `+c+`
        } else if (`+(u===2)+`) {
          vec4 values = vec4(
            getValue(batch, inIdx),
            getValue(batch, inIdx + 1),
            initializationValue,
            initializationValue
          );

          vec4 segFilter = vec4(
            int(getSegmentIdAtIndex(inIdx)) == currentSeg ? 1 : 0,
            int(getSegmentIdAtIndex(inIdx + 1)) == currentSeg ? 1 : 0,
              0,
              0
          );

          `+c+`
        } else if (`+(u===3)+`) {
          vec4 values = vec4(
            getValue(batch, inIdx),
            getValue(batch, inIdx + 1),
            getValue(batch, inIdx + 2),
            initializationValue
          );

          vec4 segFilter = vec4(
            int(getSegmentIdAtIndex(inIdx)) == currentSeg ? 1 : 0,
            int(getSegmentIdAtIndex(inIdx + 1)) == currentSeg ? 1 : 0,
            int(getSegmentIdAtIndex(inIdx + 2)) == currentSeg ? 1 : 0,
            0
          );

          `+c+`
        }
        setOutput(sumValue);
      }
    `},Tm=function(r,t,e){var n,o;if(this.variableNames=["c","a","b"],this.outputShape=t,e>4)throw Error("Where for rank "+e+" is not yet supported");if(e===1)o="resRC",n="resRC";else{for(var a=["resRC.x","resRC.y","resRC.z","resRC.w"],i=[],s=[],u=0;u<t.length;u++)s.push(""+a[u]),u<r&&i.push(""+a[u]);n=i.join(),o=s.join()}var c=Se(e);this.userCode=`
      void main() {
        `+c+` resRC = getOutputCoords();
        float cVal = getC(`+n+`);
        if (cVal >= 1.0) {
          setOutput(getA(`+o+`));
        } else {
          setOutput(getB(`+o+`));
        }
      }
    `},Nm=function(){function r(t){this.variableNames=["source"],this.outputShape=t,this.rank=t.length;var e,n=Se(this.rank),o="uniform int start["+this.rank+"];",a=function(i){if(i===1)return"sourceLoc";if(i<=6)return Va.slice(0,i).map(function(s){return"sourceLoc."+s}).join(",");throw Error("Slicing for rank "+i+" is not yet supported")}(this.rank);e=`
        `+n+` sourceLoc;
        `+n+` coords = getOutputCoords();
        `+t.map(function(i,s){return"sourceLoc."+Va[s]+" = start["+s+"] + coords."+Va[s]+";"}).join(`
`)+`
      `,this.userCode=`
      `+o+`
      void main() {
        `+e+`
        setOutput(getSource(`+a+`));
      }
    `}return r.prototype.getCustomSetupFunc=function(t){var e=this;if(t.length!==this.rank)throw Error("The rank ("+this.rank+") of the program must match the length of start ("+t.length+")");return function(n,o){e.startLoc==null&&(e.startLoc=n.getUniformLocationNoThrow(o,"start"),e.startLoc==null)||n.gl.uniform1iv(e.startLoc,t)}},r}(),Va=["x","y","z","w","u","v"],Fm=function(){function r(t){this.variableNames=["source"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=t,this.rank=t.length;var e=Se(this.rank),n=ht("coords",this.rank),o=ht("sourceLoc",this.rank),a=this.rank===1?"sourceLoc":"vec2("+o.slice(-2).join()+")",i="getChannel(getSource("+o.join()+"), "+a+")",s=`
      result.x = `+i+`;
      if (++`+n[this.rank-1]+" < "+t[this.rank-1]+`) {
        ++`+o[this.rank-1]+`;
        result.y = `+i+`;
        --`+o[this.rank-1]+`;
      }
    `,u=this.rank===1?"":`
      --`+n[this.rank-1]+`;
      if (++`+n[this.rank-2]+" < "+t[this.rank-2]+`) {
        ++`+o[this.rank-2]+`;
        result.z = `+i+`;
        if (++`+n[this.rank-1]+" < "+t[this.rank-1]+`) {
          ++`+o[this.rank-1]+`;
          result.w = `+i+`;
        }
      }
    `,c=this.rank<=4?`sourceLoc = coords +
            `+e+"("+t.map(function(l,h){return"start["+h+"]"}).join()+");":t.map(function(l,h){return o[h]+" = "+n[h]+" + start["+h+"];"}).join(`
`);this.userCode=`
      uniform int start[`+this.rank+`];
      void main() {
        `+e+` coords = getOutputCoords();
        `+e+` sourceLoc;
        `+c+`
        vec4 result = vec4(0.);
        `+s+`
        `+u+`
        setOutput(result);
      }
    `}return r.prototype.getCustomSetupFunc=function(t){var e=this;if(t.length!==this.rank)throw Error("The rank ("+this.rank+") of the program must match the length of start ("+t.length+")");return function(n,o){e.startLoc==null&&(e.startLoc=n.getUniformLocationNoThrow(o,"start"),e.startLoc==null)||n.gl.uniform1iv(e.startLoc,t)}},r}(),Pm=function(r,t,e){this.variableNames=["x"],this.outputShape=e;var n=e.length,o=Se(e.length),a=Se(e.length),i="";if(n===1)i="coords * strides + begin";else{var s=0;i=e.map(function(u,c){return s++,e.length===1?"coords * strides["+c+"] + begin["+c+"]":"coords["+(s-1)+"] * strides["+c+"] + begin["+c+"]"}).join(",")}this.userCode=`
      `+o+" begin = "+o+"("+r+`);
      `+o+" strides = "+o+"("+t+`);

      void main() {
        `+a+` coords = getOutputCoords();
        setOutput(getX(`+i+`));
      }
    `},Mm=function(){function r(t){this.gpgpu=t,this.numUsedTextures=0,this.numFreeTextures=0,this.freeTextures={},this.logEnabled=!1,this.usedTextures={}}return r.prototype.acquireTexture=function(t,e,n){var o,a=ku(e,n),i=Ru(t,a,n);if(i in this.freeTextures||(this.freeTextures[i]=[]),i in this.usedTextures||(this.usedTextures[i]=[]),this.freeTextures[i].length>0){this.numFreeTextures--,this.numUsedTextures++,this.log();var s=this.freeTextures[i].shift();return this.usedTextures[i].push(s),s}return this.numUsedTextures++,this.log(),a===vt.PACKED_2X2_FLOAT32?o=this.gpgpu.createPackedMatrixTexture(t[0],t[1]):a===vt.PACKED_2X2_FLOAT16?o=this.gpgpu.createFloat16PackedMatrixTexture(t[0],t[1]):a===vt.UNPACKED_FLOAT32?o=this.gpgpu.createFloat32MatrixTexture(t[0],t[1]):a===vt.UNPACKED_FLOAT16?o=this.gpgpu.createFloat16MatrixTexture(t[0],t[1]):a===vt.PACKED_4X1_UNSIGNED_BYTE&&(o=this.gpgpu.createUnsignedBytesMatrixTexture(t[0],t[1])),this.usedTextures[i].push(o),o},r.prototype.releaseTexture=function(t,e,n,o){if(this.freeTextures!=null){var a=Ru(e,ku(n,o),o);a in this.freeTextures||(this.freeTextures[a]=[]),this.freeTextures[a].push(t),this.numFreeTextures++,this.numUsedTextures--;var i=this.usedTextures[a],s=i.indexOf(t);if(s<0)throw new Error("Cannot release a texture that was never provided by this texture manager");i.splice(s,1),this.log()}},r.prototype.log=function(){if(this.logEnabled){var t=this.numFreeTextures+this.numUsedTextures;console.log("Free/Used",this.numFreeTextures+" / "+this.numUsedTextures,"("+t+")")}},r.prototype.getNumUsedTextures=function(){return this.numUsedTextures},r.prototype.getNumFreeTextures=function(){return this.numFreeTextures},r.prototype.dispose=function(){var t=this;if(this.freeTextures!=null){for(var e in this.freeTextures)this.freeTextures[e].forEach(function(n){t.gpgpu.deleteMatrixTexture(n)});for(var e in this.usedTextures)this.usedTextures[e].forEach(function(o){t.gpgpu.deleteMatrixTexture(o)});this.freeTextures=null,this.usedTextures=null,this.numUsedTextures=0,this.numFreeTextures=0}},r}();function ku(r,t){if(r===mt.UPLOAD)return vt.PACKED_2X2_FLOAT32;if(r===mt.RENDER||r==null)return function(e){return V().getBool("WEBGL_RENDER_FLOAT32_ENABLED")?e?vt.PACKED_2X2_FLOAT32:vt.UNPACKED_FLOAT32:e?vt.PACKED_2X2_FLOAT16:vt.UNPACKED_FLOAT16}(t);if(r===mt.DOWNLOAD||r===mt.PIXELS)return vt.PACKED_4X1_UNSIGNED_BYTE;throw new Error("Unknown logical texture type "+r)}function Ru(r,t,e){return r[0]+"_"+r[1]+"_"+t+"_"+e}var Om=function(r,t){this.variableNames=["A"];for(var e=new Array(r.length),n=0;n<e.length;n++)e[n]=r[n]*t[n];this.outputShape=e,this.rank=e.length;var o=Se(this.rank),a=function(i){var s=i.length;if(s>5)throw Error("Tile for rank "+s+" is not yet supported");if(s===1)return"imod(resRC, "+i[0]+")";for(var u=["resRC.x","resRC.y","resRC.z","resRC.w","resRC.u"],c=[],l=0;l<i.length;l++)c.push("imod("+u[l]+", "+i[l]+")");return c.join()}(r);this.userCode=`
      void main() {
        `+o+` resRC = getOutputCoords();
        setOutput(getA(`+a+`));
      }
    `},Bm=function(r,t){this.variableNames=["A"];for(var e=new Array(r.length),n=0;n<e.length;n++)e[n]=r[t[n]];this.outputShape=e,this.rank=e.length;var o=Se(this.rank),a=function(i){var s=i.length;if(s>6)throw Error("Transpose for rank "+s+" is not yet supported");for(var u=["resRC.x","resRC.y","resRC.z","resRC.w","resRC.u","resRC.v"],c=new Array(s),l=0;l<i.length;l++)c[i[l]]=u[l];return c.join()}(t);this.userCode=`
    void main() {
      `+o+` resRC = getOutputCoords();
      setOutput(getA(`+a+`));
    }
    `},Lm=function(r,t){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!0;for(var e=new Array(r.length),n=0;n<e.length;n++)e[n]=r[t[n]];if(this.outputShape=e,this.rank=e.length,this.rank>6)throw Error("Packed transpose for rank "+this.rank+" is not yet supported.");var o=Se(this.rank),a=Jl("rc",this.rank),i=new Array(this.rank);for(n=0;n<t.length;n++)i[t[n]]=a[n];var s="vec2("+i.slice(-2).join()+")",u="++"+a[this.rank-1]+" < "+e[this.rank-1],c="getChannel(getA("+i.join()+"), "+s+")";this.userCode=`
    void main() {
      `+o+` rc = getOutputCoords();
      vec4 result = vec4(0.);
      result[0] = `+c+`;
      if(`+u+`) {
        result[1] = `+c+`;
      }
      --`+a[this.rank-1]+`;
      if(++`+a[this.rank-2]+" < "+e[this.rank-2]+`) {
        result[2] = `+c+`;
        if(`+u+`) {
          result[3] = `+c+`;
        }
      }
      setOutput(result);
    }
    `},fs=1.7580993408473768,ds=1.0507009873554805,fe=function(r,t){this.variableNames=["A"],this.outputShape=r,this.userCode=`
      float unaryOperation(float x) {
        `+t+`
      }

      void main() {
        float x = getAAtOutCoords();
        float y = unaryOperation(x);

        setOutput(y);
      }
    `},Lt="if (isnan(x)) return x;",Wm="return x;",Iu="return abs(x);",xh=Lt+`
  return (x < 0.0) ? 0.0 : x;
`,bh=Lt+`
  return (x < 0.0) ? 0.0 : min(6.0, x);
`,wh="return (x >= 0.0) ? x : (exp(x) - 1.0);",Vm=`
  // Stable and Attracting Fixed Point (0, 1) for Normalized Weights.
  // see: https://arxiv.org/abs/1706.02515
  float scaleAlpha = `+fs+`;
  float scale = `+ds+`;
  return (x >= 0.0) ? scale * x : scaleAlpha * (exp(x) - 1.0);
`,Au="return -x;",Du="return ceil(x);",Tu="return floor(x);",Nu="return exp(x);",Fu="return exp(x) - 1.0;",zm=Lt+`
  return sin(x);
`,Um=Lt+`
  return cos(x);
`,Gm=Lt+`
  if (abs(x) > 1.) {
    return NAN;
  }
  return asin(x);
`,Hm=Lt+`
  if (abs(x) > 1.) {
    return NAN;
  }
  return acos(x);
`,qm=Lt+`
  return atan(x);
`,jm=Lt+"return log(x + sqrt(x * x + 1.0));",Km=Lt+`
  if (x < 1.0) return NAN;
  return log(x + sqrt(x * x - 1.0));`,Xm=Lt+`
  if ((x < -1.0) || (x > 1.0)) return NAN;
  return (log(1.0 + x) - log(1.0 - x)) / 2.0;`,co="return x;",$m="return x;",Ch=`
  vec4 result = x * vec4(greaterThanEqual(x, vec4(0.0)));
  bvec4 isNaN = isnan(x);

  result.r = isNaN.r ? x.r : result.r;
  result.g = isNaN.g ? x.g : result.g;
  result.b = isNaN.b ? x.b : result.b;
  result.a = isNaN.a ? x.a : result.a;

  return result;
`,_h=`
  vec4 result = min(x, vec4(6.)) * vec4(greaterThanEqual(x, vec4(0.0)));
  bvec4 isNaN = isnan(x);

  result.r = isNaN.r ? x.r : result.r;
  result.g = isNaN.g ? x.g : result.g;
  result.b = isNaN.b ? x.b : result.b;
  result.a = isNaN.a ? x.a : result.a;

  return result;
`,Eh=`
  vec4 result;

  result.r = (x.r >= 0.0) ? x.r : (exp(x.r) - 1.0);
  result.g = (x.g >= 0.0) ? x.g : (exp(x.g) - 1.0);
  result.b = (x.b >= 0.0) ? x.b : (exp(x.b) - 1.0);
  result.a = (x.a >= 0.0) ? x.a : (exp(x.a) - 1.0);

  return result;
`,_r=function(r,t){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!0,this.outputShape=r,this.userCode=`
      vec4 unaryOperation(vec4 x) {
        `+t+`
      }

      void main() {
        vec4 x = getAAtOutCoords();
        vec4 y = unaryOperation(x);

        setOutput(y);
      }
    `},Ym=function(r){this.variableNames=["A"],this.packedInputs=!0,this.packedOutput=!1,this.outputShape=r;var t=r.length,e=ht("rc",t),n=Se(t),o=function(s,u){if(s===1)return"rc";for(var c="",l=0;l<s;l++)c+=u[l],l<s-1&&(c+=",");return c}(t,e),a=e.slice(-2),i=t<=1?"rc":"vec2("+a.join(",")+")";this.userCode=`
      void main() {
        `+n+` rc = getOutputCoords();
        vec4 packedInput = getA(`+o+`);

        setOutput(getChannel(packedInput, `+i+`));
      }
    `},lo={};function ho(r,t){if(t===void 0&&(t=!1),r==="linear")return t?$m:Wm;if(r==="relu")return t?Ch:xh;if(r==="elu")return t?Eh:wh;if(r==="relu6")return t?_h:bh;if(r==="prelu")return t?th:eh;throw new Error("Activation "+r+" has not been implemented for the WebGL backend.")}var Jm=600,Sh=function(r){function t(e){var n,o=r.call(this)||this;if(o.pendingRead=new WeakMap,o.pendingDisposal=new WeakSet,o.dataRefCount=new WeakMap,o.numBytesInGPU=0,o.uploadWaitMs=0,o.downloadWaitMs=0,o.warnedAboutMemory=!1,o.pendingDeletes=0,o.disposed=!1,!V().getBool("HAS_WEBGL"))throw new Error("WebGL is not supported on this device");if(e==null){var a=Zt(V().getNumber("WEBGL_VERSION"));o.binaryCache=((n=V().getNumber("WEBGL_VERSION"))in lo||(lo[n]={}),lo[n]),o.gpgpu=new yh(a),o.canvas=a.canvas,o.gpgpuCreatedLocally=!0}else o.gpgpu=e,o.binaryCache={},o.gpgpuCreatedLocally=!1,o.canvas=e.gl.canvas;return o.textureManager=new Mm(o.gpgpu),o.numMBBeforeWarning=V().global.screen==null?1024:V().global.screen.height*V().global.screen.width*window.devicePixelRatio*Jm/1024/1024,o.texData=new ql(o,N),o}return Mt(t,r),t.prototype.numDataIds=function(){return this.texData.numDataIds()+(this.cpuBackend?this.cpuBackend.numDataIds():0)-this.pendingDeletes},t.prototype.write=function(e,n,o){if(V().getBool("DEBUG")&&this.checkNumericalProblems(e),o==="complex64"&&e!=null)throw new Error("Cannot write to a complex64 dtype. Please use tf.complex(real, imag).");var a={};return this.texData.set(a,{shape:n,dtype:o,values:e,usage:mt.UPLOAD}),a},t.prototype.move=function(e,n,o,a){if(V().getBool("DEBUG")&&this.checkNumericalProblems(n),a==="complex64")throw new Error("Cannot write to a complex64 dtype. Please use tf.complex(real, imag).");this.texData.set(e,{shape:o,dtype:a,values:n,usage:mt.UPLOAD})},t.prototype.readSync=function(e){var n=this.texData.get(e),o=n.values,a=n.dtype,i=n.complexTensors,s=n.slice,u=n.shape,c=n.isPacked;if(s!=null){var l=void 0;l=c?new _r(u,co):new fe(u,co);var h=this.runWebGLProgram(l,[{dataId:e,shape:u,dtype:a}],a),f=this.readSync(h.dataId);return this.disposeData(h.dataId),f}if(o!=null)return this.convertAndCacheOnCPU(e);if(a==="string")return o;var d,p,v=this.activeTimers!=null;return v&&(d=Dt()),a==="complex64"?p=wi(i.real.dataSync(),i.imag.dataSync()):p=this.getValuesFromTexture(e),v&&(this.downloadWaitMs+=Dt()-d),this.convertAndCacheOnCPU(e,p)},t.prototype.read=function(e){return Q(this,void 0,void 0,function(){var n,o,a,i,s,u,c,l,h,f,d,p,v,m,g,x,y,b,w,C,I,E;return Z(this,function(S){switch(S.label){case 0:if(this.pendingRead.has(e))return n=this.pendingRead.get(e),[2,new Promise(function(k){return n.push(k)})];if(o=this.texData.get(e),a=o.values,i=o.shape,s=o.slice,u=o.dtype,c=o.complexTensors,l=o.isPacked,s!=null)return h=void 0,h=l?new _r(i,co):new fe(i,co),f=this.runWebGLProgram(h,[{dataId:e,shape:i,dtype:u}],u),d=this.read(f.dataId),this.disposeData(f.dataId),[2,d];if(a!=null)return[2,this.convertAndCacheOnCPU(e)];if(!V().getBool("WEBGL_DOWNLOAD_FLOAT_ENABLED")&&V().getNumber("WEBGL_VERSION")===2)throw new Error("tensor.data() with WEBGL_DOWNLOAD_FLOAT_ENABLED=false and WEBGL_VERSION=2 not yet supported.");return p=null,u!=="complex64"&&V().get("WEBGL_BUFFER_SUPPORTED")&&(v=this.decode(e),m=this.texData.get(v.dataId),p=(E=this.gpgpu).createBufferFromTexture.apply(E,[m.texture].concat(Dr(i)))),this.pendingRead.set(e,[]),u==="complex64"?[3,2]:[4,this.gpgpu.createAndWaitForFence()];case 1:S.sent(),S.label=2;case 2:return u!=="complex64"?[3,4]:[4,Promise.all([c.real.data(),c.imag.data()])];case 3:return x=S.sent(),y=x[0],b=x[1],g=wi(y,b),[3,5];case 4:p==null?g=this.getValuesFromTexture(e):(w=ee(i),g=this.gpgpu.downloadFloat32MatrixFromBuffer(p,w)),S.label=5;case 5:return v!=null&&this.disposeData(v.dataId),C=this.convertAndCacheOnCPU(e,g),I=this.pendingRead.get(e),this.pendingRead.delete(e),I.forEach(function(k){return k(C)}),this.pendingDisposal.has(e)&&(this.pendingDisposal.delete(e),this.disposeData(e),this.pendingDeletes--),[2,C]}})})},t.prototype.checkNumericalProblems=function(e){if(e!=null)for(var n=0;n<e.length;n++){var o=e[n];if(!al(o))throw V().getBool("WEBGL_RENDER_FLOAT32_CAPABLE")?Error("The value "+o+" cannot be represented with your current settings. Consider enabling float32 rendering: 'tf.env().set('WEBGL_RENDER_FLOAT32_ENABLED', true);'"):Error("The value "+o+" cannot be represented on this device.")}},t.prototype.getValuesFromTexture=function(e){var n,o=this.texData.get(e),a=o.shape,i=o.dtype,s=o.isPacked,u=ee(a);if(V().getBool("WEBGL_DOWNLOAD_FLOAT_ENABLED")){var c=this.decode(e),l=this.texData.get(c.dataId),h=(n=this.gpgpu).downloadMatrixFromPackedTexture.apply(n,[l.texture].concat(Dr(a))).subarray(0,u);return this.disposeData(c.dataId),h}var f=V().getBool("WEBGL_PACK")&&s===!0,d=f?Eo(a):a,p=f?new tm(d):new em(d),v=this.runWebGLProgram(p,[{shape:d,dtype:i,dataId:e}],"float32"),m=this.texData.get(v.dataId),g=this.gpgpu.downloadByteEncodedFloatMatrixFromOutputTexture(m.texture,m.texShape[0],m.texShape[1]).subarray(0,u);return this.disposeData(v.dataId),g},t.prototype.time=function(e){return Q(this,void 0,void 0,function(){var n,o,a,i,s,u,c;return Z(this,function(l){switch(l.label){case 0:return n=this.activeTimers,o=[],a=!1,this.programTimersStack==null?(this.programTimersStack=o,a=!0):this.activeTimers.push(o),this.activeTimers=o,e(),i=sn(this.activeTimers.map(function(h){return h.query})).filter(function(h){return h!=null}),s=sn(this.activeTimers.map(function(h){return h.name})).filter(function(h){return h!=null}),this.activeTimers=n,a&&(this.programTimersStack=null),u={uploadWaitMs:this.uploadWaitMs,downloadWaitMs:this.downloadWaitMs,kernelMs:null,wallMs:null},V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_RELIABLE")>0?[4,Promise.all(i)]:[3,2];case 1:return c=l.sent(),u.kernelMs=Gc(c),u.getExtraProfileInfo=function(){return c.map(function(h,f){return{name:s[f],ms:h}}).map(function(h){return h.name+": "+h.ms}).join(", ")},[3,3];case 2:u.kernelMs={error:"WebGL query timers are not supported in this environment."},l.label=3;case 3:return this.uploadWaitMs=0,this.downloadWaitMs=0,[2,u]}})})},t.prototype.memory=function(){return{unreliable:!1,numBytesInGPU:this.numBytesInGPU}},t.prototype.startTimer=function(){return V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_RELIABLE")>0?this.gpgpu.beginQuery():{startMs:Dt(),endMs:null}},t.prototype.endTimer=function(e){return V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_RELIABLE")>0?(this.gpgpu.endQuery(),e):(e.endMs=Dt(),e)},t.prototype.getQueryTime=function(e){return Q(this,void 0,void 0,function(){var n;return Z(this,function(o){return V().getNumber("WEBGL_DISJOINT_QUERY_TIMER_EXTENSION_RELIABLE")>0?[2,this.gpgpu.waitForQueryAndGetTime(e)]:[2,(n=e).endMs-n.startMs]})})},t.prototype.disposeData=function(e){if(!this.pendingDisposal.has(e)){if(this.pendingRead.has(e))return this.pendingDisposal.add(e),void this.pendingDeletes++;if(this.texData.has(e)){this.releaseGPUData(e);var n=this.texData.get(e).complexTensors;n!=null&&(n.real.dispose(),n.imag.dispose()),this.texData.delete(e)}}},t.prototype.releaseGPUData=function(e){var n=this.texData.get(e),o=n.texture,a=n.dtype,i=n.texShape,s=n.usage,u=n.isPacked,c=n.slice,l=c&&c.origDataId||e,h=this.dataRefCount.get(l);h>1?this.dataRefCount.set(l,h-1):(this.dataRefCount.delete(l),o!=null&&(this.numBytesInGPU-=this.computeBytes(i,a),this.textureManager.releaseTexture(o,i,s,u)));var f=this.texData.get(e);f.texture=null,f.texShape=null,f.isPacked=!1,f.slice=null},t.prototype.getTexture=function(e){return this.uploadToGPU(e),this.texData.get(e).texture},t.prototype.getDataInfo=function(e){return this.texData.get(e)},t.prototype.getCPUBackend=function(){return V().getBool("WEBGL_CPU_FORWARD")?(this.cpuBackend==null&&(this.cpuBackend=N.findBackend("cpu")),this.cpuBackend):null},t.prototype.shouldExecuteOnCPU=function(e,n){var o=this;return n===void 0&&(n=128),this.getCPUBackend()!=null&&e.every(function(a){return o.texData.get(a.dataId).texture==null&&a.size<n})},t.prototype.getGPGPUContext=function(){return this.gpgpu},t.prototype.complex=function(e,n){var o=this.makeOutput(e.shape,"complex64");return this.texData.get(o.dataId).complexTensors={real:N.keep(e.clone()),imag:N.keep(n.clone())},o},t.prototype.real=function(e){return this.texData.get(e.dataId).complexTensors.real.clone()},t.prototype.imag=function(e){return this.texData.get(e.dataId).complexTensors.imag.clone()},t.prototype.slice=function(e,n,o){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.slice(e,n,o);if(ee(o)===0)return $e([],o,e.dtype);var a=this.texData.get(e.dataId).isPacked,i=rs(e.shape,n,o);if(a||!i){var s=V().getBool("WEBGL_PACK_ARRAY_OPERATIONS")?new Fm(o):new Nm(o),u=s.getCustomSetupFunc(n);return this.compileAndRun(s,[e],null,u)}return this.uploadToGPU(e.dataId),this.shallowSlice(e,n,o)},t.prototype.shallowSlice=function(e,n,o){var a=this.texData.get(e.dataId),i=this.makeOutput(o,e.dtype),s=this.texData.get(i.dataId);Object.assign(s,a),s.shape=o,s.dtype=e.dtype;var u=os(n,e.strides);a.slice&&(u+=a.slice.flatOffset),s.slice={flatOffset:u,origDataId:a.slice&&a.slice.origDataId||e.dataId};var c=this.dataRefCount.get(s.slice.origDataId)||1;return this.dataRefCount.set(s.slice.origDataId,c+1),i},t.prototype.stridedSlice=function(e,n,o,a){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.stridedSlice(e,n,o,a);var i=ia(n,o,a);if(i.some(function(u){return u===0}))return $e([],i);var s=new Pm(n,a,i);return this.compileAndRun(s,[e])},t.prototype.reverse=function(e,n){var o=V().getBool("WEBGL_PACK_ARRAY_OPERATIONS")?new Am(e.shape,n):new Im(e.shape,n);return this.compileAndRun(o,[e])},t.prototype.concat=function(e,n){if(e[0].dtype==="complex64"){var o=e.map(function(d){return St(d)}),a=e.map(function(d){return jt(d)});return Ke(this.concat(o,n),this.concat(a,n))}if(this.shouldExecuteOnCPU(e))return this.cpuBackend.concat(e,n);if(e.length===1)return e[0];if(e.length>V().getNumber("WEBGL_MAX_TEXTURES_IN_SHADER")){var i=Math.floor(e.length/2),s=this.concat(e.slice(0,i),n),u=this.concat(e.slice(i),n);return this.concat([s,u],n)}if(V().getBool("WEBGL_PACK_ARRAY_OPERATIONS")&&e[0].rank>1){var c=new Vv(e.map(function(d){return d.shape}),n);return this.compileAndRun(c,e)}var l=Vn(e.map(function(d){return d.shape}),n),h=e.map(function(d){return d.as2D(-1,ee(d.shape.slice(n)))}),f=new Wv(h.map(function(d){return d.shape}));return this.compileAndRun(f,h).reshape(l)},t.prototype.neg=function(e){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.neg(e);if(V().getBool("WEBGL_PACK_UNARY_OPERATIONS"))return this.packedUnaryOp(e,Au,e.dtype);var n=new fe(e.shape,Au);return this.compileAndRun(n,[e])},t.prototype.batchMatMul=function(e,n,o,a){var i=o?e.shape[2]:e.shape[1],s=a?n.shape[1]:n.shape[2],u=o?e.shape[1]:e.shape[2],c=e.shape[0];if((i===1||s===1)&&u>1e3){o&&(e=e.transpose([0,2,1])),a&&(n=n.transpose([0,2,1]));var l=s===1?e:e.as3D(c,u,1),h=s===1?2:1,f=s===1?n.as3D(c,1,u):n;return this.multiply(l,f).sum(h,!0)}var d=He(e.dtype,n.dtype),p=new Ba(e.shape,[c,i,s],o,a);return this.compileAndRun(p,[e,n],d)},t.prototype.fusedBatchMatMul=function(e){var n=e.a,o=e.b,a=e.transposeA,i=e.transposeB,s=e.bias,u=e.activation,c=e.preluActivationWeights,l=a?n.shape[2]:n.shape[1],h=i?o.shape[1]:o.shape[2],f=n.shape[0],d=He(n.dtype,o.dtype),p=s!=null,v=c!=null,m=u?ho(u,!0):null,g=new Ba(n.shape,[f,l,h],a,i,p,m,v),x=[n,o];return s&&x.push(s),c&&x.push(c),this.compileAndRun(g,x,d)},t.prototype.multiply=function(e,n){if(e.dtype==="complex64"){var o=this.texData.get(e.dataId),a=this.texData.get(n.dataId),i=new gu(Pv,e.shape,n.shape),s=new gu(Mv,e.shape,n.shape),u=[this.makeComplexComponentTensorInfo(e,o.complexTensors.real),this.makeComplexComponentTensorInfo(e,o.complexTensors.imag),this.makeComplexComponentTensorInfo(n,a.complexTensors.real),this.makeComplexComponentTensorInfo(n,a.complexTensors.imag)],c=this.compileAndRun(i,u),l=this.compileAndRun(s,u),h=this.complex(c,l);return c.dispose(),l.dispose(),h}if(this.shouldExecuteOnCPU([e,n]))return this.cpuBackend.multiply(e,n);if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,yu,e.dtype);var f=new Pe(yu,e.shape,n.shape);return this.compileAndRun(f,[e,n],e.dtype)},t.prototype.batchNormalization=function(e,n,o,a,i,s){var u=[e,n,o],c=null;s!=null&&(c=s.shape,u.push(s));var l=null;if(i!=null&&(l=i.shape,u.push(i)),V().getBool("WEBGL_PACK_NORMALIZATION")){var h=new Fv(e.shape,n.shape,o.shape,c,l,a);return this.compileAndRun(h,u)}var f=new Nv(e.shape,n.shape,o.shape,c,l,a);return this.compileAndRun(f,u)},t.prototype.localResponseNormalization4D=function(e,n,o,a,i){var s=V().getBool("WEBGL_PACK_NORMALIZATION")?new dm(e.shape,n,o,a,i):new hm(e.shape,n,o,a,i);return this.compileAndRun(s,[e])},t.prototype.LRNGrad=function(e,n,o,a,i,s,u){var c=new fm(n.shape,a,i,s,u);return this.compileAndRun(c,[n,o,e])},t.prototype.tile=function(e,n){if(e.dtype==="string"){var o=this.readSync(e.dataId).map(function(i){return Or(i)});return $l(ue(e.shape,e.dtype,o),n)}var a=new Om(e.shape,n);return this.compileAndRun(a,[e])},t.prototype.pad=function(e,n,o){var a=V().getBool("WEBGL_PACK_ARRAY_OPERATIONS")?new bm(e.shape,n,o):new xm(e.shape,n,o);return this.compileAndRun(a,[e])},t.prototype.transpose=function(e,n){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.transpose(e,n);var o=V().getBool("WEBGL_PACK_ARRAY_OPERATIONS")?new Lm(e.shape,n):new Bm(e.shape,n);return this.compileAndRun(o,[e])},t.prototype.gather=function(e,n,o){if(this.shouldExecuteOnCPU([e,n]))return this.cpuBackend.gather(e,n,o);var a=new sm(e.shape,n.size,o);return this.compileAndRun(a,[e,n])},t.prototype.batchToSpaceND=function(e,n,o){R(e.rank<=4,function(){return"batchToSpaceND for rank > 4 with a WebGL backend not implemented yet"});var a=n.reduce(function(h,f){return h*f}),i=Wo(e.shape,n,a),s=Vo(i.length,n.length),u=zo(e.shape,n,a),c=Ll(o,n.length),l=Wl(u,o,n.length);return e.reshape(i).transpose(s).reshape(u).slice(c,l)},t.prototype.spaceToBatchND=function(e,n,o){R(e.rank<=4,function(){return"spaceToBatchND for rank > 4 with a WebGL backend not implemented yet"});var a=n.reduce(function(f,d){return f*d}),i=[[0,0]];i.push.apply(i,o);for(var s=1+n.length;s<e.shape.length;++s)i.push([0,0]);var u=e.pad(i),c=Wo(u.shape,n,a,!1),l=Vo(c.length,n.length,!1),h=zo(u.shape,n,a,!1);return u.reshape(c).transpose(l).reshape(h)},t.prototype.reduce=function(e,n,o){var a=e.shape[0],i=e.shape[1],s=So(i),u=new wm({windowSize:s,inSize:i,batchSize:a},n),c=this.compileAndRun(u,[e],o);return c.shape[1]===1?c:this.reduce(c,n,o)},t.prototype.argReduce=function(e,n,o){o===void 0&&(o=null);var a=e.shape[0],i=e.shape[1];o!=null&&(a=o.shape[0],i=o.shape[1]);var s=So(i),u=new _v({windowSize:s,inSize:i,batchSize:a},n,o==null),c=[e];o!=null&&c.push(o);var l=this.compileAndRun(u,c,"int32");return l.shape[1]===1?l:this.argReduce(e,n,l)},t.prototype.argReducePacked=function(e,n,o){o===void 0&&(o=null);var a=o!=null?o.shape:e.shape,i=So(a[a.length-1]),s=new Av(a,i,n,o==null),u=o==null?[e]:[e,o],c=this.compileAndRun(s,u,"int32");return c.rank===e.rank?this.argReducePacked(e,n,c):c},t.prototype.sum=function(e,n){ct("sum",n,e.rank);var o=Xe(e.shape,n),a=o[0],i=ee(o[1]),s=e.as2D(-1,i),u=Na(e.dtype);return this.reduce(s,"sum",u).reshape(a)},t.prototype.prod=function(e,n){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.prod(e,n);var o=Xe(e.shape,n),a=o[0],i=ee(o[1]),s=e.as2D(-1,i),u=Na(e.dtype);return this.reduce(s,"prod",u).reshape(a)},t.prototype.unsortedSegmentSum=function(e,n,o){var a=0,i=Ot([a],e.rank),s=e;i!=null&&(s=e.transpose(i),a=Bt(1,e.rank)[0]);var u=function(d,p,v){for(var m=[],g=d.length,x=0;x<g;x++)x!==p?m.push(d[x]):m.push(v);return m}(s.shape,a,o),c=ee([s.shape[a]]),l=s.as2D(-1,c),h=Na(e.dtype),f=this.segOpCompute(l,"unsortedSegmentSum",n,h,o).reshape(u);return i!=null&&(f=f.transpose(oa(i))),f},t.prototype.segOpCompute=function(e,n,o,a,i){var s=e.shape[0],u=e.shape[1],c=function(f,d){var p,v=!1;for(f<=ns?(p=f,v=!0):p=Mo(f,Math.floor(Math.sqrt(f)));!v;)p>d||p===f?v=!0:p=Mo(f,p+1);return p}(u,i),l=new Dm({windowSize:c,inSize:u,batchSize:s,numSegments:i}),h=this.compileAndRun(l,[e,o],a);return h.shape[1]===i?h:(o=Lo(0,i).tile([u/c]),this.segOpCompute(h,n,o,a,i))},t.prototype.argMinMaxReduce=function(e,n,o){var a=[n];if(ct("arg"+o.charAt(0).toUpperCase()+o.slice(1),a,e.rank),!V().getBool("WEBGL_PACK_REDUCE")||e.rank<=2){var i=Xe(e.shape,a),s=i[0],u=ee(i[1]),c=e.as2D(-1,u);return this.argReduce(c,o).reshape(s)}return this.argReducePacked(e,o)},t.prototype.argMin=function(e,n){return this.argMinMaxReduce(e,n,"min")},t.prototype.argMax=function(e,n){return this.argMinMaxReduce(e,n,"max")},t.prototype.cumsum=function(e,n,o,a){if(n!==e.rank-1)throw new Error("WebGL cumsum shader expects an inner-most axis="+(e.rank-1)+" but got axis="+n);var i=new $v(e.shape,o,a);return this.compileAndRun(i,[e])},t.prototype.equal=function(e,n){if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  return vec4(equal(a, b));
`,"bool");var o=new Pe("return float(a == b);",e.shape,n.shape);return this.compileAndRun(o,[e,n],"bool")},t.prototype.notEqual=function(e,n){if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  return vec4(notEqual(a, b));
`,"bool");var o=new Pe("return float(a != b);",e.shape,n.shape);return this.compileAndRun(o,[e,n],"bool")},t.prototype.less=function(e,n){if(this.shouldExecuteOnCPU([e,n]))return this.cpuBackend.less(e,n);if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  return vec4(lessThan(a, b));
`,"bool");var o=new Pe("return float(a < b);",e.shape,n.shape);return this.compileAndRun(o,[e,n],"bool")},t.prototype.lessEqual=function(e,n){if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  return vec4(lessThanEqual(a, b));
`,"bool");var o=new Pe("return float(a <= b);",e.shape,n.shape);return this.compileAndRun(o,[e,n],"bool")},t.prototype.greater=function(e,n){if(this.shouldExecuteOnCPU([e,n]))return this.cpuBackend.greater(e,n);if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  return vec4(greaterThan(a, b));
`,"bool");var o=new Pe("return float(a > b);",e.shape,n.shape);return this.compileAndRun(o,[e,n],"bool")},t.prototype.greaterEqual=function(e,n){if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  return vec4(greaterThanEqual(a, b));
`,"bool");var o=new Pe("return float(a >= b);",e.shape,n.shape);return this.compileAndRun(o,[e,n],"bool")},t.prototype.logicalNot=function(e){var n=new fe(e.shape,"return float(!(x >= 1.0));");return this.compileAndRun(n,[e])},t.prototype.logicalAnd=function(e,n){if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  return vec4(
    vec4(greaterThanEqual(a, vec4(1.0))) *
    vec4(greaterThanEqual(b, vec4(1.0))));
`,"bool");var o=new Pe("return float(a >= 1.0 && b >= 1.0);",e.shape,n.shape);return this.compileAndRun(o,[e,n],"bool")},t.prototype.logicalOr=function(e,n){if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  return min(
    vec4(greaterThanEqual(a, vec4(1.0))) +
    vec4(greaterThanEqual(b, vec4(1.0))),
    vec4(1.0));
`,"bool");var o=new Pe("return float(a >= 1.0 || b >= 1.0);",e.shape,n.shape);return this.compileAndRun(o,[e,n],"bool")},t.prototype.select=function(e,n,o){var a=new Tm(e.rank,n.shape,n.rank);return this.compileAndRun(a,[e,n,o],He(n.dtype,o.dtype))},t.prototype.where=function(e){Oo("tf.where() in webgl locks the UI thread. Call tf.whereAsync() instead");var n=e.dataSync();return ls(e.shape,n)},t.prototype.topk=function(e,n,o){return Yl(e.dataSync(),e.shape,e.dtype,n)},t.prototype.min=function(e,n){ct("min",n,e.rank);var o=Xe(e.shape,n),a=o[0],i=ee(o[1]),s=e.as2D(-1,i);return this.reduce(s,"min",s.dtype).reshape(a)},t.prototype.minimum=function(e,n){if(this.shouldExecuteOnCPU([e,n]))return this.cpuBackend.minimum(e,n);var o=V().getBool("WEBGL_PACK_BINARY_OPERATIONS")?new rn(`
  vec4 result = vec4(min(a, b));
  vec4 isNaN = min(vec4(isnan(a)) + vec4(isnan(b)), vec4(1.0));
  
  result.r = isNaN.r > 0. ? NAN : result.r;
  result.g = isNaN.g > 0. ? NAN : result.g;
  result.b = isNaN.b > 0. ? NAN : result.b;
  result.a = isNaN.a > 0. ? NAN : result.a;

  return result;
`,e.shape,n.shape):new Pe(`
  if (isnan(a)) return a;
  if (isnan(b)) return b;

  return min(a, b);
`,e.shape,n.shape);return this.compileAndRun(o,[e,n])},t.prototype.mod=function(e,n){var o=V().getBool("WEBGL_PACK_BINARY_OPERATIONS")?new rn(`
  vec4 result = mod(a, b);
  vec4 isNaN = vec4(equal(b, vec4(0.0)));
  
  result.r = isNaN.r > 0. ? NAN : result.r;
  result.g = isNaN.g > 0. ? NAN : result.g;
  result.b = isNaN.b > 0. ? NAN : result.b;
  result.a = isNaN.a > 0. ? NAN : result.a;

  return result;
`,e.shape,n.shape):new Pe(`if (b == 0.0) return NAN;
  return mod(a, b);`,e.shape,n.shape);return this.compileAndRun(o,[e,n])},t.prototype.max=function(e,n){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.max(e,n);ct("max",n,e.rank);var o=Xe(e.shape,n),a=o[0],i=ee(o[1]),s=e.as2D(-1,i);return this.reduce(s,"max",s.dtype).reshape(a)},t.prototype.maximum=function(e,n){if(this.shouldExecuteOnCPU([e,n]))return this.cpuBackend.maximum(e,n);var o=V().getBool("WEBGL_PACK_BINARY_OPERATIONS")?new rn(`
  vec4 result = vec4(max(a, b));
  vec4 isNaN = min(vec4(isnan(a)) + vec4(isnan(b)), vec4(1.0));
  
  result.r = isNaN.r > 0. ? NAN : result.r;
  result.g = isNaN.g > 0. ? NAN : result.g;
  result.b = isNaN.b > 0. ? NAN : result.b;
  result.a = isNaN.a > 0. ? NAN : result.a;

  return result;
`,e.shape,n.shape):new Pe(`
  if (isnan(a)) return a;
  if (isnan(b)) return b;

  return max(a, b);
`,e.shape,n.shape);return this.compileAndRun(o,[e,n])},t.prototype.all=function(e,n){ct("all",n,e.rank);var o=Xe(e.shape,n),a=o[0],i=ee(o[1]),s=e.as2D(-1,i);return this.reduce(s,"all",s.dtype).reshape(a)},t.prototype.any=function(e,n){ct("any",n,e.rank);var o=Xe(e.shape,n),a=o[0],i=ee(o[1]),s=e.as2D(-1,i);return this.reduce(s,"any",s.dtype).reshape(a)},t.prototype.realDivide=function(e,n){if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  // vec4 one = vec4(equal(a, b));
  // return one + (vec4(1.0) - one) * a / b;
  vec4 result = a / b;
  if(a.x == b.x) {
    result.x = 1.;
  }
  if(a.y == b.y) {
    result.y = 1.;
  }
  if(a.z == b.z) {
    result.z = 1.;
  }
  if(a.w == b.w) {
    result.w = 1.;
  }

  return result;
`,"float32",!0);var o=new Pe(`
if (a == b) {
  return 1.0;
};
return a / b;`,e.shape,n.shape);return this.compileAndRun(o,[e,n],"float32")},t.prototype.floorDiv=function(e,n){if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,`
  ivec4 ia = round(a);
  ivec4 ib = round(b);
  bvec4 cond = notEqual(ib, ivec4(0));
  ivec4 result = ivec4(0);
  vec4 s = sign(a) * sign(b);

  // Windows (D3D) wants guaranteed non-zero int division at compile-time.
  if (cond[0]) {
    result[0] = idiv(ia[0], ib[0], s[0]);
  }
  if (cond[1]) {
    result[1] = idiv(ia[1], ib[1], s[1]);
  }
  if (cond[2]) {
    result[2] = idiv(ia[2], ib[2], s[2]);
  }
  if (cond[3]) {
    result[3] = idiv(ia[3], ib[3], s[3]);
  }
  return vec4(result);
`,"int32");var o=new Pe(`
  float s = sign(a) * sign(b);
  int ia = round(a);
  int ib = round(b);
  if (ib != 0) {
    // Windows (D3D) wants guaranteed non-zero int division at compile-time.
    return float(idiv(ia, ib, s));
  } else {
    return NAN;
  }
`,e.shape,n.shape);return this.compileAndRun(o,[e,n],"int32")},t.prototype.add=function(e,n){if(e.dtype==="complex64"&&n.dtype==="complex64")return this.complexSeparableBinaryOp(e,n,Ma);if(this.shouldExecuteOnCPU([e,n]))return this.cpuBackend.add(e,n);var o=He(e.dtype,n.dtype);if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,Ma,o);var a=new Pe(Ma,e.shape,n.shape);return this.compileAndRun(a,[e,n],o)},t.prototype.packedUnaryOp=function(e,n,o){var a=new _r(e.shape,n);return this.compileAndRun(a,[e],o)},t.prototype.packedBinaryOp=function(e,n,o,a,i){i===void 0&&(i=!1);var s=new rn(o,e.shape,n.shape,i);return this.compileAndRun(s,[e,n],a)},t.prototype.complexSeparableBinaryOp=function(e,n,o){var a=this,i=this.texData.get(e.dataId),s=this.texData.get(n.dataId),u=[[i.complexTensors.real,s.complexTensors.real],[i.complexTensors.imag,s.complexTensors.imag]].map(function(f){var d=f[0],p=f[1],v=a.makeComplexComponentTensorInfo(e,d),m=a.makeComplexComponentTensorInfo(n,p),g=new Pe(o,e.shape,n.shape);return a.compileAndRun(g,[v,m],He(d.dtype,p.dtype))}),c=u[0],l=u[1],h=this.complex(c,l);return c.dispose(),l.dispose(),h},t.prototype.makeComplexComponentTensorInfo=function(e,n){return{dataId:n.dataId,dtype:n.dtype,shape:e.shape}},t.prototype.addN=function(e){if(e.length===1)return e[0];if(e.length>V().get("WEBGL_MAX_TEXTURES_IN_SHADER")){var n=Math.floor(e.length/2),o=this.addN(e.slice(0,n)),a=this.addN(e.slice(n));return this.addN([o,a])}var i=e.map(function(c){return c.dtype}).reduce(function(c,l){return He(c,l)}),s=e.map(function(c){return c.shape}),u=V().getBool("WEBGL_PACK")?new Cv(e[0].shape,s):new wv(e[0].shape,s);return this.compileAndRun(u,e,i)},t.prototype.subtract=function(e,n){if(e.dtype==="complex64"&&n.dtype==="complex64")return this.complexSeparableBinaryOp(e,n,Oa);if(this.shouldExecuteOnCPU([e,n]))return this.cpuBackend.subtract(e,n);var o=He(e.dtype,n.dtype);if(V().getBool("WEBGL_PACK_BINARY_OPERATIONS"))return this.packedBinaryOp(e,n,Oa,e.dtype);var a=new Pe(Oa,e.shape,n.shape);return this.compileAndRun(a,[e,n],o)},t.prototype.pow=function(e,n){var o=V().getBool("WEBGL_PACK_BINARY_OPERATIONS")?new rn(`
  // isModRound1 has 1 for components with round(mod(b, 2.0)) == 1, 0 otherwise.
  vec4 isModRound1 = vec4(equal(round(mod(b, 2.0)), ivec4(1)));
  vec4 multiplier = sign(a) * isModRound1 + (vec4(1.0) - isModRound1);
  vec4 result = multiplier * pow(abs(a), b);

  // Ensure that a^0 = 1, including 0^0 = 1 as this correspond to TF and JS
  bvec4 isExpZero = equal(b, vec4(0.0));
  result.r = isExpZero.r ? 1.0 : result.r;
  result.g = isExpZero.g ? 1.0 : result.g;
  result.b = isExpZero.b ? 1.0 : result.b;
  result.a = isExpZero.a ? 1.0 : result.a;

  vec4 isNaN = vec4(lessThan(a, vec4(0.0))) * vec4(lessThan(floor(b), b));
  
  result.r = isNaN.r > 0. ? NAN : result.r;
  result.g = isNaN.g > 0. ? NAN : result.g;
  result.b = isNaN.b > 0. ? NAN : result.b;
  result.a = isNaN.a > 0. ? NAN : result.a;

  return result;
`,e.shape,n.shape):new Pe(`
if(a < 0.0 && floor(b) < b){
  return NAN;
}
if (b == 0.0) {
  return 1.0;
}
return (round(mod(b, 2.0)) != 1) ?
    pow(abs(a), b) : sign(a) * pow(abs(a), b);
`,e.shape,n.shape),a=He(e.dtype,n.dtype);return this.compileAndRun(o,[e,n],a)},t.prototype.ceil=function(e){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.ceil(e);if(V().getBool("WEBGL_PACK_UNARY_OPERATIONS"))return this.packedUnaryOp(e,Du,e.dtype);var n=new fe(e.shape,Du);return this.compileAndRun(n,[e])},t.prototype.floor=function(e){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.floor(e);if(V().getBool("WEBGL_PACK_UNARY_OPERATIONS"))return this.packedUnaryOp(e,Tu,e.dtype);var n=new fe(e.shape,Tu);return this.compileAndRun(n,[e])},t.prototype.sign=function(e){var n=new fe(e.shape,`
  if (isnan(x)) { return 0.0; }
  return sign(x);
`);return this.compileAndRun(n,[e])},t.prototype.isNaN=function(e){var n=new fe(e.shape,"return float(isnan(x));");return this.compileAndRun(n,[e],"bool")},t.prototype.isInf=function(e){var n=new fe(e.shape,"return float(isinf(x));");return this.compileAndRun(n,[e],"bool")},t.prototype.isFinite=function(e){var n=new fe(e.shape,"return float(!isnan(x) && !isinf(x));");return this.compileAndRun(n,[e],"bool")},t.prototype.round=function(e){var n=new fe(e.shape,`
  // OpenGL ES does not support round function.
  // The algorithm is based on banker's rounding.
  float base = floor(x);
  if ((x - base) < 0.5) {
    return floor(x);
  } else if ((x - base) > 0.5) {
    return ceil(x);
  } else {
    if (mod(base, 2.0) == 0.0) {
      return base;
    } else {
      return base + 1.0;
    }
  }
`);return this.compileAndRun(n,[e])},t.prototype.exp=function(e){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.exp(e);if(V().getBool("WEBGL_PACK_UNARY_OPERATIONS"))return this.packedUnaryOp(e,Nu,e.dtype);var n=new fe(e.shape,Nu);return this.compileAndRun(n,[e])},t.prototype.expm1=function(e){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.expm1(e);if(V().getBool("WEBGL_PACK_UNARY_OPERATIONS"))return this.packedUnaryOp(e,Fu,e.dtype);var n=new fe(e.shape,Fu);return this.compileAndRun(n,[e])},t.prototype.softmax=function(e,n){var o=Le([n],e.shape),a=this.max(e,o),i=it(a.shape,o),s=this.subtract(e,a.reshape(i)),u=this.exp(s),c=this.sum(u,o).reshape(i);return this.realDivide(u,c)},t.prototype.log=function(e){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.log(e);if(V().getBool("WEBGL_PACK_UNARY_OPERATIONS"))return this.packedUnaryOp(e,`
  vec4 result = log(x);
  vec4 isNaN = vec4(lessThan(x, vec4(0.0)));
  result.r = isNaN.r == 1.0 ? NAN : result.r;
  result.g = isNaN.g == 1.0 ? NAN : result.g;
  result.b = isNaN.b == 1.0 ? NAN : result.b;
  result.a = isNaN.a == 1.0 ? NAN : result.a;

  return result;
`,e.dtype);var n=new fe(e.shape,`if (x < 0.0) return NAN;
  return log(x);`);return this.compileAndRun(n,[e])},t.prototype.log1p=function(e){var n=new fe(e.shape,"return log(1.0 + x);");return this.compileAndRun(n,[e])},t.prototype.sqrt=function(e){var n=new fe(e.shape,"return sqrt(x);");return this.compileAndRun(n,[e])},t.prototype.rsqrt=function(e){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.rsqrt(e);var n=new fe(e.shape,"return inversesqrt(x);");return this.compileAndRun(n,[e])},t.prototype.reciprocal=function(e){var n=new fe(e.shape,"return 1.0 / x;");return this.compileAndRun(n,[e])},t.prototype.relu=function(e){var n;return n=V().getBool("WEBGL_PACK")?new _r(e.shape,Ch):new fe(e.shape,xh),this.compileAndRun(n,[e])},t.prototype.relu6=function(e){var n;return n=V().getBool("WEBGL_PACK")?new _r(e.shape,_h):new fe(e.shape,bh),this.compileAndRun(n,[e])},t.prototype.prelu=function(e,n){var o=V().getBool("WEBGL_PACK_BINARY_OPERATIONS")?new rn(th,e.shape,n.shape):new Pe(eh,e.shape,n.shape);return this.compileAndRun(o,[e,n])},t.prototype.elu=function(e){if(V().getBool("WEBGL_PACK_UNARY_OPERATIONS"))return this.packedUnaryOp(e,Eh,e.dtype);var n=new fe(e.shape,wh);return this.compileAndRun(n,[e])},t.prototype.eluDer=function(e,n){var o=V().getBool("WEBGL_PACK_BINARY_OPERATIONS")?new rn(`
  vec4 bGTEZero = vec4(greaterThanEqual(b, vec4(0.)));
  return (bGTEZero * a) + ((vec4(1.0) - bGTEZero) * (a * (b + vec4(1.0))));
`,e.shape,n.shape):new Pe("return (b >= 1.0) ? a : a * (b + 1.0);",e.shape,n.shape);return this.compileAndRun(o,[e,n])},t.prototype.selu=function(e){var n=new fe(e.shape,Vm);return this.compileAndRun(n,[e])},t.prototype.int=function(e){var n=new fe(e.shape,"return float(int(x));");return this.compileAndRun(n,[e],"int32")},t.prototype.clip=function(e,n,o){var a,i=(a=V().getBool("WEBGL_PACK_CLIP")?new Bv(e.shape):new Ov(e.shape)).getCustomSetupFunc(n,o);return this.compileAndRun(a,[e],null,i)},t.prototype.abs=function(e){if(this.shouldExecuteOnCPU([e]))return this.cpuBackend.abs(e);if(V().getBool("WEBGL_PACK_UNARY_OPERATIONS"))return this.packedUnaryOp(e,Iu,e.dtype);var n=new fe(e.shape,Iu);return this.compileAndRun(n,[e])},t.prototype.complexAbs=function(e){var n=this.texData.get(e.dataId),o=new Lv(e.shape),a=[this.makeComplexComponentTensorInfo(e,n.complexTensors.real),this.makeComplexComponentTensorInfo(e,n.complexTensors.imag)];return this.compileAndRun(o,a)},t.prototype.sigmoid=function(e){var n=new fe(e.shape,"return 1.0 / (1.0 + exp(-1.0 * x));");return this.compileAndRun(n,[e])},t.prototype.softplus=function(e){var n=new fe(e.shape,`
  float epsilon = 1.1920928955078125e-7;
  float threshold = log(epsilon) + 2.0;

  bool too_large = x > -threshold;
  bool too_small = x < threshold;

  float result;
  float exp_x = exp(x);

  if (too_large){
    result = x;
  }
  else if (too_small){
    result = exp_x;
  }
  else{
    result = log(exp_x + 1.0);
  }
  return result;
`);return this.compileAndRun(n,[e])},t.prototype.sin=function(e){var n=new fe(e.shape,zm);return this.compileAndRun(n,[e])},t.prototype.cos=function(e){var n=new fe(e.shape,Um);return this.compileAndRun(n,[e])},t.prototype.tan=function(e){var n=new fe(e.shape,"return tan(x);");return this.compileAndRun(n,[e])},t.prototype.asin=function(e){var n=new fe(e.shape,Gm);return this.compileAndRun(n,[e])},t.prototype.acos=function(e){var n=new fe(e.shape,Hm);return this.compileAndRun(n,[e])},t.prototype.atan=function(e){var n=new fe(e.shape,qm);return this.compileAndRun(n,[e])},t.prototype.atan2=function(e,n){var o=V().getBool("WEBGL_PACK_BINARY_OPERATIONS")?new rn(`
  vec4 result = atan(a, b);
  vec4 isNaN = min(vec4(isnan(a)) + vec4(isnan(b)), vec4(1.0));
  
  result.r = isNaN.r > 0. ? NAN : result.r;
  result.g = isNaN.g > 0. ? NAN : result.g;
  result.b = isNaN.b > 0. ? NAN : result.b;
  result.a = isNaN.a > 0. ? NAN : result.a;

  return result;
`,e.shape,n.shape):new Pe(`
  if (isnan(a)) return a;
  if (isnan(b)) return b;

  return atan(a, b);
`,e.shape,n.shape);return this.compileAndRun(o,[e,n])},t.prototype.sinh=function(e){var n=new fe(e.shape,`
  float e2x = exp(x);
  return (e2x - 1.0 / e2x) / 2.0;
`);return this.compileAndRun(n,[e])},t.prototype.cosh=function(e){var n=new fe(e.shape,`
  float e2x = exp(-x);
  return (e2x + 1.0 / e2x) / 2.0;
`);return this.compileAndRun(n,[e])},t.prototype.tanh=function(e){var n=new fe(e.shape,`
  float e2x = exp(-2.0 * abs(x));
  return sign(x) * (1.0 - e2x) / (1.0 + e2x);
`);return this.compileAndRun(n,[e])},t.prototype.asinh=function(e){var n=new fe(e.shape,jm);return this.compileAndRun(n,[e])},t.prototype.acosh=function(e){var n=new fe(e.shape,Km);return this.compileAndRun(n,[e])},t.prototype.atanh=function(e){var n=new fe(e.shape,Xm);return this.compileAndRun(n,[e])},t.prototype.erf=function(e){var n=new fe(e.shape,`
  // Error function is calculated approximately with elementary function.
  // See "Handbook of Mathematical Functions with Formulas,
  // Graphs, and Mathematical Tables", Abramowitz and Stegun.
  float p = 0.3275911;
  float a1 = 0.254829592;
  float a2 = -0.284496736;
  float a3 = 1.421413741;
  float a4 = -1.453152027;
  float a5 = 1.061405429;

  float sign = sign(x);
  x = abs(x);
  float t = 1.0 / (1.0 + p * x);
  return sign * (1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x));
`);return this.compileAndRun(n,[e])},t.prototype.step=function(e,n){var o=new fe(e.shape,function(a){return a===void 0&&(a=0),Lt+`
    return x > 0.0 ? 1.0 : float(`+a+`);
  `}(n));return this.compileAndRun(o,[e])},t.prototype.conv2dByMatMul=function(e,n,o,a,i,s){var u=e.shape,c=this.texData.get(e.dataId),l=o.inChannels,h=u[0]*u[1]*u[2],f=o.outChannels,d=o.dataFormat==="channelsLast",p=(h===1||f===1)&&l>1e3,v=u[2]%2!=0&&!!c.isPacked;if(p||!V().getBool("WEBGL_LAZILY_UNPACK")||!V().getBool("WEBGL_PACK_BINARY_OPERATIONS")||!v){var m=d?u[0]*u[1]*u[2]:u[0]*u[2]*u[3],g=this.reshape(e,[1,m,o.inChannels]),x=this.reshape(n,[1,o.inChannels,o.outChannels]);return this.reshape(this.fusedBatchMatMul({a:g,b:x,transposeA:!1,transposeB:!1,bias:a,activation:i,preluActivationWeights:s}),o.outShape)}var y=d?u[0]*u[1]*(u[2]+1):u[0]*u[2]*(u[3]+1),b={dataId:e.dataId,shape:[1,y,o.inChannels],dtype:e.dtype},w=c.shape;c.shape=c.shape.slice(),c.shape[c.shape.length-2]++,R(Ir(c.shape,b.shape),function(){return"packed reshape "+c.shape+" to "+b.shape+" isn't free"});var C=this.reshape(n,[1,o.inChannels,o.outChannels]),I=this.fusedBatchMatMul({a:b,b:C,transposeA:!1,transposeB:!1,bias:a,activation:i,preluActivationWeights:s}),E=this.texData.get(I.dataId);return R(E.isPacked,function(){return"batchMatMul result is expected to be packed"}),c.shape=w,E.shape=o.outShape,N.makeTensorFromDataId(I.dataId,o.outShape,I.dtype)},t.prototype.conv2dWithIm2Row=function(e,n,o,a,i,s){var u=o.filterWidth,c=o.filterHeight,l=o.inChannels,h=o.outWidth,f=o.outHeight,d=o.dataFormat==="channelsLast",p=u*c*l,v=f*h,m=[p,v],g=e.squeeze([0]),x=n.reshape([1,p,-1]),y=new lm(m,g.shape,o),b=this.compileAndRun(y,[g]).reshape([1,m[0],m[1]]),w=a!=null,C=s!=null,I=i?ho(i,!0):null,E=new Ba(b.shape,[1,v,o.outChannels],!0,!1,w,I,C),S=[b,x];a&&S.push(a),C&&S.push(s);var k=this.compileAndRun(E,S);return d?k.reshape([1,f,h,o.outChannels]):k.reshape([1,o.outChannels,f,h])},t.prototype.fusedConv2d=function(e){var n=e.input,o=e.filter,a=e.convInfo,i=e.bias,s=e.activation,u=e.preluActivationWeights;if(a.filterHeight===1&&a.filterWidth===1&&a.dilationHeight===1&&a.dilationWidth===1&&a.strideHeight===1&&a.strideWidth===1&&(a.padInfo.type==="SAME"||a.padInfo.type==="VALID"))return this.conv2dByMatMul(n,o,a,i,s,u);if(V().getBool("WEBGL_CONV_IM2COL")&&n.shape[0]===1)return this.conv2dWithIm2Row(n,o,a,i,s,u);var c=i!=null,l=u!=null,h=s?ho(s,!1):null,f=new xu(a,c,h,l),d=[n,o];return i&&d.push(i),u&&d.push(u),this.compileAndRun(f,d)},t.prototype.conv2d=function(e,n,o){if(o.filterHeight===1&&o.filterWidth===1&&o.dilationHeight===1&&o.dilationWidth===1&&o.strideHeight===1&&o.strideWidth===1&&(o.padInfo.type==="SAME"||o.padInfo.type==="VALID"))return this.conv2dByMatMul(e,n,o);if(V().getBool("WEBGL_CONV_IM2COL")&&e.shape[0]===1)return this.conv2dWithIm2Row(e,n,o);var a=new xu(o);return this.compileAndRun(a,[e,n])},t.prototype.conv2dDerInput=function(e,n,o){var a=new Uv(o);return this.compileAndRun(a,[e,n])},t.prototype.conv2dDerFilter=function(e,n,o){var a=new zv(o);return this.compileAndRun(a,[e,n])},t.prototype.fusedDepthwiseConv2D=function(e){var n,o=e.input,a=e.filter,i=e.convInfo,s=e.bias,u=e.activation,c=e.preluActivationWeights,l=V().getBool("WEBGL_PACK_DEPTHWISECONV")&&i.strideWidth<=2&&i.outChannels/i.inChannels==1,h=u?ho(u,l):null,f=[o,a],d=s!=null,p=c!=null;return d&&f.push(s),p&&f.push(c),l?(n=new wu(i,d,h,p),this.compileAndRun(n,f)):(n=new bu(i,d,h,p),this.compileAndRun(n,f))},t.prototype.depthwiseConv2D=function(e,n,o){var a;return V().getBool("WEBGL_PACK_DEPTHWISECONV")&&o.strideWidth<=2&&o.outChannels/o.inChannels==1?(a=new wu(o),this.compileAndRun(a,[e,n])):(a=new bu(o),this.compileAndRun(a,[e,n]))},t.prototype.depthwiseConv2DDerInput=function(e,n,o){var a=new jv(o);return this.compileAndRun(a,[e,n])},t.prototype.depthwiseConv2DDerFilter=function(e,n,o){var a=new qv(o);return this.compileAndRun(a,[e,n])},t.prototype.conv3d=function(e,n,o){var a=new Kv(o);return this.compileAndRun(a,[e,n])},t.prototype.conv3dDerInput=function(e,n,o){var a=new Hv(o);return this.compileAndRun(a,[e,n])},t.prototype.conv3dDerFilter=function(e,n,o){var a=new Gv(o);return this.compileAndRun(a,[e,n])},t.prototype.maxPool=function(e,n){var o=new La(n,"max",!1);return this.compileAndRun(o,[e])},t.prototype.avgPool=function(e,n){var o=new La(n,"avg",!1);return this.compileAndRun(o,[e],"float32")},t.prototype.maxPoolBackprop=function(e,n,o,a){var i=new La(a,"max",!0),s=this.compileAndRun(i,[n]),u=new pm(a),c=this.compileAndRun(u,[e,s],n.dtype);return s.dispose(),c},t.prototype.avgPoolBackprop=function(e,n,o){var a=new Dv(o);return this.compileAndRun(a,[e],n.dtype)},t.prototype.cast=function(e,n){return is(e,n,this)},t.prototype.unstack=function(e,n){for(var o=e.shape[n],a=new Array(e.rank-1),i=0,s=0;s<e.rank;s++)s!==n&&(a[i++]=e.shape[s]);var u=new Array(e.rank).fill(0),c=e.shape.slice();c[n]=1;var l=new Array(o);for(s=0;s<l.length;s++)u[n]=s,l[s]=this.slice(e,u,c).reshape(a);return l},t.prototype.avgPool3d=function(e,n){var o=new Wa(n,"avg",!1);return this.compileAndRun(o,[e],"float32")},t.prototype.avgPool3dBackprop=function(e,n,o){var a=new Tv(o);return this.compileAndRun(a,[e],n.dtype)},t.prototype.maxPool3d=function(e,n){var o=new Wa(n,"max",!1);return this.compileAndRun(o,[e],"float32")},t.prototype.maxPool3dBackprop=function(e,n,o,a){var i=new Wa(a,"max",!0),s=this.compileAndRun(i,[n]),u=new vm(a),c=this.compileAndRun(u,[e,s],n.dtype);return s.dispose(),c},t.prototype.reshape=function(e,n){var o=this.texData.get(e.dataId);if(o.isPacked&&!Ir(e.shape,n)&&(o.texture===null||!Ir(o.shape,n))){var a=this.packedReshape(e,n);return N.makeTensorFromDataId(a.dataId,a.shape,a.dtype)}return Go(e,n)},t.prototype.resizeBilinear=function(e,n,o,a){var i=V().getBool("WEBGL_PACK_IMAGE_OPERATIONS")?new Sm(e.shape,n,o,a):new Em(e.shape,n,o,a);return this.compileAndRun(i,[e],"float32")},t.prototype.resizeBilinearBackprop=function(e,n,o){var a=new _m(e,n,o);return this.compileAndRun(a,[e])},t.prototype.resizeNearestNeighbor=function(e,n,o,a){var i=new Rm(e.shape,n,o,a);return this.compileAndRun(i,[e])},t.prototype.resizeNearestNeighborBackprop=function(e,n,o){var a=new km(e,n,o);return this.compileAndRun(a,[e])},t.prototype.multinomial=function(e,n,o,a){var i=n?e:cn(e),s=i.shape[0],u=i.shape[1],c=new mm(s,u,o),l=c.getCustomSetupFunc(a);return this.compileAndRun(c,[i],"int32",l)},t.prototype.oneHot=function(e,n,o,a){var i=new gm(e.size,n,o,a);return this.compileAndRun(i,[e])},t.prototype.diag=function(e){var n=new Zv(e.size);return this.compileAndRun(n,[e])},t.prototype.nonMaxSuppression=function(e,n,o,a,i){return Oo("tf.nonMaxSuppression() in webgl locks the UI thread. Call tf.nonMaxSuppressionAsync() instead"),us(e.dataSync(),n.dataSync(),o,a,i)},t.prototype.cropAndResize=function(e,n,o,a,i,s){var u=new Xv(e.shape,n.shape,a,i,s);return this.compileAndRun(u,[e,n,o],"float32")},t.prototype.depthToSpace=function(e,n,o){R(n>1,function(){return"blockSize should be > 1 for depthToSpace, but was: "+n});var a=e.shape[0],i=o==="NHWC"?e.shape[1]:e.shape[2],s=o==="NHWC"?e.shape[2]:e.shape[3],u=o==="NHWC"?e.shape[3]:e.shape[1],c=i*n,l=s*n,h=u/(n*n),f=new Qv(o==="NHWC"?[a,c,l,h]:[a,h,c,l],n,o);return this.compileAndRun(f,[e])},t.prototype.split=function(e,n,o){return Xl(e,n,o)},t.prototype.scatterND=function(e,n,o){var a=zr(0,e,o),i=a.sliceRank,s=a.numUpdates,u=a.sliceSize,c=a.strides,l=a.outputSize,h=[l/u,u],f=e.reshape([s,i]),d=n.reshape([s,u]);if(l===0)return Go($e([]),o);var p=K(0),v=new Su(s,i,f.rank,d.rank,c,h);return this.compileAndRun(v,[d,f,p]).reshape(o)},t.prototype.sparseToDense=function(e,n,o,a){var i=zr(0,e,o),s=i.sliceRank,u=i.numUpdates,c=i.strides,l=i.outputSize,h=new Su(u,s,e.rank,n.rank,c,[l,1]);return this.compileAndRun(h,[n,e,a]).reshape(o)},t.prototype.fft=function(e){return this.fftImpl(e,!1)},t.prototype.ifft=function(e){return this.fftImpl(e,!0)},t.prototype.fftImpl=function(e,n){var o=this.texData.get(e.dataId),a=new _u(om,e.shape,n),i=new _u(am,e.shape,n),s=[this.makeComplexComponentTensorInfo(e,o.complexTensors.real),this.makeComplexComponentTensorInfo(e,o.complexTensors.imag)],u=this.compileAndRun(a,s),c=this.compileAndRun(i,s),l=this.complex(u,c).as2D(e.shape[0],e.shape[1]);return u.dispose(),c.dispose(),l},t.prototype.gatherND=function(e,n){var o=n.shape,a=o[o.length-1],i=ts(e,n),s=i[0],u=i[1],c=i[2],l=i[3],h=n.reshape([u,a]),f=e.reshape([e.size/c,c]),d=new um(a,l,[u,c]);return this.compileAndRun(d,[f,h]).reshape(s)},t.prototype.fill=function(e,n,o){if((o=o||gr(n))==="string"){var a=Mr(o,ee(e));return a.fill(n),N.makeTensor(a,e,o,this)}var i=new im(e,n),s=i.getCustomSetupFunc(n);return this.compileAndRun(i,[],o,s)},t.prototype.onesLike=function(e){if(e.dtype==="string")throw new Error("onesLike is not supported under string dtype");return this.fill(e.shape,1,e.dtype)},t.prototype.zerosLike=function(e){return this.fill(e.shape,e.dtype==="string"?"":0,e.dtype)},t.prototype.linspace=function(e,n,o){return ss(e,n,o)},t.prototype.makeTensorInfo=function(e,n){var o=this.write(null,e,n);return this.texData.get(o).usage=null,{dataId:o,shape:e,dtype:n}},t.prototype.makeOutput=function(e,n){var o=this.makeTensorInfo(e,n).dataId;return N.makeTensorFromDataId(o,e,n,this)},t.prototype.unpackTensor=function(e){var n=new Ym(e.shape);return this.runWebGLProgram(n,[e],e.dtype)},t.prototype.packTensor=function(e){var n=new ym(e.shape);return this.runWebGLProgram(n,[e],e.dtype,null,!0)},t.prototype.packedReshape=function(e,n){var o=[Wr(e.shape)].concat(Vr(e.shape)),a={dtype:e.dtype,shape:o,dataId:e.dataId},i=[Wr(n)].concat(Vr(n)),s=new Cm(i,o),u=this.runWebGLProgram(s,[a],e.dtype,null,!0);return{dataId:u.dataId,shape:n,dtype:u.dtype}},t.prototype.decode=function(e){var n,o=this.texData.get(e),a=o.isPacked,i=o.shape,s=o.dtype,u=Eo(i);return n=a?new Jv(u):new Yv(u),{dtype:s,shape:i,dataId:this.runWebGLProgram(n,[{shape:u,dtype:s,dataId:e}],s,null,!0).dataId}},t.prototype.runWebGLProgram=function(e,n,o,a,i){var s=this;i===void 0&&(i=!1);var u=this.makeTensorInfo(e.outputShape,o),c=this.texData.get(u.dataId);if(e.packedOutput&&(c.isPacked=!0),e.outPackingScheme===Lr.DENSE){var l=Dr(e.outputShape);c.texShape=l.map(function(y){return 2*y})}if(e.outTexUsage!=null&&(c.usage=e.outTexUsage),ee(u.shape)===0)return c.values=fr(u.dtype,0),u;var h=[],f=n.map(function(y){if(y.dtype==="complex64")throw new Error("GPGPUProgram does not support complex64 input. For complex64 dtypes, please separate the program into real and imaginary parts.");var b=s.texData.get(y.dataId);if(b.texture==null){if(!e.packedInputs&&ee(y.shape)<=V().getNumber("WEBGL_SIZE_UPLOAD_UNIFORM"))return{shape:y.shape,texData:null,isUniform:!0,uniformValues:b.values};e.packedInputs&&(b.isPacked=!0,b.shape=y.shape)}else if(!!b.isPacked!=!!e.packedInputs)y=b.isPacked?s.unpackTensor(y):s.packTensor(y),h.push(y),b=s.texData.get(y.dataId);else if(b.isPacked&&!Ir(b.shape,y.shape)){var w=y,C=y.shape;y.shape=b.shape,y=s.packedReshape(y,C),h.push(y),b=s.texData.get(y.dataId),w.shape=C}return s.uploadToGPU(y.dataId),{shape:y.shape,texData:b,isUniform:!1}});this.uploadToGPU(u.dataId);var d,p={shape:u.shape,texData:c,isUniform:!1},v=function(y,b,w){var C="";b.concat(w).forEach(function(S){var k=S.texData!=null&&S.texData.slice!=null&&S.texData.slice.flatOffset>0,T=S.isUniform?"uniform":S.texData.texShape;C+=S.shape+"_"+T+"_"+k});var I=y.userCode,E=y.constructor.name;return E+="_"+C+"_"+I}(e,f,p),m=this.getAndSaveBinary(v,function(){return function(y,b,w,C){var I=b.userCode,E=w.map(function(L,W){var G={logicalShape:L.shape,texShape:L.isUniform?null:L.texData.texShape,isUniform:L.isUniform,isPacked:!L.isUniform&&L.texData.isPacked,flatOffset:null};return L.texData!=null&&L.texData.slice!=null&&L.texData.slice.flatOffset>0&&(G.flatOffset=L.texData.slice.flatOffset),{name:b.variableNames[W],shapeInfo:G}}),S=E.map(function(L){return L.shapeInfo}),k={logicalShape:C.shape,texShape:C.texData.texShape,isUniform:!1,isPacked:C.texData.isPacked,flatOffset:null},T=Ev(E,k,I,b.packedInputs),A=y.createProgram(T),F=null,P=y.getUniformLocation(A,"NAN",!1);V().getNumber("WEBGL_VERSION")===1&&(F=y.getUniformLocation(A,"INFINITY",!1));for(var B={},U=0;U<b.variableNames.length;U++){var z=b.variableNames[U];B[z]=y.getUniformLocation(A,z,!1),B["offset"+z]=y.getUniformLocation(A,"offset"+z,!1)}return{program:b,source:T,webGLProgram:A,uniformLocations:B,inShapeInfos:S,outShapeInfo:k,infLoc:F,nanLoc:P}}(s.gpgpu,e,f,p)}),g=this.activeTimers!=null;if(g&&(d=this.startTimer()),function(y,b,w,C,I){Eu(b.inShapeInfos,w),Eu([b.outShapeInfo],[C]);var E=C.texData.texture,S=C.texData.texShape;C.texData.isPacked?y.setOutputPackedMatrixTexture(E,S[0],S[1]):y.setOutputMatrixTexture(E,S[0],S[1]),y.setProgram(b.webGLProgram),V().getNumber("WEBGL_VERSION")===1&&b.infLoc!==null&&y.gl.uniform1f(b.infLoc,1/0),b.nanLoc!==null&&y.gl.uniform1f(b.nanLoc,NaN),w.forEach(function(k,T){var A=b.program.variableNames[T],F=b.uniformLocations[A],P=b.uniformLocations["offset"+A];if(F!=null)if(k.isUniform)if(ee(k.shape)<2)y.gl.uniform1f(F,k.uniformValues[0]);else{var B=k.uniformValues;B instanceof Float32Array||(B=new Float32Array(B)),y.gl.uniform1fv(F,B)}else k.texData.slice!=null&&P!=null&&y.gl.uniform1i(P,k.texData.slice.flatOffset),y.setInputMatrixTexture(k.texData.texture,F,T)}),I!=null&&I(y,b.webGLProgram),y.executeProgram()}(this.gpgpu,m,f,p,a),h.forEach(function(y){return s.disposeData(y.dataId)}),g&&(d=this.endTimer(d),this.activeTimers.push({name:e.constructor.name,query:this.getQueryTime(d)})),!V().getBool("WEBGL_LAZILY_UNPACK")&&c.isPacked&&i===!1){var x=this.unpackTensor(u);return this.disposeData(u.dataId),x}return u},t.prototype.compileAndRun=function(e,n,o,a,i){i===void 0&&(i=!1),o=o||n[0].dtype;var s=this.runWebGLProgram(e,n,o,a,i);return N.makeTensorFromDataId(s.dataId,s.shape,s.dtype)},t.prototype.getAndSaveBinary=function(e,n){return e in this.binaryCache||(this.binaryCache[e]=n()),this.binaryCache[e]},t.prototype.getTextureManager=function(){return this.textureManager},t.prototype.dispose=function(){var e=this;this.disposed||(V().getBool("IS_TEST")||Object.keys(this.binaryCache).forEach(function(n){e.gpgpu.deleteProgram(e.binaryCache[n].webGLProgram),delete e.binaryCache[n]}),this.textureManager.dispose(),this.canvas!=null&&typeof HTMLCanvasElement<"u"&&this.canvas instanceof HTMLCanvasElement?this.canvas.remove():this.canvas=null,this.gpgpuCreatedLocally&&(this.gpgpu.program=null,this.gpgpu.dispose()),this.disposed=!0)},t.prototype.floatPrecision=function(){var e=this;return this.floatPrecisionValue==null&&(this.floatPrecisionValue=Y(function(){if(!V().get("WEBGL_RENDER_FLOAT32_ENABLED")){var n=V().getBool("DEBUG");V().set("DEBUG",!1);var o=e.abs(K(1e-8)).dataSync()[0];if(V().set("DEBUG",n),o>0)return 32}return 16})),this.floatPrecisionValue},t.prototype.epsilon=function(){return this.floatPrecision()===32?1e-7:1e-4},t.prototype.uploadToGPU=function(e){var n,o=this.texData.get(e),a=o.shape,i=o.dtype,s=o.values,u=o.texture,c=o.usage,l=o.isPacked;if(u==null){var h,f=this.activeTimers!=null;f&&(h=Dt());var d=o.texShape;if(d==null&&(d=Cl(a,l),o.texShape=d),s!=null){var p=Eo(a),v=void 0,m=d[1],g=d[0],x=s instanceof Uint8Array;l?(m=(n=$r(d[0],d[1]))[0],g=n[1],v=new rm(p,[g,m],x)):v=new nm(p,[g,m],x);var y=this.makeTensorInfo([g,m],i);this.texData.get(y.dataId).usage=x?mt.PIXELS:mt.UPLOAD,this.gpgpu.uploadDenseMatrixToTexture(this.getTexture(y.dataId),m,g,s);var b=this.runWebGLProgram(v,[y],i,null,!0),w=this.texData.get(b.dataId);o.texture=w.texture,o.texShape=w.texShape,o.isPacked=w.isPacked,o.usage=w.usage,this.disposeData(y.dataId),this.texData.delete(b.dataId),o.values=null,f&&(this.uploadWaitMs+=Dt()-h)}else{var C=this.acquireTexture(d,c,i,l);o.texture=C}}},t.prototype.convertAndCacheOnCPU=function(e,n){var o=this.texData.get(e),a=o.dtype;return this.releaseGPUData(e),n!=null&&(o.values=function(i,s){if(s==="float32"||s==="complex64")return i;if(s==="int32"||s==="bool"){for(var u=s==="int32"?new Int32Array(i.length):new Uint8Array(i.length),c=0;c<u.length;++c)u[c]=Math.round(i[c]);return u}throw new Error("Unknown dtype "+s)}(n,a)),o.values},t.prototype.acquireTexture=function(e,n,o,a){if(this.numBytesInGPU+=this.computeBytes(e,o),!this.warnedAboutMemory&&this.numBytesInGPU>1024*this.numMBBeforeWarning*1024){var i=(this.numBytesInGPU/1024/1024).toFixed(2);this.warnedAboutMemory=!0,console.warn("High memory usage in GPU: "+i+" MB, most likely due to a memory leak")}return this.textureManager.acquireTexture(e,n,a)},t.prototype.computeBytes=function(e,n){return e[0]*e[1]*qi(n)},t}(jl);rl()&&N.registerBackend("webgl",function(){return new Sh},2);var Qm=D({square_:function(r){var t=_(r,"x","square"),e=[t];return N.runKernelFunc(function(n,o){return o([t]),n.square(t)},{x:t},null,"Square",{},e,[])}}),Hr="SquaredDifference",kh=D({squaredDifference_:function(r,t){var e,n=_(r,"a","squaredDifference"),o=_(t,"b","squaredDifference");e=De(n,o),n=e[0],o=e[1],ve(n.shape,o.shape);var a={a:n,b:o},i=[n,o];return N.runKernelFunc(function(s,u){var c=s.squaredDifference(n,o);return u([n,o]),c},a,function(s,u){var c=u[0],l=u[1],h=K(2);return{a:function(){return s.mul(c.sub(l).mul(h))},b:function(){return s.mul(l.sub(c).mul(h))}}},Hr,{},i,[])}}),Zm=D({abs_:function(r){var t=_(r,"x","abs");return t.dtype==="complex64"?N.runKernelFunc(function(e){return e.complexAbs(t)},{$x:t}):N.runKernelFunc(function(e,n){var o=e.abs(t);return n([t]),o},{x:t},function(e,n){var o=n[0];return{x:function(){return e.mul(o.toFloat().step(-1))}}},"Abs")}}),eg=D({acos_:function(r){var t=_(r,"x","acos");return N.runKernelFunc(function(e,n){var o=e.acos(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.divStrict(K(1).sub(o.toFloat().square()).sqrt()).neg()}}})}}),tg=D({acosh_:function(r){var t=_(r,"x","acosh");return N.runKernelFunc(function(e,n){var o=e.acosh(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.divStrict(o.toFloat().square().sub(1).sqrt())}}})}}),ng=D({asin_:function(r){var t=_(r,"x","asin");return N.runKernelFunc(function(e,n){var o=e.asin(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.divStrict(K(1).sub(o.toFloat().square()).sqrt())}}})}}),rg=D({asinh_:function(r){var t=_(r,"x","asinh");return N.runKernelFunc(function(e,n){var o=e.asinh(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.divStrict(K(1).add(o.toFloat().square()).sqrt())}}})}}),og=D({atan_:function(r){var t=_(r,"x","atan");return N.runKernelFunc(function(e,n){var o=e.atan(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.div(o.toFloat().square().add(1))}}})}}),ag=D({atanh_:function(r){var t=_(r,"x","atanh");return N.runKernelFunc(function(e,n){var o=e.atanh(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.div(K(1).sub(o.toFloat().square()))}}})}}),ig=D({ceil_:function(r){var t=_(r,"x","ceil");return N.runKernelFunc(function(e){return e.ceil(t)},{$x:t},function(e){return{$x:function(){return ye(e)}}})}}),ps=D({clipByValue_:function(r,t,e){var n=_(r,"x","clipByValue");R(t<=e,function(){return"Error in clip: min ("+t+") must be less than or equal to max ("+e+")."});var o=[n],a={min:t,max:e};return N.runKernelFunc(function(i,s){var u=i.clip(n,t,e);return s([n]),u},{x:n},function(i,s){var u=s[0];return{x:function(){return i.where(u.greaterEqual(t).logicalAnd(u.lessEqual(e)),ye(i))}}},"ClipByValue",a,o)}}),sg=D({cos_:function(r){var t=_(r,"x","cos"),e=[t];return N.runKernelFunc(function(n,o){var a=n.cos(t);return o([t]),a},{x:t},function(n,o){var a=o[0];return{x:function(){return a.toFloat().sin().neg().mul(n)}}},"Cos",{},e)}}),ug=D({cosh_:function(r){var t=_(r,"x","cosh");return N.runKernelFunc(function(e,n){var o=e.cosh(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return o.toFloat().sinh().mulStrict(e)}}})}}),cg=D({erf_:function(r){var t=_(r,"x","erf");return R(t.dtype==="int32"||t.dtype==="float32",function(){return"Input dtype must be `int32` or `float32`."}),t.dtype==="int32"&&(t=t.toFloat()),N.runKernelFunc(function(e,n){var o=e.erf(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.mul(o.square().neg().exp().mul(2/Math.sqrt(Math.PI)))}}})}}),Ci=D({exp_:function(r){var t=_(r,"x","exp");return N.runKernelFunc(function(e,n){var o=e.exp(t);return n([o]),o},{x:t},function(e,n){return{x:function(){return e.mulStrict(n[0])}}},"Exp",{},[],[!0])}}),lg=D({expm1_:function(r){var t=_(r,"x","expm1");return N.runKernelFunc(function(e,n){var o=e.expm1(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.mul(o.exp())}}})}}),hg=D({floor_:function(r){var t=_(r,"x","floor");return N.runKernelFunc(function(e){return e.floor(t)},{$x:t},function(e){return{$x:function(){return ye(e)}}})}}),fg=D({log_:function(r){var t=_(r,"x","log"),e=[t];return N.runKernelFunc(function(n,o){var a=n.log(t);return o([t]),a},{x:t},function(n,o){var a=o[0];return{x:function(){return n.div(a.toFloat())}}},"Log",{},e)}}),dg=D({log1p_:function(r){var t=_(r,"x","log1p");return N.runKernelFunc(function(e,n){var o=e.log1p(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.div(o.add(1))}}})}}),pg=D({logSigmoid_:function(r){var t=_(r,"x","logSigmoid");return N.runKernelFunc(function(e,n){var o=e.softplus(t.neg()).neg();return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.mul(o.neg().sigmoid())}}})}}),Ho=D({neg_:function(r){var t=_(r,"x","neg"),e=[t];return N.runKernelFunc(function(n){return n.neg(t)},{x:t},function(n){return{x:function(){return n.neg()}}},"Neg",{},e)}}),vg=D({reciprocal_:function(r){var t=_(r,"x","reciprocal");return N.runKernelFunc(function(e,n){var o=e.reciprocal(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.div(o.square().neg())}}})}}),mg=D({round_:function(r){var t=_(r,"x","round");return N.runKernelFunc(function(e){return e.round(t)},{$x:t},function(e){return{$x:function(){return ye(e)}}})}}),Rh=D({rsqrt_:function(r){var t=_(r,"x","rsqrt"),e=[t];return N.runKernelFunc(function(n,o){var a=n.rsqrt(t);return o([t]),a},{x:t},function(n,o){var a=o[0];return{x:function(){return n.div(a.pow(1.5).mul(2)).neg()}}},"Rsqrt",{},e)}}),Ih=D({sigmoid_:function(r){var t=_(r,"x","sigmoid");return N.runKernelFunc(function(e,n){var o=e.sigmoid(t);return n([o]),o},{x:t},function(e,n){var o=n[0];return{x:function(){return e.mul(o.mul(K(1).sub(o)))}}},"Sigmoid")}}),gg=D({sign_:function(r){var t=_(r,"x","sign");return N.runKernelFunc(function(e){return e.sign(t)},{$x:t},function(e){return{$x:function(){return ye(e)}}})}}),yg=D({isNaN_:function(r){var t=_(r,"x","isNaN");return N.runKernelFunc(function(e){return e.isNaN(t)},{$x:t},function(e){return{$x:function(){return ye(e)}}})}}),xg=D({isInf_:function(r){var t=_(r,"x","isInf");return N.runKernelFunc(function(e){return e.isInf(t)},{$x:t},function(e){return{$x:function(){return ye(e)}}})}}),bg=D({isFinite_:function(r){var t=_(r,"x","isFinite");return N.runKernelFunc(function(e){return e.isFinite(t)},{$x:t},function(e){return{$x:function(){return ye(e)}}})}}),wg=D({sin_:function(r){var t=_(r,"x","sin"),e=[t];return N.runKernelFunc(function(n,o){var a=n.sin(t);return o([t]),a},{x:t},function(n,o){var a=o[0];return{x:function(){return a.toFloat().cos().mul(n)}}},"Sin",{},e)}}),Cg=D({sinh_:function(r){var t=_(r,"x","sinh");return N.runKernelFunc(function(e,n){var o=e.sinh(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return o.toFloat().cosh().mulStrict(e)}}})}}),_g=D({softplus_:function(r){var t=_(r,"x","softplus");return N.runKernelFunc(function(e,n){var o=e.softplus(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.mul(o.sigmoid())}}})}}),Eg=D({sqrt_:function(r){var t=_(r,"x","sqrt");return N.runKernelFunc(function(e,n){var o=e.sqrt(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.div(o.toFloat().sqrt().mul(2))}}})}}),Sg=D({step_:function(r,t){t===void 0&&(t=0);var e=_(r,"x","step");return N.runKernelFunc(function(n){return n.step(e,t)},{$x:e},function(n){return{$x:function(){return ye(n)}}})}}),kg=D({tan_:function(r){var t=_(r,"x","tan");return N.runKernelFunc(function(e,n){var o=e.tan(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return e.div(o.cos().square())}}})}}),Rg=D({tanh_:function(r){var t=_(r,"x","tanh");return N.runKernelFunc(function(e,n){var o=e.tanh(t);return n([o]),o},{x:t},function(e,n){var o=n[0];return{x:function(){return K(1).sub(o.square()).mulStrict(e)}}},"Tanh",{},null,[!0])}});function Ah(r,t,e,n,o,a){var i,s,u=_(r,"x","batchNorm"),c=_(t,"mean","batchNorm"),l=_(e,"variance","batchNorm");return o!=null&&(i=_(o,"scale","batchNorm")),n!=null&&(s=_(n,"offset","batchNorm")),R(u.rank===2,function(){return"Error in batchNorm3D: x must be rank 3 but got rank "+u.rank+"."}),R(c.rank===2||c.rank===1,function(){return"Error in batchNorm2D: mean must be rank 2 or rank 1 but got rank "+c.rank+"."}),R(l.rank===2||l.rank===1,function(){return"Error in batchNorm2D: variance must be rank 2 or rank 1 but got rank "+l.rank+"."}),i!=null&&R(i.rank===2||i.rank===1,function(){return"Error in batchNorm2D: scale must be rank 2 or rank 1 but got rank "+i.rank+"."}),s!=null&&R(s.rank===2||s.rank===1,function(){return"Error in batchNorm2D: offset must be rank 2 or rank 1 but got rank "+s.rank+"."}),Jr(u,c,l,s,i,a)}function Dh(r,t,e,n,o,a){var i,s,u=_(r,"x","batchNorm"),c=_(t,"mean","batchNorm"),l=_(e,"variance","batchNorm");return o!=null&&(i=_(o,"scale","batchNorm")),n!=null&&(s=_(n,"offset","batchNorm")),R(u.rank===3,function(){return"Error in batchNorm3D: x must be rank 3 but got rank "+u.rank+"."}),R(c.rank===3||c.rank===1,function(){return"Error in batchNorm3D: mean must be rank 3 or rank 1 but got rank "+c.rank+"."}),R(l.rank===3||l.rank===1,function(){return"Error in batchNorm3D: variance must be rank 3 or rank 1 but got rank "+l.rank+"."}),i!=null&&R(i.rank===3||i.rank===1,function(){return"Error in batchNorm3D: scale must be rank 3 or rank 1 but got rank "+i.rank+"."}),s!=null&&R(s.rank===3||s.rank===1,function(){return"Error in batchNorm3D: offset must be rank 3 or rank 1 but got rank "+s.rank+"."}),Jr(u,c,l,s,i,a)}function Th(r,t,e,n,o,a){var i,s,u=_(r,"x","batchNorm"),c=_(t,"mean","batchNorm"),l=_(e,"variance","batchNorm");return o!=null&&(i=_(o,"scale","batchNorm")),n!=null&&(s=_(n,"offset","batchNorm")),R(u.rank===4,function(){return"Error in batchNorm4D: x must be rank 4 but got rank "+u.rank+"."}),R(c.rank===4||c.rank===1,function(){return"Error in batchNorm4D: mean must be rank 4 or rank 1 but got rank "+c.rank+"."}),R(l.rank===4||l.rank===1,function(){return"Error in batchNorm4D: variance must be rank 4 or rank 1 but got rank "+l.rank+"."}),i!=null&&R(i.rank===4||i.rank===1,function(){return"Error in batchNorm4D: scale must be rank 4 or rank 1 but got rank "+i.rank+"."}),s!=null&&R(s.rank===4||s.rank===1,function(){return"Error in batchNorm4D: offset must be rank 4 or rank 1 but got rank "+s.rank+"."}),Jr(u,c,l,s,i,a)}function Jr(r,t,e,n,o,a){a==null&&(a=.001);var i,s,u,c=_(r,"x","batchNorm"),l=_(t,"mean","batchNorm"),h=_(e,"variance","batchNorm");o!=null&&(i=_(o,"scale","batchNorm")),n!=null&&(s=_(n,"offset","batchNorm")),R(l.rank===h.rank,function(){return"Batch normalization gradient requires mean and variance to have equal ranks."}),R(s==null||l.rank===s.rank,function(){return"Batch normalization gradient requires mean and offset to have equal ranks."}),R(i==null||l.rank===i.rank,function(){return"Batch normalization gradient requires mean and scale to have equal ranks."}),u=c.rank===0||c.rank===1?c.as4D(1,1,1,c.size):c.rank===2?c.as4D(1,1,c.shape[0],c.shape[1]):c.rank===3?c.as4D(1,c.shape[0],c.shape[1],c.shape[2]):c;var f=[c,l,h,i];return N.runKernelFunc(function(d,p){var v=d.batchNormalization(u,fo(l),fo(h),a,fo(i),fo(s));return p([c,l,h,i]),v},{x:c,mean:l,variance:h,scale:i,offset:s},function(d,p){var v=p,m=v[0],g=v[1],x=v[2],y=v[3],b=y==null?K(1):y,w=Be(g.shape,u.shape),C=[];if(g.rank===1){for(var I=0;I<u.shape.length-1;++I)C.push(u.shape[I]);C.push(1)}var E=m.sub(g),S=d.mul(b),k=Rh(x.add(K(a))),T=k.mul(k).mul(k).mul(K(-.5));return{x:function(){return g.rank===1?d.mul(ir(k.as4D(1,1,1,g.shape[0]),C)).mul(b).reshape(m.shape):d.mul(k).mul(b).reshape(m.shape)},mean:function(){var A=k.mul(K(-1)).mul(S);return g.rank===1&&(A=A.sum(w)),A.reshape(g.shape)},variance:function(){var A=T.mul(E).mul(S);return g.rank===1&&(A=A.sum(w)),A.reshape(g.shape)},scale:function(){var A=E.mul(k),F=d.mul(A);return g.rank===1&&(F=F.sum(w)),F.reshape(g.shape)},offset:function(){var A=d;return g.rank===1&&(A=A.sum(w)),A.reshape(g.shape)}}},"BatchNormalization",{varianceEpsilon:a},f).reshape(c.shape)}function fo(r){return r==null?null:r.rank===0?r.as1D():r.rank===1?r:r.rank===2?r.as4D(1,1,r.shape[0],r.shape[1]):r.rank===3?r.as4D(1,r.shape[0],r.shape[1],r.shape[2]):r}function ca(){Al("tf.batchNormalization() is going away. Use tf.batchNorm() instead, and note the positional argument change of scale, offset, and varianceEpsilon")}var Ig=D({batchNormalization2d_:function(r,t,e,n,o,a){return n===void 0&&(n=.001),ca(),Ah(r,t,e,a,o,n)}}),Ag=D({batchNormalization3d_:function(r,t,e,n,o,a){return n===void 0&&(n=.001),ca(),Dh(r,t,e,a,o,n)}}),Dg=D({batchNormalization4d_:function(r,t,e,n,o,a){return n===void 0&&(n=.001),ca(),Th(r,t,e,a,o,n)}}),Tg=D({batchNormalization_:function(r,t,e,n,o,a){return n===void 0&&(n=.001),ca(),Jr(r,t,e,a,o,n)}}),Nh=D({batchNorm_:Jr}),Ng=D({batchNorm2d_:Ah}),Fg=D({batchNorm3d_:Dh}),Pg=D({batchNorm4d_:Th}),la=D({logicalAnd_:function(r,t){var e=_(r,"a","logicalAnd","bool"),n=_(t,"b","logicalAnd","bool");return ve(e.shape,n.shape),N.runKernelFunc(function(o){return o.logicalAnd(e,n)},{a:e,b:n},null,"LogicalAnd")}}),Mg=D({logicalNot_:function(r){var t=_(r,"x","logicalNot","bool");return N.runKernelFunc(function(e){return e.logicalNot(t)},{$x:t})}}),Fh=D({logicalOr_:function(r,t){var e=_(r,"a","logicalOr","bool"),n=_(t,"b","logicalOr","bool");return ve(e.shape,n.shape),N.runKernelFunc(function(o){return o.logicalOr(e,n)},{$a:e,$b:n})}}),Og=D({logicalXor_:function(r,t){var e=_(r,"a","logicalXor","bool"),n=_(t,"b","logicalXor","bool");return ve(e.shape,n.shape),Fh(r,t).logicalAnd(la(r,t).logicalNot())}}),Un=D({where_:function(r,t,e){var n=_(t,"a","where"),o=_(e,"b","where"),a=_(r,"condition","where","bool");return be(n.shape,o.shape,"Error in where: "),a.rank===1?R(a.shape[0]===n.shape[0],function(){return"The first dimension of `a` must match the size of `condition`."}):be(a.shape,o.shape,"Error in where: "),N.runKernelFunc(function(i,s){var u=i.select(a,n,o);return s([a]),u},{$condition:a,$a:n,$b:o},function(i,s){var u=s[0];return{$condition:function(){return ye(u).toFloat()},$a:function(){return i.mul(u.cast(i.dtype))},$b:function(){return i.mul(u.logicalNot().cast(i.dtype))}}})}}),Ph=function(r){return Q(this,void 0,void 0,function(){var t,e,n;return Z(this,function(o){switch(o.label){case 0:return[4,(t=_(r,"condition","whereAsync","bool")).data()];case 1:return e=o.sent(),n=ls(t.shape,e),r!==t&&t.dispose(),[2,n]}})})},pe=D({add_:function(r,t){var e,n=_(r,"a","add"),o=_(t,"b","add");e=De(n,o),n=e[0],o=e[1];var a=ve(n.shape,o.shape);return N.runKernelFunc(function(i){return i.add(n,o)},{a:n,b:o},function(i){return{a:function(){var s=i,u=Be(n.shape,a);return u.length>0&&(s=s.sum(u)),s.reshape(n.shape)},b:function(){var s=i,u=Be(o.shape,a);return u.length>0&&(s=s.sum(u)),s.reshape(o.shape)}}},"Add")}}),Bg=D({addN_:function(r){R(Array.isArray(r),function(){return"The argument passed to tf.addN() must be a list of tensors"}),R(r.length>=1,function(){return"Must pass at least one tensor to tf.addN(), but got "+r.length});var t=r.map(function(o,a){return _(o,"tensors"+a,"addN")}),e=t[0];t.forEach(function(o){if(o.dtype!==e.dtype)throw new Error("All tensors passed to tf.addN() must have the same dtype")}),t.forEach(function(o){if(!Oe(o.shape,e.shape))throw new Error("All tensors passed to tf.addN() must have the same shape")});var n=t;return N.runKernelFunc(function(o){return o.addN(t)},n,function(o){var a={};return t.forEach(function(i,s){a[s]=function(){return o.clone()}}),a},"AddN")}}),Lg=D({addStrict_:function(r,t){var e=_(r,"a","addStrict"),n=_(t,"b","addStrict");return be(e.shape,n.shape,"Error in addStrict: "),e.add(n)}}),Wg=D({atan2_:function(r,t){var e,n=_(r,"a","atan2"),o=_(t,"b","atan2");e=De(n,o),n=e[0],o=e[1];var a=ve(n.shape,o.shape);return N.runKernelFunc(function(i,s){var u=i.atan2(n,o);return s([n,o]),u},{$a:n,$b:o},function(i,s){var u=s[0],c=s[1];return{$a:function(){var l=pe(u.square(),c.square()),h=i.mul(c.div(l)),f=Be(u.shape,a);return f.length>0&&(h=h.sum(f)),h.reshape(u.shape)},$b:function(){var l=pe(u.square(),c.square()),h=Ho(i.mul(u.div(l))),f=Be(c.shape,a);return f.length>0&&(h=h.sum(f)),h.reshape(c.shape)}}})}}),Tt=D({div_:function(r,t){var e,n=_(r,"a","div"),o=_(t,"b","div");if(e=De(n,o),n=e[0],o=e[1],n.dtype==="int32"&&o.dtype==="int32")return Mh(n,o);var a=ve(n.shape,o.shape);return N.runKernelFunc(function(i,s){var u=i.realDivide(n,o);return s([n,o]),u},{a:n,b:o},function(i,s){var u=s[0],c=s[1];return{a:function(){var l=i.div(c.toFloat()),h=Be(u.shape,a);return h.length>0?l.sum(h).reshape(u.shape):l},b:function(){var l=i.mul(u.toFloat()),h=Be(c.shape,a);h.length>0&&(l=l.sum(h).reshape(c.shape));var f=c.square();return l.div(f.toFloat()).neg()}}},"Div")}}),Vg=D({divNoNan_:function(r,t){var e,n=_(r,"a","div"),o=_(t,"b","div");n=(e=De(n,o))[0],o=e[1];var a=Tt(n,o),i=ye(a),s=o.equal(i);return Un(s,i,a)}}),zg=D({divStrict_:function(r,t){var e=_(r,"a","div"),n=_(t,"b","div");return be(e.shape,n.shape,"Error in divideStrict: "),e.div(n)}}),Mh=D({floorDiv_:function(r,t){var e,n=_(r,"a","floorDiv"),o=_(t,"b","floorDiv");e=De(n,o),n=e[0],o=e[1];var a=ve(n.shape,o.shape);return N.runKernelFunc(function(i,s){var u=i.floorDiv(n,o);return s([n,o]),u},{a:n,b:o},function(i,s){var u=s[0],c=s[1];return{a:function(){var l=i.div(c.toFloat()),h=Be(u.shape,a);return h.length>0?l.sum(h).reshape(u.shape):l},b:function(){var l=i.mul(u.toFloat()),h=Be(c.shape,a);h.length>0&&(l=l.sum(h).reshape(c.shape));var f=c.square();return l.div(f.toFloat()).neg()}}},"FloorDiv")}}),vs=D({maximum_:function(r,t){var e,n=_(r,"a","maximum"),o=_(t,"b","maximum");return e=De(n,o),n=e[0],o=e[1],n.dtype==="bool"&&(n=n.toInt(),o=o.toInt()),ve(n.shape,o.shape),N.runKernelFunc(function(a,i){var s=a.maximum(n,o);return i([n,o]),s},{a:n,b:o},function(a,i){var s=i[0],u=i[1];return{a:function(){return a.mul(s.greaterEqual(u).toFloat())},b:function(){return a.mul(s.less(u).toFloat())}}},"Maximum")}}),Ug=D({maximumStrict_:function(r,t){var e=_(r,"a","maximumStrict"),n=_(t,"b","maximumStrict");return be(e.shape,n.shape,"Error in maximumStrict: "),e.maximum(n)}}),Oh=D({minimum_:function(r,t){var e,n=_(r,"a","minimum"),o=_(t,"b","minimum");return e=De(n,o),n=e[0],o=e[1],n.dtype==="bool"&&(n=n.toInt(),o=o.toInt()),ve(n.shape,o.shape),N.runKernelFunc(function(a,i){var s=a.minimum(n,o);return i([n,o]),s},{a:n,b:o},function(a,i){var s=i[0],u=i[1];return{a:function(){return a.mul(s.lessEqual(u).toFloat())},b:function(){return a.mul(s.greater(u).toFloat())}}},"Minimum")}}),Gg=D({minimumStrict_:function(r,t){var e=_(r,"a","minimumStrict"),n=_(t,"b","minimumStrict");return be(e.shape,n.shape,"Error in minimumStrict: "),e.minimum(n)}}),Hg=D({mod_:function(r,t){var e,n=_(r,"a","mod"),o=_(t,"b","mod");e=De(n,o),n=e[0],o=e[1];var a=ve(n.shape,o.shape);return N.runKernelFunc(function(i,s){var u=i.mod(n,o);return s([n,o]),u},{$a:n,$b:o},function(i,s){var u=s[0],c=s[1];return{$a:function(){var l=Be(u.shape,a);return l.length>0?i.sum(l).reshape(u.shape):i},$b:function(){var l=i.mul(u.div(c).floor().neg()),h=Be(c.shape,a);return h.length>0?l.sum(h).reshape(c.shape):l}}})}}),qg=D({modStrict_:function(r,t){var e=_(r,"a","modStrict"),n=_(t,"b","modStrict");return be(e.shape,n.shape,"Error in modStrict: "),e.mod(n)}}),et=D({mul_:function(r,t){var e,n=_(r,"a","mul"),o=_(t,"b","mul");e=De(n,o),n=e[0],o=e[1];var a=ve(n.shape,o.shape);return N.runKernelFunc(function(i,s){var u=i.multiply(n,o);return s([n,o]),u},{a:n,b:o},function(i,s){var u=s[0],c=s[1];return{a:function(){var l=i.mul(c.toFloat()),h=Be(u.shape,a);return h.length>0?l.sum(h).reshape(u.shape):l},b:function(){var l=i.mul(u.toFloat()),h=Be(c.shape,a);return h.length>0?l.sum(h).reshape(c.shape):l}}},"Mul")}}),jg=D({mulStrict_:function(r,t){var e=_(r,"a","mul"),n=_(t,"b","mul");return be(e.shape,n.shape,"Error in multiplyStrict: "),e.mul(n)}}),qo=D({pow_:function(r,t){var e,n=_(r,"base","pow"),o=_(t,"exp","pow");e=De(n,o),n=e[0],o=e[1];var a=ve(n.shape,o.shape),i=[n,o];return N.runKernelFunc(function(s,u){var c=s.pow(n,o);return u([n,o,c]),c},{a:n,b:o},function(s,u){var c=u[0],l=u[1],h=u[2];return{a:function(){var f=l.toFloat(),d=s.mul(f.mul(c.pow(f.sub(K(1))))),p=Be(c.shape,a);return p.length>0&&(d=d.sum(p)),d.reshape(c.shape)},b:function(){var f=c.greater(0),d=c.log().where(f,ye(c)),p=s.mul(h.mul(d)),v=Be(l.shape,a);return v.length>0&&(p=p.sum(v)),p.reshape(l.shape)}}},"Pow",{},i,[!0])}}),Kg=D({powStrict_:function(r,t){return be(r.shape,t.shape,"Error in powStrict: "),r.pow(t)}}),Xg=D({squaredDifferenceStrict_:function(r,t){var e=_(r,"a","squaredDifferenceStrict"),n=_(t,"b","squaredDifferenceStrict");return be(e.shape,n.shape,"Error in squaredDifferenceStrict: "),e.squaredDifference(n)}}),Ue=D({sub_:function(r,t){var e,n=_(r,"a","sub"),o=_(t,"b","sub");e=De(n,o),n=e[0],o=e[1];var a=ve(n.shape,o.shape);return N.runKernelFunc(function(i){return i.subtract(n,o)},{a:n,b:o},function(i){return{a:function(){var s=i,u=Be(n.shape,a);return u.length>0&&(s=s.sum(u)),s.reshape(n.shape)},b:function(){var s=i,u=Be(o.shape,a);return u.length>0&&(s=s.sum(u)),s.neg().reshape(o.shape)}}},"Sub")}}),$g=D({subStrict_:function(r,t){var e=_(r,"a","subStrict"),n=_(t,"b","subStrict");return be(e.shape,n.shape,"Error in subStrict: "),e.sub(n)}}),Bh=D({equal_:function(r,t){var e,n=_(r,"a","equal"),o=_(t,"b","equal");return e=De(n,o),n=e[0],o=e[1],ve(n.shape,o.shape),N.runKernelFunc(function(a){return a.equal(n,o)},{$a:n,$b:o})}}),Yg=D({equalStrict_:function(r,t){var e=_(r,"a","equalStrict"),n=_(t,"b","equalStrict");return be(e.shape,n.shape,"Error in equalStrict: "),e.equal(n)}}),Jg=D({greater_:function(r,t){var e,n=_(r,"a","greater"),o=_(t,"b","greater");return e=De(n,o),n=e[0],o=e[1],ve(n.shape,o.shape),N.runKernelFunc(function(a){return a.greater(n,o)},{a:n,b:o},null,"Greater")}}),Lh=D({greaterEqual_:function(r,t){var e,n=_(r,"a","greaterEqual"),o=_(t,"b","greaterEqual");return e=De(n,o),n=e[0],o=e[1],ve(n.shape,o.shape),N.runKernelFunc(function(a,i){var s=a.greaterEqual(n,o);return i([n,o]),s},{a:n,b:o},function(a,i){var s=i[0],u=i[1];return{a:function(){return ye(s)},b:function(){return ye(u)}}},"GreaterEqual")}}),Qg=D({greaterEqualStrict_:function(r,t){var e=_(r,"a","greaterEqualStrict"),n=_(t,"b","greaterEqualStrict");return be(e.shape,n.shape,"Error in greaterEqualStrict: "),e.greaterEqual(n)}}),Zg=D({greaterStrict_:function(r,t){var e=_(r,"a","greaterStrict"),n=_(t,"b","greaterStrict");return be(e.shape,n.shape,"Error in greaterStrict: "),e.greater(n)}}),ey=D({less_:function(r,t){var e,n=_(r,"a","less"),o=_(t,"b","less");return e=De(n,o),n=e[0],o=e[1],ve(n.shape,o.shape),N.runKernelFunc(function(a){return a.less(n,o)},{a:n,b:o},null,"Less")}}),ty=D({lessEqual_:function(r,t){var e,n=_(r,"a","lessEqual"),o=_(t,"b","lessEqual");return e=De(n,o),n=e[0],o=e[1],ve(n.shape,o.shape),N.runKernelFunc(function(a,i){var s=a.lessEqual(n,o);return i([n,o]),s},{a:n,b:o},null,"LessEqual")}}),ny=D({lessEqualStrict_:function(r,t){var e=_(r,"a","lessEqualStrict"),n=_(t,"b","lessEqualStrict");return be(e.shape,n.shape,"Error in lessEqualStrict: "),e.lessEqual(n)}}),ry=D({lessStrict_:function(r,t){var e=_(r,"a","lessStrict"),n=_(t,"b","lessStrict");return be(e.shape,n.shape,"Error in lessStrict: "),e.less(n)}}),oy=D({notEqual_:function(r,t){var e,n=_(r,"a","notEqual"),o=_(t,"b","notEqual");return e=De(n,o),n=e[0],o=e[1],ve(n.shape,o.shape),N.runKernelFunc(function(a){return a.notEqual(n,o)},{a:n,b:o},null,"NotEqual")}}),ay=D({notEqualStrict_:function(r,t){var e=_(r,"a","notEqualStrict"),n=_(t,"b","notEqualStrict");return be(e.shape,n.shape,"Error in notEqualStrict: "),e.notEqual(n)}});function Pu(r,t){for(var e=[],n=r;n<t;++n)e.push(n);return e}function Mu(r){for(var t=[],e=0;e<r.length;++e)for(var n=0;n<r[e].length;++n)t.push(r[e][n]);return t}var ms=D({gather_:function(r,t,e){e===void 0&&(e=0);var n=_(r,"x","gather"),o=_(t,"indices","gather","int32");e=Le(e,n.shape)[0];var a=function(i,s,u){for(var c=i.shape[u],l=[],h=1,f=1,d=0;d<u;d++)l.push(i.shape[d]),h*=i.shape[d];for(d=0;d<s.rank;d++)l.push(s.shape[d]);for(d=u+1;d<i.rank;d++)l.push(i.shape[d]),f*=i.shape[d];return{batchSize:h,sliceSize:f,dimSize:c,outputShape:l}}(n,o,e);return N.runKernelFunc(function(i,s){var u=i.gather(n,o.flatten(),e);return s([o]),u},{x:n,indices:o},function(i,s){var u=s[0];return{x:function(){var c=n.shape,l=u.size,h=c.slice(0,e),f=h.length,d=c.slice(e,c.length).slice(1),p=d.length,v=Pu(0,f),m=Pu(f+1,f+1+p),g=Mu([h,[l],d]),x=i.reshape(g),y=u.reshape([l]),b=Mu([[f],v,m]),w=x.transpose(b),C=Wh(w,y,n.shape[e]),I=oa(b);return C=C.transpose(I)},indices:function(){return u}}},"Gather",{axis:e}).reshape(a.outputShape)}}),Wh=D({unsortedSegmentSum_:function(r,t,e){var n=_(r,"x","unsortedSegmentSum"),o=_(t,"segmentIds","unsortedSegmentSum","int32");return R(Fe(e),function(){return"numSegments must be of dtype int"}),N.runKernelFunc(function(a,i){var s=a.unsortedSegmentSum(n,o,e);return i([o]),s},{$x:n},function(a,i){var s=i[0];return{$x:function(){return function(u,c){for(var l=vs(c,ye(c)),h=ms(u,l),f=Lh(c,K(0,"int32")),d=h.rank-f.rank,p=0;p<d;++p)f=_t(f,p+1);f=la(f,xr(h.shape,"bool"));var v=ye(h);return Un(f,h,v)}(a,s)}}})}}),iy=function(r,t,e){return Q(this,void 0,void 0,function(){var n,o,a,i,s,u,c,l,h,f,d,p,v;return Z(this,function(m){switch(m.label){case 0:for(n=_(r,"tensor","boolMask"),o=_(t,"mask","boolMask","bool"),a=e==null?0:e,i=o.rank,s=n.shape,R(i>0,function(){return"mask cannot be scalar"}),be(s.slice(a,a+i),o.shape,"mask's shape must match the first K dimensions of tensor's shape,"),u=1,c=a;c<a+i;c++)u*=s[c];return l=s.slice(0,a).concat([u],s.slice(a+i)),h=n.reshape(l),f=o.reshape([-1]),[4,Ph(f)];case 1:return d=m.sent(),p=d.squeeze([1]),v=ms(h,p,a),r!==n&&n.dispose(),t!==o&&o.dispose(),p.dispose(),h.dispose(),f.dispose(),d.dispose(),[2,v]}})})};function Vh(r,t,e,n,o,a,i){a===void 0&&(a="NHWC"),R(r.length===t.rank,function(){return"Length of inShape ("+r.length+") and rank of dy ("+t.rank+") must match"});var s=r,u=t,c=!1;t.rank===3&&(c=!0,u=t.as4D(1,t.shape[0],t.shape[1],t.shape[2]),s=[1,r[0],r[1],r[2]]),R(s.length===4,function(){return"Error in conv2dDerInput: inShape must be length 4, but got length "+s.length+"."}),R(u.rank===4,function(){return"Error in conv2dDerInput: dy must be rank 4, but got rank "+u.rank}),R(e.rank===4,function(){return"Error in conv2dDerInput: filter must be rank 4, but got rank "+e.rank});var l=a==="NHWC"?s[3]:s[1],h=a==="NHWC"?u.shape[3]:u.shape[1];R(l===e.shape[2],function(){return"Error in conv2dDerInput: depth of input ("+l+") must match input depth for filter "+e.shape[2]+"."}),R(h===e.shape[3],function(){return"Error in conv2dDerInput: depth of output ("+h+") must match output depth for filter "+e.shape[3]+"."}),i!=null&&R(Fe(o),function(){return"Error in conv2dDerInput: pad must be an integer when using, dimRoundingMode "+i+" but got pad "+o+"."});var f=ua(a),d=In(s,e.shape,n,1,o,i,!1,f),p=N.runKernelFunc(function(v,m){var g=v.conv2dDerInput(u,e,d);return m([e,u]),g},{dy4D:u,filter:e},function(v,m){var g=m[0],x=m[1];return{dy4D:function(){return It(v,g,n,o,a,1,i)},filter:function(){return gs(v,x,g.shape,n,o,a,i)}}});return c?p.as3D(p.shape[1],p.shape[2],p.shape[3]):p}function za(r){var t=function(a){return typeof a=="number"?[a,a,a]:a.length===2?[a[0],a[1],1]:a}(r),e=t[0],n=t[1],o=t[2];return e===1&&n===1&&o===1}function zh(r,t,e,n,o){R(r.length===t.rank,function(){return"Length of inShape ("+r.length+") and rank of dy ("+t.rank+") must match"});var a=r,i=t,s=!1;t.rank===4&&(s=!0,i=t.as5D(1,t.shape[0],t.shape[1],t.shape[2],t.shape[3]),a=[1,r[0],r[1],r[2],r[3]]);var u=a[4],c=i.shape[4];R(a.length===5,function(){return"Error in conv3dDerInput: inShape must be length 5, but got length "+a.length+"."}),R(i.rank===5,function(){return"Error in conv3dDerInput: dy must be rank 5, but got rank "+i.rank}),R(e.rank===5,function(){return"Error in conv3dDerInput: filter must be rank 5, but got rank "+e.rank}),R(u===e.shape[3],function(){return"Error in conv3dDerInput: depth of input ("+u+") must match input depth for filter "+e.shape[3]+"."}),R(c===e.shape[4],function(){return"Error in conv3dDerInput: depth of output ("+c+") must match output depth for filter "+e.shape[4]+"."});var l=Gr(a,e.shape,n,1,o),h=N.runKernelFunc(function(f){return f.conv3dDerInput(i,e,l)},{dy5D:i});return s?h.as4D(h.shape[1],h.shape[2],h.shape[3],h.shape[4]):h}var sy=D({conv1d_:function(r,t,e,n,o,a,i){o===void 0&&(o="NWC"),a===void 0&&(a=1);var s=_(r,"x","conv1d"),u=_(t,"filter","conv1d"),c=s,l=!1;s.rank===2&&(l=!0,c=s.as3D(1,s.shape[0],s.shape[1])),R(c.rank===3,function(){return"Error in conv1d: input must be rank 3, but got rank "+c.rank+"."}),R(u.rank===3,function(){return"Error in conv1d: filter must be rank 3, but got rank "+u.rank+"."}),i!=null&&R(Fe(n),function(){return"Error in conv1d: pad must be an integer when using, dimRoundingMode "+i+" but got pad "+n+"."}),R(c.shape[2]===u.shape[1],function(){return"Error in conv1d: depth of input ("+c.shape[2]+") must match input depth for filter "+u.shape[1]+"."}),R(st(e,a),function(){return"Error in conv1D: Either stride or dilation must be 1. Got stride "+e+" and dilation '"+a+"'"}),R(o==="NWC",function(){return"Error in conv1d: got dataFormat of "+o+" but only NWC is currently supported."});var h=u.as4D(1,u.shape[0],u.shape[1],u.shape[2]),f=c.as4D(c.shape[0],1,c.shape[1],c.shape[2]),d=It(f,h,[1,e],n,"NHWC",[1,a],i);return l?d.as2D(d.shape[2],d.shape[3]):d.as3D(d.shape[0],d.shape[2],d.shape[3])}}),It=D({conv2d_:function(r,t,e,n,o,a,i){o===void 0&&(o="NHWC"),a===void 0&&(a=[1,1]);var s=_(r,"x","conv2d"),u=_(t,"filter","conv2d"),c=s,l=!1;s.rank===3&&(l=!0,c=s.as4D(1,s.shape[0],s.shape[1],s.shape[2])),R(c.rank===4,function(){return"Error in conv2d: input must be rank 4, but got rank "+c.rank+"."}),R(u.rank===4,function(){return"Error in conv2d: filter must be rank 4, but got rank "+u.rank+"."}),i!=null&&R(Fe(n),function(){return"Error in conv2d: pad must be an integer when using, dimRoundingMode "+i+" but got pad "+n+"."});var h=o==="NHWC"?c.shape[3]:c.shape[1];R(h===u.shape[2],function(){return"Error in conv2d: depth of input ("+h+") must match input depth for filter "+u.shape[2]+"."}),R(st(e,a),function(){return"Error in conv2D: Either strides or dilations must be 1. Got strides "+e+" and dilations '"+a+"'"});var f=ua(o),d=In(c.shape,u.shape,e,a,n,i,!1,f),p=[u,c],v=N.runKernelFunc(function(m,g){var x=m.conv2d(c,u,d);return g([u,c]),x},{x:c,filter:u},function(m,g){var x=g,y=x[0],b=x[1];return R(zn(a),function(){return"Error in gradient of conv2D: dilation rates greater than 1 are not yet supported in gradients. Got dilations '"+a+"'"}),{x:function(){return Uh(b.shape,m,y,e,n,o)},filter:function(){return gs(b,m,y.shape,e,n,o)}}},"Conv2D",d,p);return l?v.as3D(v.shape[1],v.shape[2],v.shape[3]):v}}),uy=D({conv3d_:function(r,t,e,n,o,a){o===void 0&&(o="NDHWC"),a===void 0&&(a=[1,1,1]);var i=_(r,"x","conv3d"),s=_(t,"filter","conv3d"),u=i,c=!1;i.rank===4&&(c=!0,u=i.as5D(1,i.shape[0],i.shape[1],i.shape[2],i.shape[3])),R(u.rank===5,function(){return"Error in conv3d: input must be rank 5, but got rank "+u.rank+"."}),R(s.rank===5,function(){return"Error in conv3d: filter must be rank 5, but got rank "+s.rank+"."}),R(u.shape[4]===s.shape[3],function(){return"Error in conv3d: depth of input ("+u.shape[4]+") must match input depth for filter "+s.shape[3]+"."}),R(function(f,d){return za(f)||za(d)}(e,a),function(){return"Error in conv3D: Either strides or dilations must be 1. Got strides "+e+" and dilations '"+a+"'"}),R(o==="NDHWC",function(){return"Error in conv3d: got dataFormat of "+o+" but only NDHWC is currently supported."});var l=Gr(u.shape,s.shape,e,a,n),h=N.runKernelFunc(function(f,d){var p=f.conv3d(u,s,l);return d([u,s]),p},{x:u,$filter:s},function(f,d){R(za(a),function(){return"Error in gradient of conv3D: dilation rates greater than 1 are not yet supported in gradients. Got dilations '"+a+"'"});var p=d[0],v=d[1];return{x:function(){return zh(p.shape,f,v,e,n)},$filter:function(){return function(m,g,x,y,b){var w=m;m.rank===4&&(w=m.as5D(1,m.shape[0],m.shape[1],m.shape[2],m.shape[3]));var C=g;C.rank===4&&(C=g.as5D(1,g.shape[0],g.shape[1],g.shape[2],g.shape[3])),R(w.rank===5,function(){return"Error in conv3dDerFilter: input must be rank 5, but got shape "+w.shape+"."}),R(C.rank===5,function(){return"Error in conv3dDerFilter: dy must be rank 5, but got shape "+C.shape+"."}),R(x.length===5,function(){return"Error in conv3dDerFilter: filterShape must be length 5, but got "+x+"."}),R(w.shape[4]===x[3],function(){return"Error in conv3dDerFilter: depth of input "+w.shape[4]+") must match input depth in filter ("+x[3]+"."}),R(C.shape[4]===x[4],function(){return"Error in conv3dDerFilter: depth of dy ("+C.shape[4]+") must match output depth for filter ("+x[4]+")."});var I=Gr(w.shape,x,y,1,b);return N.runKernelFunc(function(E){return E.conv3dDerFilter(w,C,I)},{x5D:w,dy5D:C})}(p,f,v.shape,e,n)}}});return c?h.as4D(h.shape[1],h.shape[2],h.shape[3],h.shape[4]):h}}),gs=D({conv2dDerFilter_:function(r,t,e,n,o,a,i){a===void 0&&(a="NHWC");var s=r;r.rank===3&&(s=r.as4D(1,r.shape[0],r.shape[1],r.shape[2]));var u=t;u.rank===3&&(u=t.as4D(1,t.shape[0],t.shape[1],t.shape[2])),R(s.rank===4,function(){return"Error in conv2dDerFilter: input must be rank 4, but got shape "+s.shape+"."}),R(u.rank===4,function(){return"Error in conv2dDerFilter: dy must be rank 4, but got shape "+u.shape+"."}),R(e.length===4,function(){return"Error in conv2dDerFilter: filterShape must be length 4, but got "+e+"."});var c=a==="NHWC"?s.shape[3]:s.shape[1],l=a==="NHWC"?u.shape[3]:u.shape[1];R(c===e[2],function(){return"Error in conv2dDerFilter: depth of input "+c+") must match input depth in filter ("+e[2]+"."}),R(l===e[3],function(){return"Error in conv2dDerFilter: depth of dy ("+l+") must match output depth for filter ("+e[3]+")."}),i!=null&&R(Fe(o),function(){return"Error in conv2dDerFilter: pad must be an integer when using, dimRoundingMode "+i+" but got pad "+o+"."});var h=ua(a),f=In(s.shape,e,n,1,o,i,!1,h);return N.runKernelFunc(function(d){return d.conv2dDerFilter(s,u,f)},{x4D:s,dy4D:u})}}),Uh=D({conv2dDerInput_:Vh}),ha=D({depthwiseConv2d_:function(r,t,e,n,o,a,i){a===void 0&&(a=[1,1]);var s=_(r,"x","depthwiseConv2d"),u=_(t,"filter","depthwiseConv2d"),c=s,l=!1;s.rank===3&&(l=!0,c=s.as4D(1,s.shape[0],s.shape[1],s.shape[2])),R(c.rank===4,function(){return"Error in depthwiseConv2d: input must be rank 4, but got rank "+c.rank+"."}),R(u.rank===4,function(){return"Error in depthwiseConv2d: filter must be rank 4, but got rank "+u.rank+"."}),R(c.shape[3]===u.shape[2],function(){return"Error in depthwiseConv2d: number of input channels ("+c.shape[3]+") must match the inChannels dimension in filter "+u.shape[2]+"."}),a==null&&(a=[1,1]),R(st(e,a),function(){return"Error in depthwiseConv2d: Either strides or dilations must be 1. Got strides "+e+" and dilations '"+a+"'"}),i!=null&&R(Fe(n),function(){return"Error in depthwiseConv2d: pad must be an integer when using, dimRoundingMode "+i+" but got pad "+n+"."});var h=In(c.shape,u.shape,e,a,n,i,!0),f=[c,u],d=N.runKernelFunc(function(p,v){var m=p.depthwiseConv2D(c,u,h);return v([c,u]),m},{x:c,filter:u},function(p,v){R(zn(a),function(){return"Error in gradient of depthwiseConv2d: dilation rates greater than 1 are not yet supported. Got dilations '"+a+"'"});var m=v[0],g=v[1];return{x:function(){return Gh(m.shape,p,g,h)},filter:function(){return Hh(m,p,g.shape,h)}}},"DepthwiseConv2dNative",h,f);return l?d.as3D(d.shape[1],d.shape[2],d.shape[3]):d}}),Gh=D({depthwiseConv2dDerInput_:function(r,t,e,n){var o=t,a=!1;t.rank===3&&(a=!0,o=t.as4D(1,t.shape[0],t.shape[1],t.shape[2]));var i=N.runKernelFunc(function(s){return s.depthwiseConv2DDerInput(o,e,n)},{dy4D:o});return a?i.as3D(i.shape[1],i.shape[2],i.shape[3]):i}}),Hh=D({depthwiseConv2dDerFilter_:function(r,t,e,n){var o=r;r.rank===3&&(o=r.as4D(1,r.shape[0],r.shape[1],r.shape[2]));var a=t;return a.rank===3&&(a=t.as4D(1,t.shape[0],t.shape[1],t.shape[2])),N.runKernelFunc(function(i){return i.depthwiseConv2DDerFilter(o,a,n)},{x4D:o,dy4D:a})}}),ys=D({separableConv2d_:function(r,t,e,n,o,a,i){a===void 0&&(a=[1,1]),i===void 0&&(i="NHWC");var s=_(r,"x","separableConv2d"),u=_(t,"depthwiseFilter","separableConv2d"),c=_(e,"pointwiseFilter","separableConv2d"),l=s,h=!1;if(s.rank===3&&(h=!0,l=s.as4D(1,s.shape[0],s.shape[1],s.shape[2])),i==="NCHW")throw new Error("separableConv2d currently does not support dataFormat NCHW; only NHWC is supported");R(l.rank===4,function(){return"Error in separableConv2d: input must be rank 4, but got rank "+l.rank+"."}),R(u.rank===4,function(){return"Error in separableConv2d: depthwise filter must be rank 4, but got rank "+u.rank+"."}),R(c.rank===4,function(){return"Error in separableConv2d: pointwise filter must be rank 4, but got rank "+u.rank+"."}),R(c.shape[0]===1,function(){return"Error in separableConv2d: the first dimension of pointwise filter  must be 1, but got "+c.shape[0]+"."}),R(c.shape[1]===1,function(){return"Error in separableConv2d: the second dimension of pointwise filter must be 1, but got "+c.shape[1]+"."});var f=u.shape[2],d=u.shape[3];R(c.shape[2]===f*d,function(){return"Error in separableConv2d: the third dimension of pointwise filter must be "+f*d+", but got "+c.shape[2]+"."});var p=ha(l,u,n,o,i,a),v=It(p,c,1,"valid",i);return h?v.as3D(v.shape[1],v.shape[2],v.shape[3]):v}}),cy=D({conv2dTranspose_:function(r,t,e,n,o,a){return Vh(e,_(r,"x","conv2dTranspose"),_(t,"filter","conv2dTranspose"),n,o,"NHWC",a)}}),ly=D({conv3dTranspose_:function(r,t,e,n,o){return zh(e,_(r,"x","conv3dTranspose"),_(t,"filter","conv3dTranspose"),n,o)}}),fa=D({matMul_:function(r,t,e,n){var o;e===void 0&&(e=!1),n===void 0&&(n=!1);var a=_(r,"a","matMul"),i=_(t,"b","matMul");o=De(a,i),a=o[0],i=o[1];var s=e?a.shape[a.rank-2]:a.shape[a.rank-1],u=n?i.shape[i.rank-1]:i.shape[i.rank-2],c=e?a.shape[a.rank-1]:a.shape[a.rank-2],l=n?i.shape[i.rank-2]:i.shape[i.rank-1],h=a.shape.slice(0,-2),f=i.shape.slice(0,-2),d=ee(h),p=ee(f);R(a.rank>=2&&i.rank>=2&&a.rank===i.rank,function(){return"Error in matMul: inputs must have the same rank of at least 2, got ranks "+a.rank+" and "+i.rank+"."}),R(Oe(h,f),function(){return"Error in matMul: outer dimensions ("+h+") and ("+f+") of Tensors with shapes "+a.shape+" and "+i.shape+" must match."}),R(s===u,function(){return"Error in matMul: inner shapes ("+s+") and ("+u+") of Tensors with shapes "+a.shape+" and "+i.shape+" and transposeA="+e+" and transposeB="+n+" must match."});var v=a.shape.slice(0,-2).concat([c,l]),m=e?a.as3D(d,s,c):a.as3D(d,c,s),g=n?i.as3D(p,l,u):i.as3D(p,u,l),x={transposeA:e,transposeB:n};return N.runKernelFunc(function(y,b){var w=y.batchMatMul(m,g,e,n);return b([m,g]),w},{a:m,b:g},function(y,b){var w=b,C=w[0],I=w[1];return e||n?!e&&n?{a:function(){return y.matMul(I,!1,!1)},b:function(){return y.matMul(C,!0,!1)}}:e&&!n?{a:function(){return I.matMul(y,!1,!0)},b:function(){return C.matMul(y,!1,!1)}}:{a:function(){return I.matMul(y,!0,!0)},b:function(){return y.matMul(C,!0,!0)}}:{a:function(){return y.matMul(I,!1,!0)},b:function(){return C.matMul(y,!0,!1)}}},"BatchMatMul",x).reshape(v)}}),hy=D({dot_:function(r,t){var e=_(r,"t1","dot"),n=_(t,"t2","dot");R(!(e.rank!==1&&e.rank!==2||n.rank!==1&&n.rank!==2),function(){return"Error in dot: inputs must all be rank 1 or 2, but got ranks "+e.rank+" and "+n.rank+"."});var o=e.rank===1?e.size:e.shape[1],a=n.rank===1?n.size:n.shape[0];return R(o===a,function(){return"Error in dot: inner dimensions of inputs must match, but got "+o+" and "+a+"."}),e.rank===1&&n.rank===1?e.as2D(1,-1).matMul(n.as2D(-1,1)).asScalar():e.rank===1&&n.rank===2?e.as2D(1,-1).matMul(n.as2D(n.shape[0],n.shape[1])).as1D():e.rank===2&&n.rank===1?e.matMul(n.as2D(-1,1)).as1D():e.matMul(n.as2D(n.shape[0],n.shape[1]))}}),fy=D({outerProduct_:function(r,t){var e=_(r,"v1","outerProduct"),n=_(t,"v2","outerProduct");return R(e.rank===1&&n.rank===1,function(){return"Error in outerProduct: inputs must be rank 1, but got ranks "+e.rank+" and "+n.rank+"."}),e.as2D(-1,1).matMul(n.as2D(1,-1))}}),Qr=D({reverse_:function(r,t){var e=_(r,"x","reverse");if(e.rank===0)return e.clone();var n=Le(t,e.shape);return N.runKernelFunc(function(o){return o.reverse(e,n)},{$x:e},function(o){return{$x:function(){return o.reverse(n)}}}).reshapeAs(e)}}),dy=D({reverse1d_:function(r){var t=_(r,"x","reverse");return R(t.rank===1,function(){return"Error in reverse1D: x must be rank 1 but got rank "+t.rank+"."}),Qr(t,0)}}),py=D({reverse2d_:function(r,t){var e=_(r,"x","reverse");return R(e.rank===2,function(){return"Error in reverse2D: x must be rank 2 but got rank "+e.rank+"."}),Qr(e,t)}}),vy=D({reverse3d_:function(r,t){var e=_(r,"x","reverse");return R(e.rank===3,function(){return"Error in reverse3D: x must be rank 3 but got rank "+e.rank+"."}),Qr(e,t)}}),my=D({reverse4d_:function(r,t){var e=_(r,"x","reverse");return R(e.rank===4,function(){return"Error in reverse4D: x must be rank 4 but got rank "+e.rank+"."}),Qr(e,t)}});function qh(r,t,e,n,o,a){var i=_(r,"x","maxPool"),s=i,u=!1;i.rank===3&&(u=!0,s=i.as4D(1,i.shape[0],i.shape[1],i.shape[2])),n==null&&(n=[1,1]),R(s.rank===4,function(){return"Error in maxPool: input must be rank 4 but got rank "+s.rank+"."}),R(st(e,n),function(){return"Error in maxPool: Either strides or dilations must be 1. Got strides "+e+" and dilations '"+n+"'"}),a!=null&&R(Fe(o),function(){return"Error in maxPool: pad must be an integer when using, dimRoundingMode "+a+" but got pad "+o+"."});var c=pr(s.shape,t,e,n,o,a);if(c.filterWidth===1&&c.filterHeight===1&&Oe(c.inShape,c.outShape))return i.clone();var l=[s],h=N.runKernelFunc(function(f,d){var p=f.maxPool(s,c);return d([s,p]),p},{x:s},function(f,d){var p=d[0],v=d[1];return{x:function(){return function(m,g,x,y,b,w,C,I){var E=_(m,"dy","maxPoolBackprop"),S=_(g,"input","maxPoolBackprop"),k=_(x,"output","maxPoolBackprop");R(S.rank===E.rank,function(){return"Rank of input ("+S.rank+") does not match rank of dy ("+E.rank+")"}),w==null&&(w=[1,1]),R(st(b,w),function(){return"Error in maxPoolBackProp: Either strides or dilations must be 1. Got strides "+b+" and dilations '"+w+"'"}),R(E.rank===4,function(){return"Error in maxPoolBackprop: dy must be rank 4 but got rank "+E.rank+"."}),R(S.rank===4,function(){return"Error in maxPoolBackprop: input must be rank 4 but got rank "+S.rank+"."}),I!=null&&R(Fe(C),function(){return"Error in maxPoolBackprop: pad must be an integer when using, dimRoundingMode "+I+" but got pad "+C+"."});var T=pr(S.shape,y,b,w,C,I);return N.runKernelFunc(function(A){return A.maxPoolBackprop(E,S,k,T)},{$dy:E,$input:S})}(f,p,v,t,e,n,o)}}},"MaxPool",c,l);return u?h.as3D(h.shape[1],h.shape[2],h.shape[3]):h}function jh(r,t,e,n,o,a){var i=_(r,"x","avgPool","float32");n==null&&(n=[1,1]),R(st(e,n),function(){return"Error in avgPool: Either strides or dilations must be 1. Got strides "+e+" and dilations '"+n+"'"});var s=i,u=!1;i.rank===3&&(u=!0,s=i.as4D(1,i.shape[0],i.shape[1],i.shape[2])),R(s.rank===4,function(){return"Error in avgPool: x must be rank 4 but got rank "+s.rank+"."}),a!=null&&R(Fe(o),function(){return"Error in avgPool: pad must be an integer when using, dimRoundingMode "+a+" but got pad "+o+"."});var c=pr(s.shape,t,e,n,o,a);if(c.filterWidth===1&&c.filterHeight===1&&Oe(c.inShape,c.outShape))return i.clone();var l=N.runKernelFunc(function(h){return h.avgPool(s,c)},{x:s},function(h){return{x:function(){return function(f,d,p,v,m,g){var x=_(f,"dy","avgPoolBackprop"),y=_(d,"input","avgPoolBackprop");R(y.rank===x.rank,function(){return"Rank of input ("+y.rank+") does not match rank of dy ("+x.rank+")"}),m==null&&(m=[1,1]),R(st(v,m),function(){return"Error in avgPoolBackprop: Either strides or dilations must be 1. Got strides "+v+" and dilations '"+m+"'"});var b=y,w=x,C=!1;y.rank===3&&(C=!0,b=y.as4D(1,y.shape[0],y.shape[1],y.shape[2]),w=x.as4D(1,x.shape[0],x.shape[1],x.shape[2])),R(w.rank===4,function(){return"Error in avgPoolBackprop: dy must be rank 4 but got rank "+w.rank+"."}),R(b.rank===4,function(){return"Error in avgPoolBackprop: input must be rank 4 but got rank "+b.rank+"."});var I=pr(b.shape,p,v,m,g),E=N.runKernelFunc(function(S){return S.avgPoolBackprop(w,b,I)},{dy4D:w,input4D:b});return C?E.as3D(E.shape[1],E.shape[2],E.shape[3]):E}(h,s,t,e,n,o)}}},"AvgPool",c);return l=l.cast(i.dtype),u?l.as3D(l.shape[1],l.shape[2],l.shape[3]):l}var Ge=D({maxPool_:function(r,t,e,n,o){return qh(r,t,e,1,n,o)}}),Zr=D({avgPool_:function(r,t,e,n,o){return jh(r,t,e,1,n,o)}}),gy=D({pool_:function(r,t,e,n,o,a){o==null&&(o=[1,1]),a==null&&(a=1),n===0&&(n="valid");var i=_(r,"x","maxPool"),s=i,u=!1;i.rank===3&&(u=!0,s=i.as4D(1,i.shape[0],i.shape[1],i.shape[2])),R(st(a,o),function(){return"Error in pool: Either strides or dilations must be 1. Got strides "+a+" and dilations '"+o+"'"});var c,l=pr(s.shape,t,a,o,n),h=[l.dilationHeight,l.dilationWidth];c=n==="same"?function(b,w){var C=b.map(function(S,k){return S+(S-1)*(w[k]-1)}).map(function(S){return S-1}),I=C.map(function(S){return Math.floor(S/2)}),E=C.map(function(S,k){return S-I[k]});return C.map(function(S,k){return[I[k],E[k]]})}([l.filterHeight,l.filterWidth],h):[[0,0],[0,0]];var f=h[0]===1&&h[1]===1,d=function(b,w,C){var I=C.map(function(P){return P[0]}),E=C.map(function(P){return P[1]}),S=b.concat(I,E),k=w.map(function(P,B){return(P-S[B]%P)%P}),T=E.map(function(P,B){return P+k[B]}),A=w.map(function(P,B){return[I[B],T[B]]}),F=w.map(function(P,B){return[0,k[B]]});return[A,F]}([l.inHeight,l.inWidth],h,c),p=d[0],v=d[1],m=f?n:"valid",g=f?s:Ol(s,h,p),x=(e==="avg"?function(){return jh(g,t,a,1,m)}:function(){return qh(g,t,a,1,m)})(),y=f?x:Fl(x,h,v);return u?y.as3D(y.shape[1],y.shape[2],y.shape[3]):y}}),yy=D({maxPool3d_:function(r,t,e,n,o,a,i){a===void 0&&(a="NDHWC");var s=_(r,"x","maxPool3d"),u=s,c=!1;s.rank===4&&(c=!0,u=s.as5D(1,s.shape[0],s.shape[1],s.shape[2],s.shape[3])),i==null&&(i=[1,1,1]),R(u.rank===5,function(){return"Error in maxPool3d: x must be rank 5 but got rank "+u.rank+"."}),R(a==="NDHWC",function(){return"Error in maxPool3d: Only NDHWC is currently supported, but got dataFormat of "+a}),R(st(e,i),function(){return"Error in maxPool3d: Either strides or dilations must be 1. Got strides "+e+" and dilations '"+i+"'"}),o!=null&&R(Fe(n),function(){return"Error in maxPool3d: pad must be an integer when using, dimRoundingMode "+o+" but got pad "+n+"."});var l=Ur(u.shape,t,e,i,n,o,a),h=N.runKernelFunc(function(f,d){var p=f.maxPool3d(u,l);return d([u,p]),p},{x:u},function(f,d){var p=d[0],v=d[1];return{x:function(){return function(m,g,x,y,b,w,C,I){var E=_(m,"dy","maxPool3dBackprop"),S=_(g,"input","maxPool3dBackprop"),k=_(x,"output","maxPool3dBackprop"),T=E,A=S,F=k,P=!1;S.rank===4&&(P=!0,T=E.as5D(1,E.shape[0],E.shape[1],E.shape[2],E.shape[3]),A=S.as5D(1,S.shape[0],S.shape[1],S.shape[2],S.shape[3]),F=k.as5D(1,k.shape[0],k.shape[1],k.shape[2],k.shape[3])),R(T.rank===5,function(){return"Error in maxPool3dBackprop: dy must be rank 5 but got rank "+T.rank+"."}),R(A.rank===5,function(){return"Error in maxPool3dBackprop: input must be rank 5 but got rank "+A.rank+"."}),R(F.rank===5,function(){return"Error in maxPool3dBackprop: output must be rank 5 but got rank "+F.rank+"."}),w==null&&(w=[1,1,1]),R(st(b,w),function(){return"Error in maxPool3dBackprop: Either strides or dilations must be 1. Got strides "+b+" and dilations '"+w+"'"}),I!=null&&R(Fe(C),function(){return"Error in maxPool3dBackprop: pad must be an integer when using, dimRoundingMode "+I+" but got pad "+C+"."});var B=Ur(A.shape,y,b,w,C,I),U=N.runKernelFunc(function(z){return z.maxPool3dBackprop(T,A,F,B)},{dy5D:T,input5D:A});return P?U.as4D(U.shape[1],U.shape[2],U.shape[3],U.shape[4]):U}(f,p,v,t,e,i,n,o)}}});return c?h.as4D(h.shape[1],h.shape[2],h.shape[3],h.shape[4]):h}}),xy=D({avgPool3d_:function(r,t,e,n,o,a,i){a===void 0&&(a="NDHWC");var s=_(r,"x","avgPool3d","float32"),u=s,c=!1;s.rank===4&&(c=!0,u=s.as5D(1,s.shape[0],s.shape[1],s.shape[2],s.shape[3])),i==null&&(i=[1,1,1]),R(u.rank===5,function(){return"Error in avgPool3d: x must be rank 5 but got rank "+u.rank+"."}),R(a==="NDHWC",function(){return"Error in avgPool3d: Only NDHWC is currently supported, but got dataFormat of "+a}),R(st(e,i),function(){return"Error in avgPool3d: Either strides or dilations must be 1. Got strides "+e+" and dilations '"+i+"'"}),o!=null&&R(Fe(n),function(){return"Error in avgPool3d: pad must be an integer when using, dimRoundingMode "+o+" but got pad "+n+"."});var l=Ur(u.shape,t,e,i,n,o,a),h=N.runKernelFunc(function(f){return f.avgPool3d(u,l)},{x:u},function(f){return{x:function(){return function(d,p,v,m,g,x,y){var b=_(d,"dy","avgPool3dBackprop"),w=_(p,"input","avgPool3dBackprop"),C=b,I=w,E=!1;w.rank===4&&(E=!0,C=b.as5D(1,b.shape[0],b.shape[1],b.shape[2],b.shape[3]),I=w.as5D(1,w.shape[0],w.shape[1],w.shape[2],w.shape[3])),R(C.rank===5,function(){return"Error in avgPool3dBackprop: dy must be rank 5 but got rank "+C.rank+"."}),R(I.rank===5,function(){return"Error in avgPool3dBackprop: input must be rank 5 but got rank "+I.rank+"."}),g==null&&(g=[1,1,1]),R(st(m,g),function(){return"Error in avgPool3dBackprop: Either strides or dilations must be 1. Got strides "+m+" and dilations '"+g+"'"}),y!=null&&R(Fe(x),function(){return"Error in maxPool3dBackprop: pad must be an integer when using, dimRoundingMode "+y+" but got pad "+x+"."});var S=Ur(I.shape,v,m,g,x,y),k=N.runKernelFunc(function(T){return T.avgPool3dBackprop(C,I,S)},{dy5D:C,input5D:I});return E?k.as4D(k.shape[1],k.shape[2],k.shape[3],k.shape[4]):k}(f,u,t,e,i,n,o)}}});return h=h.cast(u.dtype),c?h.as4D(h.shape[1],h.shape[2],h.shape[3],h.shape[4]):h}}),Jt=D({slice_:function(r,t,e){var n,o,a=_(r,"x","slice");if(a.rank===0)throw new Error("Slicing scalar is not possible");(n=typeof t=="number"?[t].concat(new Array(a.rank-1).fill(0)):t.length<a.rank?t.concat(new Array(a.rank-t.length).fill(0)):t.slice()).forEach(function(u){R(u!==-1,function(){return"slice() does not support negative begin indexing."})}),o=(o=e==null?new Array(a.rank).fill(-1):typeof e=="number"?[e].concat(new Array(a.rank-1).fill(-1)):e.length<a.rank?e.concat(new Array(a.rank-e.length).fill(-1)):e).map(function(u,c){return u>=0?u:(R(u===-1,function(){return"Negative size values should be exactly -1 but got "+u+" for the slice() size at index "+c+"."}),a.shape[c]-n[c])}),Ul(a,n,o);var i=a.shape,s={begin:n,size:o};return N.runKernelFunc(function(u){return u.slice(a,n,o)},{x:a},function(u){for(var c=[],l=0;l<u.rank;l++)c.push([n[l],i[l]-n[l]-o[l]]);return{x:function(){return u.pad(c)}}},"Slice",s)}}),by=D({slice1d_:function(r,t,e){var n=_(r,"x","slice1d");return R(n.rank===1,function(){return"slice1d expects a rank-1 tensor, but got a rank-"+n.rank+" tensor"}),Jt(n,[t],[e])}}),wy=D({slice2d_:function(r,t,e){var n=_(r,"x","slice2d");return R(n.rank===2,function(){return"slice2d expects a rank-2 tensor, but got a rank-"+n.rank+" tensor"}),Jt(n,t,e)}}),Kh=D({slice3d_:function(r,t,e){var n=_(r,"x","slice3d");return R(n.rank===3,function(){return"slice3d expects a rank-3 tensor, but got a rank-"+n.rank+" tensor"}),Jt(n,t,e)}}),Cy=D({slice4d_:function(r,t,e){var n=_(r,"x","slice4d");return R(n.rank===4,function(){return"slice4d expects a rank-4 tensor, but got a rank-"+n.rank+" tensor"}),Jt(n,t,e)}});function Xh(r,t,e,n,o){return t.rank<e.rank&&(t=t.reshape(it(t.shape,n))),r.rank<e.rank&&(r=r.reshape(it(r.shape,n))),{x:function(){var a=r.mul(e.equal(t).cast(r.dtype));return o==null?a:a.transpose(o)}}}var _y=D({all_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=_(r,"x","all","bool"),o=Le(t,n.shape),a=o,i=Ot(a,n.rank);i!=null&&(n=n.transpose(i),a=Bt(a.length,n.rank));var s=N.runKernelFunc(function(c){return c.all(n,a)},{$x:n});if(e){var u=it(s.shape,o);return s.reshape(u)}return s}}),Ey=D({any_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=_(r,"x","any","bool"),o=Le(t,n.shape),a=o,i=Ot(a,n.rank);i!=null&&(n=n.transpose(i),a=Bt(a.length,n.rank));var s=N.runKernelFunc(function(c){return c.any(n,a)},{$x:n});if(e){var u=it(s.shape,o);return s.reshape(u)}return s}}),Sy=D({argMax_:function(r,t){t===void 0&&(t=0);var e=_(r,"x","argMax");t==null&&(t=0);var n=Le(t,e.shape),o=Ot(n,e.rank);o!=null&&(e=e.transpose(o),n=Bt(n.length,e.rank));var a={axis:n[0]},i=[e];return N.runKernelFunc(function(s,u){var c=s.argMax(e,n[0]);return u([e]),c},{x:e},function(s,u){var c=u[0];return{x:function(){return ye(c)}}},"ArgMax",a,i)}}),ky=D({argMin_:function(r,t){t===void 0&&(t=0);var e=_(r,"x","argMin");t==null&&(t=0);var n=Le(t,e.shape),o=Ot(n,e.rank);return o!=null&&(e=e.transpose(o),n=Bt(n.length,e.rank)),N.runKernelFunc(function(a,i){var s=a.argMin(e,n[0]);return i([e]),s},{$x:e},function(a,i){var s=i[0];return{$x:function(){return ye(s)}}})}}),Ry=D({logSumExp_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=_(r,"x","logSumExp"),o=Le(t,n.shape),a=n.max(o,!0),i=n.sub(a).exp().sum(o).log(),s=a.reshape(i.shape).add(i);if(e){var u=it(s.shape,o);return s.reshape(u)}return s}}),da=D({max_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=_(r,"x","max"),o=n,a=Le(t,n.shape),i=a,s=Ot(i,n.rank);s!=null&&(n=n.transpose(s),i=Bt(i.length,n.rank));var u=[n],c=N.runKernelFunc(function(h,f){var d=h.max(n,i);return f([o,d]),d},{x:n},function(h,f){return Xh(h,f[1],f[0],a,s)},"Max",{axes:i},u,[!0]);if(e){var l=it(c.shape,a);c=c.reshape(l)}return c}}),Iy=D({mean_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=_(r,"x","mean"),o=Le(t,n.shape),a=ee(Xe(n.shape,o)[1]);return sa(function(i){var s=K(a);return{value:(s.dtype===i.dtype?i:i.cast(s.dtype)).div(s).sum(t,e),gradFunc:function(u){var c=i.shape.slice();return o.forEach(function(l){c[l]=1}),u.reshape(c).mul(xr(i.shape,"float32")).div(a)}}})(n)}}),Ay=D({min_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=_(r,"x","min"),o=n,a=Le(t,n.shape),i=a,s=Ot(i,n.rank);s!=null&&(n=n.transpose(s),i=Bt(i.length,n.rank));var u=[n],c=N.runKernelFunc(function(h,f){var d=h.min(n,i);return f([o,d]),d},{x:n},function(h,f){return Xh(h,f[1],f[0],a,s)},"Min",{axes:i},u,[!0]);if(e){var l=it(c.shape,a);c=c.reshape(l)}return c}}),Dy=D({moments_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=Le(t,(r=_(r,"x","moments")).shape),o=r.mean(n,e),a=o.shape;e||(a=it(o.shape,n));var i=r.toFloat().sub(o.reshape(a)).square();return{mean:o,variance:i.mean(n,e)}}}),$h=D({sum_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=_(r,"x","sum");n.dtype==="bool"&&(n=n.toInt());var o=Le(t,n.shape);return sa(function(a){var i=Ot(o,a.rank),s=o,u=a;i!=null&&(u=a.transpose(i),s=Bt(s.length,a.rank));var c=function(d){var p=a.shape.slice();return o.forEach(function(v){p[v]=1}),d.reshape(p).mul(xr(a.shape,"float32"))},l={axes:s},h=N.runKernelFunc(function(d){return d.sum(u,s)},{x:u},function(d){return{x:function(){return c(d)}}},"Sum",l);if(e){var f=it(h.shape,o);h=h.reshape(f)}return{value:h,gradFunc:c}})(n)}}),Ty=D({prod_:function(r,t,e){t===void 0&&(t=null),e===void 0&&(e=!1);var n=_(r,"x","prod");n.dtype==="bool"&&(n=n.toInt());var o=Le(t,n.shape),a=Ot(o,n.rank),i=o,s=n;a!=null&&(s=n.transpose(a),i=Bt(i.length,n.rank));var u=N.runKernelFunc(function(l){return l.prod(s,i)},{permutedX:s});if(e){var c=it(u.shape,o);u=u.reshape(c)}return u}}),Yh=D({elu_:function(r){var t=_(r,"x","elu");return N.runKernelFunc(function(e,n){var o=e.elu(t);return n([o]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){return N.runKernelFunc(function(a){return a.eluDer(e,o)},{dy:e,y:o})}}})}}),Ny=D({leakyRelu_:function(r,t){t===void 0&&(t=.2);var e=_(r,"x","leakyRelu");return vs(K(t).mul(e),e)}}),Jh=D({prelu_:function(r,t){var e=_(r,"x","prelu"),n=_(t,"alpha","prelu");return N.runKernelFunc(function(o,a){var i=o.prelu(e,n);return a([e,n]),i},{x:e,alpha:n},function(o,a){var i=a[0],s=a[1],u=i.greater(0);return{x:function(){return Un(u,o,o.mul(s))},alpha:function(){var c=Un(u,ye(o),o.mul(i)),l=Be(s.shape,o.shape);return l.length>0&&(c=c.sum(l)),c.reshape(s.shape)}}},"Prelu")}}),Ne=D({relu_:function(r){var t=_(r,"x","relu");return t.dtype==="bool"?t.toInt():N.runKernelFunc(function(e,n){var o=e.relu(t);return n([t]),o},{x:t},function(e,n){var o=n[0];return{x:function(){return e.mulStrict(o.step().toFloat())}}},"Relu")}}),Qh=D({relu6_:function(r){var t=_(r,"x","relu6");return t.dtype==="bool"?t.toInt():N.runKernelFunc(function(e,n){var o=e.relu6(t);return n([t]),o},{x:t},function(e,n){var o=n[0],a=o.lessEqual(6).mul(o.step());return{x:function(){return e.mulStrict(a.toFloat())}}},"Relu6")}}),Fy=D({selu_:function(r){var t=_(r,"x","selu");return N.runKernelFunc(function(e,n){var o=e.selu(t);return n([t]),o},{$x:t},function(e,n){var o=n[0];return{$x:function(){var a=o.greater(K(0)),i=K(fs),s=K(ds),u=e.mul(s),c=e.mul(i).mul(o.toFloat().exp());return Un(a,u,c)}}})}}),_n=D({transpose_:function(r,t){var e=_(r,"x","transpose");if(t==null&&(t=e.shape.map(function(o,a){return a}).reverse()),R(e.rank===t.length,function(){return"Error in transpose: rank of input "+e.rank+" must match length of perm "+t+"."}),t.forEach(function(o){R(o>=0&&o<e.rank,function(){return"All entries in 'perm' must be between 0 and "+(e.rank-1)+" but got "+t})}),e.rank<=1)return e.clone();var n={perm:t};return N.runKernelFunc(function(o){return o.transpose(e,t)},{x:e},function(o){var a=oa(t);return{x:function(){return o.transpose(a)}}},"Transpose",n)}}),Py=D({localResponseNormalization_:function(r,t,e,n,o){t===void 0&&(t=5),e===void 0&&(e=1),n===void 0&&(n=1),o===void 0&&(o=.5);var a=_(r,"x","localResponseNormalization");R(a.rank===4||a.rank===3,function(){return`Error in localResponseNormalization: x must be rank 3 or 4 but got
               rank `+a.rank+"."}),R(Fe(t),function(){return"Error in localResponseNormalization: depthRadius must be an integer but got depthRadius "+t+"."});var i=a,s=!1;a.rank===3&&(s=!0,i=a.as4D(1,a.shape[0],a.shape[1],a.shape[2]));var u=N.runKernelFunc(function(c,l){var h=c.localResponseNormalization4D(i,t,e,n,o);return l([i,h]),h},{x4D:i},function(c,l){var h=l[0],f=l[1];return{x4D:function(){return N.runKernelFunc(function(d){return d.LRNGrad(c,h,f,t,e,n,o)},{})}}});return s?u.as3D(u.shape[1],u.shape[2],u.shape[3]):u}}),Zh=D({norm_:function(r,t,e,n){t===void 0&&(t="euclidean"),e===void 0&&(e=null),n===void 0&&(n=!1);var o=function s(u,c,l){if(l===void 0&&(l=null),u.rank===0)return u.abs();if(u.rank!==1&&l===null)return s(u.reshape([-1]),c,l);if(u.rank===1||typeof l=="number"||Array.isArray(l)&&l.length===1){if(c===1)return u.abs().sum(l);if(c===1/0)return u.abs().max(l);if(c===-1/0)return u.abs().min(l);if(c==="euclidean"||c===2)return u.abs().pow(K(2,"int32")).sum(l).sqrt();throw new Error("Error in norm: invalid ord value: "+c)}if(Array.isArray(l)&&l.length===2){if(c===1)return u.abs().sum(l[0]).max(l[1]-1);if(c===1/0)return u.abs().sum(l[1]).max(l[0]);if(c===-1/0)return u.abs().sum(l[1]).min(l[0]);if(c==="fro"||c==="euclidean")return u.square().sum(l).sqrt();throw new Error("Error in norm: invalid ord value: "+c)}throw new Error("Error in norm: invalid axis: "+l)}(r=_(r,"x","norm"),t,e),a=o.shape;if(n){var i=Le(e,r.shape);a=it(o.shape,i)}return o.reshape(a)}}),My=D({basicLSTMCell_:function(r,t,e,n,o,a){var i=_(r,"forgetBias","basicLSTMCell"),s=_(t,"lstmKernel","basicLSTMCell"),u=_(e,"lstmBias","basicLSTMCell"),c=_(n,"data","basicLSTMCell"),l=_(o,"c","basicLSTMCell"),h=_(a,"h","basicLSTMCell"),f=c.concat(h,1).matMul(s).add(u),d=f.shape[0],p=f.shape[1]/4,v=[d,p],m=f.slice([0,0],v),g=f.slice([0,p],v),x=f.slice([0,2*p],v),y=f.slice([0,3*p],v),b=m.sigmoid().mulStrict(g.tanh()).addStrict(l.mulStrict(i.add(x).sigmoid())),w=b.tanh().mulStrict(y.sigmoid());return[b,w]}}),Oy=D({multiRNNCell_:function(r,t,e,n){for(var o=_(t,"data","multiRNNCell"),a=Bo(e,"c","multiRNNCell"),i=Bo(n,"h","multiRNNCell"),s=o,u=[],c=0;c<r.length;c++){var l=r[c](s,a[c],i[c]);u.push(l[0]),u.push(l[1]),s=l[1]}var h=[],f=[];for(c=0;c<u.length;c+=2)h.push(u[c]),f.push(u[c+1]);return[h,f]}}),By=D({movingAverage_:function(r,t,e,n,o){o===void 0&&(o=!0);var a=_(r,"v","movingAverage"),i=_(t,"x","movingAverage"),s=_(e,"decay","movingAverage");nl(a,i),R(Oe(a.shape,i.shape),function(){return"Shape mismatch in v and x"});var u=K(1),c=u.sub(s),l=i.sub(a).mul(c);if(o){R(n!=null,function(){return"When using zeroDebias: true, step is required."});var h=_(n,"step","movingAverage");l=l.div(u.sub(qo(s,h)))}return a.add(l)}}),Ly=D({stridedSlice_:function(r,t,e,n,o,a,i,s,u){if(o===void 0&&(o=0),a===void 0&&(a=0),i===void 0&&(i=0),s===void 0&&(s=0),u===void 0&&(u=0),n==null&&(n=new Array(t.length)),i!==0)throw new Error("ellipsis mask is not yet supported");var c=_(r,"x","stridedSlice"),l=xi(s),h=c.shape.slice();l.forEach(function(m){t[m]=0,e[m]=1,h.splice(m,0,1)}),c=c.reshape(h);for(var f=0;f<c.rank;f++)t[f]=Gl(o,t,n,c.shape,f),e[f]=Hl(a,e,n,c.shape,f),n[f]=n[f]||1;var d=xi(u);d.forEach(function(m){e[m]=t[m]+1,n[m]=1});var p=ia(t,e,n),v=p.filter(function(m,g){return d.indexOf(g)===-1});return n.every(function(m){return m===1})?Jt(c,t,p).reshape(v):N.runKernelFunc(function(m){return m.stridedSlice(c,t,e,n)},{$x:c}).reshape(v)}}),Wy=D({topk_:function(r,t,e){t===void 0&&(t=1),e===void 0&&(e=!0);var n=_(r,"x","topk");if(n.rank===0)throw new Error("topk() expects the input to be of rank 1 or higher");var o=n.shape[n.shape.length-1];if(t>o)throw new Error("'k' passed to topk() must be <= the last dimension ("+o+") but got "+t);var a=N.runKernelFunc(function(i){return i.topk(n,t,e)},{$x:n});return{values:a[0],indices:a[1]}}}),Vy=D({scatterND_:function(r,t,e){var n=_(r,"indices","scatterND","int32"),o=_(t,"updates","scatterND");return zl(o,n,e),N.runKernelFunc(function(a){return a.scatterND(n,o,e)},{indices:n,updates:o},null,"ScatterNd",{shape:e})}}),xs=D({fft_:function(r){R(r.dtype==="complex64",function(){return"The dtype for tf.spectral.fft() must be complex64 but got "+r.dtype+"."});var t=r.shape[r.shape.length-1],e=r.size/t,n=r.as2D(e,t);return N.runKernelFunc(function(o){return o.fft(n)},{input:r}).reshape(r.shape)}}),jo=D({ifft_:function(r){R(r.dtype==="complex64",function(){return"The dtype for tf.spectral.ifft() must be complex64 but got "+r.dtype+"."});var t=r.shape[r.shape.length-1],e=r.size/t,n=r.as2D(e,t);return N.runKernelFunc(function(o){return o.ifft(n)},{input:r}).reshape(r.shape)}}),bs=D({rfft_:function(r,t){R(r.dtype==="float32",function(){return"The dtype for rfft() must be real value but got "+r.dtype});var e,n=r.shape[r.shape.length-1],o=r.size/n;if(t!=null&&t<n){var a=r.shape.map(function(g){return 0}),i=r.shape.map(function(g){return g});i[r.shape.length-1]=t,e=r.slice(a,i),n=t}else if(t!=null&&t>n){var s=r.shape.map(function(g){return g});s[r.shape.length-1]=t-n,e=r.concat(Ie(s),r.shape.length-1),n=t}else e=r;var u=e.zerosLike(),c=Ke(e,u).as2D(o,n),l=xs(c),h=Math.floor(n/2)+1,f=St(l),d=jt(l),p=f.split([h,n-h],f.shape.length-1),v=d.split([h,n-h],d.shape.length-1),m=e.shape.slice();return m[e.shape.length-1]=h,Ke(p[0],v[0]).reshape(m)}}),ef=D({irfft_:function(r){var t=r.shape[r.shape.length-1],e=r.size/t;if(t<=2){var n=r.as2D(e,t),o=jo(n);return St(o)}var a=[e,2*(t-1)],i=St(r).as2D(e,t),s=jt(r).as2D(e,t),u=i.slice([0,1],[e,t-2]).reverse(1),c=s.slice([0,1],[e,t-2]).reverse(1).mul(K(-1)),l=i.concat(u,1),h=s.concat(c,1);return n=Ke(l,h).as2D(a[0],a[1]),o=jo(n),St(o)}}),zy=Object.freeze({fft:xs,ifft:jo,rfft:bs,irfft:ef}),Uy=D({sparseToDense_:function(r,t,e,n){n===void 0&&(n=0);var o=_(r,"sparseIndices","sparseToDense","int32"),a=_(t,"sparseValues","sparseToDense"),i=_(n,"defaultValue","sparseToDense",a.dtype);return function(s,u,c,l){if(s.dtype!=="int32")throw new Error("tf.sparseToDense() expects the indices to be int32 type, but the dtype was "+s.dtype+".");if(s.rank>2)throw new Error("sparseIndices should be a scalar, vector, or matrix, but got shape "+s.shape+".");var h=s.rank>0?s.shape[0]:1,f=s.rank>1?s.shape[1]:1;if(c.length!==f)throw new Error("outputShape has incorrect number of elements:, "+c.length+", should be: "+f+".");var d=u.size;if(u.rank!==0&&(u.rank!==1||d!==h))throw new Error("sparseValues has incorrect shape "+u.shape+", should be [] or ["+h+"]");if(u.dtype!==l.dtype)throw new Error("sparseValues.dtype must match defaultValues.dtype")}(o,a,e,i),N.runKernelFunc(function(s){return s.sparseToDense(o,a,e,i)},{$sparseIndices:o,$sparseValues:a,$defaultValue:i})}}),Gy=D({gatherND_:function(r,t){var e=_(t,"indices","gatherND","int32"),n=_(r,"x","gatherND");return N.runKernelFunc(function(o){return o.gatherND(n,e)},{x:n,indices:e},null,"GatherNd")}}),Hy=D({diag_:function(r){var t=_(r,"x","diag").flatten(),e=r.shape.concat(r.shape);return N.runKernelFunc(function(n){return n.diag(t)},{$x:t}).reshape(e)}}),qy=D({dropout_:function(r,t,e,n){var o=_(r,"x","dropout");if(R(o.dtype==="float32",function(){return"x has to be a floating point tensor since it's going to be scaled, but got a "+o.dtype+" tensor instead."}),R(t>=0&&t<1,function(){return"rate must be a float in the range [0, 1), but got "+t+"."}),t===0)return r instanceof Te?o.clone():o;var a=function(u,c){if(c==null)return u.shape.slice();if(Oe(u.shape,c))return c;if(u.shape.length===c.length){for(var l=[],h=0;h<u.shape.length;h++)c[h]==null&&u.shape[h]!=null?l.push(u.shape[h]):l.push(c[h]);return l}return c}(o,e),i=1-t,s=Ml(a,0,1,"float32",n).add(i).floor().div(i);return o.mul(s)}});function tf(r,t,e){for(var n=1-r%2,o=new Float32Array(r),a=0;a<r;++a){var i=2*Math.PI*a/(r+n-1);o[a]=t-e*Math.cos(i)}return Me(o,"float32")}var ws=D({hannWindow_:function(r){return tf(r,.5,.5)}}),nf=D({hammingWindow_:function(r){return tf(r,.54,.46)}}),Cs=D({frame_:function(r,t,e,n,o){n===void 0&&(n=!1),o===void 0&&(o=0);for(var a=0,i=[];a+t<=r.size;)i.push(Jt(r,a,t)),a+=e;if(n)for(;a<r.size;){var s=a+t-r.size,u=Ve([Jt(r,a,t-s),Xt([s],o)]);i.push(u),a+=e}return i.length===0?bn([],[0,t]):Ve(i).as2D(i.length,t)}}),rf=D({stft_:function(r,t,e,n,o){var a;o===void 0&&(o=ws),n==null&&(a=t,n=Math.floor(Math.pow(2,Math.ceil(Math.log(a)/Math.log(2)))));for(var i=Cs(r,t,e),s=et(i,o(t)),u=[],c=0;c<i.shape[0];c++)u.push(bs(s.slice([c,0],[1,t]),n));return Ve(u)}}),jy=Object.freeze({hannWindow:ws,hammingWindow:nf,frame:Cs,stft:rf}),Ze,Ky=function(r,t,e){return e===void 0&&(e=1),Q(this,void 0,void 0,function(){var n,o,a,i,s,u,c,l,h,f,d,p,v,m;return Z(this,function(g){switch(g.label){case 0:return n=_(r,"predictions","inTopK"),o=_(t,"targets","inTopK"),R(n.rank>1,function(){return"inTopK() expects the predictions to be of rank 2 or higher, but got "+n.rank}),R(n.rank-1===o.rank,function(){return"predictions rank should be 1 larger than targets rank, but got predictions rank "+n.rank+" and targets rank "+o.rank}),be(n.shape.slice(0,n.shape.length-1),o.shape,"predictions's shape should be align with the targets' shape, except the last dimension."),a=n.shape[n.shape.length-1],R(e>0&&e<=a,function(){return"'k' passed to inTopK() must be > 0 && <= the predictions last dimension ("+a+"), but got "+e}),[4,n.data()];case 1:return i=g.sent(),[4,o.data()];case 2:for(s=g.sent(),u=[i.length/a,a],l=u[1],h=fr("bool",c=u[0]),f=0;f<c;f++){for(d=f*l,p=i.subarray(d,d+l),v=[],m=0;m<p.length;m++)v.push({value:p[m],index:m});for(v.sort(function(x,y){return y.value-x.value}),h[f]=0,m=0;m<e;m++)if(v[m].index===s[f]){h[f]=1;break}}return r!==n&&n.dispose(),t!==o&&o.dispose(),[2,$e(h,o.shape,"bool")]}})})};(function(r){r[r.NONE=0]="NONE",r[r.MEAN=1]="MEAN",r[r.SUM=2]="SUM",r[r.SUM_BY_NONZERO_WEIGHTS=3]="SUM_BY_NONZERO_WEIGHTS"})(Ze||(Ze={}));var Xy=D({absoluteDifference_:function(r,t,e,n){n===void 0&&(n=Ze.SUM_BY_NONZERO_WEIGHTS);var o=_(r,"labels","absoluteDifference"),a=_(t,"predictions","absoluteDifference"),i=null;e!=null&&(i=_(e,"weights","absoluteDifference")),be(o.shape,a.shape,"Error in absoluteDifference: ");var s=o.sub(a).abs();return ln(s,i,n)}}),ln=D({computeWeightedLoss_:function(r,t,e){e===void 0&&(e=Ze.SUM_BY_NONZERO_WEIGHTS);var n=_(r,"losses","computeWeightedLoss"),o=null;t!=null&&(o=_(t,"weights","computeWeightedLoss"));var a=o==null?n:n.mul(o);if(e===Ze.NONE)return a;if(e===Ze.SUM)return a.sum();if(e===Ze.MEAN){if(o==null)return a.mean();var i=n.size/o.size,s=a.sum().div(o.sum());return i>1?s.div(K(i)):s}if(e===Ze.SUM_BY_NONZERO_WEIGHTS){if(o==null)return a.sum().div(K(n.size));var u=o.mul(xr(n.shape)).notEqual(K(0)).sum().toFloat();return a.sum().div(u)}throw Error("Unknown reduction: "+e)}}),$y=D({cosineDistance_:function(r,t,e,n,o){o===void 0&&(o=Ze.SUM_BY_NONZERO_WEIGHTS);var a=_(r,"labels","cosineDistance"),i=_(t,"predictions","cosineDistance"),s=null;n!=null&&(s=_(n,"weights","cosineDistance")),be(a.shape,i.shape,"Error in cosineDistance: ");var u=K(1).sub(a.mul(i).sum(e,!0));return ln(u,s,o)}}),Yy=D({hingeLoss_:function(r,t,e,n){n===void 0&&(n=Ze.SUM_BY_NONZERO_WEIGHTS);var o=_(r,"labels","hingeLoss"),a=_(t,"predictions","hingeLoss"),i=null;e!=null&&(i=_(e,"weights","hingeLoss")),be(o.shape,a.shape,"Error in hingeLoss: ");var s=K(1);o=K(2).mul(o).sub(s);var u=s.sub(o.mul(a)).relu();return ln(u,i,n)}}),Jy=D({huberLoss_:function(r,t,e,n,o){n===void 0&&(n=1),o===void 0&&(o=Ze.SUM_BY_NONZERO_WEIGHTS);var a=_(r,"labels","huberLoss"),i=_(t,"predictions","huberLoss"),s=null;e!=null&&(s=_(e,"weights","huberLoss")),be(a.shape,i.shape,"Error in huberLoss: ");var u=K(n),c=i.sub(a).abs(),l=Oh(c,u),h=c.sub(l),f=K(.5).mul(l.square()).add(u.mul(h));return ln(f,s,o)}}),Qy=D({logLoss_:function(r,t,e,n,o){n===void 0&&(n=1e-7),o===void 0&&(o=Ze.SUM_BY_NONZERO_WEIGHTS);var a=_(r,"labels","logLoss"),i=_(t,"predictions","logLoss"),s=null;e!=null&&(s=_(e,"weights","logLoss")),be(a.shape,i.shape,"Error in logLoss: ");var u=K(1),c=K(n),l=a.mul(i.add(c).log()).neg().sub(u.sub(a).mul(u.sub(i).add(c).log()));return ln(l,s,o)}}),Zy=D({meanSquaredError_:function(r,t,e,n){n===void 0&&(n=Ze.SUM_BY_NONZERO_WEIGHTS);var o=_(r,"labels","meanSquaredError"),a=_(t,"predictions","meanSquaredError"),i=null;e!=null&&(i=_(e,"weights","meanSquaredError")),be(o.shape,a.shape,"Error in meanSquaredError: ");var s=o.squaredDifference(a);return ln(s,i,n)}}),e0=D({sigmoidCrossEntropy_:function(r,t,e,n,o){n===void 0&&(n=0),o===void 0&&(o=Ze.SUM_BY_NONZERO_WEIGHTS);var a=_(r,"multiClassLabels","sigmoidCrossEntropy"),i=_(t,"logits","sigmoidCrossEntropy"),s=null;if(e!=null&&(s=_(e,"weights","sigmoidCrossEntropy")),be(a.shape,i.shape,"Error in sigmoidCrossEntropy: "),n>0){var u=K(n),c=K(1),l=K(.5);a=a.mul(c.sub(u)).add(l.mul(u))}var h=function(f,d){var p=_(f,"labels","sigmoidCrossEntropyWithLogits"),v=_(d,"logits","sigmoidCrossEntropyWithLogits");be(p.shape,v.shape,"Error in sigmoidCrossEntropyWithLogits: ");var m=v.relu(),g=v.mul(p),x=v.abs().neg().exp().log1p();return m.sub(g).add(x)}(a,i);return ln(h,s,o)}}),t0=D({softmaxCrossEntropy_:function(r,t,e,n,o){n===void 0&&(n=0),o===void 0&&(o=Ze.SUM_BY_NONZERO_WEIGHTS);var a=_(r,"onehotLabels","softmaxCrossEntropy"),i=_(t,"logits","softmaxCrossEntropy"),s=null;if(e!=null&&(s=_(e,"weights","softmaxCrossEntropy")),be(a.shape,i.shape,"Error in softmaxCrossEntropy: "),n>0){var u=K(n),c=K(1),l=K(a.shape[1]);a=a.mul(c.sub(u)).add(u.div(l))}var h=function(f,d,p){if(p===void 0&&(p=-1),p===-1&&(p=d.rank-1),p!==d.rank-1)throw Error("Softmax cross entropy along a non-last dimension is not yet supported. Labels / logits was rank "+d.rank+" and dim was "+p);return sa(function(v,m,g){var x=m.logSumExp([p],!0),y=m.toFloat().sub(x);return g([v,y]),{value:y.mul(v).neg().sum([p]),gradFunc:function(b,w){var C=w[0],I=w[1],E=it(b.shape,[p]);return[b.reshape(E).mul(C.toFloat().sub(I.exp())),b.reshape(E).mul(I.exp().sub(C.toFloat()))]}}})(f,d)}(a,i);return ln(h,s,o)}}),n0=Object.freeze({get Reduction(){return Ze},absoluteDifference:Xy,computeWeightedLoss:ln,cosineDistance:$y,hingeLoss:Yy,huberLoss:Jy,logLoss:Qy,meanSquaredError:Zy,sigmoidCrossEntropy:e0,softmaxCrossEntropy:t0});function Ou(r,t){return t===void 0&&(t=!1),N.tidy(function(){if(r.shape.length!==2)throw new Error("qr2d() requires a 2D Tensor, but got a "+r.shape.length+"D Tensor.");for(var e=r.shape[0],n=r.shape[1],o=Pl(e),a=r.clone(),i=bn([[1]],[1,1]),s=i.clone(),u=e>=n?n:e,c=function(h){var f,d=a,p=s,v=o;f=N.tidy(function(){var m=a.slice([h,h],[e-h,1]),g=m.norm(),x=a.slice([h,h],[1,1]),y=bn([[-1]]).where(x.greater(0),bn([[1]])),b=x.sub(y.mul(g)),w=m.div(b);s=w.shape[0]===1?i.clone():i.concat(w.slice([1,0],[w.shape[0]-1,w.shape[1]]),0);var C=y.matMul(b).div(g).neg(),I=a.slice([h,0],[e-h,n]),E=C.mul(s);if(h===0)a=I.sub(E.matMul(s.transpose().matMul(I)));else{var S=I.sub(E.matMul(s.transpose().matMul(I)));a=a.slice([0,0],[h,n]).concat(S,0)}var k=o.slice([0,h],[e,o.shape[1]-h]);if(h===0)o=k.sub(k.matMul(s).matMul(E.transpose()));else{var T=k.sub(k.matMul(s).matMul(E.transpose()));o=o.slice([0,0],[e,h]).concat(T,1)}return[s,a,o]}),s=f[0],a=f[1],o=f[2],ft([d,p,v])},l=0;l<u;++l)c(l);return!t&&e>n&&(o=o.slice([0,0],[e,n]),a=a.slice([0,0],[n,n])),[o,a]})}var r0=D({bandPart_:function(r,t,e){if(t%1!=0)throw new Error("bandPart(): numLower must be an integer, got "+t+".");if(e%1!=0)throw new Error("bandPart(): numUpper must be an integer, got "+e+".");var n=_(r,"a","bandPart");if(n.rank<2)throw new Error("bandPart(): Rank must be at least 2, got "+n.rank+".");var o=n.shape,a=n.shape.slice(-2),i=a[0],s=a[1];if(!(t<=i))throw new Error("bandPart(): numLower ("+t+") must not be greater than the number of rows ("+i+").");if(!(e<=s))throw new Error("bandPart(): numUpper ("+e+") must not be greater than the number of columns ("+s+").");t<0&&(t=i),e<0&&(e=s);var u=Lo(0,i,1,"int32").reshape([-1,1]),c=Lo(0,s,1,"int32"),l=Ue(u,c),h=la(l.lessEqual(K(+t,"int32")),l.greaterEqual(K(-e,"int32"))),f=Ie([i,s],n.dtype);return bt(ze(n.reshape([-1,i,s])).map(function(d){return Un(h,d,f)})).reshape(o)}}),o0=D({gramSchmidt_:function(r){var t;if(Array.isArray(r)){t=!1,R(r!=null&&r.length>0,function(){return"Gram-Schmidt process: input must not be null, undefined, or empty"});for(var e=r[0].shape[0],n=function(u){R(r[u].shape[0]===e,function(){return"Gram-Schmidt: Non-unique lengths found in the input vectors: ("+r[u].shape[0]+" vs. "+e+")"})},o=1;o<r.length;++o)n(o)}else t=!0,r=Zi(r,r.shape[0],0).map(function(u){return Bl(u,[0])});R(r.length<=r[0].shape[0],function(){return"Gram-Schmidt: Number of vectors ("+r.length+") exceeds number of dimensions ("+r[0].shape[0]+")."});var a=[],i=r,s=function(u){a.push(N.tidy(function(){var c=i[u];if(u>0)for(var l=0;l<u;++l){var h=$h(a[l].mulStrict(c)).mul(a[l]);c=c.sub(h)}return c.div(Zh(c,"euclidean"))}))};for(o=0;o<r.length;++o)s(o);return t?bt(a,0):a}}),a0=D({qr_:function(r,t){if(t===void 0&&(t=!1),r.rank<2)throw new Error("qr() requires input tensor to have a rank >= 2, but got rank "+r.rank);if(r.rank===2)return Ou(r,t);var e=r.shape.slice(0,r.shape.length-2).reduce(function(i,s){return i*s}),n=ze(r.reshape([e,r.shape[r.shape.length-2],r.shape[r.shape.length-1]]),0),o=[],a=[];return n.forEach(function(i){var s=Ou(i,t),u=s[0],c=s[1];o.push(u),a.push(c)}),[bt(o,0).reshape(r.shape),bt(a,0).reshape(r.shape)]}}),i0=Object.freeze({bandPart:r0,gramSchmidt:o0,qr:a0});function pa(r,t,e,n,o,a){n==null&&(n=.5),o==null&&(o=Number.NEGATIVE_INFINITY),a==null&&(a=0);var i=r.shape[0];return e=Math.min(e,i),R(0<=n&&n<=1,function(){return"iouThreshold must be in [0, 1], but was '"+n+"'"}),R(r.rank===2,function(){return"boxes must be a 2D tensor, but was of rank '"+r.rank+"'"}),R(r.shape[1]===4,function(){return"boxes must have 4 columns, but 2nd dimension was "+r.shape[1]}),R(t.rank===1,function(){return"scores must be a 1D tensor"}),R(t.shape[0]===i,function(){return"scores has incompatible shape with boxes. Expected "+i+", but was "+t.shape[0]}),R(0<=a&&a<=1,function(){return"softNmsSigma must be in [0, 1], but was '"+a+"'"}),{maxOutputSize:e,iouThreshold:n,scoreThreshold:o,softNmsSigma:a}}var s0=D({resizeBilinear_:function(r,t,e){e===void 0&&(e=!1);var n=_(r,"images","resizeBilinear");R(n.rank===3||n.rank===4,function(){return"Error in resizeBilinear: x must be rank 3 or 4, but got rank "+n.rank+"."}),R(t.length===2,function(){return"Error in resizeBilinear: new shape must 2D, but got shape "+t+"."});var o=n,a=!1;n.rank===3&&(a=!0,o=n.as4D(1,n.shape[0],n.shape[1],n.shape[2]));var i=t[0],s=t[1],u=N.runKernelFunc(function(c,l){return l([o]),c.resizeBilinear(o,i,s,e)},{x:o},function(c,l){return{x:function(){return N.runKernelFunc(function(h){return h.resizeBilinearBackprop(c,l[0],e)},{})}}},"ResizeBilinear",{alignCorners:e,newHeight:i,newWidth:s});return a?u.as3D(u.shape[1],u.shape[2],u.shape[3]):u}}),u0=D({resizeNearestNeighbor_:function(r,t,e){e===void 0&&(e=!1);var n=_(r,"images","resizeNearestNeighbor");R(n.rank===3||n.rank===4,function(){return"Error in resizeNearestNeighbor: x must be rank 3 or 4, but got rank "+n.rank+"."}),R(t.length===2,function(){return"Error in resizeNearestNeighbor: new shape must 2D, but got shape "+t+"."}),R(n.dtype==="float32"||n.dtype==="int32",function(){return"`images` must have `int32` or `float32` as dtype"});var o=n,a=!1;n.rank===3&&(a=!0,o=n.as4D(1,n.shape[0],n.shape[1],n.shape[2]));var i=t[0],s=t[1],u=N.runKernelFunc(function(c,l){return l([o]),c.resizeNearestNeighbor(o,i,s,e)},{batchImages:o},function(c,l){return{batchImages:function(){return N.runKernelFunc(function(h){return h.resizeNearestNeighborBackprop(c,l[0],e)},{})}}});return a?u.as3D(u.shape[1],u.shape[2],u.shape[3]):u}}),c0=D({nonMaxSuppression_:function(r,t,e,n,o){n===void 0&&(n=.5),o===void 0&&(o=Number.NEGATIVE_INFINITY);var a=_(r,"boxes","nonMaxSuppression"),i=_(t,"scores","nonMaxSuppression"),s=pa(a,i,e,n,o);e=s.maxOutputSize,n=s.iouThreshold,o=s.scoreThreshold;var u={maxOutputSize:e,iouThreshold:n,scoreThreshold:o};return N.runKernelFunc(function(c){return c.nonMaxSuppression(a,i,e,n,o)},{boxes:a,scores:i},null,"NonMaxSuppressionV3",u)}}),l0=function(r,t,e,n,o){return n===void 0&&(n=.5),o===void 0&&(o=Number.NEGATIVE_INFINITY),Q(this,void 0,void 0,function(){var a,i,s,u,c,l,h;return Z(this,function(f){switch(f.label){case 0:return a=_(r,"boxes","nonMaxSuppressionAsync"),i=_(t,"scores","nonMaxSuppressionAsync"),s=pa(a,i,e,n,o),e=s.maxOutputSize,n=s.iouThreshold,o=s.scoreThreshold,[4,Promise.all([a.data(),i.data()])];case 1:return u=f.sent(),c=u[0],l=u[1],h=us(c,l,e,n,o),a!==r&&a.dispose(),i!==t&&i.dispose(),[2,h]}})})},h0=D({nonMaxSuppressionWithScore_:function(r,t,e,n,o,a){n===void 0&&(n=.5),o===void 0&&(o=Number.NEGATIVE_INFINITY),a===void 0&&(a=0);var i=_(r,"boxes","nonMaxSuppression"),s=_(t,"scores","nonMaxSuppression"),u=pa(i,s,e,n,o,a),c={maxOutputSize:e=u.maxOutputSize,iouThreshold:n=u.iouThreshold,scoreThreshold:o=u.scoreThreshold,softNmsSigma:a=u.softNmsSigma},l=N.runKernel("NonMaxSuppressionV5",{boxes:i,scores:s},c);return{selectedIndices:l[0],selectedScores:l[1]}}}),f0=function(r,t,e,n,o,a){return n===void 0&&(n=.5),o===void 0&&(o=Number.NEGATIVE_INFINITY),a===void 0&&(a=0),Q(this,void 0,void 0,function(){var i,s,u,c,l,h,f;return Z(this,function(d){switch(d.label){case 0:return i=_(r,"boxes","nonMaxSuppressionAsync"),s=_(t,"scores","nonMaxSuppressionAsync"),u=pa(i,s,e,n,o,a),e=u.maxOutputSize,n=u.iouThreshold,o=u.scoreThreshold,a=u.softNmsSigma,[4,Promise.all([i.data(),s.data()])];case 1:return c=d.sent(),l=c[0],h=c[1],f=cs(l,h,e,n,o,a),i!==r&&i.dispose(),s!==t&&s.dispose(),[2,f]}})})},d0=D({cropAndResize_:function(r,t,e,n,o,a){var i=_(r,"image","cropAndResize"),s=_(t,"boxes","cropAndResize","float32"),u=_(e,"boxInd","cropAndResize","int32");o=o||"bilinear",a=a||0;var c=s.shape[0];return R(i.rank===4,function(){return"Error in cropAndResize: image must be rank 4,but got rank "+i.rank+"."}),R(s.rank===2&&s.shape[1]===4,function(){return"Error in cropAndResize: boxes must be have size ["+c+",4] but had shape "+s.shape+"."}),R(u.rank===1&&u.shape[0]===c,function(){return"Error in cropAndResize: boxInd must be have size ["+c+"] but had shape "+s.shape+"."}),R(n.length===2,function(){return"Error in cropAndResize: cropSize must be of length 2, but got length "+n.length+"."}),R(n[0]>=1&&n[1]>=1,function(){return"cropSize must be atleast [1,1], but was "+n}),R(o==="bilinear"||o==="nearest",function(){return"method must be bilinear or nearest, but was "+o}),N.runKernelFunc(function(l,h){return l.cropAndResize(i,s,u,n,o,a)},{images:i,boxes:s,boxInd:u},null,"CropAndResize",{method:o,extrapolationValue:a,cropSize:n})}}),_s=Object.freeze({resizeBilinear:s0,resizeNearestNeighbor:u0,nonMaxSuppression:c0,nonMaxSuppressionAsync:l0,nonMaxSuppressionWithScore:h0,nonMaxSuppressionWithScoreAsync:f0,cropAndResize:d0}),Es=function(r,t){return!(r>0)||t==="linear"},Ss=function(r,t,e){if(e==null||e==="linear")return r;if(e==="relu")return r.mul(t.step());throw new Error("Gradient for activation "+e+" has not been implemented yet.")},ks=function(r,t){var e=t,n=Be(r.shape,t.shape);return n.length>0&&(e=e.sum(n)),e.reshape(r.shape)},Rs=function(r,t,e){if(t==="linear")return r;if(t==="relu")return Ne(r);if(t==="elu")return Yh(r);if(t==="relu6")return Qh(r);if(t==="prelu")return Jh(r,e);throw new Error("Unknown fused activation "+t+".")},p0=D({fusedMatMul_:function(r){var t,e=r.a,n=r.b,o=r.transposeA,a=o!==void 0&&o,i=r.transposeB,s=i!==void 0&&i,u=r.bias,c=r.activation,l=c===void 0?"linear":c,h=r.preluActivationWeights;if(Es(N.state.gradientDepth,l)===!1){var f=fa(e,n,a,s);return u!=null&&(f=pe(f,u)),Rs(f,l,h)}var d=_(e,"a","fused matMul"),p=_(n,"b","fused matMul");t=De(d,p),d=t[0],p=t[1];var v=a?d.shape[d.rank-2]:d.shape[d.rank-1],m=s?p.shape[p.rank-1]:p.shape[p.rank-2],g=a?d.shape[d.rank-1]:d.shape[d.rank-2],x=s?p.shape[p.rank-2]:p.shape[p.rank-1],y=d.shape.slice(0,-2),b=p.shape.slice(0,-2),w=ee(y),C=ee(b);R(d.rank>=2&&p.rank>=2&&d.rank===p.rank,function(){return"Error in fused matMul: inputs must have the same rank of at least 2, got ranks "+d.rank+" and "+p.rank+"."}),R(Oe(y,b),function(){return"Error in fused matMul: outer dimensions ("+y+") and ("+b+") of Tensors with shapes "+d.shape+" and "+p.shape+" must match."}),R(v===m,function(){return"Error in fused matMul: inner shapes ("+v+") and ("+m+") of Tensors with shapes "+d.shape+" and "+p.shape+" and transposeA="+a+" and transposeB="+s+" must match."});var I,E,S=d.shape.slice(0,-2).concat([g,x]),k=a?d.as3D(w,v,g):d.as3D(w,g,v),T=s?p.as3D(C,x,m):p.as3D(C,m,x);u!=null&&ve(S,(I=De(I=_(u,"bias","fused matMul"),d)[0]).shape),h!=null&&(E=_(h,"prelu weights","fused matMul"));var A={a:k,b:T};u!=null&&(A.bias=I),h!=null&&(A.preluActivationWeights=E);var F=[k,T];return N.runKernelFunc(function(P,B){var U=P.fusedBatchMatMul({a:k,b:T,transposeA:a,transposeB:s,bias:I,activation:l,preluActivationWeights:E});return B([k,T,U]),U},A,function(P,B){var U=B[0],z=B[1],L=B[2],W=Ss(P,L,l),G={};return u!=null&&(G={bias:function(){return ks(I,W)}}),Object.assign(a||s?!a&&s?{a:function(){return W.matMul(z,!1,!1)},b:function(){return W.matMul(U,!0,!1)}}:a&&!s?{a:function(){return z.matMul(W,!1,!0)},b:function(){return U.matMul(W,!1,!1)}}:{a:function(){return z.matMul(W,!0,!0)},b:function(){return W.matMul(U,!0,!0)}}:{a:function(){return W.matMul(z,!1,!0)},b:function(){return U.matMul(W,!0,!1)}},G)},"_FusedMatMul",{transposeA:a,transposeB:s,activation:l},F,[!0]).reshape(S)}}),v0=D({fusedConv2d_:function(r){var t=r.x,e=r.filter,n=r.strides,o=r.pad,a=r.dataFormat,i=a===void 0?"NHWC":a,s=r.dilations,u=s===void 0?[1,1]:s,c=r.dimRoundingMode,l=r.bias,h=r.activation,f=h===void 0?"linear":h,d=r.preluActivationWeights;if(f=f||"linear",Es(N.state.gradientDepth,f)===!1){var p=It(t,e,n,o,i,u,c);return l!=null&&(p=pe(p,l)),Rs(p,f,d)}var v=_(t,"x","conv2d"),m=_(e,"filter","conv2d"),g=v,x=!1;v.rank===3&&(x=!0,g=v.as4D(1,v.shape[0],v.shape[1],v.shape[2])),R(g.rank===4,function(){return"Error in fused conv2d: input must be rank 4, but got rank "+g.rank+"."}),R(m.rank===4,function(){return"Error in fused conv2d: filter must be rank 4, but got rank "+m.rank+"."}),c!=null&&R(Fe(o),function(){return"Error in fused conv2d: pad must be an integer when using, dimRoundingMode "+c+" but got pad "+o+"."}),R(g.shape[3]===m.shape[2],function(){return"Error in conv2d: depth of input ("+g.shape[3]+") must match input depth for filter "+m.shape[2]+"."}),R(st(n,u),function(){return"Error in conv2D: Either strides or dilations must be 1. Got strides "+n+" and dilations '"+u+"'"}),R(i==="NHWC",function(){return"Error in conv2d: got dataFormat of "+i+" but only NHWC is currently supported."});var y,b,w=In(g.shape,m.shape,n,u,o,c);l!=null&&(y=De(y=_(l,"bias","fused conv2d"),v)[0],ve(w.outShape,y.shape)),d!=null&&(b=_(d,"prelu weights","fused conv2d"));var C={x:g,filter:m};l!=null&&(C.bias=y),d!=null&&(C.preluActivationWeights=b);var I=[m,g],E=N.runKernelFunc(function(S,k){var T=S.fusedConv2d({input:g,filter:m,convInfo:w,bias:y,activation:f,preluActivationWeights:b});return k([m,g,T]),T},C,function(S,k){var T=k,A=T[0],F=T[1],P=T[2],B=Ss(S,P,f);R(zn(u),function(){return"Error in gradient of fused conv2D: dilation rates greater than 1 are not yet supported in gradients. Got dilations '"+u+"'"});var U={};return l!=null&&(U={bias:function(){return ks(y,B)}}),Object.assign({x:function(){return Uh(F.shape,B,A,n,o)},filter:function(){return gs(F,B,A.shape,n,o)}},U)},"FusedConv2D",{convInfo:w,activation:f},I,[!0]);return x?E.as3D(E.shape[1],E.shape[2],E.shape[3]):E}}),m0=D({fusedDepthwiseConv2d_:function(r){var t=r.x,e=r.filter,n=r.strides,o=r.pad,a=r.dataFormat,i=a===void 0?"NHWC":a,s=r.dilations,u=s===void 0?[1,1]:s,c=r.dimRoundingMode,l=r.bias,h=r.activation,f=h===void 0?"linear":h,d=r.preluActivationWeights;if(Es(N.state.gradientDepth,f)===!1){var p=ha(t,e,n,o,i,u,c);return l!=null&&(p=pe(p,l)),Rs(p,f,d)}var v=_(t,"x","depthwiseConv2d"),m=_(e,"filter","depthwiseConv2d"),g=v,x=!1;v.rank===3&&(x=!0,g=v.as4D(1,v.shape[0],v.shape[1],v.shape[2])),R(g.rank===4,function(){return"Error in fused depthwiseConv2d: input must be rank 4, but got rank "+g.rank+"."}),R(m.rank===4,function(){return"Error in fused depthwiseConv2d: filter must be rank 4, but got rank "+m.rank+"."}),R(g.shape[3]===m.shape[2],function(){return"Error in fused depthwiseConv2d: number of input channels ("+g.shape[3]+") must match the inChannels dimension in filter "+m.shape[2]+"."}),u==null&&(u=[1,1]),R(st(n,u),function(){return"Error in fused depthwiseConv2d: Either strides or dilations must be 1. Got strides "+n+" and dilations '"+u+"'"}),c!=null&&R(Fe(o),function(){return"Error in fused depthwiseConv2d: pad must be an integer when using dimRoundingMode "+c+" but got pad "+o+"."});var y,b,w=In(g.shape,m.shape,n,u,o,c,!0);l!=null&&(y=De(y=_(l,"bias","fused conv2d"),v)[0],ve(w.outShape,y.shape)),d!=null&&(b=_(d,"prelu weights","fused depthwiseConv2d"));var C={x:g,filter:m};l!=null&&(C.bias=y),d!=null&&(C.preluActivationWeights=b);var I=[m,g],E=N.runKernelFunc(function(S,k){var T=S.fusedDepthwiseConv2D({input:g,filter:m,convInfo:w,bias:y,activation:f,preluActivationWeights:b});return k([m,g,T]),T},C,function(S,k){R(zn(u),function(){return"Error in gradient of fused depthwiseConv2d: dilation rates greater than 1 are not yet supported. Got dilations '"+u+"'"});var T=k[0],A=k[1],F=k[2],P=Ss(S,F,f),B={};return l!=null&&(B={bias:function(){return ks(y,P)}}),Object.assign({x:function(){return Gh(A.shape,P,T,w)},filter:function(){return Hh(A,P,T.shape,w)}},B)},"FusedDepthwiseConv2D",{convInfo:w,activation:f},I,[!0]);return x?E.as3D(E.shape[1],E.shape[2],E.shape[3]):E}}),g0=Object.freeze({matMul:p0,conv2d:v0,depthwiseConv2d:m0}),y0=Object.freeze({image:_s,linalg:i0,losses:n0,spectral:zy,fused:g0,signal:jy,square:Qm,squaredDifference:kh,conv1d:sy,conv2d:It,conv3d:uy,depthwiseConv2d:ha,separableConv2d:ys,conv2dTranspose:cy,conv3dTranspose:ly,op:D,batchNormalization2d:Ig,batchNormalization3d:Ag,batchNormalization4d:Dg,batchNormalization:Tg,batchNorm:Nh,batchNorm2d:Ng,batchNorm3d:Fg,batchNorm4d:Pg,booleanMaskAsync:iy,complex:Ke,real:St,imag:jt,concat:Ve,concat1d:Wp,concat2d:Vp,concat3d:zp,concat4d:Up,split:Zi,matMul:fa,dot:hy,outerProduct:fy,reverse:Qr,reverse1d:dy,reverse2d:py,reverse3d:vy,reverse4d:my,maxPool:Ge,avgPool:Zr,pool:gy,maxPool3d:yy,avgPool3d:xy,slice:Jt,slice1d:by,slice2d:wy,slice3d:Kh,slice4d:Cy,abs:Zm,acos:eg,acosh:tg,asin:ng,asinh:rg,atan:og,atanh:ag,ceil:ig,clipByValue:ps,cos:sg,cosh:ug,erf:cg,exp:Ci,expm1:lg,floor:hg,log:fg,log1p:dg,logSigmoid:pg,neg:Ho,reciprocal:vg,round:mg,rsqrt:Rh,sigmoid:Ih,sign:gg,isNaN:yg,isInf:xg,isFinite:bg,sin:wg,sinh:Cg,softplus:_g,sqrt:Eg,step:Sg,tan:kg,tanh:Rg,all:_y,any:Ey,argMax:Sy,argMin:ky,logSumExp:Ry,max:da,mean:Iy,min:Ay,moments:Dy,sum:$h,prod:Ty,equal:Bh,equalStrict:Yg,greater:Jg,greaterEqual:Lh,greaterEqualStrict:Qg,greaterStrict:Zg,less:ey,lessEqual:ty,lessEqualStrict:ny,lessStrict:ry,notEqual:oy,notEqualStrict:ay,add:pe,addN:Bg,addStrict:Lg,atan2:Wg,div:Tt,divNoNan:Vg,divStrict:zg,floorDiv:Mh,maximum:vs,maximumStrict:Ug,minimum:Oh,minimumStrict:Gg,mod:Hg,modStrict:qg,mul:et,mulStrict:jg,pow:qo,powStrict:Kg,squaredDifferenceStrict:Xg,sub:Ue,subStrict:$g,elu:Yh,leakyRelu:Ny,prelu:Jh,relu:Ne,relu6:Qh,selu:Fy,logicalAnd:la,logicalNot:Mg,logicalOr:Fh,logicalXor:Og,where:Un,whereAsync:Ph,buffer:ue,print:Jp,batchToSpaceND:Fl,broadcastTo:Qp,cast:Zp,clone:ev,cumsum:tv,depthToSpace:nv,expandDims:_t,eye:Pl,multinomial:rv,oneHot:yi,pad:qn,pad1d:ov,pad2d:av,pad3d:iv,pad4d:sv,rand:uv,randomNormal:cv,randomGamma:lv,randomUniform:Ml,reshape:Nt,spaceToBatchND:Ol,squeeze:Bl,stack:bt,tile:ir,truncatedNormal:hv,unstack:ze,setdiff1dAsync:fv,fill:Xt,linspace:Lp,ones:xr,range:Lo,scalar:K,tensor:$e,tensor1d:Me,tensor2d:bn,tensor3d:Qi,tensor4d:lt,tensor5d:Mp,tensor6d:Op,variable:Bp,zeros:Ie,onesLike:Nl,zerosLike:ye,transpose:_n,softmax:cn,logSoftmax:pv,localResponseNormalization:Py,norm:Zh,gather:ms,unsortedSegmentSum:Wh,basicLSTMCell:My,multiRNNCell:Oy,movingAverage:By,stridedSlice:Ly,topk:Wy,scatterND:Vy,fft:xs,ifft:jo,rfft:bs,irfft:ef,sparseToDense:Uy,gatherND:Gy,diag:Hy,dropout:qy,hannWindow:ws,hammingWindow:nf,frame:Cs,stft:rf,inTopKAsync:Ky});function H(r,t){Array.isArray(r)||(r=[r]),r.forEach(function(e){e!=null&&R(e.dtype!=="complex64",function(){return t+" does not support complex64 tensors."})})}function Ua(r,t,e,n){if(e==="linear")return r.linear(t);if(e==="relu")return r.relu(t);if(e==="elu")return r.elu(t);if(e==="relu6")return r.relu6(t);if(e==="prelu")return r.prelu(t,n);throw new Error("Activation "+e+" has not been implemented for the CPU backend.")}var x0=function(r){function t(){var e=r.call(this)||this;return e.blockSize=48,e.firstUse=!0,e.data=new ql(e,N),e}return Mt(t,r),t.prototype.write=function(e,n,o){this.firstUse&&(this.firstUse=!1,V().get("IS_NODE")&&Oo(`
============================
Hi there \u{1F44B}. Looks like you are running TensorFlow.js in Node.js. To speed things up dramatically, install our node backend, which binds to TensorFlow C++, by running npm i @tensorflow/tfjs-node, or npm i @tensorflow/tfjs-node-gpu if you have CUDA. Then call require('@tensorflow/tfjs-node'); (-gpu suffix for CUDA) at the start of your program. Visit https://github.com/tensorflow/tfjs-node for more details.
============================`));var a={};return this.data.set(a,{values:e,dtype:o}),a},t.prototype.move=function(e,n,o,a){this.data.set(e,{values:n,dtype:a})},t.prototype.numDataIds=function(){return this.data.numDataIds()},t.prototype.read=function(e){return Q(this,void 0,void 0,function(){return Z(this,function(n){return[2,this.readSync(e)]})})},t.prototype.readSync=function(e){var n=this.data.get(e),o=n.dtype,a=n.complexTensors;return o==="complex64"?wi(this.readSync(a.real.dataId),this.readSync(a.imag.dataId)):this.data.get(e).values},t.prototype.bufferSync=function(e){var n=this.readSync(e.dataId),o=n;if(e.dtype==="string")try{o=n.map(function(a){return Or(a)})}catch{throw new Error("Failed to decode encoded string bytes into utf-8")}return ue(e.shape,e.dtype,o)},t.prototype.makeOutput=function(e,n,o){var a=this.write(e,n,o);return N.makeTensorFromDataId(a,n,o,this)},t.prototype.disposeData=function(e){if(this.data.has(e)){var n=this.data.get(e).complexTensors;n!=null&&(n.real.dispose(),n.imag.dispose()),this.data.delete(e)}},t.prototype.time=function(e){return Q(this,void 0,void 0,function(){var n;return Z(this,function(o){return n=Dt(),e(),[2,{kernelMs:Dt()-n}]})})},t.prototype.memory=function(){return{unreliable:!0,reasons:["The reported memory is an upper bound. Due to automatic garbage collection, the true allocated memory may be less."]}},t.prototype.complex=function(e,n){var o=this.makeOutput(null,e.shape,"complex64");return this.data.get(o.dataId).complexTensors={real:N.keep(e.clone()),imag:N.keep(n.clone())},o},t.prototype.real=function(e){return this.data.get(e.dataId).complexTensors.real.clone()},t.prototype.imag=function(e){return this.data.get(e.dataId).complexTensors.imag.clone()},t.prototype.slice=function(e,n,o){if(H(e,"slice"),rs(e.shape,n,o)){var a=os(n,e.strides),i=ee(o);return $e(this.readSync(e.dataId).subarray(a,a+i),o,e.dtype)}for(var s=ue(o,e.dtype),u=this.bufferSync(e),c=0;c<s.size;++c){var l=s.indexToLoc(c).map(function(h,f){return h+n[f]});s.values[c]=u.get.apply(u,l)}return s.toTensor()},t.prototype.stridedSlice=function(e,n,o,a){H(e,"stridedSlice");var i=ia(n,o,a);if(i.some(function(d){return d===0}))return $e([],i);for(var s=ue(i,e.dtype),u=this.bufferSync(e),c=0;c<s.size;c++){for(var l=s.indexToLoc(c),h=new Array(l.length),f=0;f<h.length;f++)h[f]=l[f]*a[f]+n[f];s.set.apply(s,[u.get.apply(u,h)].concat(l))}return s.toTensor()},t.prototype.diag=function(e){for(var n=this.readSync(e.dataId),o=ue([e.size,e.size],e.dtype),a=o.values,i=0;i<n.length;i++)a[i*e.size+i]=n[i];return o.toTensor()},t.prototype.unstack=function(e,n){for(var o=e.shape[n],a=new Array(e.rank-1),i=0,s=0;s<e.rank;s++)s!==n&&(a[i++]=e.shape[s]);var u=new Array(e.rank).fill(0),c=e.shape.slice();c[n]=1;var l=new Array(o);for(s=0;s<l.length;s++)u[n]=s,l[s]=this.slice(e,u,c).reshape(a);return l},t.prototype.reverse=function(e,n){H(e,"reverse");for(var o=ue(e.shape,e.dtype),a=this.bufferSync(e),i=function(u){var c=o.indexToLoc(u),l=c.slice();n.forEach(function(h){return l[h]=e.shape[h]-1-l[h]}),o.set.apply(o,[a.get.apply(a,l)].concat(c))},s=0;s<o.size;s++)i(s);return o.toTensor()},t.prototype.concat=function(e,n){var o=this;if(e[0].dtype==="complex64"){var a=e.map(function(d){return St(d)}),i=e.map(function(d){return jt(d)});return Ke(this.concat(a,n),this.concat(i,n))}var s=e.map(function(d){var p=ee(d.shape.slice(n));return d.as2D(-1,p)}),u=Vn(s.map(function(d){return d.shape}),1),c=ue(u,e[0].dtype).values;if(s[0].shape[0]===1){var l=0;s.forEach(function(d){c.set(o.readSync(d.dataId),l),l+=d.size})}else{var h=0;s.forEach(function(d){for(var p=o.readSync(d.dataId),v=0,m=0;m<d.shape[0];++m)for(var g=m*u[1]+h,x=0;x<d.shape[1];++x)c[g+x]=p[v++];h+=d.shape[1]})}var f=Vn(e.map(function(d){return d.shape}),n);return $e(c,f,e[0].dtype)},t.prototype.neg=function(e){return H(e,"neg"),this.multiply(K(-1),e)},t.prototype.add=function(e,n){return e.dtype==="complex64"||n.dtype==="complex64"?this.broadcastedBinaryComplexOp(e.cast("complex64"),n.cast("complex64"),function(o,a,i,s){return{real:o+i,imag:a+s}}):this.broadcastedBinaryOp(e,n,He(e.dtype,n.dtype),function(o,a){return o+a})},t.prototype.addN=function(e){var n=this;H(e,"addN");for(var o=e.map(function(l){return n.readSync(l.dataId)}),a=ue(e[0].shape,e[0].dtype),i=a.values,s=0;s<e.length;s++)for(var u=o[s],c=0;c<i.length;c++)i[c]+=u[c];return a.toTensor()},t.prototype.softmax=function(e,n){var o=Le([n],e.shape),a=this.max(e,o),i=it(a.shape,o),s=this.subtract(e,a.reshape(i)),u=this.exp(s),c=this.sum(u,o).reshape(i);return this.realDivide(u,c)},t.prototype.subtract=function(e,n){return e.dtype==="complex64"||n.dtype==="complex64"?this.broadcastedBinaryComplexOp(e.cast("complex64"),n.cast("complex64"),function(o,a,i,s){return{real:o-i,imag:a-s}}):this.broadcastedBinaryOp(e,n,He(e.dtype,n.dtype),function(o,a){return o-a})},t.prototype.pow=function(e,n){return H([e,n],"pow"),this.broadcastedBinaryOp(e,n,e.dtype,function(o,a){return Math.pow(o,a)})},t.prototype.batchMatMul=function(e,n,o,a){H([e,n],"matMul");for(var i=o?e.shape[1]:e.shape[2],s=o?e.shape[2]:e.shape[1],u=a?n.shape[1]:n.shape[2],c=e.shape[0],l=this.readSync(e.dataId),h=this.readSync(n.dataId),f=o?[e.strides[0],1,e.strides[1]]:[e.strides[0],e.strides[1],1],d=f[0],p=f[1],v=f[2],m=a?[1,n.strides[1],n.strides[0]]:[n.strides[1],1,n.strides[0]],g=m[0],x=m[1],y=m[2],b=s*u,w=ue([c,s,u],e.dtype),C=w.values,I=this.blockSize,E=0;E<c;E++)for(var S=0;S<s;S+=I)for(var k=0;k<u;k+=I)for(var T=0;T<i;T+=I)for(var A=Math.min(S+I,s),F=Math.min(k+I,u),P=Math.min(T+I,i),B=S;B<A;B++)for(var U=k;U<F;U++){for(var z=0,L=T;L<P;L++)z+=l[E*d+B*p+L*v]*h[L*g+U*x+E*y];C[E*b+(B*u+U)]+=z}return w.toTensor()},t.prototype.fusedBatchMatMul=function(e){var n=e.a,o=e.b,a=e.transposeA,i=e.transposeB,s=e.bias,u=e.activation,c=e.preluActivationWeights,l=this.batchMatMul(n,o,a,i);return s&&(l=this.add(l,s)),u&&(l=Ua(this,l,u,c)),l},t.prototype.multiply=function(e,n){return e.dtype==="complex64"||n.dtype==="complex64"?this.broadcastedBinaryComplexOp(e.cast("complex64"),n.cast("complex64"),function(o,a,i,s){return{real:o*i-a*s,imag:o*s+a*i}}):this.broadcastedBinaryOp(e,n,He(e.dtype,n.dtype),function(o,a){return o*a})},t.prototype.realDivide=function(e,n){return H([e,n],"realDivide"),this.broadcastedBinaryOp(e,n,"float32",function(o,a){return o/a})},t.prototype.floorDiv=function(e,n){return H([e,n],"floorDiv"),this.broadcastedBinaryOp(e,n,"int32",function(o,a){return Math.floor(o/a)})},t.prototype.sum=function(e,n){H(e,"sum"),ct("sum",n,e.rank);for(var o=Xe(e.shape,n),a=o[0],i=o[1],s=Ie(a,He(e.dtype,"int32")),u=ee(i),c=this.readSync(s.dataId),l=this.readSync(e.dataId),h=0;h<c.length;++h){for(var f=h*u,d=0,p=0;p<u;++p)d+=l[f+p];c[h]=d}return s},t.prototype.prod=function(e,n){H(e,"sum");for(var o=Xe(e.shape,n),a=o[0],i=o[1],s=Ie(a,He(e.dtype,"int32")),u=ee(i),c=this.readSync(s.dataId),l=this.readSync(e.dataId),h=0;h<c.length;++h){for(var f=h*u,d=1,p=0;p<u;++p)d*=l[f+p];c[h]=d}return s},t.prototype.unsortedSegmentSum=function(e,n,o){H(e,"unsortedSegmentSum");for(var a=[],i=e.rank-n.rank,s=0;s<i;++s)n=n.expandDims(s+1);for(s=0;s<o;++s){var u=K(s,"int32"),c=Bh(u,n).asType("float32").mul(e).sum(0);a.push(c)}return bt(a)},t.prototype.argMin=function(e,n){H(e,"argMin");var o=[n];ct("argMin",o,e.rank);for(var a=Xe(e.shape,o),i=a[0],s=a[1],u=Ie(i,"int32"),c=ee(s),l=this.readSync(u.dataId),h=this.readSync(e.dataId),f=0;f<l.length;++f){for(var d=f*c,p=h[d],v=0,m=0;m<c;++m){var g=h[d+m];g<p&&(p=g,v=m)}l[f]=v}return u},t.prototype.argMax=function(e,n){H(e,"argMax");var o=[n];ct("argMax",o,e.rank);for(var a=Xe(e.shape,o),i=a[0],s=a[1],u=Ie(i,"int32"),c=ee(s),l=this.readSync(u.dataId),h=this.readSync(e.dataId),f=0;f<l.length;++f){for(var d=f*c,p=h[d],v=0,m=0;m<c;++m){var g=h[d+m];g>p&&(p=g,v=m)}l[f]=v}return u},t.prototype.cumsum=function(e,n,o,a){if(H(e,"cumsum"),n!==e.rank-1)throw new Error("backend.cumsum in CPU expects an inner-most axis="+(e.rank-1)+" but got axis="+n);for(var i=He(e.dtype,"int32"),s=Ie(e.shape,i),u=this.readSync(s.dataId),c=this.readSync(e.dataId),l=e.shape[e.rank-1],h=a?function(m,g){return m+l-g-1}:function(m,g){return m+g},f=0;f<c.length;f+=l)for(var d=0;d<l;d++){var p=h(f,d);if(d===0)u[p]=o?0:c[p];else{var v=h(f,d-1);u[p]=o?c[v]+u[v]:c[p]+u[v]}}return s},t.prototype.equal=function(e,n){return H([e,n],"equal"),this.broadcastedBinaryOp(e,n,"bool",function(o,a){return o===a?1:0})},t.prototype.notEqual=function(e,n){return H([e,n],"notEqual"),this.broadcastedBinaryOp(e,n,"bool",function(o,a){return o!==a?1:0})},t.prototype.less=function(e,n){return H([e,n],"less"),this.broadcastedBinaryOp(e,n,"bool",function(o,a){return o<a?1:0})},t.prototype.lessEqual=function(e,n){return H([e,n],"lessEqual"),this.broadcastedBinaryOp(e,n,"bool",function(o,a){return o<=a?1:0})},t.prototype.greater=function(e,n){return H([e,n],"greater"),this.broadcastedBinaryOp(e,n,"bool",function(o,a){return o>a?1:0})},t.prototype.greaterEqual=function(e,n){return H([e,n],"greaterEqual"),this.broadcastedBinaryOp(e,n,"bool",function(o,a){return o>=a?1:0})},t.prototype.logicalNot=function(e){H(e,"logicalNot");for(var n=this.readSync(e.dataId),o=new Uint8Array(n.length),a=0;a<n.length;++a)o[a]=n[a]?0:1;return this.makeOutput(o,e.shape,"bool")},t.prototype.logicalAnd=function(e,n){return H([e,n],"logicalAnd"),this.broadcastedBinaryOp(e,n,"bool",function(o,a){return o&&a})},t.prototype.logicalOr=function(e,n){return H([e,n],"logicalOr"),this.broadcastedBinaryOp(e,n,"bool",function(o,a){return o||a})},t.prototype.select=function(e,n,o){H([e,n,o],"select");for(var a=this.readSync(e.dataId),i=this.readSync(n.dataId),s=this.readSync(o.dataId),u=Ie(n.shape,He(n.dtype,o.dtype)),c=this.readSync(u.dataId),l=0,h=e.rank===0||e.rank>1||n.rank===1?1:ee(n.shape.slice(1)),f=0;f<a.length;f++)for(var d=0;d<h;d++)a[f]===1?c[l++]=i[f]:c[l++]=s[f];return u},t.prototype.where=function(e){H([e],"where");var n=this.readSync(e.dataId);return ls(e.shape,n)},t.prototype.topk=function(e,n,o){return H(e,"topk"),Yl(this.readSync(e.dataId),e.shape,e.dtype,n)},t.prototype.min=function(e,n){H(e,"min"),ct("min",n,e.rank);for(var o=Xe(e.shape,n),a=o[0],i=o[1],s=Ie(a,e.dtype),u=ee(i),c=this.readSync(s.dataId),l=this.readSync(e.dataId),h=0;h<c.length;++h){for(var f=h*u,d=l[f],p=0;p<u;++p){var v=l[f+p];v<d&&(d=v)}c[h]=d}return s},t.prototype.minimum=function(e,n){return H([e,n],"minimum"),this.broadcastedBinaryOp(e,n,e.dtype,function(o,a){return Math.min(o,a)})},t.prototype.mod=function(e,n){return H([e,n],"mod"),this.broadcastedBinaryOp(e,n,e.dtype,function(o,a){var i=o%a;return o<0&&a<0||o>=0&&a>=0?i:(i+a)%a})},t.prototype.max=function(e,n){H(e,"max"),ct("max",n,e.rank);for(var o=Xe(e.shape,n),a=o[0],i=o[1],s=Ie(a,e.dtype),u=ee(i),c=this.readSync(s.dataId),l=this.readSync(e.dataId),h=0;h<c.length;++h){for(var f=h*u,d=l[f],p=0;p<u;++p){var v=l[f+p];v>d&&(d=v)}c[h]=d}return s},t.prototype.maximum=function(e,n){return H([e,n],"maximum"),this.broadcastedBinaryOp(e,n,e.dtype,function(o,a){return Math.max(o,a)})},t.prototype.all=function(e,n){H(e,"all"),ct("all",n,e.rank);for(var o=Xe(e.shape,n),a=o[0],i=o[1],s=Ie(a,e.dtype),u=ee(i),c=this.readSync(s.dataId),l=this.readSync(e.dataId),h=0;h<c.length;++h){for(var f=h*u,d=l[f],p=0;p<u;++p){var v=l[f+p];d=d&&v}c[h]=d}return s},t.prototype.any=function(e,n){H(e,"any"),ct("any",n,e.rank);for(var o=Xe(e.shape,n),a=o[0],i=o[1],s=Ie(a,e.dtype),u=ee(i),c=this.readSync(s.dataId),l=this.readSync(e.dataId),h=0;h<c.length;++h){for(var f=h*u,d=l[f],p=0;p<u;++p){var v=l[f+p];d=d||v}c[h]=d}return s},t.prototype.squaredDifference=function(e,n){return H([e,n],"squaredDifference"),this.broadcastedBinaryOp(e,n,e.dtype,function(o,a){var i=o-a;return i*i})},t.prototype.ceil=function(e){H(e,"ceil");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a)o[a]=Math.ceil(n[a]);return this.makeOutput(o,e.shape,"float32")},t.prototype.floor=function(e){H(e,"floor");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a)o[a]=Math.floor(n[a]);return this.makeOutput(o,e.shape,"float32")},t.prototype.sign=function(e){H(e,"x");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a)n[a]<0?o[a]=-1:n[a]>0?o[a]=1:o[a]=0;return this.makeOutput(o,e.shape,"float32")},t.prototype.isNaN=function(e){H(e,"x");for(var n=this.readSync(e.dataId),o=new Uint8Array(n.length),a=0;a<n.length;++a)Number.isNaN(n[a])&&(o[a]=1);return this.makeOutput(o,e.shape,"bool")},t.prototype.isInf=function(e){H(e,"x");for(var n=this.readSync(e.dataId),o=new Uint8Array(n.length),a=0;a<n.length;++a)Math.abs(n[a])===1/0&&(o[a]=1);return this.makeOutput(o,e.shape,"bool")},t.prototype.isFinite=function(e){H(e,"x");for(var n=this.readSync(e.dataId),o=new Uint8Array(n.length),a=0;a<n.length;++a)Number.isFinite(n[a])&&(o[a]=1);return this.makeOutput(o,e.shape,"bool")},t.prototype.round=function(e){H(e,"round");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a){var i=Math.floor(n[a]);n[a]-i<.5?o[a]=Math.floor(n[a]):n[a]-i>.5?o[a]=Math.ceil(n[a]):o[a]=i%2==0?i:i+1}return this.makeOutput(o,e.shape,"float32")},t.prototype.exp=function(e){H(e,"exp");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a)o[a]=Math.exp(n[a]);return this.makeOutput(o,e.shape,"float32")},t.prototype.expm1=function(e){H(e,"expm1");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a)o[a]=Math.expm1(n[a]);return this.makeOutput(o,e.shape,"float32")},t.prototype.log=function(e){H(e,"log");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a){var i=n[a];o[a]=Math.log(i)}return this.makeOutput(o,e.shape,"float32")},t.prototype.log1p=function(e){H(e,"log1p");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a){var i=n[a];o[a]=Math.log1p(i)}return this.makeOutput(o,e.shape,"float32")},t.prototype.sqrt=function(e){H(e,"sqrt");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a){var i=n[a];o[a]=Math.sqrt(i)}return this.makeOutput(o,e.shape,"float32")},t.prototype.rsqrt=function(e){H(e,"rsqrt");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a){var i=n[a];o[a]=1/Math.sqrt(i)}return this.makeOutput(o,e.shape,"float32")},t.prototype.reciprocal=function(e){H(e,"reciprocal");for(var n=this.readSync(e.dataId),o=new Float32Array(n.length),a=0;a<n.length;++a)o[a]=1/n[a];return this.makeOutput(o,e.shape,"float32")},t.prototype.linear=function(e){return e},t.prototype.relu=function(e){H(e,"relu");for(var n=Ie(e.shape,e.dtype),o=this.readSync(n.dataId),a=this.readSync(e.dataId),i=0;i<a.length;++i)o[i]=Math.max(0,a[i]);return n},t.prototype.relu6=function(e){H(e,"relu");for(var n=Ie(e.shape,e.dtype),o=this.readSync(n.dataId),a=this.readSync(e.dataId),i=0;i<a.length;++i)o[i]=Math.min(Math.max(0,a[i]),6);return n},t.prototype.prelu=function(e,n){return H([e,n],"prelu"),this.broadcastedBinaryOp(e,n,e.dtype,function(o,a){return o<0?a*o:o})},t.prototype.elu=function(e){H(e,"elu");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a){var i=o[a];n[a]=i>=0?i:Math.exp(i)-1}return this.makeOutput(n,e.shape,"float32")},t.prototype.eluDer=function(e,n){H([e,n],"eluDer");for(var o=new Float32Array(n.size),a=this.readSync(n.dataId),i=this.readSync(e.dataId),s=0;s<a.length;++s){var u=a[s];o[s]=u>=1?i[s]:i[s]*(u+1)}return this.makeOutput(o,n.shape,"float32")},t.prototype.selu=function(e){H(e,"selu");for(var n=fs,o=ds,a=new Float32Array(e.size),i=this.readSync(e.dataId),s=0;s<i.length;++s){var u=i[s];a[s]=u>=0?o*u:n*(Math.exp(u)-1)}return this.makeOutput(a,e.shape,"float32")},t.prototype.clip=function(e,n,o){H(e,"clip");for(var a=new Float32Array(e.size),i=this.readSync(e.dataId),s=0;s<i.length;++s){var u=i[s];a[s]=u>o?o:u<n?n:u}return this.makeOutput(a,e.shape,"float32")},t.prototype.abs=function(e){for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.abs(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.complexAbs=function(e){for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<e.size;++a){var i=o[2*a],s=o[2*a+1];n[a]=Math.hypot(i,s)}return this.makeOutput(n,e.shape,"float32")},t.prototype.int=function(e){H(e,"int");for(var n=new Int32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=o[a];return this.makeOutput(n,e.shape,"int32")},t.prototype.sigmoid=function(e){H(e,"sigmoid");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=1/(1+Math.exp(-o[a]));return this.makeOutput(n,e.shape,"float32")},t.prototype.softplus=function(e){H(e,"softplus");for(var n=Math.log(11920928955078125e-23)+2,o=new Float32Array(e.size),a=this.readSync(e.dataId),i=0;i<a.length;++i){var s=a[i]>-n,u=a[i]<n,c=Math.exp(a[i]),l=void 0;l=u?c:s?a[i]:Math.log(1+c),o[i]=l}return this.makeOutput(o,e.shape,"float32")},t.prototype.sin=function(e){H(e,"sin");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.sin(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.cos=function(e){H(e,"cos");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.cos(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.tan=function(e){H(e,"tan");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.tan(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.asin=function(e){H(e,"asin");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.asin(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.acos=function(e){H(e,"acos");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.acos(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.atan=function(e){H(e,"atan");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.atan(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.atan2=function(e,n){return H([e,n],"atan2"),this.broadcastedBinaryOp(e,n,e.dtype,function(o,a){return Math.atan2(o,a)})},t.prototype.sinh=function(e){H(e,"sinh");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.sinh(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.cosh=function(e){H(e,"cosh");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.cosh(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.tanh=function(e){H(e,"tanh");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Hc(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.asinh=function(e){H(e,"asinh");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.asinh(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.acosh=function(e){H(e,"acosh");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.acosh(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.atanh=function(e){H(e,"atanh");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a)n[a]=Math.atanh(o[a]);return this.makeOutput(n,e.shape,"float32")},t.prototype.erf=function(e){H(e,"erf");for(var n=new Float32Array(e.size),o=this.readSync(e.dataId),a=0;a<o.length;++a){var i=Math.sign(o[a]),s=Math.abs(o[a]),u=1/(1+.3275911*s);n[a]=i*(1-((((1.061405429*u-1.453152027)*u+1.421413741)*u-.284496736)*u+.254829592)*u*Math.exp(-s*s))}return this.makeOutput(n,e.shape,"float32")},t.prototype.step=function(e,n){n===void 0&&(n=0),H(e,"step");for(var o=new Float32Array(e.size),a=this.readSync(e.dataId),i=0;i<a.length;++i){var s=a[i];isNaN(s)?o[i]=NaN:o[i]=s>0?1:n}return this.makeOutput(o,e.shape,"float32")},t.prototype.fusedConv2d=function(e){var n=e.input,o=e.filter,a=e.convInfo,i=e.bias,s=e.activation,u=e.preluActivationWeights,c=this.conv2d(n,o,a);return i&&(c=this.add(c,i)),s&&(c=Ua(this,c,s,u)),c},t.prototype.conv2d=function(e,n,o){H([e,n],"conv2d");for(var a=o.filterHeight,i=o.filterWidth,s=o.dilationHeight,u=o.dilationWidth,c=o.padInfo.left,l=o.padInfo.top,h=o.dataFormat==="channelsLast",f=ue(o.outShape,e.dtype),d=e.strides[0],p=h?e.strides[1]:e.strides[2],v=h?e.strides[2]:1,m=h?1:e.strides[1],g=f.strides[0],x=h?f.strides[1]:f.strides[2],y=h?f.strides[2]:1,b=h?1:f.strides[1],w=this.readSync(e.dataId),C=this.readSync(n.dataId),I=f.values,E=0;E<o.batchSize;++E)for(var S=E*d,k=E*g,T=0;T<o.outHeight;++T)for(var A=k+T*x,F=T*o.strideHeight-l,P=0;P<a;P++){var B=F+P*s;if(!(B<0||B>=o.inHeight))for(var U=P*n.strides[0],z=S+B*p,L=0;L<o.outWidth;++L)for(var W=A+L*y,G=L*o.strideWidth-c,j=0;j<i;j++){var q=G+j*u;if(!(q<0||q>=o.inWidth))for(var X=z+q*v,oe=U+j*n.strides[1],re=0;re<o.inChannels;++re){for(var ie=w[X+re*m],ce=0;ce<o.outChannels;++ce)I[W+ce*b]+=ie*C[oe+ce];oe+=o.outChannels}}}return f.toTensor()},t.prototype.conv3d=function(e,n,o){for(var a=o.filterDepth,i=o.filterHeight,s=o.filterWidth,u=o.dilationDepth,c=o.dilationHeight,l=o.dilationWidth,h=o.padInfo.front,f=o.padInfo.left,d=o.padInfo.top,p=ue(o.outShape,e.dtype),v=this.readSync(e.dataId),m=this.readSync(n.dataId),g=p.values,x=0;x<o.batchSize;++x)for(var y=x*e.strides[0],b=x*p.strides[0],w=0;w<o.outDepth;++w)for(var C=b+w*p.strides[1],I=w*o.strideDepth-h,E=0;E<a;E++){var S=I+E*u;if(!(S<0||S>=o.inDepth))for(var k=E*n.strides[0],T=y+S*e.strides[1],A=0;A<o.outHeight;++A)for(var F=C+A*p.strides[2],P=A*o.strideHeight-d,B=0;B<i;B++){var U=P+B*c;if(!(U<0||U>=o.inHeight))for(var z=k+B*n.strides[1],L=T+U*e.strides[2],W=0;W<o.outWidth;++W)for(var G=F+W*o.outChannels,j=W*o.strideWidth-f,q=0;q<s;q++){var X=j+q*l;if(!(X<0||X>=o.inWidth))for(var oe=z+q*n.strides[2],re=L+X*o.inChannels,ie=oe,ce=0;ce<o.inChannels;++ce){for(var de=v[re+ce],le=0;le<o.outChannels;++le)g[G+le]+=de*m[ie+le];ie+=o.outChannels}}}}return p.toTensor()},t.prototype.conv2dDerInput=function(e,n,o){H([e,n],"conv2dDerInput");for(var a=ue(o.inShape,"float32"),i=a.values,s=this.readSync(e.dataId),u=this.readSync(n.dataId),c=n.strides,l=c[0],h=c[1],f=c[2],d=o.batchSize,p=o.filterHeight,v=o.filterWidth,m=o.inChannels,g=o.inHeight,x=o.inWidth,y=o.outChannels,b=o.outHeight,w=o.outWidth,C=o.strideHeight,I=o.strideWidth,E=o.dataFormat,S=p-1-o.padInfo.top,k=v-1-o.padInfo.left,T=E==="channelsLast",A=a.strides[0],F=T?a.strides[1]:a.strides[2],P=T?a.strides[2]:1,B=T?1:a.strides[1],U=e.strides[0],z=T?e.strides[1]:e.strides[2],L=T?e.strides[2]:1,W=T?1:e.strides[1],G=0;G<d;++G)for(var j=0;j<m;++j)for(var q=0;q<g;++q)for(var X=q-S,oe=Math.max(0,Math.ceil(X/C)),re=Math.min(b,(p+X)/C),ie=0;ie<x;++ie){for(var ce=ie-k,de=Math.max(0,Math.ceil(ce/I)),le=Math.min(w,(v+ce)/I),_e=0,me=oe;me<re;++me)for(var we=me*C-X,ge=de;ge<le;++ge)for(var Ee=U*G+z*me+L*ge,ke=l*(p-1-we)+h*(v-1-(ge*I-ce))+f*j,Re=0;Re<y;++Re)_e+=s[Ee+W*Re]*u[ke+Re];i[A*G+F*q+P*ie+B*j]=_e}return a.toTensor()},t.prototype.conv3dDerInput=function(e,n,o){for(var a=ue(o.inShape,"float32"),i=a.values,s=a.strides,u=s[0],c=s[1],l=s[2],h=s[3],f=this.readSync(e.dataId),d=e.strides,p=d[0],v=d[1],m=d[2],g=d[3],x=this.readSync(n.dataId),y=n.strides,b=y[0],w=y[1],C=y[2],I=y[3],E=o.batchSize,S=o.filterDepth,k=o.filterHeight,T=o.filterWidth,A=o.inChannels,F=o.inDepth,P=o.inHeight,B=o.inWidth,U=o.outChannels,z=o.outDepth,L=o.outHeight,W=o.outWidth,G=o.strideDepth,j=o.strideHeight,q=o.strideWidth,X=S-1-o.padInfo.front,oe=k-1-o.padInfo.top,re=T-1-o.padInfo.left,ie=0;ie<E;++ie)for(var ce=0;ce<A;++ce)for(var de=0;de<F;++de)for(var le=de-X,_e=Math.max(0,Math.ceil(le/G)),me=Math.min(z,(S+le)/G),we=0;we<P;++we)for(var ge=we-oe,Ee=Math.max(0,Math.ceil(ge/j)),ke=Math.min(L,(k+ge)/j),Re=0;Re<B;++Re){for(var wt=Re-re,Ct=Math.max(0,Math.ceil(wt/q)),ot=Math.min(W,(T+wt)/q),pn=0,At=_e;At<me;++At)for(var Vt=At*G-le,he=Ee;he<ke;++he)for(var Kn=he*j-ge,en=Ct;en<ot;++en)for(var _a=p*ie+v*At+m*he+g*en,Xn=b*(S-1-Vt)+w*(k-1-Kn)+C*(T-1-(en*q-wt))+I*ce,zt=0;zt<U;++zt)pn+=f[_a+zt]*x[Xn+zt];i[u*ie+c*de+l*we+h*Re+ce]=pn}return a.toTensor()},t.prototype.conv2dDerFilter=function(e,n,o){H([e,n],"conv2dDerFilter");for(var a=o.strideHeight,i=o.strideWidth,s=o.filterHeight,u=o.filterWidth,c=o.dataFormat==="channelsLast",l=ue(o.filterShape,"float32"),h=o.padInfo.left,f=o.padInfo.top,d=this.bufferSync(e),p=this.bufferSync(n),v=0;v<s;++v)for(var m=Math.max(0,Math.ceil((f-v)/a)),g=Math.min(o.outHeight,(o.inHeight+f-v)/a),x=0;x<u;++x)for(var y=Math.max(0,Math.ceil((h-x)/i)),b=Math.min(o.outWidth,(o.inWidth+h-x)/i),w=0;w<o.inChannels;++w)for(var C=0;C<o.outChannels;++C){for(var I=0,E=0;E<o.batchSize;++E)for(var S=m;S<g;++S)for(var k=v+S*a-f,T=y;T<b;++T){var A=x+T*i-h;I+=c?d.get(E,k,A,w)*p.get(E,S,T,C):d.get(E,w,k,A)*p.get(E,C,S,T)}l.set(I,v,x,w,C)}return l.toTensor()},t.prototype.conv3dDerFilter=function(e,n,o){for(var a=o.strideDepth,i=o.strideHeight,s=o.strideWidth,u=o.filterDepth,c=o.filterHeight,l=o.filterWidth,h=ue(o.filterShape,"float32"),f=h.values,d=h.strides,p=d[0],v=d[1],m=d[2],g=d[3],x=this.readSync(n.dataId),y=n.strides,b=y[0],w=y[1],C=y[2],I=y[3],E=this.readSync(e.dataId),S=e.strides,k=S[0],T=S[1],A=S[2],F=S[3],P=o.padInfo.front,B=o.padInfo.left,U=o.padInfo.top,z=0;z<u;++z)for(var L=Math.max(0,Math.ceil((P-z)/a)),W=Math.min(o.outDepth,(o.inDepth+P-z)/a),G=z*p,j=0;j<c;++j)for(var q=Math.max(0,Math.ceil((U-j)/i)),X=Math.min(o.outHeight,(o.inHeight+U-j)/i),oe=j*v+G,re=0;re<l;++re)for(var ie=Math.max(0,Math.ceil((B-re)/s)),ce=Math.min(o.outWidth,(o.inWidth+B-re)/s),de=re*m+oe,le=0;le<o.inChannels;++le)for(var _e=le*g+de,me=0;me<o.outChannels;++me){for(var we=0,ge=0;ge<o.batchSize;++ge)for(var Ee=ge*k,ke=ge*b,Re=L;Re<W;++Re)for(var wt=(z+Re*a-P)*T+Ee,Ct=Re*w+ke,ot=q;ot<X;++ot)for(var pn=(j+ot*i-U)*A+wt,At=ot*C+Ct,Vt=ie;Vt<ce;++Vt){var he=Vt*I+At;we+=E[(re+Vt*s-B)*F+pn+le]*x[he+me]}f[_e+me]=we}return h.toTensor()},t.prototype.fusedDepthwiseConv2D=function(e){var n=e.input,o=e.filter,a=e.convInfo,i=e.bias,s=e.activation,u=e.preluActivationWeights,c=this.depthwiseConv2D(n,o,a);return i&&(c=this.add(c,i)),s&&(c=Ua(this,c,s,u)),c},t.prototype.depthwiseConv2D=function(e,n,o){H([e,n],"depthwiseConv2D");for(var a=o.filterHeight,i=o.filterWidth,s=o.dilationHeight,u=o.dilationWidth,c=o.padInfo.left,l=o.padInfo.top,h=o.outChannels/o.inChannels,f=ue(o.outShape,e.dtype),d=this.readSync(e.dataId),p=this.readSync(n.dataId),v=f.values,m=0;m<o.batchSize;++m)for(var g=m*e.strides[0],x=m*f.strides[0],y=0;y<o.outHeight;++y)for(var b=x+y*f.strides[1],w=y*o.strideHeight-c,C=0;C<a;++C){var I=w+C*s;if(!(I<0||I>=o.inHeight))for(var E=C*n.strides[0],S=g+I*e.strides[1],k=0;k<o.outWidth;++k)for(var T=b+k*f.strides[2],A=k*o.strideWidth-l,F=0;F<i;++F){var P=A+F*u;if(!(P<0||P>=o.inWidth))for(var B=E+F*n.strides[1],U=S+P*o.inChannels,z=T,L=B,W=0;W<o.inChannels;++W){for(var G=d[U+W],j=0;j<h;++j)v[z+j]+=G*p[L+j];z+=h,L+=h}}}return f.toTensor()},t.prototype.depthwiseConv2DDerInput=function(e,n,o){H([e,n],"depthwiseConv2DDerInput");for(var a=ue(o.inShape,"float32"),i=a.values,s=a.strides,u=s[0],c=s[1],l=s[2],h=this.readSync(e.dataId),f=e.strides,d=f[0],p=f[1],v=f[2],m=this.readSync(n.dataId),g=n.strides,x=g[0],y=g[1],b=g[2],w=o.batchSize,C=o.filterHeight,I=o.filterWidth,E=o.inChannels,S=o.inHeight,k=o.inWidth,T=o.outChannels,A=o.outHeight,F=o.outWidth,P=o.strideHeight,B=o.strideWidth,U=C-1-o.padInfo.top,z=I-1-o.padInfo.left,L=T/E,W=0;W<w;++W)for(var G=0;G<E;++G)for(var j=0;j<S;++j)for(var q=j-U,X=Math.max(0,Math.ceil(q/P)),oe=Math.min(A,(C+q)/P),re=0;re<k;++re){for(var ie=re-z,ce=Math.max(0,Math.ceil(ie/B)),de=Math.min(F,(I+ie)/B),le=0,_e=X;_e<oe;++_e)for(var me=_e*P-q,we=ce;we<de;++we)for(var ge=d*W+p*_e+v*we,Ee=x*(C-1-me)+y*(I-1-(we*B-ie))+b*G,ke=0;ke<L;++ke)le+=h[ge+(G*L+ke)]*m[Ee+ke];i[u*W+c*j+l*re+G]=le}return a.toTensor()},t.prototype.depthwiseConv2DDerFilter=function(e,n,o){H([e,n],"depthwiseConv2DDerFilter");for(var a=o.strideHeight,i=o.strideWidth,s=o.filterHeight,u=o.filterWidth,c=ue(o.filterShape,"float32"),l=o.padInfo.left,h=o.padInfo.top,f=o.outChannels/o.inChannels,d=this.bufferSync(e),p=this.bufferSync(n),v=0;v<s;++v)for(var m=Math.max(0,Math.ceil((h-v)/a)),g=Math.min(o.outHeight,(o.inHeight+h-v)/a),x=0;x<u;++x)for(var y=Math.max(0,Math.ceil((l-x)/i)),b=Math.min(o.outWidth,(o.inWidth+l-x)/i),w=0;w<o.outChannels;++w){for(var C=Math.trunc(w/f),I=w%f,E=0,S=0;S<o.batchSize;++S)for(var k=m;k<g;++k)for(var T=v+k*a-h,A=y;A<b;++A){var F=x+A*i-l;E+=d.get(S,T,F,C)*p.get(S,k,A,w)}c.set(E,v,x,C,I)}return c.toTensor()},t.prototype.tile=function(e,n){return H(e,"tile"),$l(this.bufferSync(e),n)},t.prototype.pad=function(e,n,o){H(e,"pad");var a=n.map(function(f,d){return f[0]+e.shape[d]+f[1]}),i=n.map(function(f){return f[0]}),s=this.bufferSync(e),u=ue(a,e.dtype);o!==0&&u.values.fill(o);for(var c=0;c<e.size;c++){var l=s.indexToLoc(c),h=l.map(function(f,d){return f+i[d]});u.set.apply(u,[s.get.apply(s,l)].concat(h))}return u.toTensor()},t.prototype.transpose=function(e,n){H(e,"transpose");for(var o=new Array(e.rank),a=0;a<o.length;a++)o[a]=e.shape[n[a]];var i=this.readSync(e.dataId),s=ue(o,e.dtype),u=this.bufferSync(e);for(a=0;a<e.size;++a){for(var c=u.indexToLoc(a),l=new Array(c.length),h=0;h<l.length;h++)l[h]=c[n[h]];var f=s.locToIndex(l);s.values[f]=i[a]}return s.toTensor()},t.prototype.gather=function(e,n,o){H([e,n],"gather");var a=e.shape.slice(),i=this.readSync(n.dataId);a[o]=i.length;for(var s=ue(a,e.dtype),u=this.bufferSync(e),c=0;c<s.size;++c){var l=s.indexToLoc(c),h=l.slice();h[o]=i[l[o]];var f=u.locToIndex(h);s.values[c]=u.values[f]}return s.toTensor()},t.prototype.batchToSpaceND=function(e,n,o){H([e],"batchToSpaceND");var a=n.reduce(function(h,f){return h*f}),i=Wo(e.shape,n,a),s=Vo(i.length,n.length),u=zo(e.shape,n,a),c=Ll(o,n.length),l=Wl(u,o,n.length);return e.reshape(i).transpose(s).reshape(u).slice(c,l)},t.prototype.spaceToBatchND=function(e,n,o){H([e],"spaceToBatchND");var a=n.reduce(function(f,d){return f*d}),i=[[0,0]];i.push.apply(i,o);for(var s=1+n.length;s<e.shape.length;++s)i.push([0,0]);var u=e.pad(i),c=Wo(u.shape,n,a,!1),l=Vo(c.length,n.length,!1),h=zo(u.shape,n,a,!1);return u.reshape(c).transpose(l).reshape(h)},t.prototype.pool=function(e,n,o){H(e,"pool");for(var a=n.strideHeight,i=n.strideWidth,s=n.dilationHeight,u=n.dilationWidth,c=n.effectiveFilterHeight,l=n.effectiveFilterWidth,h=n.padInfo.top,f=n.padInfo.left,d=o==="max"?Number.NEGATIVE_INFINITY:Number.POSITIVE_INFINITY,p=this.readSync(e.dataId),v=ue(n.outShape,e.dtype),m=v.values,g=n.outShape[1]*n.outShape[2]*n.outShape[3],x=n.outShape[2]*n.outShape[3],y=n.outShape[3],b=0;b<n.batchSize;++b)for(var w=b*g,C=b*e.strides[0],I=0;I<n.inChannels;++I)for(var E=0;E<n.outHeight;++E)for(var S=E*a-h,k=Math.max(0,S),T=Math.min(n.inHeight,c+S),A=w+E*x,F=0;F<n.outWidth;++F){for(var P=F*i-f,B=Math.max(0,P),U=Math.min(n.inWidth,l+P),z=d,L=0,W=0,G=k;G<T;G+=s){for(var j=C+G*e.strides[1],q=B;q<U;q+=u){var X=p[j+q*e.strides[2]+I];o==="max"&&X>z?z=X:o==="avg"&&(L+=X,W++)}if(isNaN(z))break}m[A+F*y+I]=o==="avg"?L/W:z}return v.toTensor()},t.prototype.maxPool=function(e,n){return this.pool(e,n,"max")},t.prototype.maxPoolPositions=function(e,n){for(var o=ue(n.outShape,"int32"),a=n.strideHeight,i=n.strideWidth,s=n.dilationHeight,u=n.dilationWidth,c=n.effectiveFilterHeight,l=n.effectiveFilterWidth,h=n.padInfo.top,f=n.padInfo.left,d=this.bufferSync(e),p=0;p<n.batchSize;++p)for(var v=0;v<n.inChannels;++v)for(var m=0;m<n.outHeight;++m){for(var g=m*a-h,x=g;x<0;)x+=s;for(var y=Math.min(n.inHeight,c+g),b=0;b<n.outWidth;++b){for(var w=b*i-f,C=w;C<0;)C+=u;for(var I=Math.min(n.inWidth,l+w),E=Number.NEGATIVE_INFINITY,S=-1,k=x;k<y;k+=s)for(var T=k-g,A=C;A<I;A+=u){var F=A-w,P=d.get(p,k,A,v);P>E&&(E=P,S=T*l+F)}o.set(S,p,m,b,v)}}return o.toTensor()},t.prototype.maxPoolBackprop=function(e,n,o,a){H([n,o],"maxPoolBackprop");for(var i=this.maxPoolPositions(n,a),s=a.strideHeight,u=a.strideWidth,c=a.dilationHeight,l=a.dilationWidth,h=a.effectiveFilterHeight,f=a.effectiveFilterWidth,d=f-1-a.padInfo.left,p=h-1-a.padInfo.top,v=ue(n.shape,"float32"),m=this.bufferSync(i),g=this.bufferSync(e),x=0;x<a.batchSize;++x)for(var y=0;y<a.inChannels;++y)for(var b=0;b<a.inHeight;++b)for(var w=0;w<a.inWidth;++w){for(var C=b-p,I=w-d,E=0,S=0;S<h;S+=c){var k=(C+S)/s;if(!(k<0||k>=a.outHeight||Math.floor(k)!==k))for(var T=0;T<f;T+=l){var A=(I+T)/u;if(!(A<0||A>=a.outWidth||Math.floor(A)!==A)){var F=h*f-1-m.get(x,k,A,y)===S*f+T?1:0;F!==0&&(E+=g.get(x,k,A,y)*F)}}}v.set(E,x,b,w,y)}return v.toTensor()},t.prototype.avgPoolBackprop=function(e,n,o){H([e,n],"avgPoolBackprop");for(var a=o.strideHeight,i=o.strideWidth,s=o.filterHeight,u=o.filterWidth,c=o.dilationHeight,l=o.dilationWidth,h=o.effectiveFilterHeight,f=o.effectiveFilterWidth,d=f-1-o.padInfo.left,p=h-1-o.padInfo.top,v=ue(n.shape,"float32"),m=1/(s*u),g=this.bufferSync(e),x=0;x<o.batchSize;++x)for(var y=0;y<o.inChannels;++y)for(var b=0;b<o.inHeight;++b)for(var w=0;w<o.inWidth;++w){for(var C=b-p,I=w-d,E=0,S=0;S<h;S+=c){var k=(C+S)/a;if(!(k<0||k>=o.outHeight||Math.floor(k)!==k))for(var T=0;T<f;T+=l){var A=(I+T)/i;A<0||A>=o.outWidth||Math.floor(A)!==A||(E+=g.get(x,k,A,y))}}v.set(E*m,x,b,w,y)}return v.toTensor()},t.prototype.pool3d=function(e,n,o){H(e,"pool3d");for(var a=n.strideDepth,i=n.strideHeight,s=n.strideWidth,u=n.dilationDepth,c=n.dilationHeight,l=n.dilationWidth,h=n.effectiveFilterDepth,f=n.effectiveFilterHeight,d=n.effectiveFilterWidth,p=n.padInfo.front,v=n.padInfo.top,m=n.padInfo.left,g=o==="max"?Number.NEGATIVE_INFINITY:Number.POSITIVE_INFINITY,x=this.readSync(e.dataId),y=ue(n.outShape,e.dtype),b=y.values,w=n.outShape[1]*n.outShape[2]*n.outShape[3]*n.outShape[4],C=n.outShape[2]*n.outShape[3]*n.outShape[4],I=n.outShape[3]*n.outShape[4],E=n.outShape[4],S=0;S<n.batchSize;++S)for(var k=S*w,T=S*e.strides[0],A=0;A<n.inChannels;++A)for(var F=0;F<n.outDepth;++F){for(var P=F*a-p,B=P;B<0;)B+=u;for(var U=Math.min(n.inDepth,h+P),z=k+F*C,L=0;L<n.outHeight;++L){for(var W=L*i-v,G=W;G<0;)G+=c;for(var j=Math.min(n.inHeight,f+W),q=z+L*I,X=0;X<n.outWidth;++X){for(var oe=X*s-m,re=oe;re<0;)re+=l;for(var ie=Math.min(n.inWidth,d+oe),ce=q+X*E,de=g,le=0,_e=0,me=B;me<U;me+=u){for(var we=T+me*e.strides[1],ge=G;ge<j;ge+=c){for(var Ee=we+ge*e.strides[2],ke=re;ke<ie;ke+=l){var Re=x[Ee+ke*e.strides[3]+A];if(o==="max"&&Re>de?de=Re:o==="avg"&&(le+=Re,_e++),isNaN(de))break}if(isNaN(de))break}if(isNaN(de))break}b[ce+A]=o==="avg"?le/_e:de}}}return y.toTensor()},t.prototype.avgPool3d=function(e,n){return H(e,"avgPool3d"),this.pool3d(e,n,"avg").toFloat()},t.prototype.avgPool3dBackprop=function(e,n,o){H([e,n],"avgPool3dBackprop");for(var a=o.strideDepth,i=o.strideHeight,s=o.strideWidth,u=o.filterDepth,c=o.filterHeight,l=o.filterWidth,h=o.dilationDepth,f=o.dilationHeight,d=o.dilationWidth,p=o.effectiveFilterDepth,v=o.effectiveFilterHeight,m=o.effectiveFilterWidth,g=p-1-o.padInfo.front,x=m-1-o.padInfo.left,y=v-1-o.padInfo.top,b=ue(n.shape,"float32"),w=1/(u*c*l),C=this.bufferSync(e),I=0;I<o.batchSize;++I)for(var E=0;E<o.inChannels;++E)for(var S=0;S<o.inDepth;++S)for(var k=0;k<o.inHeight;++k)for(var T=0;T<o.inWidth;++T){for(var A=S-g,F=k-y,P=T-x,B=0,U=0;U<p;U+=h){var z=(A+U)/a;if(!(z<0||z>=o.outDepth||Math.floor(z)!==z))for(var L=0;L<v;L+=f){var W=(F+L)/i;if(!(W<0||W>=o.outHeight||Math.floor(W)!==W))for(var G=0;G<m;G+=d){var j=(P+G)/s;j<0||j>=o.outWidth||Math.floor(j)!==j||(B+=C.get(I,z,W,j,E))}}}b.set(B*w,I,S,k,T,E)}return b.toTensor()},t.prototype.maxPool3d=function(e,n){return H(e,"maxPool3d"),this.pool3d(e,n,"max").toFloat()},t.prototype.maxPool3dPositions=function(e,n){for(var o=ue(n.outShape,"int32"),a=n.strideDepth,i=n.strideHeight,s=n.strideWidth,u=n.dilationDepth,c=n.dilationHeight,l=n.dilationWidth,h=n.effectiveFilterDepth,f=n.effectiveFilterHeight,d=n.effectiveFilterWidth,p=n.padInfo.front,v=n.padInfo.top,m=n.padInfo.left,g=this.bufferSync(e),x=0;x<n.batchSize;++x)for(var y=0;y<n.inChannels;++y)for(var b=0;b<n.outDepth;++b){for(var w=b*a-p,C=w;C<0;)C+=u;for(var I=Math.min(n.inDepth,h+w),E=0;E<n.outHeight;++E){for(var S=E*i-v,k=S;k<0;)k+=c;for(var T=Math.min(n.inHeight,f+S),A=0;A<n.outWidth;++A){for(var F=A*s-m,P=F;P<0;)P+=l;for(var B=Math.min(n.inWidth,d+F),U=Number.NEGATIVE_INFINITY,z=-1,L=C;L<I;L+=u)for(var W=L-w,G=k;G<T;G+=c)for(var j=G-S,q=P;q<B;q+=l){var X=q-F,oe=g.get(x,L,G,q,y);oe>=U&&(U=oe,z=W*f*d+j*f+X)}o.set(z,x,b,E,A,y)}}}return o.toTensor()},t.prototype.maxPool3dBackprop=function(e,n,o,a){H([n,o],"maxPool3dBackprop");for(var i=this.maxPool3dPositions(n,a),s=a.strideDepth,u=a.strideHeight,c=a.strideWidth,l=a.dilationDepth,h=a.dilationHeight,f=a.dilationWidth,d=a.effectiveFilterDepth,p=a.effectiveFilterHeight,v=a.effectiveFilterWidth,m=d-1-a.padInfo.front,g=v-1-a.padInfo.left,x=p-1-a.padInfo.top,y=ue(n.shape,"float32"),b=this.bufferSync(i),w=this.bufferSync(e),C=0;C<a.batchSize;++C)for(var I=0;I<a.inChannels;++I)for(var E=0;E<a.inDepth;++E)for(var S=0;S<a.inHeight;++S)for(var k=0;k<a.inWidth;++k){for(var T=E-m,A=S-x,F=k-g,P=0,B=0;B<d;B+=l){var U=(T+B)/s;if(!(U<0||U>=a.outDepth||Math.floor(U)!==U))for(var z=0;z<p;z+=h){var L=(A+z)/u;if(!(L<0||L>=a.outHeight||Math.floor(L)!==L))for(var W=0;W<v;W+=f){var G=(F+W)/c;if(!(G<0||G>=a.outWidth||Math.floor(G)!==G)){var j=d*p*v-1-b.get(C,U,L,G,I)===B*p*v+z*v+W?1:0;j!==0&&(P+=w.get(C,U,L,G,I)*j)}}}}y.set(P,C,E,S,k,I)}return y.toTensor()},t.prototype.cast=function(e,n){return is(e,n,this)},t.prototype.reshape=function(e,n){return Go(e,n)},t.prototype.avgPool=function(e,n){return H(e,"avgPool"),this.pool(e,n,"avg").toFloat()},t.prototype.resizeBilinear=function(e,n,o,a){H(e,"resizeBilinear");for(var i=e.shape,s=i[0],u=i[1],c=i[2],l=i[3],h=this.readSync(e.dataId),f=new Float32Array(ee([s,n,o,l])),d=[a&&n>1?u-1:u,a&&o>1?c-1:c],p=[a&&n>1?n-1:n,a&&o>1?o-1:o],v=0,m=d[0]/p[0],g=d[1]/p[1],x=0;x<s;x++)for(var y=0;y<n;y++)for(var b=m*y,w=Math.floor(b),C=b-w,I=Math.min(u-1,Math.ceil(b)),E=x*e.strides[0]+w*e.strides[1],S=x*e.strides[0]+I*e.strides[1],k=0;k<o;k++)for(var T=g*k,A=Math.floor(T),F=T-A,P=Math.min(c-1,Math.ceil(T)),B=E+A*e.strides[2],U=S+A*e.strides[2],z=E+P*e.strides[2],L=S+P*e.strides[2],W=0;W<l;W++){var G=h[B+W],j=h[U+W],q=G+(h[z+W]-G)*F,X=q+(j+(h[L+W]-j)*F-q)*C;f[v++]=X}return $e(f,[s,n,o,l])},t.prototype.resizeBilinearBackprop=function(e,n,o){H([e,n],"resizeBilinearBackprop");for(var a=n.shape,i=a[0],s=a[1],u=a[2],c=a[3],l=e.shape,h=l[1],f=l[2],d=new Float32Array(i*s*u*c),p=[o&&h>1?s-1:s,o&&f>1?u-1:u],v=[o&&h>1?h-1:h,o&&f>1?f-1:f],m=p[0]/v[0],g=p[1]/v[1],x=this.readSync(e.dataId),y=0,b=0;b<i;b++)for(var w=b*n.strides[0],C=0;C<h;C++)for(var I=C*m,E=Math.floor(I),S=Math.min(Math.ceil(I),s-1),k=w+E*n.strides[1],T=w+S*n.strides[1],A=I-E,F=1-A,P=0;P<f;P++)for(var B=P*g,U=Math.floor(B),z=Math.min(Math.ceil(B),u-1),L=B-U,W=1-L,G=k+U*n.strides[2],j=k+z*n.strides[2],q=T+U*n.strides[2],X=T+z*n.strides[2],oe=F*W,re=F*L,ie=A*W,ce=A*L,de=0;de<c;de++){var le=x[y++];d[G+de]+=le*oe,d[j+de]+=le*re,d[q+de]+=le*ie,d[X+de]+=le*ce}return lt(d,[i,u,s,c],n.dtype)},t.prototype.resizeNearestNeighbor=function(e,n,o,a){H(e,"resizeNearestNeighbor");for(var i=e.shape,s=i[0],u=i[1],c=i[2],l=i[3],h=this.readSync(e.dataId),f=new Float32Array(s*n*o*l),d=[a&&n>1?u-1:u,a&&o>1?c-1:c],p=[a&&n>1?n-1:n,a&&o>1?o-1:o],v=d[0]/p[0],m=d[1]/p[1],g=0,x=0;x<s;x++)for(var y=x*e.strides[0],b=0;b<n;b++)for(var w=v*b,C=y+Math.min(u-1,a?Math.round(w):Math.floor(w))*e.strides[1],I=0;I<o;I++)for(var E=m*I,S=C+Math.min(c-1,a?Math.round(E):Math.floor(E))*e.strides[2],k=0;k<l;k++){var T=h[S+k];f[g++]=T}return $e(f,[s,n,o,l],e.dtype)},t.prototype.resizeNearestNeighborBackprop=function(e,n,o){H([e,n],"resizeNearestNeighborBackprop");for(var a=n.shape,i=a[0],s=a[1],u=a[2],c=a[3],l=e.shape,h=l[1],f=l[2],d=new Float32Array(i*s*u*c),p=this.readSync(e.dataId),v=[o&&h>1?s-1:s,o&&f>1?u-1:u],m=[o&&h>1?h-1:h,o&&f>1?f-1:f],g=v[0]/m[0],x=v[1]/m[1],y=1/g,b=1/x,w=2*Math.ceil(y)+2,C=2*Math.ceil(b)+2,I=0;I<i;I++)for(var E=I*n.strides[0],S=0;S<s;S++)for(var k=E+S*n.strides[1],T=Math.floor(S*y),A=Math.floor(T-w/2),F=0;F<u;F++)for(var P=k+F*n.strides[2],B=Math.floor(F*b),U=Math.floor(B-C/2),z=0;z<c;z++){for(var L=0,W=0;W<w;W++){var G=W+A;if(!(G<0||G>=h)){var j=E+G*e.strides[1],q=G*g;if(S===Math.min(s-1,o?Math.round(q):Math.floor(q)))for(var X=0;X<C;X++){var oe=X+U;if(!(oe<0||oe>=f)){var re=j+oe*e.strides[2],ie=oe*x;F===Math.min(u-1,o?Math.round(ie):Math.floor(ie))&&(L+=p[re+z])}}}}d[P+z]=L}return lt(d,n.shape,n.dtype)},t.prototype.batchNormalization=function(e,n,o,a,i,s){H([e,n,o,i,s],"batchNorm");for(var u=this.readSync(e.dataId),c=this.readSync(n.dataId),l=this.readSync(o.dataId),h=i?this.readSync(i.dataId):new Float32Array([1]),f=s?this.readSync(s.dataId):new Float32Array([0]),d=new Float32Array(u.length),p=f.length,v=h.length,m=l.length,g=c.length,x=0,y=0,b=0,w=0,C=0;C<u.length;++C)d[C]=f[x++]+(u[C]-c[y++])*h[b++]/Math.sqrt(l[w++]+a),x>=p&&(x=0),y>=g&&(y=0),b>=v&&(b=0),w>=m&&(w=0);return lt(d,e.shape)},t.prototype.localResponseNormalization4D=function(e,n,o,a,i){H(e,"localResponseNormalization4D");var s=e.shape[3],u=s-1,c=this.readSync(e.dataId),l=e.size,h=new Float32Array(l);function f(m){for(var g=m%s,x=m-g+Math.max(0,g-n),y=m-g+Math.min(g+n,u),b=0;x<=y;x++){var w=c[x];b+=w*w}return b}for(var d=0;d<l;d++){var p=f(d),v=c[d]*Math.pow(o+a*p,-i);h[d]=v}return lt(h,e.shape)},t.prototype.LRNGrad=function(e,n,o,a,i,s,u){H(e,"LRNGrad");for(var c=e.shape[3],l=this.readSync(e.dataId),h=this.readSync(n.dataId),f=this.readSync(o.dataId),d=new Float32Array(e.size),p=e.size,v=0;v<p;v++){for(var m=v%c,g=v-m+Math.max(0,m-a),x=v-m+Math.min(c,m+a+1),y=0,b=g;b<x;b++)y+=Math.pow(h[b],2);for(y=s*y+i,b=g;b<x;b++){var w=-2*s*u*h[b]*f[v]/y;v===b&&(w+=Math.pow(y,-u)),w*=l[v],d[b]+=w}}return lt(d,e.shape)},t.prototype.multinomial=function(e,n,o,a){H(e,"multinomial");for(var i=n?e:cn(e),s=i.shape[0],u=i.shape[1],c=Ie([s,o],"int32"),l=this.readSync(c.dataId),h=this.readSync(i.dataId),f=0;f<s;++f){var d=f*u,p=new Float32Array(u-1);p[0]=h[d];for(var v=1;v<p.length;++v)p[v]=p[v-1]+h[d+v];for(var m=aa(a.toString()),g=f*o,x=0;x<o;++x){var y=m();l[g+x]=p.length;for(var b=0;b<p.length;b++)if(y<p[b]){l[g+x]=b;break}}}return c},t.prototype.oneHot=function(e,n,o,a){H(e,"oneHot");var i=new Float32Array(e.size*n);i.fill(a);for(var s=this.readSync(e.dataId),u=0;u<e.size;++u)s[u]>=0&&s[u]<n&&(i[u*n+s[u]]=o);return bn(i,[e.size,n],"int32")},t.prototype.nonMaxSuppression=function(e,n,o,a,i){return H(e,"nonMaxSuppression"),us(this.readSync(e.dataId),this.readSync(n.dataId),o,a,i)},t.prototype.fft=function(e){return this.fftBatch(e,!1)},t.prototype.ifft=function(e){return this.fftBatch(e,!0)},t.prototype.fftBatch=function(e,n){for(var o=e.shape[0],a=e.shape[1],i=ue(e.shape,"float32"),s=ue(e.shape,"float32"),u=St(e).as2D(o,a),c=jt(e).as2D(o,a),l=0;l<o;l++)for(var h=u.slice([l,0],[1,a]),f=c.slice([l,0],[1,a]),d=Ke(h,f),p=this.readSync(this.fftImpl(d,n).dataId),v=0;v<a;v++){var m=vu(p,v);i.values[l*a+v]=m.real,s.values[l*a+v]=m.imag}return Ke(i.toTensor(),s.toTensor()).as2D(o,a)},t.prototype.fftImpl=function(e,n){var o=e.as1D(),a=o.size;if(this.isExponentOf2(a)){var i=this.fftRadix2(o,a,n).as2D(e.shape[0],e.shape[1]);return n&&(i=Ke(St(i).div(K(a)),jt(i).div(K(a)))),i}var s=this.readSync(e.dataId),u=function(c){for(var l=new Float32Array(c.length/2),h=new Float32Array(c.length/2),f=0;f<c.length;f+=2)l[f/2]=c[f],h[f/2]=c[f+1];return{real:l,imag:h}}(this.fourierTransformByMatmul(s,a,n));return Ke(u.real,u.imag).as2D(e.shape[0],e.shape[1])},t.prototype.isExponentOf2=function(e){return(e&e-1)==0},t.prototype.fftRadix2=function(e,n,o){if(n===1)return e;var a=this.readSync(e.dataId),i=n/2,s=function(g){for(var x=Math.ceil(g.length/4),y=new Float32Array(x),b=new Float32Array(x),w=0;w<g.length;w+=4)y[Math.floor(w/4)]=g[w],b[Math.floor(w/4)]=g[w+1];return{real:y,imag:b}}(a),u=Ke(s.real,s.imag).as1D(),c=function(g){for(var x=Math.floor(g.length/4),y=new Float32Array(x),b=new Float32Array(x),w=2;w<g.length;w+=4)y[Math.floor(w/4)]=g[w],b[Math.floor(w/4)]=g[w+1];return{real:y,imag:b}}(a),l=Ke(c.real,c.imag).as1D();u=this.fftRadix2(u,i,o),l=this.fftRadix2(l,i,o);var h=function(g,x){for(var y=new Float32Array(g/2),b=new Float32Array(g/2),w=0;w<Math.ceil(g/2);w++){var C=(x?2:-2)*Math.PI*(w/g);y[w]=Math.cos(C),b[w]=Math.sin(C)}return{real:y,imag:b}}(n,o),f=Ke(h.real,h.imag).mul(l),d=u.add(f),p=u.sub(f),v=St(d).concat(St(p)),m=jt(d).concat(jt(p));return Ke(v,m).as1D()},t.prototype.fourierTransformByMatmul=function(e,n,o){for(var a=new Float32Array(2*n),i=0;i<n;i++){for(var s=0,u=0,c=0;c<n;c++){var l=mv(i*c,n,o),h=vu(e,c);s+=h.real*l.real-h.imag*l.imag,u+=h.real*l.imag+h.imag*l.real}o&&(s/=n,u/=n),vv(a,s,u,i)}return a},t.prototype.depthToSpace=function(e,n,o){R(o==="NHWC",function(){return"Only NHWC dataFormat supported on CPU for depthToSpace. Got "+o}),R(n>1,function(){return"blockSize should be > 1 for depthToSpace, but was: "+n});for(var a=e.shape[0],i=e.shape[1],s=e.shape[2],u=e.shape[3],c=i*n,l=s*n,h=u/(n*n),f=this.readSync(e.dataId),d=new Float32Array(a*c*l*h),p=0,v=0;v<a;++v)for(var m=0;m<c;++m)for(var g=Math.floor(m/n),x=m%n,y=0;y<l;++y)for(var b=Math.floor(y/n),w=(x*n+y%n)*h,C=0;C<h;++C){var I=C+w+u*(b+s*(g+i*v));d[p++]=f[I]}return lt(d,[a,c,l,h])},t.prototype.broadcastedBinaryOp=function(e,n,o,a){var i=ve(e.shape,n.shape),s=ue(i,o),u=this.readSync(e.dataId),c=this.readSync(n.dataId),l=an(e.shape,i),h=an(n.shape,i),f=s.values;if(l.length+h.length===0)for(var d=0;d<f.length;++d)f[d]=a(u[d%u.length],c[d%c.length]);else{var p=this.bufferSync(e),v=this.bufferSync(n),m=function(g){var x=s.indexToLoc(g),y=x.slice(-e.rank);l.forEach(function(I){return y[I]=0});var b=p.locToIndex(y),w=x.slice(-n.rank);h.forEach(function(I){return w[I]=0});var C=v.locToIndex(w);f[g]=a(u[b],c[C])};for(d=0;d<f.length;++d)m(d)}return s.toTensor()},t.prototype.broadcastedBinaryComplexOp=function(e,n,o){var a=ve(e.shape,n.shape),i=ue(a,"float32"),s=ue(a,"float32"),u=this.readSync(e.dataId),c=this.readSync(n.dataId),l=an(e.shape,a),h=an(n.shape,a),f=i.values,d=s.values;if(l.length+h.length===0)for(var p=0;p<f.length;p++){var v=p%u.length,m=p%c.length,g=o(u[2*v],u[2*v+1],c[2*m],c[2*m+1]);f[p]=g.real,d[p]=g.imag}else{var x=this.bufferSync(this.data.get(e.dataId).complexTensors.real),y=this.bufferSync(this.data.get(n.dataId).complexTensors.real),b=function(w){var C=i.indexToLoc(w),I=C.slice(-e.rank);l.forEach(function(A){return I[A]=0});var E=x.locToIndex(I),S=C.slice(-n.rank);h.forEach(function(A){return S[A]=0});var k=y.locToIndex(S),T=o(u[2*E],u[2*E+1],c[2*k],c[2*k+1]);f[w]=T.real,d[w]=T.imag};for(p=0;p<f.length;p++)b(p)}return this.complex(i.toTensor(),s.toTensor())},t.prototype.split=function(e,n,o){return Xl(e,n,o)},t.prototype.dispose=function(){},t.prototype.floatPrecision=function(){return 32},t.prototype.epsilon=function(){return 1e-7},t.prototype.cropAndResize=function(e,n,o,a,i,s){for(var u=e.shape,c=u[0],l=u[1],h=u[2],f=u[3],d=n.shape[0],p=a[0],v=a[1],m=ue([d,p,v,f],"float32"),g=this.readSync(n.dataId),x=this.readSync(o.dataId),y=this.readSync(e.dataId),b=e.strides,w=m.strides,C=0;C<d;C++){var I=4*C,E=g[I],S=g[I+1],k=g[I+2],T=g[I+3],A=x[C];if(!(A>=c))for(var F=p>1?(k-E)*(l-1)/(p-1):0,P=v>1?(T-S)*(h-1)/(v-1):0,B=0;B<p;B++){var U=p>1?E*(l-1)+B*F:.5*(E+k)*(l-1);if(U<0||U>l-1)for(var z=0;z<v;z++)for(var L=0;L<f;L++){var W=L+z*w[2]+B*w[1]+C*w[0];m.values[W]=s}else if(i==="bilinear"){var G=Math.floor(U),j=Math.ceil(U),q=U-G;for(z=0;z<v;z++)if((me=v>1?S*(h-1)+z*P:.5*(S+T)*(h-1))<0||me>h-1)for(L=0;L<f;L++)W=L+z*w[2]+B*w[1]+C*w[0],m.values[W]=s;else{var X=Math.floor(me),oe=Math.ceil(me),re=me-X;for(L=0;L<f;L++){var ie=y[W=L+X*b[2]+G*b[1]+A*b[0]],ce=y[W=L+oe*b[2]+G*b[1]+A*b[0]],de=y[W=L+X*b[2]+j*b[1]+A*b[0]],le=ie+(ce-ie)*re,_e=de+(y[W=L+oe*b[2]+j*b[1]+A*b[0]]-de)*re;W=L+z*w[2]+B*w[1]+C*w[0],m.values[W]=le+(_e-le)*q}}}else for(z=0;z<v;++z){var me;if((me=v>1?S*(h-1)+z*P:.5*(S+T)*(h-1))<0||me>h-1)for(L=0;L<f;L++)W=L+z*w[2]+B*w[1]+C*w[0],m.values[W]=s;else{var we=Math.round(me),ge=Math.round(U);for(L=0;L<f;L++){var Ee=L+we*b[2]+ge*b[1]+A*b[0],ke=L+z*w[2]+B*w[1]+C*w[0];m.values[ke]=y[Ee]}}}}}return m.toTensor()},t.prototype.sparseToDense=function(e,n,o,a){var i=zr(0,e,o),s=i.sliceRank,u=i.numUpdates,c=i.sliceSize,l=i.strides,h=i.outputSize;return this.scatter(e,n,o,h,c,u,s,l,a,!1)},t.prototype.gatherND=function(e,n){var o=n.shape,a=o[o.length-1],i=ts(e,n),s=i[0],u=i[1],c=i[2],l=i[3];if(u===0)return $e([],s,e.dtype);for(var h=new Br([u,c],e.dtype),f=this.readSync(n.dataId),d=this.readSync(e.dataId),p=0;p<u;p++){for(var v=[],m=0,g=0;g<a;g++){var x=f[p*a+g];m+=x*l[g],v.push(x)}if(m<0||m>=e.size/c)throw new Error("Invalid indices: "+v+" does not index into "+e.shape);for(var y=0;y<c;y++)h.values[p*c+y]=d[m*c+y]}return h.toTensor().reshape(s)},t.prototype.scatterND=function(e,n,o){var a=zr(0,e,o),i=a.sliceRank,s=a.numUpdates,u=a.sliceSize,c=a.strides,l=a.outputSize,h=K(0);return this.scatter(e,n,o,l,u,s,i,c,h,!0)},t.prototype.fill=function(e,n,o){var a=Mr(o=o||gr(n),ee(e));return a.fill(n),N.makeTensor(a,e,o,this)},t.prototype.onesLike=function(e){if(e.dtype==="string")throw new Error("onesLike is not supported for string tensors");return this.fill(e.shape,1,e.dtype)},t.prototype.zerosLike=function(e){var n=Mr(e.dtype,ee(e.shape));return this.makeOutput(n,e.shape,e.dtype)},t.prototype.linspace=function(e,n,o){return ss(e,n,o)},t.prototype.scatter=function(e,n,o,a,i,s,u,c,l,h){var f=[a/i,i],d=this.readSync(e.dataId),p=this.readSync(n.dataId);if(a===0)return $e([],o,n.dtype);var v=new Br(f,n.dtype);v.values.fill(this.readSync(l.dataId)[0]);for(var m=0;m<s;m++){for(var g=[],x=0,y=0;y<u;y++){var b=d[m*u+y];g.push(b),x+=b*c[y]}if(x<0||x>=a/i)throw new Error("Invalid indices: "+g+" does not index into "+o);for(var w=0;w<i;w++)h?v.values[x*i+w]+=p[m*i+w]:v.values[x*i+w]=n.rank===0?p[0]:p[m*i+w]}return v.toTensor().reshape(o)},t}(jl);N.registerBackend("cpu",function(){return new x0},1);for(var Ga=0,Bu=[{kernelName:"NonMaxSuppressionV5",backendName:"cpu",kernelFunc:function(r){var t=r.inputs,e=r.backend,n=r.attrs,o=t,a=o.boxes,i=o.scores,s=n,u=s.maxOutputSize,c=s.iouThreshold,l=s.scoreThreshold,h=s.softNmsSigma,f=e;H(a,"NonMaxSuppressionWithScore");var d=cs(f.data.get(a.dataId).values,f.data.get(i.dataId).values,u,c,l,h);return[d.selectedIndices,d.selectedScores]}},{kernelName:"Square",backendName:"cpu",kernelFunc:function(r){var t=r.inputs,e=r.backend,n=t.x,o=e;H(n,"square");for(var a=o.data.get(n.dataId).values,i=new Float32Array(a.length),s=0;s<a.length;++s){var u=a[s];i[s]=u*u}return{dataId:o.write(i,n.shape,n.dtype),shape:n.shape,dtype:n.dtype}}},{kernelName:Hr,backendName:"cpu",kernelFunc:function(r){var t=r.inputs,e=r.backend,n=t,o=n.a,a=n.b,i=e;H([o,a],Hr);var s=i.data.get(o.dataId).values,u=i.data.get(a.dataId).values,c=function(f,d,p,v,m,g){var x=ve(f,d),y=x.length,b=Ft(x),w=fr(m,ee(x)),C=f.length,I=d.length,E=Ft(f),S=Ft(d),k=an(f,x),T=an(d,x);if(k.length+T.length===0)for(var A=0;A<w.length;++A)w[A]=g(p[A%p.length],v[A%v.length]);else{var F=function(P){var B=Zc(P,y,b),U=B.slice(-C);k.forEach(function(G){return U[G]=0});var z=ci(U,C,E),L=B.slice(-I);T.forEach(function(G){return L[G]=0});var W=ci(L,I,S);w[P]=g(p[z],v[W])};for(A=0;A<w.length;++A)F(A)}return[w,x]}(o.shape,a.shape,s,u,o.dtype,function(f,d){var p=f-d;return p*p}),l=c[0],h=c[1];return{dataId:i.write(l,h,o.dtype),shape:h,dtype:o.dtype}}}];Ga<Bu.length;Ga++)zc(Bu[Ga]);var Qn,b0=function(r){this.variableNames=["A"];var t=Qe(),e=r[0],n=r[1];this.outputShape=r,this.userCode=`
      void main() {
        ivec3 coords = getOutputCoords();
        int texR = coords[0];
        int texC = coords[1];
        int depth = coords[2];
        vec2 uv = (vec2(texC, texR) + halfCR) / vec2(`+n+".0, "+e+`.0);

        vec4 values = `+t.texture2D+`(A, uv);
        float value;
        if (depth == 0) {
          value = values.r;
        } else if (depth == 1) {
          value = values.g;
        } else if (depth == 2) {
          value = values.b;
        } else if (depth == 3) {
          value = values.a;
        }

        setOutput(floor(value * 255.0 + 0.5));
      }
    `},w0=function(r){this.variableNames=["A"],this.packedInputs=!1,this.packedOutput=!0;var t=Qe(),e=r[0],n=r[1];this.outputShape=r,this.userCode=`
      void main() {
        ivec3 coords = getOutputCoords();
        int texR = coords[0];
        int texC = coords[1];
        int depth = coords[2];

        vec4 result = vec4(0.);

        for(int row=0; row<=1; row++) {
          for(int col=0; col<=1; col++) {
            texC = coords[1] + row;
            depth = coords[2] + col;

            vec2 uv = (vec2(texC, texR) + halfCR) /
                       vec2(`+n+".0, "+e+`.0);
            vec4 values = `+t.texture2D+`(A, uv);
            float value;
            if (depth == 0) {
              value = values.r;
            } else if (depth == 1) {
              value = values.g;
            } else if (depth == 2) {
              value = values.b;
            } else if (depth == 3) {
              value = values.a;
            }

            result[row * 2 + col] = floor(value * 255.0 + 0.5);
          }
        }

        `+t.output+` = result;
      }
    `};for(var Ha=0,Lu=[{kernelName:"FromPixels",backendName:"webgl",kernelFunc:function(r){var t=r.inputs,e=r.backend,n=r.attrs,o=t.pixels,a=n.numChannels,i=typeof HTMLVideoElement<"u"&&o instanceof HTMLVideoElement,s=typeof HTMLImageElement<"u"&&o instanceof HTMLImageElement,u=i?[o.videoWidth,o.videoHeight]:[o.width,o.height],c=u[0],l=u[1],h=[l,c],f=[l,c,a];(s||i)&&(Qn==null&&(Qn=document.createElement("canvas").getContext("2d")),Qn.canvas.width=c,Qn.canvas.height=l,Qn.drawImage(o,0,0,c,l),o=Qn.canvas);var d=e.makeTensorInfo(h,"int32");e.texData.get(d.dataId).usage=mt.PIXELS,e.gpgpu.uploadPixelDataToTexture(e.getTexture(d.dataId),o);var p=V().getBool("WEBGL_PACK")?new w0(f):new b0(f),v=e.runWebGLProgram(p,[d],"int32");return e.disposeData(d.dataId),v}},{kernelName:"NonMaxSuppressionV5",backendName:"webgl",kernelFunc:function(r){var t=r.inputs,e=r.backend,n=r.attrs;Oo("tf.nonMaxSuppression() in webgl locks the UI thread. Call tf.nonMaxSuppressionAsync() instead");var o=t,a=o.boxes,i=o.scores,s=n,u=s.maxOutputSize,c=s.iouThreshold,l=s.scoreThreshold,h=s.softNmsSigma,f=e,d=cs(f.readSync(a.dataId),f.readSync(i.dataId),u,c,l,h);return[d.selectedIndices,d.selectedScores]}},{kernelName:"Square",backendName:"webgl",kernelFunc:function(r){var t=r.inputs,e=r.backend,n=t.x,o=e,a=new fe(n.shape,"return x * x;");return o.runWebGLProgram(a,[n],n.dtype)}},{kernelName:Hr,backendName:"webgl",kernelFunc:function(r){var t=r.inputs,e=r.backend,n=t,o=n.a,a=n.b,i=e,s=V().getBool("WEBGL_PACK_BINARY_OPERATIONS")?new rn("return (a - b) * (a - b);",o.shape,a.shape):new Pe("return (a - b) * (a - b);",o.shape,a.shape);return i.compileAndRun(s,[o,a])}}];Ha<Lu.length;Ha++)zc(Lu[Ha]);for(var qa=0,Wu=[{kernelName:"Square",gradFunc:function(r,t){var e=t[0];return{x:function(){return r.mul(e.toFloat().mul(2))}}}},{kernelName:Hr,gradFunc:function(r,t){var e=t[0],n=t[1],o=K(2);return{a:function(){return et(r,et(o,Ue(e,n)))},b:function(){return et(r,et(o,Ue(n,e)))}}}}];qa<Wu.length;qa++)Ep(Wu[qa]);var C0=function(){function r(){}return r.prototype.fetch=function(t,e){return fetch(t,e)},r.prototype.now=function(){return performance.now()},r.prototype.encode=function(t,e){if(e!=="utf-8"&&e!=="utf8")throw new Error("Browser's encoder only supports utf-8, but got "+e);return this.textEncoder==null&&(this.textEncoder=new TextEncoder),this.textEncoder.encode(t)},r.prototype.decode=function(t,e){return new TextDecoder(e).decode(t)},r}();V().get("IS_BROWSER")&&V().setPlatform("browser",new C0);var ja,_0=function(){return require("node-fetch")},E0=function(){function r(){this.util=require("util"),this.textEncoder=new this.util.TextEncoder}return r.prototype.fetch=function(t,e){return V().global.fetch!=null?V().global.fetch(t,e):(ja==null&&(ja=_0()),ja(t,e))},r.prototype.now=function(){var t=process.hrtime();return 1e3*t[0]+t[1]/1e6},r.prototype.encode=function(t,e){if(e!=="utf-8"&&e!=="utf8")throw new Error("Node built-in encoder only supports utf-8, but got "+e);return this.textEncoder.encode(t)},r.prototype.decode=function(t,e){return t.length===0?"":new this.util.TextDecoder(e).decode(t)},r}();V().get("IS_NODE")&&V().setPlatform("node",new E0);var _i={float32:4,int32:4,uint16:2,uint8:1,bool:1},Ko=4;function of(r,t){for(var e={},n=0,o=function(s){var u=s.name,c=s.dtype,l=s.shape,h=ee(l),f=void 0;if("quantization"in s){var d=s.quantization;if(d.dtype!=="uint8"&&d.dtype!=="uint16")throw new Error("Weight "+s.name+" has unknown quantization dtype "+d.dtype+". Supported quantization dtypes are: 'uint8' and 'uint16'.");var p=_i[d.dtype],v=r.slice(n,n+h*p),m=d.dtype==="uint8"?new Uint8Array(v):new Uint16Array(v);if(c==="float32")f=Float32Array.from(m,function(C){return C*d.scale+d.min});else{if(c!=="int32")throw new Error("Unsupported dtype in weight '"+u+"': "+c);f=Int32Array.from(m,function(C){return Math.round(C*d.scale+d.min)})}n+=h*p}else if(c==="string"){var g=ee(s.shape);f=[];for(var x=0;x<g;x++){var y=new Uint32Array(r.slice(n,n+Ko))[0];n+=Ko;var b=new Uint8Array(r.slice(n,n+y));f.push(b),n+=y}}else{var w=_i[c];if(v=r.slice(n,n+h*w),c==="float32")f=new Float32Array(v);else if(c==="int32")f=new Int32Array(v);else{if(c!=="bool")throw new Error("Unsupported dtype in weight '"+u+"': "+c);f=new Uint8Array(v)}n+=h*w}e[u]=$e(f,l,c)},a=0,i=t;a<i.length;a++)o(i[a]);return e}function S0(r){if(r===null)throw new Error("Invalid input value: "+JSON.stringify(r));var t=0,e=[];r.forEach(function(a){if(t+=a.byteLength,e.push(a.byteLength===a.buffer.byteLength?a:new a.constructor(a)),!(a instanceof Float32Array||a instanceof Int32Array||a instanceof Uint8Array))throw new Error("Unsupported TypedArray subtype: "+a.constructor.name)});var n=new Uint8Array(t),o=0;return e.forEach(function(a){n.set(new Uint8Array(a.buffer),o),o+=a.byteLength}),n.buffer}var Ei=typeof Buffer<"u"&&(typeof Blob>"u"||typeof atob>"u"||typeof btoa>"u");function Vu(r){return Ei?Buffer.byteLength(r):new Blob([r]).size}function Is(r){var t=0;r.forEach(function(o){t+=o.byteLength});var e=new Uint8Array(t),n=0;return r.forEach(function(o){e.set(new Uint8Array(o),n),n+=o.byteLength}),e.buffer}function zu(r){for(r=r.trim();r.endsWith("/");)r=r.slice(0,r.length-1);var t=r.split("/");return t[t.length-1]}function eo(r){if(r.modelTopology instanceof ArrayBuffer)throw new Error("Expected JSON model topology, received ArrayBuffer.");return{dateSaved:new Date,modelTopologyType:"JSON",modelTopologyBytes:r.modelTopology==null?0:Vu(JSON.stringify(r.modelTopology)),weightSpecsBytes:r.weightSpecs==null?0:Vu(JSON.stringify(r.weightSpecs)),weightDataBytes:r.weightData==null?0:r.weightData.byteLength}}var yt=function(){function r(){this.saveRouters=[],this.loadRouters=[]}return r.getInstance=function(){return r.instance==null&&(r.instance=new r),r.instance},r.registerSaveRouter=function(t){r.getInstance().saveRouters.push(t)},r.registerLoadRouter=function(t){r.getInstance().loadRouters.push(t)},r.getSaveHandlers=function(t){return r.getHandlers(t,"save")},r.getLoadHandlers=function(t,e){return r.getHandlers(t,"load",e)},r.getHandlers=function(t,e,n){var o=[];return(e==="load"?r.getInstance().loadRouters:r.getInstance().saveRouters).forEach(function(a){var i=a(t,n);i!==null&&o.push(i)}),o},r}(),ur="://",wn=function(){function r(){this.managers={}}return r.getInstance=function(){return r.instance==null&&(r.instance=new r),r.instance},r.registerManager=function(t,e){R(t!=null,function(){return"scheme must not be undefined or null."}),t.endsWith(ur)&&(t=t.slice(0,t.indexOf(ur))),R(t.length>0,function(){return"scheme must not be an empty string."});var n=r.getInstance();R(n.managers[t]==null,function(){return"A model store manager is already registered for scheme '"+t+"'."}),n.managers[t]=e},r.getManager=function(t){var e=this.getInstance().managers[t];if(e==null)throw new Error("Cannot find model manager for scheme '"+t+"'");return e},r.getSchemes=function(){return Object.keys(this.getInstance().managers)},r}();function ko(r){if(r.indexOf(ur)===-1)throw new Error("The url string provided does not contain a scheme. Supported schemes are: "+wn.getSchemes().join(","));return{scheme:r.split(ur)[0],path:r.split(ur)[1]}}function Uu(r,t,e){return e===void 0&&(e=!1),Q(this,void 0,void 0,function(){var n,o,a,i,s,u,c,l,h;return Z(this,function(f){switch(f.label){case 0:return R(r!==t,function(){return"Old path and new path are the same: '"+r+"'"}),R((n=yt.getLoadHandlers(r)).length>0,function(){return"Copying failed because no load handler is found for source URL "+r+"."}),R(n.length<2,function(){return"Copying failed because more than one ("+n.length+") load handlers for source URL "+r+"."}),o=n[0],R((a=yt.getSaveHandlers(t)).length>0,function(){return"Copying failed because no save handler is found for destination URL "+t+"."}),R(a.length<2,function(){return"Copying failed because more than one ("+n.length+") save handlers for destination URL "+t+"."}),i=a[0],s=ko(r).scheme,u=ko(r).path,c=s===ko(r).scheme,[4,o.load()];case 1:return l=f.sent(),e&&c?[4,wn.getManager(s).removeModel(u)]:[3,3];case 2:f.sent(),f.label=3;case 3:return[4,i.save(l)];case 4:return h=f.sent(),!e||c?[3,6]:[4,wn.getManager(s).removeModel(u)];case 5:f.sent(),f.label=6;case 6:return[2,h.modelArtifactsInfo]}})})}var On="models_store",xn="model_info_store";function af(){if(!V().getBool("IS_BROWSER"))throw new Error("Failed to obtain IndexedDB factory because the current environmentis not a web browser.");var r=window||self,t=r.indexedDB||r.mozIndexedDB||r.webkitIndexedDB||r.msIndexedDB||r.shimIndexedDB;if(t==null)throw new Error("The current browser does not appear to support IndexedDB.");return t}function Si(r){var t=r.result;t.createObjectStore(On,{keyPath:"modelPath"}),t.createObjectStore(xn,{keyPath:"modelPath"})}var cr=function(){function r(t){if(this.indexedDB=af(),t==null||!t)throw new Error("For IndexedDB, modelPath must not be null, undefined or empty.");this.modelPath=t}return r.prototype.save=function(t){return Q(this,void 0,void 0,function(){return Z(this,function(e){if(t.modelTopology instanceof ArrayBuffer)throw new Error("BrowserLocalStorage.save() does not support saving model topology in binary formats yet.");return[2,this.databaseAction(this.modelPath,t)]})})},r.prototype.load=function(){return Q(this,void 0,void 0,function(){return Z(this,function(t){return[2,this.databaseAction(this.modelPath)]})})},r.prototype.databaseAction=function(t,e){var n=this;return new Promise(function(o,a){var i=n.indexedDB.open("tensorflowjs",1);i.onupgradeneeded=function(){return Si(i)},i.onsuccess=function(){var s=i.result;if(e==null){var u=s.transaction(On,"readonly"),c=u.objectStore(On).get(n.modelPath);c.onsuccess=function(){if(c.result==null)return s.close(),a(new Error("Cannot find model with path '"+n.modelPath+"' in IndexedDB."));o(c.result.modelArtifacts)},c.onerror=function(v){return s.close(),a(c.error)},u.oncomplete=function(){return s.close()}}else{var l,h=eo(e),f=s.transaction(xn,"readwrite"),d=f.objectStore(xn),p=d.put({modelPath:n.modelPath,modelArtifactsInfo:h});p.onsuccess=function(){var v=(l=s.transaction(On,"readwrite")).objectStore(On).put({modelPath:n.modelPath,modelArtifacts:e,modelArtifactsInfo:h});v.onsuccess=function(){return o({modelArtifactsInfo:h})},v.onerror=function(m){var g=(d=f.objectStore(xn)).delete(n.modelPath);g.onsuccess=function(){return s.close(),a(v.error)},g.onerror=function(x){return s.close(),a(v.error)}}},p.onerror=function(v){return s.close(),a(p.error)},f.oncomplete=function(){l==null?s.close():l.oncomplete=function(){return s.close()}}}},i.onerror=function(s){return a(i.error)}})},r.URL_SCHEME="indexeddb://",r}(),Gu=function(r){return V().getBool("IS_BROWSER")&&!Array.isArray(r)&&r.startsWith(cr.URL_SCHEME)?(t=r.slice(cr.URL_SCHEME.length),new cr(t)):null;var t};yt.registerSaveRouter(Gu),yt.registerLoadRouter(Gu);var k0=function(){function r(){this.indexedDB=af()}return r.prototype.listModels=function(){return Q(this,void 0,void 0,function(){var t=this;return Z(this,function(e){return[2,new Promise(function(n,o){var a=t.indexedDB.open("tensorflowjs",1);a.onupgradeneeded=function(){return Si(a)},a.onsuccess=function(){var i=a.result,s=i.transaction(xn,"readonly"),u=s.objectStore(xn).getAll();u.onsuccess=function(){for(var c={},l=0,h=u.result;l<h.length;l++){var f=h[l];c[f.modelPath]=f.modelArtifactsInfo}n(c)},u.onerror=function(c){return i.close(),o(u.error)},s.oncomplete=function(){return i.close()}},a.onerror=function(i){return o(a.error)}})]})})},r.prototype.removeModel=function(t){return Q(this,void 0,void 0,function(){var e=this;return Z(this,function(n){var o;return t=(o=t).startsWith(cr.URL_SCHEME)?o.slice(cr.URL_SCHEME.length):o,[2,new Promise(function(a,i){var s=e.indexedDB.open("tensorflowjs",1);s.onupgradeneeded=function(){return Si(s)},s.onsuccess=function(){var u,c=s.result,l=c.transaction(xn,"readwrite"),h=l.objectStore(xn),f=h.get(t);f.onsuccess=function(){if(f.result==null)return c.close(),i(new Error("Cannot find model with path '"+t+"' in IndexedDB."));var d=h.delete(t),p=function(){var v=(u=c.transaction(On,"readwrite")).objectStore(On).delete(t);v.onsuccess=function(){return a(f.result.modelArtifactsInfo)},v.onerror=function(m){return i(f.error)}};d.onsuccess=p,d.onerror=function(v){return p(),c.close(),i(f.error)}},f.onerror=function(d){return c.close(),i(f.error)},l.oncomplete=function(){u==null?c.close():u.oncomplete=function(){return c.close()}}},s.onerror=function(u){return i(s.error)}})]})})},r}();if(V().getBool("IS_BROWSER"))try{wn.registerManager(cr.URL_SCHEME,new k0)}catch{}var on="/",or="tensorflowjs_models",sf="info",R0="model_topology",I0="weight_specs",A0="weight_data",D0="model_metadata";function uf(r){return{info:[or,r,sf].join(on),topology:[or,r,R0].join(on),weightSpecs:[or,r,I0].join(on),weightData:[or,r,A0].join(on),modelMetadata:[or,r,D0].join(on)}}function T0(r){var t=r.split(on);if(t.length<3)throw new Error("Invalid key format: "+r);return t.slice(1,t.length-1).join(on)}var lr=function(){function r(t){if(!V().getBool("IS_BROWSER")||typeof window>"u"||window.localStorage===void 0)throw new Error("The current environment does not support local storage.");if(this.LS=window.localStorage,t==null||!t)throw new Error("For local storage, modelPath must not be null, undefined or empty.");this.modelPath=t,this.keys=uf(this.modelPath)}return r.prototype.save=function(t){return Q(this,void 0,void 0,function(){var e,n,o;return Z(this,function(a){if(t.modelTopology instanceof ArrayBuffer)throw new Error("BrowserLocalStorage.save() does not support saving model topology in binary formats yet.");e=JSON.stringify(t.modelTopology),n=JSON.stringify(t.weightSpecs),o=eo(t);try{return this.LS.setItem(this.keys.info,JSON.stringify(o)),this.LS.setItem(this.keys.topology,e),this.LS.setItem(this.keys.weightSpecs,n),this.LS.setItem(this.keys.weightData,function(i){if(Ei)return Buffer.from(i).toString("base64");for(var s=new Uint8Array(i),u="",c=0,l=s.length;c<l;c++)u+=String.fromCharCode(s[c]);return btoa(u)}(t.weightData)),this.LS.setItem(this.keys.modelMetadata,JSON.stringify({format:t.format,generatedBy:t.generatedBy,convertedBy:t.convertedBy,userDefinedMetadata:t.userDefinedMetadata})),[2,{modelArtifactsInfo:o}]}catch{throw this.LS.removeItem(this.keys.info),this.LS.removeItem(this.keys.topology),this.LS.removeItem(this.keys.weightSpecs),this.LS.removeItem(this.keys.weightData),this.LS.removeItem(this.keys.modelMetadata),new Error("Failed to save model '"+this.modelPath+"' to local storage: size quota being exceeded is a possible cause of this failure: modelTopologyBytes="+o.modelTopologyBytes+", weightSpecsBytes="+o.weightSpecsBytes+", weightDataBytes="+o.weightDataBytes+".")}return[2]})})},r.prototype.load=function(){return Q(this,void 0,void 0,function(){var t,e,n,o,a,i,s;return Z(this,function(u){if((t=JSON.parse(this.LS.getItem(this.keys.info)))==null)throw new Error("In local storage, there is no model with name '"+this.modelPath+"'");if(t.modelTopologyType!=="JSON")throw new Error("BrowserLocalStorage does not support loading non-JSON model topology yet.");if(e={},(n=JSON.parse(this.LS.getItem(this.keys.topology)))==null)throw new Error("In local storage, the topology of model '"+this.modelPath+"' is missing.");if(e.modelTopology=n,(o=JSON.parse(this.LS.getItem(this.keys.weightSpecs)))==null)throw new Error("In local storage, the weight specs of model '"+this.modelPath+"' are missing.");if(e.weightSpecs=o,(a=this.LS.getItem(this.keys.modelMetadata))!=null&&(i=JSON.parse(a),e.format=i.format,e.generatedBy=i.generatedBy,e.convertedBy=i.convertedBy,e.userDefinedMetadata=i.userDefinedMetadata),(s=this.LS.getItem(this.keys.weightData))==null)throw new Error("In local storage, the binary weight values of model '"+this.modelPath+"' are missing.");return e.weightData=function(c){if(Ei){var l=Buffer.from(c,"base64");return l.buffer.slice(l.byteOffset,l.byteOffset+l.byteLength)}for(var h=atob(c),f=new Uint8Array(h.length),d=0;d<h.length;++d)f.set([h.charCodeAt(d)],d);return f.buffer}(s),[2,e]})})},r.URL_SCHEME="localstorage://",r}(),Hu=function(r){return V().getBool("IS_BROWSER")&&!Array.isArray(r)&&r.startsWith(lr.URL_SCHEME)?(t=r.slice(lr.URL_SCHEME.length),new lr(t)):null;var t};yt.registerSaveRouter(Hu),yt.registerLoadRouter(Hu);var N0=function(){function r(){R(V().getBool("IS_BROWSER"),function(){return"Current environment is not a web browser"}),R(typeof window>"u"||window.localStorage!==void 0,function(){return"Current browser does not appear to support localStorage"}),this.LS=window.localStorage}return r.prototype.listModels=function(){return Q(this,void 0,void 0,function(){var t,e,n,o,a,i;return Z(this,function(s){for(t={},e=or+on,n=on+sf,o=0;o<this.LS.length;++o)(a=this.LS.key(o)).startsWith(e)&&a.endsWith(n)&&(i=T0(a),t[i]=JSON.parse(this.LS.getItem(a)));return[2,t]})})},r.prototype.removeModel=function(t){return Q(this,void 0,void 0,function(){var e,n;return Z(this,function(o){var a;if(t=(a=t).startsWith(lr.URL_SCHEME)?a.slice(lr.URL_SCHEME.length):a,e=uf(t),this.LS.getItem(e.info)==null)throw new Error("Cannot find model at path '"+t+"'");return n=JSON.parse(this.LS.getItem(e.info)),this.LS.removeItem(e.info),this.LS.removeItem(e.topology),this.LS.removeItem(e.weightSpecs),this.LS.removeItem(e.weightData),[2,n]})})},r}();if(V().getBool("IS_BROWSER"))try{wn.registerManager(lr.URL_SCHEME,new N0)}catch{}var F0="model",P0=".json",M0=".weights.bin";function qu(r){return new Promise(function(t){return setTimeout(t)}).then(r)}var Ka=function(){function r(t){if(!V().getBool("IS_BROWSER"))throw new Error("browserDownloads() cannot proceed because the current environment is not a browser.");t.startsWith(r.URL_SCHEME)&&(t=t.slice(r.URL_SCHEME.length)),t!=null&&t.length!==0||(t=F0),this.modelTopologyFileName=t+P0,this.weightDataFileName=t+M0}return r.prototype.save=function(t){return Q(this,void 0,void 0,function(){var e,n,o,a,i,s;return Z(this,function(u){switch(u.label){case 0:if(typeof document>"u")throw new Error("Browser downloads are not supported in this environment since `document` is not present");if(e=window.URL.createObjectURL(new Blob([t.weightData],{type:"application/octet-stream"})),!(t.modelTopology instanceof ArrayBuffer))return[3,1];throw new Error("BrowserDownloads.save() does not support saving model topology in binary formats yet.");case 1:return n=[{paths:["./"+this.weightDataFileName],weights:t.weightSpecs}],o={modelTopology:t.modelTopology,format:t.format,generatedBy:t.generatedBy,convertedBy:t.convertedBy,weightsManifest:n},a=window.URL.createObjectURL(new Blob([JSON.stringify(o)],{type:"application/json"})),(i=this.jsonAnchor==null?document.createElement("a"):this.jsonAnchor).download=this.modelTopologyFileName,i.href=a,[4,qu(function(){return i.dispatchEvent(new MouseEvent("click"))})];case 2:return u.sent(),t.weightData==null?[3,4]:((s=this.weightDataAnchor==null?document.createElement("a"):this.weightDataAnchor).download=this.weightDataFileName,s.href=e,[4,qu(function(){return s.dispatchEvent(new MouseEvent("click"))})]);case 3:u.sent(),u.label=4;case 4:return[2,{modelArtifactsInfo:eo(t)}]}})})},r.URL_SCHEME="downloads://",r}(),O0=function(){function r(t){if(t==null||t.length<1)throw new Error("When calling browserFiles, at least 1 file is required, but received "+t);this.files=t}return r.prototype.load=function(){return Q(this,void 0,void 0,function(){var t,e,n=this;return Z(this,function(o){return t=this.files[0],e=this.files.slice(1),[2,new Promise(function(a,i){var s=new FileReader;s.onload=function(u){var c=JSON.parse(u.target.result),l=c.modelTopology;if(l!=null){e.length===0&&a({modelTopology:l});var h=c.weightsManifest;if(h!=null){var f;try{f=n.checkManifestAndWeightFiles(h,e)}catch(m){return void i(m)}var d=[],p=[],v=[];h.forEach(function(m){m.paths.forEach(function(g){p.push(g),v.push(null)}),d.push.apply(d,m.weights)}),h.forEach(function(m){m.paths.forEach(function(g){var x=new FileReader;x.onload=function(y){var b=y.target.result,w=p.indexOf(g);v[w]=b,v.indexOf(null)===-1&&a({modelTopology:l,weightSpecs:d,weightData:Is(v),format:c.format,generatedBy:c.generatedBy,convertedBy:c.convertedBy,userDefinedMetadata:c.userDefinedMetadata})},x.onerror=function(y){return i("Failed to weights data from file of path '"+g+"'.")},x.readAsArrayBuffer(f[g])})})}else i(new Error("weightManifest field is missing from file "+t.name))}else i(new Error("modelTopology field is missing from file "+t.name))},s.onerror=function(u){return i("Failed to read model topology and weights manifest JSON from file '"+t.name+"'. BrowserFiles supports loading Keras-style tf.Model artifacts only.")},s.readAsText(t)})]})})},r.prototype.checkManifestAndWeightFiles=function(t,e){for(var n=[],o=e.map(function(u){return zu(u.name)}),a={},i=0,s=t;i<s.length;i++)s[i].paths.forEach(function(u){var c=zu(u);if(n.indexOf(c)!==-1)throw new Error("Duplicate file basename found in weights manifest: '"+c+"'");if(n.push(c),o.indexOf(c)===-1)throw new Error("Weight file with basename '"+c+"' is not provided.");a[u]=e[o.indexOf(c)]});if(n.length!==e.length)throw new Error("Mismatch in the number of files in weights manifest ("+n.length+") and the number of weight files provided ("+e.length+").");return a},r}();function ju(r,t,e,n){(function(a){R(a!=null&&Array.isArray(a)&&a.length>0,function(){return"promises must be a none empty array"})})(r),function(a,i){R(a>=0&&a<=1,function(){return"Progress fraction must be in range [0, 1], but got startFraction "+a}),R(i>=0&&i<=1,function(){return"Progress fraction must be in range [0, 1], but got endFraction "+i}),R(i>=a,function(){return"startFraction must be no more than endFraction, but got startFraction "+a+" and endFraction "+i})}(e=e==null?0:e,n=n==null?1:n);var o=0;return Promise.all(r.map(function(a){return a.then(function(i){var s=e+ ++o/r.length*(n-e);return t(s),i}),a}))}function cf(r,t){return Q(this,void 0,void 0,function(){var e,n,o,a,i,s,u,c,l;return Z(this,function(h){switch(h.label){case 0:return t==null&&(t={}),e=t.fetchFunc==null?V().platform.fetch:t.fetchFunc,n=r.map(function(f){return e(f,t.requestInit,{isBinary:!0})}),o=0,a=.5,t.onProgress!=null?[3,2]:[4,Promise.all(n)];case 1:return i=h.sent(),[3,4];case 2:return[4,ju(n,t.onProgress,o,a)];case 3:i=h.sent(),h.label=4;case 4:return s=i.map(function(f){return f.arrayBuffer()}),u=.5,c=1,t.onProgress!=null?[3,6]:[4,Promise.all(s)];case 5:return l=h.sent(),[3,8];case 6:return[4,ju(s,t.onProgress,u,c)];case 7:l=h.sent(),h.label=8;case 8:return[2,l]}})})}function Ku(r){var t=this;return function(e,n,o){return n===void 0&&(n=""),Q(t,void 0,void 0,function(){var a,i,s,u,c,l,h,f,d,p;return Z(this,function(v){switch(v.label){case 0:if(a=e.map(function(){return!1}),i={},s=o!=null?o.map(function(){return!1}):[],u=[],e.forEach(function(m,g){var x=0;m.weights.forEach(function(y){var b="quantization"in y?y.quantization.dtype:y.dtype,w=_i[b]*ee(y.shape),C=function(){a[g]=!0,i[g]==null&&(i[g]=[]),i[g].push({manifestEntry:y,groupOffset:x,sizeBytes:w})};o!=null?o.forEach(function(I,E){I===y.name&&(C(),s[E]=!0)}):C(),u.push(y.name),x+=w})}),!s.every(function(m){return m}))throw c=o.filter(function(m,g){return!s[g]}),new Error("Could not find weights in manifest with names: "+c.join(", ")+`. 
Manifest JSON has weights with names: `+u.join(", ")+".");return l=a.reduce(function(m,g,x){return g&&m.push(x),m},[]),h=[],l.forEach(function(m){e[m].paths.forEach(function(g){var x=n+(n.endsWith("/")?"":"/")+g;h.push(x)})}),[4,r(h)];case 1:return f=v.sent(),d={},p=0,l.forEach(function(m){for(var g=e[m].paths.length,x=0,y=0;y<g;y++)x+=f[p+y].byteLength;for(var b=new ArrayBuffer(x),w=new Uint8Array(b),C=0,I=0;I<g;I++){var E=new Uint8Array(f[p+I]);w.set(E,C),C+=E.byteLength}i[m].forEach(function(S){var k=of(b.slice(S.groupOffset,S.groupOffset+S.sizeBytes),[S.manifestEntry]);for(var T in k)d[T]=k[T]}),p+=g}),[2,d]}})})}}yt.registerSaveRouter(function(r){return V().getBool("IS_BROWSER")&&!Array.isArray(r)&&r.startsWith(Ka.URL_SCHEME)?function(t){return t===void 0&&(t="model"),new Ka(t)}(r.slice(Ka.URL_SCHEME.length)):null});var lf=function(){function r(t,e){if(this.DEFAULT_METHOD="POST",e==null&&(e={}),this.weightPathPrefix=e.weightPathPrefix,this.onProgress=e.onProgress,e.fetchFunc!=null?(R(typeof e.fetchFunc=="function",function(){return"Must pass a function that matches the signature of `fetch` (see https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)"}),this.fetch=e.fetchFunc):this.fetch=V().platform.fetch,R(t!=null&&t.length>0,function(){return"URL path for http must not be null, undefined or empty."}),Array.isArray(t)&&R(t.length===2,function(){return"URL paths for http must have a length of 2, (actual length is "+t.length+")."}),this.path=t,e.requestInit!=null&&e.requestInit.body!=null)throw new Error("requestInit is expected to have no pre-existing body, but has one.");this.requestInit=e.requestInit||{}}return r.prototype.save=function(t){return Q(this,void 0,void 0,function(){var e,n,o,a;return Z(this,function(i){switch(i.label){case 0:if(t.modelTopology instanceof ArrayBuffer)throw new Error("BrowserHTTPRequest.save() does not support saving model topology in binary formats yet.");return(e=Object.assign({method:this.DEFAULT_METHOD},this.requestInit)).body=new FormData,n=[{paths:["./model.weights.bin"],weights:t.weightSpecs}],o={modelTopology:t.modelTopology,format:t.format,generatedBy:t.generatedBy,convertedBy:t.convertedBy,userDefinedMetadata:t.userDefinedMetadata,weightsManifest:n},e.body.append("model.json",new Blob([JSON.stringify(o)],{type:"application/json"}),"model.json"),t.weightData!=null&&e.body.append("model.weights.bin",new Blob([t.weightData],{type:"application/octet-stream"}),"model.weights.bin"),[4,this.fetch(this.path,e)];case 1:if((a=i.sent()).ok)return[2,{modelArtifactsInfo:eo(t),responses:[a]}];throw new Error("BrowserHTTPRequest.save() failed due to HTTP response status "+a.status+".")}})})},r.prototype.load=function(){return Q(this,void 0,void 0,function(){var t,e,n,o,a,i,s,u,c,l,h,f;return Z(this,function(d){switch(d.label){case 0:return[4,this.fetch(this.path,this.requestInit)];case 1:if(!(t=d.sent()).ok)throw new Error("Request to "+this.path+" failed with status code "+t.status+". Please verify this URL points to the model JSON of the model to load.");d.label=2;case 2:return d.trys.push([2,4,,5]),[4,t.json()];case 3:return e=d.sent(),[3,5];case 4:throw d.sent(),n="Failed to parse model JSON of response from "+this.path+".",this.path.endsWith(".pb")?n+=" Your path contains a .pb file extension. Support for .pb models have been removed in TensorFlow.js 1.0 in favor of .json models. You can re-convert your Python TensorFlow model using the TensorFlow.js 1.0 conversion scripts or you can convert your.pb models with the 'pb2json'NPM script in the tensorflow/tfjs-converter repository.":n+=" Please make sure the server is serving valid JSON for this request.",new Error(n);case 5:if(o=e.modelTopology,a=e.weightsManifest,i=e.generatedBy,s=e.convertedBy,u=e.format,c=e.userDefinedMetadata,o==null&&a==null)throw new Error("The JSON from HTTP path "+this.path+" contains neither model topology or manifest for weights.");return a==null?[3,7]:[4,this.loadWeights(a)];case 6:f=d.sent(),l=f[0],h=f[1],d.label=7;case 7:return[2,{modelTopology:o,weightSpecs:l,weightData:h,userDefinedMetadata:c,generatedBy:i,convertedBy:s,format:u}]}})})},r.prototype.loadWeights=function(t){return Q(this,void 0,void 0,function(){var e,n,o,a,i,s,u,c,l,h,f;return Z(this,function(d){switch(d.label){case 0:for(e=Array.isArray(this.path)?this.path[1]:this.path,n=function(p){var v=p.lastIndexOf("/"),m=p.lastIndexOf("?"),g=p.substring(0,v),x=m>v?p.substring(m):"";return[g+"/",x]}(e),o=n[0],a=n[1],i=this.weightPathPrefix||o,s=[],u=0,c=t;u<c.length;u++)l=c[u],s.push.apply(s,l.weights);return h=[],t.forEach(function(p){p.paths.forEach(function(v){h.push(i+v+a)})}),[4,cf(h,{requestInit:this.requestInit,fetchFunc:this.fetch,onProgress:this.onProgress})];case 1:return f=d.sent(),[2,[s,Is(f)]]}})})},r.URL_SCHEME_REGEX=/^https?:\/\//,r}();function ki(r){return r.match(lf.URL_SCHEME_REGEX)!=null}var Xu=function(r,t){return typeof fetch>"u"?null:(Array.isArray(r)?r.every(function(e){return ki(e)}):ki(r))?Ri(r,{onProgress:t}):null};function Ri(r,t){return new lf(r,t)}yt.registerSaveRouter(Xu),yt.registerLoadRouter(Xu);var Xa=function(){function r(t){this.modelArtifacts=t}return r.prototype.load=function(){return Q(this,void 0,void 0,function(){return Z(this,function(t){return[2,this.modelArtifacts]})})},r}(),B0=function(){function r(t){this.saveHandler=t}return r.prototype.save=function(t){return Q(this,void 0,void 0,function(){return Z(this,function(e){return[2,this.saveHandler(t)]})})},r}(),hf=Object.freeze({browserFiles:function(r){return new O0(r)},browserHTTPRequest:function(r,t){return Ri(r,t)},concatenateArrayBuffers:Is,decodeWeights:of,encodeWeights:function(r,t){return Q(this,void 0,void 0,function(){var e,n,o,a,i,s=this;return Z(this,function(u){switch(u.label){case 0:for(e=[],n=[],o=Array.isArray(r)?r.map(function(c){return c.name}):Object.keys(r),a=function(c){var l=o[c],h=Array.isArray(r)?r[c].tensor:r[l];if(h.dtype!=="float32"&&h.dtype!=="int32"&&h.dtype!=="bool"&&h.dtype!=="string")throw new Error("Unsupported dtype in weight '"+l+"': "+h.dtype);var f={name:l,shape:h.shape,dtype:h.dtype};if(h.dtype==="string"){var d=new Promise(function(p){return Q(s,void 0,void 0,function(){var v,m,g,x,y,b,w;return Z(this,function(C){switch(C.label){case 0:return[4,h.bytes()];case 1:for(v=C.sent(),m=v.reduce(function(I,E){return I+E.length},0)+Ko*v.length,g=new Uint8Array(m),x=0,y=0;y<v.length;y++)b=v[y],w=new Uint8Array(new Uint32Array([b.length]).buffer),g.set(w,x),x+=Ko,g.set(b,x),x+=b.length;return p(g),[2]}})})});n.push(d)}else n.push(h.data());t!=null&&(f.group=t),e.push(f)},i=0;i<o.length;++i)a(i);return[4,Promise.all(n)];case 1:return[2,{data:S0(u.sent()),specs:e}]}})})},fromMemory:function(r,t,e,n){return arguments.length===1?r.modelTopology!=null||r.weightSpecs!=null?new Xa(r):(console.warn("Please call tf.io.fromMemory() with only one argument. The argument should be of type ModelArtifacts. The multi-argument signature of tf.io.fromMemory() has been deprecated and will be removed in a future release."),new Xa({modelTopology:r})):(console.warn("Please call tf.io.fromMemory() with only one argument. The argument should be of type ModelArtifacts. The multi-argument signature of tf.io.fromMemory() has been deprecated and will be removed in a future release."),new Xa({modelTopology:r,weightSpecs:t,weightData:e,trainingConfig:n}))},getLoadHandlers:function(r,t){return yt.getLoadHandlers(r,t)},getModelArtifactsInfoForJSON:eo,getSaveHandlers:function(r){return yt.getSaveHandlers(r)},http:Ri,isHTTPScheme:ki,loadWeights:function(r,t,e,n){return t===void 0&&(t=""),Q(this,void 0,void 0,function(){return Z(this,function(o){return[2,Ku(function(a){return cf(a,{requestInit:n})})(r,t,e)]})})},registerLoadRouter:function(r){return yt.registerLoadRouter(r)},registerSaveRouter:function(r){return yt.registerSaveRouter(r)},weightsLoaderFactory:Ku,withSaveHandler:function(r){return new B0(r)},copyModel:function(r,t){return Q(this,void 0,void 0,function(){return Z(this,function(e){return[2,Uu(r,t,!1)]})})},listModels:function(){return Q(this,void 0,void 0,function(){var r,t,e,n,o,a,i;return Z(this,function(s){switch(s.label){case 0:r=wn.getSchemes(),t={},e=0,n=r,s.label=1;case 1:return e<n.length?(o=n[e],[4,wn.getManager(o).listModels()]):[3,4];case 2:for(i in a=s.sent())t[o+ur+i]=a[i];s.label=3;case 3:return e++,[3,1];case 4:return[2,t]}})})},moveModel:function(r,t){return Q(this,void 0,void 0,function(){return Z(this,function(e){return[2,Uu(r,t,!0)]})})},removeModel:function(r){return Q(this,void 0,void 0,function(){var t;return Z(this,function(e){return t=ko(r),[2,wn.getManager(t.scheme).removeModel(t.path)]})})}}),Zn,L0=D({confusionMatrix_:function(r,t,e){var n=_(r,"labels","confusionMatrix"),o=_(t,"predictions","confusionMatrix");R(e==null||e>0&&Number.isInteger(e),function(){return"If provided, numClasses must be a positive integer, but got "+e}),R(n.rank===1,function(){return"Expected the rank of labels to be 1, but got "+n.rank}),R(o.rank===1,function(){return"Expected the rank of predictions to be 1, but got "+o.rank}),R(n.shape[0]===o.shape[0],function(){return"Mismatch in the number of examples: "+n.shape[0]+" vs. "+o.shape[0]+". Labels and predictions should have the same number of elements."}),R(e>0&&Number.isInteger(e),function(){return"numClasses is required to be a positive integer, but got "+e});var a=yi(n.asType("int32"),e),i=yi(o.asType("int32"),e);return a.transpose().matMul(i).asType("int32")}});Object.freeze({confusionMatrix:L0});var W0=D({fromPixels_:function(r,t){if(t===void 0&&(t=3),t>4)throw new Error("Cannot construct Tensor with more than 4 channels from pixels.");if(r==null)throw new Error("pixels passed to tf.browser.fromPixels() can not be null");var e=!1,n=!1,o=!1,a=!1,i=!1;if(r.data instanceof Uint8Array)e=!0;else if(typeof ImageData<"u"&&r instanceof ImageData)n=!0;else if(typeof HTMLVideoElement<"u"&&r instanceof HTMLVideoElement)o=!0;else if(typeof HTMLImageElement<"u"&&r instanceof HTMLImageElement)a=!0;else{if(r.getContext==null)throw new Error("pixels passed to tf.browser.fromPixels() must be either an HTMLVideoElement, HTMLImageElement, HTMLCanvasElement, ImageData in browser, or OffscreenCanvas, ImageData in webworker or {data: Uint32Array, width: number, height: number}, but was "+r.constructor.name);i=!0}if(o&&o&&r.readyState<2)throw new Error("The video element has not loaded data yet. Please wait for `loadeddata` event on the <video> element.");if(Vc("FromPixels",N.backendName)!=null)return N.runKernel("FromPixels",{pixels:r},{numChannels:t});var s,u,c=o?[r.videoWidth,r.videoHeight]:[r.width,r.height],l=c[0],h=c[1];if(i?s=r.getContext("2d").getImageData(0,0,l,h).data:n||e?s=r.data:(a||o)&&(Zn==null&&(Zn=document.createElement("canvas").getContext("2d")),Zn.canvas.width=l,Zn.canvas.height=h,Zn.drawImage(r,0,0,l,h),s=Zn.getImageData(0,0,l,h).data),t===4)u=new Int32Array(s);else{var f=l*h;u=new Int32Array(f*t);for(var d=0;d<f;d++)for(var p=0;p<t;++p)u[d*t+p]=s[4*d+p]}return Qi(u,[h,l,t],"int32")}}),As=Object.freeze({toPixels:function(r,t){return Q(this,void 0,void 0,function(){var e,n,o,a,i,s,u,c,l,h,f,d,p,v,m,g,x,y,b,w,C,I,E;return Z(this,function(S){switch(S.label){case 0:if(e=_(r,"img","toPixels"),r instanceof Te||(e=e.toInt()),e.rank!==2&&e.rank!==3)throw new Error("toPixels only supports rank 2 or 3 tensors, got rank "+e.rank+".");if(n=e.shape.slice(0,2),o=n[0],a=n[1],(i=e.rank===2?1:e.shape[2])>4||i===2)throw new Error("toPixels only supports depth of size 1, 3 or 4 but got "+i);return[4,e.data()];case 1:return s=S.sent(),u=e.min(),c=e.max(),[4,Promise.all([u.data(),c.data()])];case 2:if(l=S.sent(),h=l[0],f=l[1],d=h[0],p=f[0],u.dispose(),c.dispose(),e.dtype==="float32"){if(d<0||p>1)throw new Error("Tensor values for a float32 Tensor must be in the range [0 - 1] but got range ["+d+" - "+p+"].")}else{if(e.dtype!=="int32")throw new Error("Unsupported type for toPixels: "+e.dtype+". Please use float32 or int32 tensors.");if(d<0||p>255)throw new Error("Tensor values for a int32 Tensor must be in the range [0 - 255] but got range ["+d+" - "+p+"].")}for(v=e.dtype==="float32"?255:1,m=new Uint8ClampedArray(a*o*4),g=0;g<o*a;++g)x=void 0,y=void 0,b=void 0,w=void 0,i===1?(x=s[g]*v,y=s[g]*v,b=s[g]*v,w=255):i===3?(x=s[3*g]*v,y=s[3*g+1]*v,b=s[3*g+2]*v,w=255):i===4&&(x=s[4*g]*v,y=s[4*g+1]*v,b=s[4*g+2]*v,w=s[4*g+3]*v),m[(C=4*g)+0]=Math.round(x),m[C+1]=Math.round(y),m[C+2]=Math.round(b),m[C+3]=Math.round(w);return t!=null&&(t.width=a,t.height=o,I=t.getContext("2d"),E=new ImageData(m,a,o),I.putImageData(E,0,0)),e!==r&&e.dispose(),[2,m]}})})},fromPixels:W0}),ff=function(){function r(){}return r.prototype.getClassName=function(){return this.constructor.className},r.fromConfig=function(t,e){return new t(e)},r}(),df=function(){function r(){this.classNameMap={}}return r.getMap=function(){return r.instance==null&&(r.instance=new r),r.instance},r.register=function(t){r.getMap().classNameMap[t.className]=[t,t.fromConfig]},r}();function An(r){R(r.className!=null,function(){return"Class being registered does not have the static className property defined."}),R(typeof r.className=="string",function(){return"className is required to be a string, but got type "+typeof r.className}),R(r.className.length>0,function(){return"Class being registered has an empty-string as its className, which is disallowed."}),df.register(r)}Object.freeze({Serializable:ff,SerializationMap:df,registerClass:An});var V0=.001,pf=.1;function $a(){return N.backend.floatPrecision()===32?V0:pf}function Ya(r,t,e){var n=!0;if((Ye(r)||Ye(t))&&(n=!1),Ye(r)&&Ye(t)&&(n=!0),n){var o=r.constructor.name,a=t.constructor.name;if(o!==a)throw new Error("Arrays are of different type. Actual: "+o+". Expected: "+a)}if(Array.isArray(r)&&Array.isArray(t)){var i=Yt(r),s=Yt(t);if(!Oe(i,s))throw new Error("Arrays have different shapes. Actual: ["+i+"]. Expected: ["+s+"]")}var u=Ye(r)?r:sn(r),c=Ye(t)?t:sn(t);if(u.length!==c.length)throw new Error("Arrays have different lengths actual: "+u.length+" vs expected: "+c.length+`.
Actual:   `+u+`.
Expected: `+c+".");for(var l=0;l<c.length;++l){var h=u[l],f=c[l];if(!e(h,f))throw new Error("Arrays differ: actual["+l+"] = "+h+", expected["+l+"] = "+f+`.
Actual:   `+u+`.
Expected: `+c+".")}}function Ja(r,t,e){return!isFinite(r)&&!isFinite(t)||!(isNaN(r)||isNaN(t)||Math.abs(r-t)>e)}Object.freeze({TEST_EPSILON_FLOAT16:pf,expectArraysClose:function(r,t,e){return e==null&&(e=$a()),Ya(r,t,function(n,o){return Ja(n,o,e)})},testEpsilon:$a,expectPromiseToFail:function(r,t){r().then(function(){return t.fail()},function(){return t()})},expectArraysEqual:function(r,t){var e=typeof t=="string"||typeof t=="number"||typeof t=="boolean"?[t]:t;return yn(r)||yn(r[0])||yn(t)||yn(t[0])?Ya(r,e,function(n,o){return n==o}):Ya(r,t,function(n,o){return Ja(n,o,0)})},expectNumbersClose:function(r,t,e){if(e==null&&(e=$a()),!Ja(r,t,e))throw new Error("Numbers differ: actual === "+r+", expected === "+t)},expectValuesInRange:function(r,t,e){for(var n=0;n<r.length;n++)if(r[n]<t||r[n]>e)throw new Error("Value out of range:"+r[n]+" low: "+t+", high: "+e)},expectArrayBuffersEqual:function(r,t){expect(new Float32Array(r)).toEqual(new Float32Array(t))}});Object.freeze({gpgpu_util:cm,webgl_util:Fp,forceHalfFloat:function(){V().set("WEBGL_FORCE_F16_TEXTURES",!0)},MathBackendWebGL:Sh,setWebGLContext:ol,GPGPUContext:yh});var jn=function(r){function t(){return r!==null&&r.apply(this,arguments)||this}return Mt(t,r),t.prototype.minimize=function(e,n,o){n===void 0&&(n=!1);var a=this.computeGradients(e,o),i=a.value,s=a.grads;if(o!=null){var u=o.map(function(c){return{name:c.name,tensor:s[c.name]}});this.applyGradients(u)}else this.applyGradients(s);return ft(s),n?i:(i.dispose(),null)},Object.defineProperty(t.prototype,"iterations",{get:function(){return this.iterations_==null&&(this.iterations_=0),this.iterations_},enumerable:!0,configurable:!0}),t.prototype.incrementIterations=function(){this.iterations_=this.iterations+1},t.prototype.computeGradients=function(e,n){return dv(e,n)},t.prototype.dispose=function(){this.iterations_!=null&&ft(this.iterations_)},t.prototype.saveIterations=function(){return Q(this,void 0,void 0,function(){return Z(this,function(e){return this.iterations_==null&&(this.iterations_=0),[2,{name:"iter",tensor:K(this.iterations_,"int32")}]})})},t.prototype.getWeights=function(){return Q(this,void 0,void 0,function(){return Z(this,function(e){throw new Error("getWeights() is not implemented for this optimizer yet.")})})},t.prototype.setWeights=function(e){return Q(this,void 0,void 0,function(){return Z(this,function(n){throw new Error("setWeights() is not implemented for this optimizer class "+this.getClassName())})})},t.prototype.extractIterations=function(e){return Q(this,void 0,void 0,function(){var n;return Z(this,function(o){switch(o.label){case 0:return n=this,[4,e[0].tensor.data()];case 1:return n.iterations_=o.sent()[0],[2,e.slice(1)]}})})},t}(ff);Object.defineProperty(jn,Symbol.hasInstance,{value:function(r){return r.minimize!=null&&r.computeGradients!=null&&r.applyGradients!=null}});var z0=function(r){function t(e,n,o){o===void 0&&(o=null);var a=r.call(this)||this;return a.learningRate=e,a.rho=n,a.epsilon=o,a.accumulatedGrads=[],a.accumulatedUpdates=[],o==null&&(a.epsilon=N.backend.epsilon()),a}return Mt(t,r),t.prototype.applyGradients=function(e){var n=this;(Array.isArray(e)?e.map(function(o){return o.name}):Object.keys(e)).forEach(function(o,a){var i=N.registeredVariables[o];n.accumulatedGrads[a]==null&&(n.accumulatedGrads[a]={originalName:o+"/accum_grad",variable:Y(function(){return ye(i).variable(!1)})}),n.accumulatedUpdates[a]==null&&(n.accumulatedUpdates[a]={originalName:o+"/accum_var",variable:Y(function(){return ye(i).variable(!1)})});var s=Array.isArray(e)?e[a].tensor:e[o];if(s!=null){var u=n.accumulatedGrads[a].variable,c=n.accumulatedUpdates[a].variable;Y(function(){var l=u.mul(n.rho).add(s.square().mul(1-n.rho)),h=c.add(n.epsilon).sqrt().div(u.add(n.epsilon).sqrt()).mul(s),f=c.mul(n.rho).add(h.square().mul(1-n.rho));u.assign(l),c.assign(f);var d=h.mul(-n.learningRate).add(i);i.assign(d)})}}),this.incrementIterations()},t.prototype.dispose=function(){this.accumulatedUpdates!=null&&(ft(this.accumulatedGrads.map(function(e){return e.variable})),ft(this.accumulatedUpdates.map(function(e){return e.variable})))},t.prototype.getWeights=function(){return Q(this,void 0,void 0,function(){var e;return Z(this,function(n){switch(n.label){case 0:return e=this.accumulatedGrads.concat(this.accumulatedUpdates),[4,this.saveIterations()];case 1:return[2,[n.sent()].concat(e.map(function(o){return{name:o.originalName,tensor:o.variable}}))]}})})},t.prototype.setWeights=function(e){return Q(this,void 0,void 0,function(){var n;return Z(this,function(o){switch(o.label){case 0:return[4,this.extractIterations(e)];case 1:return e=o.sent(),n=e.length/2,this.accumulatedGrads=e.slice(0,n).map(function(a){return{originalName:a.name,variable:a.tensor.variable(!1)}}),this.accumulatedUpdates=e.slice(n,2*n).map(function(a){return{originalName:a.name,variable:a.tensor.variable(!1)}}),[2]}})})},t.prototype.getConfig=function(){return{learningRate:this.learningRate,rho:this.rho,epsilon:this.epsilon}},t.fromConfig=function(e,n){return new e(n.learningRate,n.rho,n.epsilon)},t.className="Adadelta",t}(jn);An(z0);var U0=function(r){function t(e,n){n===void 0&&(n=.1);var o=r.call(this)||this;return o.learningRate=e,o.initialAccumulatorValue=n,o.accumulatedGrads=[],o}return Mt(t,r),t.prototype.applyGradients=function(e){var n=this;(Array.isArray(e)?e.map(function(o){return o.name}):Object.keys(e)).forEach(function(o,a){var i=N.registeredVariables[o];n.accumulatedGrads[a]==null&&(n.accumulatedGrads[a]={originalName:o+"/accumulator",variable:Y(function(){return Xt(i.shape,n.initialAccumulatorValue).variable(!1)})});var s=Array.isArray(e)?e[a].tensor:e[o];if(s!=null){var u=n.accumulatedGrads[a].variable;Y(function(){var c=u.add(s.square());u.assign(c);var l=s.div(c.add(N.backend.epsilon()).sqrt()).mul(-n.learningRate).add(i);i.assign(l)})}}),this.incrementIterations()},t.prototype.dispose=function(){this.accumulatedGrads!=null&&ft(this.accumulatedGrads.map(function(e){return e.variable}))},t.prototype.getWeights=function(){return Q(this,void 0,void 0,function(){return Z(this,function(e){switch(e.label){case 0:return[4,this.saveIterations()];case 1:return[2,[e.sent()].concat(this.accumulatedGrads.map(function(n){return{name:n.originalName,tensor:n.variable}}))]}})})},t.prototype.setWeights=function(e){return Q(this,void 0,void 0,function(){return Z(this,function(n){switch(n.label){case 0:return[4,this.extractIterations(e)];case 1:return e=n.sent(),this.accumulatedGrads=e.map(function(o){return{originalName:o.name,variable:o.tensor.variable(!1)}}),[2]}})})},t.prototype.getConfig=function(){return{learningRate:this.learningRate,initialAccumulatorValue:this.initialAccumulatorValue}},t.fromConfig=function(e,n){return new e(n.learningRate,n.initialAccumulatorValue)},t.className="Adagrad",t}(jn);An(U0);var G0=function(r){function t(e,n,o,a){a===void 0&&(a=null);var i=r.call(this)||this;return i.learningRate=e,i.beta1=n,i.beta2=o,i.epsilon=a,i.accumulatedFirstMoment=[],i.accumulatedSecondMoment=[],Y(function(){i.accBeta1=K(n).variable(),i.accBeta2=K(o).variable()}),a==null&&(i.epsilon=N.backend.epsilon()),i}return Mt(t,r),t.prototype.applyGradients=function(e){var n=this,o=Array.isArray(e)?e.map(function(a){return a.name}):Object.keys(e);Y(function(){var a=Ue(1,n.accBeta1),i=Ue(1,n.accBeta2);o.forEach(function(s,u){var c=N.registeredVariables[s];n.accumulatedFirstMoment[u]==null&&(n.accumulatedFirstMoment[u]={originalName:s+"/m",variable:Y(function(){return ye(c).variable(!1)})}),n.accumulatedSecondMoment[u]==null&&(n.accumulatedSecondMoment[u]={originalName:s+"/v",variable:Y(function(){return ye(c).variable(!1)})});var l=Array.isArray(e)?e[u].tensor:e[s];if(l!=null){var h=n.accumulatedFirstMoment[u].variable,f=n.accumulatedSecondMoment[u].variable,d=h.mul(n.beta1).add(l.mul(1-n.beta1)),p=f.mul(n.beta2).add(l.square().mul(1-n.beta2)),v=d.div(a),m=p.div(i);h.assign(d),f.assign(p);var g=v.div(m.sqrt().add(n.epsilon)).mul(-n.learningRate).add(c);c.assign(g)}}),n.accBeta1.assign(n.accBeta1.mul(n.beta1)),n.accBeta2.assign(n.accBeta2.mul(n.beta2))}),this.incrementIterations()},t.prototype.dispose=function(){this.accBeta1.dispose(),this.accBeta2.dispose(),this.accumulatedFirstMoment!=null&&ft(this.accumulatedFirstMoment.map(function(e){return e.variable})),this.accumulatedSecondMoment!=null&&ft(this.accumulatedSecondMoment.map(function(e){return e.variable}))},t.prototype.getWeights=function(){return Q(this,void 0,void 0,function(){var e;return Z(this,function(n){switch(n.label){case 0:return e=this.accumulatedFirstMoment.concat(this.accumulatedSecondMoment),[4,this.saveIterations()];case 1:return[2,[n.sent()].concat(e.map(function(o){return{name:o.originalName,tensor:o.variable}}))]}})})},t.prototype.setWeights=function(e){return Q(this,void 0,void 0,function(){var n,o=this;return Z(this,function(a){switch(a.label){case 0:return[4,this.extractIterations(e)];case 1:return e=a.sent(),Y(function(){o.accBeta1.assign(qo(o.beta1,o.iterations_+1)),o.accBeta2.assign(qo(o.beta2,o.iterations_+1))}),n=e.length/2,this.accumulatedFirstMoment=e.slice(0,n).map(function(i){return{originalName:i.name,variable:i.tensor.variable(!1)}}),this.accumulatedSecondMoment=e.slice(n,2*n).map(function(i){return{originalName:i.name,variable:i.tensor.variable(!1)}}),[2]}})})},t.prototype.getConfig=function(){return{learningRate:this.learningRate,beta1:this.beta1,beta2:this.beta2,epsilon:this.epsilon}},t.fromConfig=function(e,n){return new e(n.learningRate,n.beta1,n.beta2,n.epsilon)},t.className="Adam",t}(jn);An(G0);var H0=function(r){function t(e,n,o,a,i){a===void 0&&(a=null),i===void 0&&(i=0);var s=r.call(this)||this;return s.learningRate=e,s.beta1=n,s.beta2=o,s.epsilon=a,s.decay=i,s.accumulatedFirstMoment=[],s.accumulatedWeightedInfNorm=[],Y(function(){s.iteration=K(0).variable(),s.accBeta1=K(n).variable()}),a==null&&(s.epsilon=N.backend.epsilon()),s}return Mt(t,r),t.prototype.applyGradients=function(e){var n=this,o=Array.isArray(e)?e.map(function(a){return a.name}):Object.keys(e);Y(function(){var a=Ue(1,n.accBeta1),i=Tt(-n.learningRate,n.iteration.mul(n.decay).add(1));o.forEach(function(s,u){var c=N.registeredVariables[s];n.accumulatedFirstMoment[u]==null&&(n.accumulatedFirstMoment[u]={originalName:s+"/m",variable:ye(c).variable(!1)}),n.accumulatedWeightedInfNorm[u]==null&&(n.accumulatedWeightedInfNorm[u]={originalName:s+"/v",variable:ye(c).variable(!1)});var l=Array.isArray(e)?e[u].tensor:e[s];if(l!=null){var h=n.accumulatedFirstMoment[u].variable,f=n.accumulatedWeightedInfNorm[u].variable,d=h.mul(n.beta1).add(l.mul(1-n.beta1)),p=f.mul(n.beta2),v=l.abs(),m=p.maximum(v);h.assign(d),f.assign(m);var g=i.div(a).mul(d.div(m.add(n.epsilon))).add(c);c.assign(g)}}),n.iteration.assign(n.iteration.add(1)),n.accBeta1.assign(n.accBeta1.mul(n.beta1))}),this.incrementIterations()},t.prototype.dispose=function(){this.accBeta1.dispose(),this.iteration.dispose(),this.accumulatedFirstMoment!=null&&ft(this.accumulatedFirstMoment.map(function(e){return e.variable})),this.accumulatedWeightedInfNorm!=null&&ft(this.accumulatedWeightedInfNorm.map(function(e){return e.variable}))},t.prototype.getWeights=function(){return Q(this,void 0,void 0,function(){return Z(this,function(e){throw new Error("getWeights() is not implemented for Adamax yet.")})})},t.prototype.setWeights=function(e){return Q(this,void 0,void 0,function(){return Z(this,function(n){throw new Error("setWeights() is not implemented for Adamax yet.")})})},t.prototype.getConfig=function(){return{learningRate:this.learningRate,beta1:this.beta1,beta2:this.beta2,epsilon:this.epsilon,decay:this.decay}},t.fromConfig=function(e,n){return new e(n.learningRate,n.beta1,n.beta2,n.epsilon,n.decay)},t.className="Adamax",t}(jn);An(H0);var vf=function(r){function t(e){var n=r.call(this)||this;return n.learningRate=e,n.setLearningRate(e),n}return Mt(t,r),t.prototype.applyGradients=function(e){var n=this;(Array.isArray(e)?e.map(function(o){return o.name}):Object.keys(e)).forEach(function(o,a){var i=Array.isArray(e)?e[a].tensor:e[o];if(i!=null){var s=N.registeredVariables[o];Y(function(){var u=n.c.mul(i).add(s);s.assign(u)})}}),this.incrementIterations()},t.prototype.setLearningRate=function(e){this.learningRate=e,this.c!=null&&this.c.dispose(),this.c=Pp(K(-e))},t.prototype.dispose=function(){this.c.dispose()},t.prototype.getWeights=function(){return Q(this,void 0,void 0,function(){return Z(this,function(e){switch(e.label){case 0:return[4,this.saveIterations()];case 1:return[2,[e.sent()]]}})})},t.prototype.setWeights=function(e){return Q(this,void 0,void 0,function(){return Z(this,function(n){switch(n.label){case 0:return[4,this.extractIterations(e)];case 1:if((e=n.sent()).length!==0)throw new Error("SGD optimizer does not have settable weights.");return[2]}})})},t.prototype.getConfig=function(){return{learningRate:this.learningRate}},t.fromConfig=function(e,n){return new e(n.learningRate)},t.className="SGD",t}(jn);An(vf);var q0=function(r){function t(e,n,o){o===void 0&&(o=!1);var a=r.call(this,e)||this;return a.learningRate=e,a.momentum=n,a.useNesterov=o,a.accumulations=[],a.m=K(a.momentum),a}return Mt(t,r),t.prototype.applyGradients=function(e){var n=this;(Array.isArray(e)?e.map(function(o){return o.name}):Object.keys(e)).forEach(function(o,a){var i=N.registeredVariables[o];n.accumulations[a]==null&&(n.accumulations[a]={originalName:o+"/momentum",variable:Y(function(){return ye(i).variable(!1)})});var s=n.accumulations[a].variable,u=Array.isArray(e)?e[a].tensor:e[o];u!=null&&Y(function(){var c,l=n.m.mul(s).add(u);c=n.useNesterov?n.c.mul(u.add(l.mul(n.m))).add(i):n.c.mul(l).add(i),s.assign(l),i.assign(c)})}),this.incrementIterations()},t.prototype.dispose=function(){this.m.dispose(),this.accumulations!=null&&ft(this.accumulations.map(function(e){return e.variable}))},t.prototype.setMomentum=function(e){this.momentum=e},t.prototype.getWeights=function(){return Q(this,void 0,void 0,function(){return Z(this,function(e){switch(e.label){case 0:return[4,this.saveIterations()];case 1:return[2,[e.sent()].concat(this.accumulations.map(function(n){return{name:n.originalName,tensor:n.variable}}))]}})})},t.prototype.setWeights=function(e){return Q(this,void 0,void 0,function(){return Z(this,function(n){switch(n.label){case 0:return[4,this.extractIterations(e)];case 1:return e=n.sent(),this.accumulations=e.map(function(o){return{originalName:o.name,variable:o.tensor.variable(!1)}}),[2]}})})},t.prototype.getConfig=function(){return{learningRate:this.learningRate,momentum:this.momentum,useNesterov:this.useNesterov}},t.fromConfig=function(e,n){return new e(n.learningRate,n.momentum,n.useNesterov)},t.className="Momentum",t}(vf);An(q0);var j0=function(r){function t(e,n,o,a,i){n===void 0&&(n=.9),o===void 0&&(o=0),a===void 0&&(a=null),i===void 0&&(i=!1);var s=r.call(this)||this;if(s.learningRate=e,s.decay=n,s.momentum=o,s.epsilon=a,s.accumulatedMeanSquares=[],s.accumulatedMoments=[],s.accumulatedMeanGrads=[],s.centered=i,a==null&&(s.epsilon=N.backend.epsilon()),e==null)throw new Error("learningRate for RMSPropOptimizer must be defined.");return s}return Mt(t,r),t.prototype.applyGradients=function(e){var n=this;(Array.isArray(e)?e.map(function(o){return o.name}):Object.keys(e)).forEach(function(o,a){var i=N.registeredVariables[o];n.accumulatedMeanSquares[a]==null&&(n.accumulatedMeanSquares[a]={originalName:o+"/rms",variable:Y(function(){return ye(i).variable(!1)})}),n.accumulatedMoments[a]==null&&(n.accumulatedMoments[a]={originalName:o+"/momentum",variable:Y(function(){return ye(i).variable(!1)})}),n.accumulatedMeanGrads[a]==null&&n.centered&&(n.accumulatedMeanGrads[a]={originalName:o+"/mg",variable:Y(function(){return ye(i).variable(!1)})});var s=Array.isArray(e)?e[a].tensor:e[o];if(s!=null){var u=n.accumulatedMeanSquares[a].variable,c=n.accumulatedMoments[a].variable;Y(function(){var l=u.mul(n.decay).add(s.square().mul(1-n.decay));if(n.centered){var h=n.accumulatedMeanGrads[a].variable,f=h.mul(n.decay).add(s.mul(1-n.decay)),d=c.mul(n.momentum).add(s.mul(n.learningRate).div(l.sub(f.square().add(n.epsilon)).sqrt()));u.assign(l),h.assign(f),c.assign(d);var p=i.sub(d);i.assign(p)}else{var v=u.mul(n.decay).add(s.square().mul(1-n.decay));d=c.mul(n.momentum).add(s.mul(n.learningRate).div(v.add(n.epsilon).sqrt())),u.assign(v),c.assign(d),p=i.sub(d),i.assign(p)}})}}),this.incrementIterations()},t.prototype.dispose=function(){this.accumulatedMeanSquares!=null&&ft(this.accumulatedMeanSquares.map(function(e){return e.variable})),this.accumulatedMeanGrads!=null&&this.centered&&ft(this.accumulatedMeanGrads.map(function(e){return e.variable})),this.accumulatedMoments!=null&&ft(this.accumulatedMoments.map(function(e){return e.variable}))},t.prototype.getWeights=function(){return Q(this,void 0,void 0,function(){var e;return Z(this,function(n){switch(n.label){case 0:return e=this.accumulatedMeanSquares.concat(this.accumulatedMoments),this.centered&&e.push.apply(e,this.accumulatedMeanGrads),[4,this.saveIterations()];case 1:return[2,[n.sent()].concat(e.map(function(o){return{name:o.originalName,tensor:o.variable}}))]}})})},t.prototype.setWeights=function(e){return Q(this,void 0,void 0,function(){var n;return Z(this,function(o){switch(o.label){case 0:return[4,this.extractIterations(e)];case 1:return e=o.sent(),n=this.centered?e.length/3:e.length/2,this.accumulatedMeanSquares=e.slice(0,n).map(function(a){return{originalName:a.name,variable:a.tensor.variable(!1)}}),this.accumulatedMoments=e.slice(n,2*n).map(function(a){return{originalName:a.name,variable:a.tensor.variable(!1)}}),this.centered&&(this.accumulatedMeanGrads=e.slice(2*n,3*n).map(function(a){return{originalName:a.name,variable:a.tensor.variable(!1)}})),[2]}})})},t.prototype.getConfig=function(){return{learningRate:this.learningRate,decay:this.decay,momentum:this.momentum,epsilon:this.epsilon,centered:this.centered}},t.fromConfig=function(e,n){return new e(n.learningRate,n.decay,n.momentum,n.epsilon,n.centered)},t.className="RMSProp",t}(jn);An(j0);Te.prototype.squaredDifference=function(r){return kh(this,r)},O=y0;function Nn(r,t,e){if(e===void 0&&(e=!1),r.beginPath(),t.slice(1).forEach(function(a,i){var s=a.x,u=a.y,c=t[i];r.moveTo(c.x,c.y),r.lineTo(s,u)}),e){var n=t[t.length-1],o=t[0];if(!n||!o)return;r.moveTo(n.x,n.y),r.lineTo(o.x,o.y)}r.stroke()}/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */var Ii=function(r,t){return Ii=Object.setPrototypeOf||{__proto__:[]}instanceof Array&&function(e,n){e.__proto__=n}||function(e,n){for(var o in n)n.hasOwnProperty(o)&&(e[o]=n[o])},Ii(r,t)};function se(r,t){Ii(r,t);function e(){this.constructor=r}r.prototype=t===null?Object.create(t):(e.prototype=t.prototype,new e)}var Je=function(){return Je=Object.assign||function(t){for(var e,n=1,o=arguments.length;n<o;n++){e=arguments[n];for(var a in e)Object.prototype.hasOwnProperty.call(e,a)&&(t[a]=e[a])}return t},Je.apply(this,arguments)};function te(r,t,e,n){function o(a){return a instanceof e?a:new e(function(i){i(a)})}return new(e||(e=Promise))(function(a,i){function s(l){try{c(n.next(l))}catch(h){i(h)}}function u(l){try{c(n.throw(l))}catch(h){i(h)}}function c(l){l.done?a(l.value):o(l.value).then(s,u)}c((n=n.apply(r,t||[])).next())})}function ne(r,t){var e={label:0,sent:function(){if(a[0]&1)throw a[1];return a[1]},trys:[],ops:[]},n,o,a,i;return i={next:s(0),throw:s(1),return:s(2)},typeof Symbol=="function"&&(i[Symbol.iterator]=function(){return this}),i;function s(c){return function(l){return u([c,l])}}function u(c){if(n)throw new TypeError("Generator is already executing.");for(;e;)try{if(n=1,o&&(a=c[0]&2?o.return:c[0]?o.throw||((a=o.return)&&a.call(o),0):o.next)&&!(a=a.call(o,c[1])).done)return a;switch(o=0,a&&(c=[c[0]&2,a.value]),c[0]){case 0:case 1:a=c;break;case 4:return e.label++,{value:c[1],done:!1};case 5:e.label++,o=c[1],c=[0];continue;case 7:c=e.ops.pop(),e.trys.pop();continue;default:if(a=e.trys,!(a=a.length>0&&a[a.length-1])&&(c[0]===6||c[0]===2)){e=0;continue}if(c[0]===3&&(!a||c[1]>a[0]&&c[1]<a[3])){e.label=c[1];break}if(c[0]===6&&e.label<a[1]){e.label=a[1],a=c;break}if(a&&e.label<a[2]){e.label=a[2],e.ops.push(c);break}a[2]&&e.ops.pop(),e.trys.pop();continue}c=t.call(r,e)}catch(l){c=[6,l],o=0}finally{n=a=0}if(c[0]&5)throw c[1];return{value:c[0]?c[1]:void 0,done:!0}}}function Nr(){for(var r=0,t=0,e=arguments.length;t<e;t++)r+=arguments[t].length;for(var n=Array(r),o=0,t=0;t<e;t++)for(var a=arguments[t],i=0,s=a.length;i<s;i++,o++)n[o]=a[i];return n}var Ln=function(){function r(t,e){if(!Wn(t)||!Wn(e))throw new Error("Dimensions.constructor - expected width and height to be valid numbers, instead have "+JSON.stringify({width:t,height:e}));this._width=t,this._height=e}return Object.defineProperty(r.prototype,"width",{get:function(){return this._width},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"height",{get:function(){return this._height},enumerable:!0,configurable:!0}),r.prototype.reverse=function(){return new r(1/this.width,1/this.height)},r}();function va(r,t){return r instanceof Te&&r.shape.length===t}function K0(r){return va(r,2)}function ma(r){return va(r,3)}function Cn(r){return va(r,4)}function X0(r){return r%1!==0}function $u(r){return r%2===0}function $0(r,t){t===void 0&&(t=2);var e=Math.pow(10,t);return Math.floor(r*e)/e}function Yu(r){return r&&r.width&&r.height}function Y0(r,t){var e=r.width,n=r.height,o=t/Math.max(n,e);return new Ln(Math.round(e*o),Math.round(n*o))}function Ds(r){return r.reduce(function(t,e){return t.add(e)},new xe(0,0)).div(new xe(r.length,r.length))}function qr(r,t,e){return Array(r).fill(0).map(function(n,o){return t+o*e})}function Wn(r){return!!r&&r!==1/0&&r!==-1/0&&!isNaN(r)||r===0}function Ju(r){return Wn(r)&&0<=r&&r<=1}var xe=function(){function r(t,e){this._x=t,this._y=e}return Object.defineProperty(r.prototype,"x",{get:function(){return this._x},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"y",{get:function(){return this._y},enumerable:!0,configurable:!0}),r.prototype.add=function(t){return new r(this.x+t.x,this.y+t.y)},r.prototype.sub=function(t){return new r(this.x-t.x,this.y-t.y)},r.prototype.mul=function(t){return new r(this.x*t.x,this.y*t.y)},r.prototype.div=function(t){return new r(this.x/t.x,this.y/t.y)},r.prototype.abs=function(){return new r(Math.abs(this.x),Math.abs(this.y))},r.prototype.magnitude=function(){return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2))},r.prototype.floor=function(){return new r(Math.floor(this.x),Math.floor(this.y))},r}(),En=function(){function r(t,e){e===void 0&&(e=!0);var n=t||{},o=[n.left,n.top,n.right,n.bottom].every(Wn),a=[n.x,n.y,n.width,n.height].every(Wn);if(!a&&!o)throw new Error("Box.constructor - expected box to be IBoundingBox | IRect, instead have "+JSON.stringify(n));var i=a?[n.x,n.y,n.width,n.height]:[n.left,n.top,n.right-n.left,n.bottom-n.top],s=i[0],u=i[1],c=i[2],l=i[3];r.assertIsValidBox({x:s,y:u,width:c,height:l},"Box.constructor",e),this._x=s,this._y=u,this._width=c,this._height=l}return r.isRect=function(t){return!!t&&[t.x,t.y,t.width,t.height].every(Wn)},r.assertIsValidBox=function(t,e,n){if(n===void 0&&(n=!1),!r.isRect(t))throw new Error(e+" - invalid box: "+JSON.stringify(t)+", expected object with properties x, y, width, height");if(!n&&(t.width<0||t.height<0))throw new Error(e+" - width ("+t.width+") and height ("+t.height+") must be positive numbers")},Object.defineProperty(r.prototype,"x",{get:function(){return this._x},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"y",{get:function(){return this._y},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"width",{get:function(){return this._width},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"height",{get:function(){return this._height},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"left",{get:function(){return this.x},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"top",{get:function(){return this.y},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"right",{get:function(){return this.x+this.width},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"bottom",{get:function(){return this.y+this.height},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"area",{get:function(){return this.width*this.height},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"topLeft",{get:function(){return new xe(this.left,this.top)},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"topRight",{get:function(){return new xe(this.right,this.top)},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"bottomLeft",{get:function(){return new xe(this.left,this.bottom)},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"bottomRight",{get:function(){return new xe(this.right,this.bottom)},enumerable:!0,configurable:!0}),r.prototype.round=function(){var t=[this.x,this.y,this.width,this.height].map(function(i){return Math.round(i)}),e=t[0],n=t[1],o=t[2],a=t[3];return new r({x:e,y:n,width:o,height:a})},r.prototype.floor=function(){var t=[this.x,this.y,this.width,this.height].map(function(i){return Math.floor(i)}),e=t[0],n=t[1],o=t[2],a=t[3];return new r({x:e,y:n,width:o,height:a})},r.prototype.toSquare=function(){var t=this,e=t.x,n=t.y,o=t.width,a=t.height,i=Math.abs(o-a);return o<a&&(e-=i/2,o+=i),a<o&&(n-=i/2,a+=i),new r({x:e,y:n,width:o,height:a})},r.prototype.rescale=function(t){var e=Yu(t)?t.width:t,n=Yu(t)?t.height:t;return new r({x:this.x*e,y:this.y*n,width:this.width*e,height:this.height*n})},r.prototype.pad=function(t,e){var n=[this.x-t/2,this.y-e/2,this.width+t,this.height+e],o=n[0],a=n[1],i=n[2],s=n[3];return new r({x:o,y:a,width:i,height:s})},r.prototype.clipAtImageBorders=function(t,e){var n=this,o=n.x,a=n.y,i=n.right,s=n.bottom,u=Math.max(o,0),c=Math.max(a,0),l=i-u,h=s-c,f=Math.min(l,t-u),d=Math.min(h,e-c);return new r({x:u,y:c,width:f,height:d}).floor()},r.prototype.shift=function(t,e){var n=this,o=n.width,a=n.height,i=this.x+t,s=this.y+e;return new r({x:i,y:s,width:o,height:a})},r.prototype.padAtBorders=function(t,e){var n=this.width+1,o=this.height+1,a=1,i=1,s=n,u=o,c=this.left,l=this.top,h=this.right,f=this.bottom;return h>e&&(s=-h+e+n,h=e),f>t&&(u=-f+t+o,f=t),c<1&&(u=2-c,c=1),l<1&&(u=2-l,l=1),{dy:i,edy:u,dx:a,edx:s,y:l,ey:f,x:c,ex:h,w:n,h:o}},r.prototype.calibrate=function(t){return new r({left:this.left+t.left*this.width,top:this.top+t.top*this.height,right:this.right+t.right*this.width,bottom:this.bottom+t.bottom*this.height}).toSquare().round()},r}(),ga=function(r){se(t,r);function t(e,n,o,a,i){return i===void 0&&(i=!1),r.call(this,{left:e,top:n,right:o,bottom:a},i)||this}return t}(En),mf=function(){function r(t,e,n,o,a){this._imageDims=new Ln(a.width,a.height),this._score=t,this._classScore=e,this._className=n,this._box=new En(o).rescale(this._imageDims)}return Object.defineProperty(r.prototype,"score",{get:function(){return this._score},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"classScore",{get:function(){return this._classScore},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"className",{get:function(){return this._className},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"box",{get:function(){return this._box},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"imageDims",{get:function(){return this._imageDims},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"imageWidth",{get:function(){return this.imageDims.width},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"imageHeight",{get:function(){return this.imageDims.height},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"relativeBox",{get:function(){return new En(this._box).rescale(this.imageDims.reverse())},enumerable:!0,configurable:!0}),r.prototype.forSize=function(t,e){return new r(this.score,this.classScore,this.className,this.relativeBox,{width:t,height:e})},r}(),Wt=function(r){se(t,r);function t(e,n,o){return r.call(this,e,e,"",n,o)||this}return t.prototype.forSize=function(e,n){var o=r.prototype.forSize.call(this,e,n),a=o.score,i=o.relativeBox,s=o.imageDims;return new t(a,i,s)},t}(mf);function J0(r,t,e){e===void 0&&(e=!0);var n=Math.max(0,Math.min(r.right,t.right)-Math.max(r.left,t.left)),o=Math.max(0,Math.min(r.bottom,t.bottom)-Math.max(r.top,t.top)),a=n*o;return e?a/(r.area+t.area-a):a/Math.min(r.area,t.area)}function Q0(r){var t=r.map(function(s){return s.x}),e=r.map(function(s){return s.y}),n=t.reduce(function(s,u){return u<s?u:s},1/0),o=e.reduce(function(s,u){return u<s?u:s},1/0),a=t.reduce(function(s,u){return s<u?u:s},0),i=e.reduce(function(s,u){return s<u?u:s},0);return new ga(n,o,a,i)}function jr(r,t,e,n){n===void 0&&(n=!0);for(var o=t.map(function(s,u){return{score:s,boxIndex:u}}).sort(function(s,u){return s.score-u.score}).map(function(s){return s.boxIndex}),a=[],i=function(){var s=o.pop();a.push(s);for(var u=o,c=[],l=0;l<u.length;l++){var h=u[l],f=r[s],d=r[h];c.push(J0(f,d,n))}o=o.filter(function(p,v){return c[v]<=e})};o.length>0;)i();return a}function to(r,t){return Y(function(){var e=t[0],n=t[1],o=t[2],a=Xt(Nr(r.shape.slice(0,3),[1]),e),i=Xt(Nr(r.shape.slice(0,3),[1]),n),s=Xt(Nr(r.shape.slice(0,3),[1]),o),u=Ve([a,i,s],3);return Ue(r,u)})}function Z0(r,t){return t===void 0&&(t=!1),Y(function(){var e=r.shape.slice(1),n=e[0],o=e[1];if(n===o)return r;var a=Math.abs(n-o),i=Math.round(a*(t?.5:1)),s=n>o?2:1,u=function(d){var p=r.shape.slice();return p[s]=d,Xt(p,0)},c=u(i),l=a-c.shape[s],h=t&&l?u(l):null,f=[h,r,c].filter(function(d){return!!d}).map(function(d){return d.toFloat()});return Ve(f,s)})}function Qa(r){return 1/(1+Math.exp(-r))}var Ts=function(r){se(t,r);function t(e,n,o,a,i){return i===void 0&&(i=!1),r.call(this,{x:e,y:n,width:o,height:a},i)||this}return t}(En),ex=.5,tx=.43,nx=.45,vr=function(){function r(t,e,n){n===void 0&&(n=new xe(0,0));var o=e.width,a=e.height;this._imgDims=new Ln(o,a),this._shift=n,this._positions=t.map(function(i){return i.mul(new xe(o,a)).add(n)})}return Object.defineProperty(r.prototype,"shift",{get:function(){return new xe(this._shift.x,this._shift.y)},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"imageWidth",{get:function(){return this._imgDims.width},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"imageHeight",{get:function(){return this._imgDims.height},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"positions",{get:function(){return this._positions},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"relativePositions",{get:function(){var t=this;return this._positions.map(function(e){return e.sub(t._shift).div(new xe(t.imageWidth,t.imageHeight))})},enumerable:!0,configurable:!0}),r.prototype.forSize=function(t,e){return new this.constructor(this.relativePositions,{width:t,height:e})},r.prototype.shiftBy=function(t,e){return new this.constructor(this.relativePositions,this._imgDims,new xe(t,e))},r.prototype.shiftByPoint=function(t){return this.shiftBy(t.x,t.y)},r.prototype.align=function(t,e){if(e===void 0&&(e={}),t){var n=t instanceof Wt?t.box.floor():new En(t);return this.shiftBy(n.x,n.y).align(null,e)}var o=Object.assign({},{useDlibAlignment:!1,minBoxPadding:.2},e),a=o.useDlibAlignment,i=o.minBoxPadding;return a?this.alignDlib():this.alignMinBbox(i)},r.prototype.alignDlib=function(){var t=this.getRefPointsForAlignment(),e=t[0],n=t[1],o=t[2],a=function(h){return o.sub(h).magnitude()},i=(a(e)+a(n))/2,s=Math.floor(i/nx),u=Ds(t),c=Math.floor(Math.max(0,u.x-ex*s)),l=Math.floor(Math.max(0,u.y-tx*s));return new Ts(c,l,Math.min(s,this.imageWidth+c),Math.min(s,this.imageHeight+l))},r.prototype.alignMinBbox=function(t){var e=Q0(this.positions);return e.pad(e.width*t,e.height*t)},r.prototype.getRefPointsForAlignment=function(){throw new Error("getRefPointsForAlignment not implemented by base class")},r}(),rx=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.getRefPointsForAlignment=function(){var e=this.positions;return[e[0],e[1],Ds([e[3],e[4]])]},t}(vr),gf=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.getJawOutline=function(){return this.positions.slice(0,17)},t.prototype.getLeftEyeBrow=function(){return this.positions.slice(17,22)},t.prototype.getRightEyeBrow=function(){return this.positions.slice(22,27)},t.prototype.getNose=function(){return this.positions.slice(27,36)},t.prototype.getLeftEye=function(){return this.positions.slice(36,42)},t.prototype.getRightEye=function(){return this.positions.slice(42,48)},t.prototype.getMouth=function(){return this.positions.slice(48,68)},t.prototype.getRefPointsForAlignment=function(){return[this.getLeftEye(),this.getRightEye(),this.getMouth()].map(Ds)},t}(vr),Qu=function(){function r(t,e){this._label=t,this._distance=e}return Object.defineProperty(r.prototype,"label",{get:function(){return this._label},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"distance",{get:function(){return this._distance},enumerable:!0,configurable:!0}),r.prototype.toString=function(t){return t===void 0&&(t=!0),""+this.label+(t?" ("+$0(this.distance)+")":"")},r}(),Zu=function(r){se(t,r);function t(e,n){var o=r.call(this,e)||this;return o._label=n,o}return t.assertIsValidLabeledBox=function(e,n){if(En.assertIsValidBox(e,n),!Wn(e.label))throw new Error(n+" - expected property label ("+e.label+") to be a number")},Object.defineProperty(t.prototype,"label",{get:function(){return this._label},enumerable:!0,configurable:!0}),t}(En),po=function(){function r(t,e){if(typeof t!="string")throw new Error("LabeledFaceDescriptors - constructor expected label to be a string");if(!Array.isArray(e)||e.some(function(n){return!(n instanceof Float32Array)}))throw new Error("LabeledFaceDescriptors - constructor expected descriptors to be an array of Float32Array");this._label=t,this._descriptors=e}return Object.defineProperty(r.prototype,"label",{get:function(){return this._label},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"descriptors",{get:function(){return this._descriptors},enumerable:!0,configurable:!0}),r.prototype.toJSON=function(){return{label:this.label,descriptors:this.descriptors.map(function(t){return Array.from(t)})}},r.fromJSON=function(t){var e=t.descriptors.map(function(n){return new Float32Array(n)});return new r(t.label,e)},r}();(function(r){se(t,r);function t(e,n,o,a){var i=r.call(this,e,n)||this;return i._score=o,i._classScore=a,i}return t.assertIsValidPredictedBox=function(e,n){if(Zu.assertIsValidLabeledBox(e,n),!Ju(e.score)||!Ju(e.classScore))throw new Error(n+" - expected properties score ("+e.score+") and ("+e.classScore+") to be a number between [0, 1]")},Object.defineProperty(t.prototype,"score",{get:function(){return this._score},enumerable:!0,configurable:!0}),Object.defineProperty(t.prototype,"classScore",{get:function(){return this._classScore},enumerable:!0,configurable:!0}),t})(Zu);function yf(r){return r.detection instanceof Wt}function Kr(r,t){var e={detection:t};return Object.assign({},r,e)}function xf(){var r=window.fetch||function(){throw new Error("fetch - missing fetch implementation for browser environment")},t=function(){throw new Error("readFile - filesystem not available for browser environment")};return{Canvas:HTMLCanvasElement,CanvasRenderingContext2D,Image:HTMLImageElement,ImageData,Video:HTMLVideoElement,createCanvasElement:function(){return document.createElement("canvas")},createImageElement:function(){return document.createElement("img")},fetch:r,readFile:t}}function bf(r){var t="";if(!r)try{r=require("fs")}catch(n){t=n.toString()}var e=r?function(n){return new Promise(function(o,a){r.readFile(n,function(i,s){return i?a(i):o(s)})})}:function(){throw new Error("readFile - failed to require fs in nodejs environment with error: "+t)};return{readFile:e}}function wf(){var r=global.Canvas||global.HTMLCanvasElement,t=global.Image||global.HTMLImageElement,e=function(){if(r)return new r;throw new Error("createCanvasElement - missing Canvas implementation for nodejs environment")},n=function(){if(t)return new t;throw new Error("createImageElement - missing Image implementation for nodejs environment")},o=global.fetch||function(){throw new Error("fetch - missing fetch implementation for nodejs environment")},a=bf();return Je({Canvas:r||function(){function i(){}return i}(),CanvasRenderingContext2D:global.CanvasRenderingContext2D||function(){function i(){}return i}(),Image:t||function(){function i(){}return i}(),ImageData:global.ImageData||function(){function i(){}return i}(),Video:global.HTMLVideoElement||function(){function i(){}return i}(),createCanvasElement:e,createImageElement:n,fetch:o},a)}function Cf(){return typeof window=="object"&&typeof document<"u"&&typeof HTMLImageElement<"u"&&typeof HTMLCanvasElement<"u"&&typeof HTMLVideoElement<"u"&&typeof ImageData<"u"&&typeof CanvasRenderingContext2D<"u"}function _f(){return typeof global=="object"&&typeof require=="function"&&typeof module<"u"&&typeof process<"u"&&!!process.version}var We;function ox(){if(!We)throw new Error("getEnv - environment is not defined, check isNodejs() and isBrowser()");return We}function Ai(r){We=r}function Ns(){Cf()&&Ai(xf()),_f()&&Ai(wf())}function ax(r){if(We||Ns(),!We)throw new Error("monkeyPatch - environment is not defined, check isNodejs() and isBrowser()");var t=r.Canvas,e=t===void 0?We.Canvas:t,n=r.Image,o=n===void 0?We.Image:n;We.Canvas=e,We.Image=o,We.createCanvasElement=r.createCanvasElement||function(){return new e},We.createImageElement=r.createImageElement||function(){return new o},We.ImageData=r.ImageData||We.ImageData,We.Video=r.Video||We.Video,We.fetch=r.fetch||We.fetch,We.readFile=r.readFile||We.readFile}var rt={getEnv:ox,setEnv:Ai,initialize:Ns,createBrowserEnv:xf,createFileSystem:bf,createNodejsEnv:wf,monkeyPatch:ax,isBrowser:Cf,isNodejs:_f};Ns();function Ef(r){return!rt.isNodejs()&&typeof r=="string"?document.getElementById(r):r}function Sn(r){var t=rt.getEnv(),e=t.Canvas,n=t.CanvasRenderingContext2D;if(r instanceof n)return r;var o=Ef(r);if(!(o instanceof e))throw new Error("resolveContext2d - expected canvas to be of instance of Canvas");var a=o.getContext("2d");if(!a)throw new Error("resolveContext2d - canvas 2d context is null");return a}var ec;(function(r){r.TOP_LEFT="TOP_LEFT",r.TOP_RIGHT="TOP_RIGHT",r.BOTTOM_LEFT="BOTTOM_LEFT",r.BOTTOM_RIGHT="BOTTOM_RIGHT"})(ec||(ec={}));function Sf(r){var t=rt.getEnv(),e=t.Image,n=t.Video;return r instanceof e&&r.complete||r instanceof n&&r.readyState>=3}function ix(r){return new Promise(function(t,e){if(r instanceof rt.getEnv().Canvas||Sf(r))return t();function n(a){!a.currentTarget||(a.currentTarget.removeEventListener("load",n),a.currentTarget.removeEventListener("error",o),t(a))}function o(a){!a.currentTarget||(a.currentTarget.removeEventListener("load",n),a.currentTarget.removeEventListener("error",o),e(a))}r.addEventListener("load",n),r.addEventListener("error",o)})}function kf(r){var t=rt.getEnv(),e=t.Image,n=t.Video;return r instanceof e?new Ln(r.naturalWidth,r.naturalHeight):r instanceof n?new Ln(r.videoWidth,r.videoHeight):new Ln(r.width,r.height)}function ya(r){var t=r.width,e=r.height,n=rt.getEnv().createCanvasElement,o=n();return o.width=t,o.height=e,o}function Fs(r,t){var e=rt.getEnv().ImageData;if(!(r instanceof e)&&!Sf(r))throw new Error("createCanvasFromMedia - media has not finished loading yet");var n=t||kf(r),o=n.width,a=n.height,i=ya({width:o,height:a});return r instanceof e?Sn(i).putImageData(r,0,0):Sn(i).drawImage(r,0,0,o,a),i}function sx(r,t){return te(this,void 0,void 0,function(){var e,n,o,a,i,s;return ne(this,function(u){switch(u.label){case 0:return e=t||rt.getEnv().createCanvasElement(),n=r.shape.slice(Cn(r)?1:0),o=n[0],a=n[1],i=n[2],s=Y(function(){return r.as3D(o,a,i).toInt()}),[4,As.toPixels(s,e)];case 1:return u.sent(),s.dispose(),[2,e]}})})}function tc(r){var t=rt.getEnv(),e=t.Image,n=t.Canvas,o=t.Video;return r instanceof e||r instanceof n||r instanceof o}function ux(r,t,e){e===void 0&&(e=!1);var n=rt.getEnv(),o=n.Image,a=n.Canvas;if(!(r instanceof o||r instanceof a))throw new Error("imageToSquare - expected arg0 to be HTMLImageElement | HTMLCanvasElement");var i=kf(r),s=t/Math.max(i.height,i.width),u=s*i.width,c=s*i.height,l=ya({width:t,height:t}),h=r instanceof a?r:Fs(r),f=Math.abs(u-c)/2,d=e&&u<c?f:0,p=e&&c<u?f:0;return Sn(l).drawImage(h,d,p,u,c),l}var Xo=function(){function r(t,e){var n=this;if(e===void 0&&(e=!1),this._imageTensors=[],this._canvases=[],this._treatAsBatchInput=!1,this._inputDimensions=[],!Array.isArray(t))throw new Error("NetInput.constructor - expected inputs to be an Array of TResolvedNetInput or to be instanceof tf.Tensor4D, instead have "+t);this._treatAsBatchInput=e,this._batchSize=t.length,t.forEach(function(o,a){if(ma(o)){n._imageTensors[a]=o,n._inputDimensions[a]=o.shape;return}if(Cn(o)){var i=o.shape[0];if(i!==1)throw new Error("NetInput - tf.Tensor4D with batchSize "+i+" passed, but not supported in input array");n._imageTensors[a]=o,n._inputDimensions[a]=o.shape.slice(1);return}var s=o instanceof rt.getEnv().Canvas?o:Fs(o);n._canvases[a]=s,n._inputDimensions[a]=[s.height,s.width,3]})}return Object.defineProperty(r.prototype,"imageTensors",{get:function(){return this._imageTensors},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"canvases",{get:function(){return this._canvases},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"isBatchInput",{get:function(){return this.batchSize>1||this._treatAsBatchInput},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"batchSize",{get:function(){return this._batchSize},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"inputDimensions",{get:function(){return this._inputDimensions},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"inputSize",{get:function(){return this._inputSize},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"reshapedInputDimensions",{get:function(){var t=this;return qr(this.batchSize,0,1).map(function(e,n){return t.getReshapedInputDimensions(n)})},enumerable:!0,configurable:!0}),r.prototype.getInput=function(t){return this.canvases[t]||this.imageTensors[t]},r.prototype.getInputDimensions=function(t){return this._inputDimensions[t]},r.prototype.getInputHeight=function(t){return this._inputDimensions[t][0]},r.prototype.getInputWidth=function(t){return this._inputDimensions[t][1]},r.prototype.getReshapedInputDimensions=function(t){if(typeof this.inputSize!="number")throw new Error("getReshapedInputDimensions - inputSize not set, toBatchTensor has not been called yet");var e=this.getInputWidth(t),n=this.getInputHeight(t);return Y0({width:e,height:n},this.inputSize)},r.prototype.toBatchTensor=function(t,e){var n=this;return e===void 0&&(e=!0),this._inputSize=t,Y(function(){var o=qr(n.batchSize,0,1).map(function(i){var s=n.getInput(i);if(s instanceof Te){var u=Cn(s)?s:s.expandDims();return u=Z0(u,e),(u.shape[1]!==t||u.shape[2]!==t)&&(u=_s.resizeBilinear(u,[t,t])),u.as3D(t,t,3)}if(s instanceof rt.getEnv().Canvas)return As.fromPixels(ux(s,t,e));throw new Error("toBatchTensor - at batchIdx "+i+", expected input to be instanceof tf.Tensor or instanceof HTMLCanvasElement, instead have "+s)}),a=bt(o.map(function(i){return i.toFloat()})).as4D(n.batchSize,t,t,3);return a})},r}();function qe(r){return te(this,void 0,void 0,function(){var t,e,n;return ne(this,function(o){switch(o.label){case 0:if(r instanceof Xo)return[2,r];if(t=Array.isArray(r)?r:[r],!t.length)throw new Error("toNetInput - empty array passed as input");return e=function(a){return Array.isArray(r)?" at input index "+a+":":""},n=t.map(Ef),n.forEach(function(a,i){if(!tc(a)&&!ma(a)&&!Cn(a))throw typeof t[i]=="string"?new Error("toNetInput -"+e(i)+" string passed, but could not resolve HTMLElement for element id "+t[i]):new Error("toNetInput -"+e(i)+" expected media to be of type HTMLImageElement | HTMLVideoElement | HTMLCanvasElement | tf.Tensor3D, or to be an element id");if(Cn(a)){var s=a.shape[0];if(s!==1)throw new Error("toNetInput -"+e(i)+" tf.Tensor4D with batchSize "+s+" passed, but not supported in input array")}}),[4,Promise.all(n.map(function(a){return tc(a)&&ix(a)}))];case 1:return o.sent(),[2,new Xo(n,Array.isArray(r))]}})})}function Ps(r,t){return te(this,void 0,void 0,function(){var e,n,o,a,i,s,u;return ne(this,function(c){switch(c.label){case 0:return e=rt.getEnv().Canvas,n=r,r instanceof e?[3,5]:[4,qe(r)];case 1:if(o=c.sent(),o.batchSize>1)throw new Error("extractFaces - batchSize > 1 not supported");return a=o.getInput(0),a instanceof e?(i=a,[3,4]):[3,2];case 2:return[4,sx(a)];case 3:i=c.sent(),c.label=4;case 4:n=i,c.label=5;case 5:return s=Sn(n),u=t.map(function(l){return l instanceof Wt?l.forSize(n.width,n.height).box.floor():l}).map(function(l){return l.clipAtImageBorders(n.width,n.height)}),[2,u.map(function(l){var h=l.x,f=l.y,d=l.width,p=l.height,v=ya({width:d,height:p});return Sn(v).putImageData(s.getImageData(h,f,d,p),0,0),v})]}})})}function Ms(r,t){return te(this,void 0,void 0,function(){return ne(this,function(e){if(!ma(r)&&!Cn(r))throw new Error("extractFaceTensors - expected image tensor to be 3D or 4D");if(Cn(r)&&r.shape[0]>1)throw new Error("extractFaceTensors - batchSize > 1 not supported");return[2,Y(function(){var n=r.shape.slice(Cn(r)?1:0),o=n[0],a=n[1],i=n[2],s=t.map(function(c){return c instanceof Wt?c.forSize(a,o).box:c}).map(function(c){return c.clipAtImageBorders(a,o)}),u=s.map(function(c){var l=c.x,h=c.y,f=c.width,d=c.height;return Kh(r.as3D(o,a,i),[h,l,0],[d,f,i])});return u})]})})}function cx(r,t){return te(this,void 0,void 0,function(){var e,n;return ne(this,function(o){switch(o.label){case 0:return e=rt.getEnv().fetch,[4,e(r,t)];case 1:if(n=o.sent(),!(n.status<400))throw new Error("failed to fetch: ("+n.status+") "+n.statusText+", from url: "+n.url);return[2,n]}})})}function lx(r){return te(this,void 0,void 0,function(){return ne(this,function(t){switch(t.label){case 0:return[4,cx(r)];case 1:return[2,t.sent().json()]}})})}function Rf(r,t){var e=t+"-weights_manifest.json";if(!r)return{modelBaseUri:"",manifestUri:e};if(r==="/")return{modelBaseUri:"/",manifestUri:"/"+e};var n=r.startsWith("http://")?"http://":r.startsWith("https://")?"https://":"";r=r.replace(n,"");var o=r.split("/").filter(function(s){return s}),a=r.endsWith(".json")?o[o.length-1]:e,i=n+(r.endsWith(".json")?o.slice(0,o.length-1):o).join("/");return i=r.startsWith("/")?"/"+i:i,{modelBaseUri:i,manifestUri:i==="/"?"/"+a:i+"/"+a}}function hx(r,t){return te(this,void 0,void 0,function(){var e,n,o,a;return ne(this,function(i){switch(i.label){case 0:return e=Rf(r,t),n=e.manifestUri,o=e.modelBaseUri,[4,lx(n)];case 1:return a=i.sent(),[2,hf.loadWeights(a,o)]}})})}var hn=function(){function r(t){this._name=t,this._params=void 0,this._paramMappings=[]}return Object.defineProperty(r.prototype,"params",{get:function(){return this._params},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"paramMappings",{get:function(){return this._paramMappings},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"isLoaded",{get:function(){return!!this.params},enumerable:!0,configurable:!0}),r.prototype.getParamFromPath=function(t){var e=this.traversePropertyPath(t),n=e.obj,o=e.objProp;return n[o]},r.prototype.reassignParamFromPath=function(t,e){var n=this.traversePropertyPath(t),o=n.obj,a=n.objProp;o[a].dispose(),o[a]=e},r.prototype.getParamList=function(){var t=this;return this._paramMappings.map(function(e){var n=e.paramPath;return{path:n,tensor:t.getParamFromPath(n)}})},r.prototype.getTrainableParams=function(){return this.getParamList().filter(function(t){return t.tensor instanceof dr})},r.prototype.getFrozenParams=function(){return this.getParamList().filter(function(t){return!(t.tensor instanceof dr)})},r.prototype.variable=function(){var t=this;this.getFrozenParams().forEach(function(e){var n=e.path,o=e.tensor;t.reassignParamFromPath(n,o.variable())})},r.prototype.freeze=function(){var t=this;this.getTrainableParams().forEach(function(e){var n=e.path,o=e.tensor,a=$e(o.dataSync());o.dispose(),t.reassignParamFromPath(n,a)})},r.prototype.dispose=function(t){t===void 0&&(t=!0),this.getParamList().forEach(function(e){if(t&&e.tensor.isDisposed)throw new Error("param tensor has already been disposed for path "+e.path);e.tensor.dispose()}),this._params=void 0},r.prototype.serializeParams=function(){return new Float32Array(this.getParamList().map(function(t){var e=t.tensor;return Array.from(e.dataSync())}).reduce(function(t,e){return t.concat(e)}))},r.prototype.load=function(t){return te(this,void 0,void 0,function(){return ne(this,function(e){switch(e.label){case 0:return t instanceof Float32Array?(this.extractWeights(t),[2]):[4,this.loadFromUri(t)];case 1:return e.sent(),[2]}})})},r.prototype.loadFromUri=function(t){return te(this,void 0,void 0,function(){var e;return ne(this,function(n){switch(n.label){case 0:if(t&&typeof t!="string")throw new Error(this._name+".loadFromUri - expected model uri");return[4,hx(t,this.getDefaultModelName())];case 1:return e=n.sent(),this.loadFromWeightMap(e),[2]}})})},r.prototype.loadFromDisk=function(t){return te(this,void 0,void 0,function(){var e,n,o,a,i,s,u,c,l,h;return ne(this,function(f){switch(f.label){case 0:if(t&&typeof t!="string")throw new Error(this._name+".loadFromDisk - expected model file path");return e=rt.getEnv().readFile,n=Rf(t,this.getDefaultModelName()),o=n.manifestUri,a=n.modelBaseUri,i=function(d){return Promise.all(d.map(function(p){return e(p).then(function(v){return v.buffer})}))},s=hf.weightsLoaderFactory(i),l=(c=JSON).parse,[4,e(o)];case 1:return u=l.apply(c,[f.sent().toString()]),[4,s(u,a)];case 2:return h=f.sent(),this.loadFromWeightMap(h),[2]}})})},r.prototype.loadFromWeightMap=function(t){var e=this.extractParamsFromWeigthMap(t),n=e.paramMappings,o=e.params;this._paramMappings=n,this._params=o},r.prototype.extractWeights=function(t){var e=this.extractParams(t),n=e.paramMappings,o=e.params;this._paramMappings=n,this._params=o},r.prototype.traversePropertyPath=function(t){if(!this.params)throw new Error("traversePropertyPath - model has no loaded params");var e=t.split("/").reduce(function(a,i){if(!a.nextObj.hasOwnProperty(i))throw new Error("traversePropertyPath - object does not have property "+i+", for path "+t);return{obj:a.nextObj,objProp:i,nextObj:a.nextObj[i]}},{nextObj:this.params}),n=e.obj,o=e.objProp;if(!n||!o||!(n[o]instanceof Te))throw new Error("traversePropertyPath - parameter is not a tensor, for path "+t);return{obj:n,objProp:o}},r}();function xt(r,t,e){return Y(function(){var n=ys(r,t.depthwise_filter,t.pointwise_filter,e,"same");return n=pe(n,t.bias),n})}function Za(r,t,e){return e===void 0&&(e=!1),Y(function(){var n=Ne(e?pe(It(r,t.conv0.filters,[2,2],"same"),t.conv0.bias):xt(r,t.conv0,[2,2])),o=xt(n,t.conv1,[1,1]),a=Ne(pe(n,o)),i=xt(a,t.conv2,[1,1]);return Ne(pe(n,pe(o,i)))})}function vo(r,t,e,n){return e===void 0&&(e=!1),n===void 0&&(n=!0),Y(function(){var o=Ne(e?pe(It(r,t.conv0.filters,n?[2,2]:[1,1],"same"),t.conv0.bias):xt(r,t.conv0,n?[2,2]:[1,1])),a=xt(o,t.conv1,[1,1]),i=Ne(pe(o,a)),s=xt(i,t.conv2,[1,1]),u=Ne(pe(o,pe(a,s))),c=xt(u,t.conv3,[1,1]);return Ne(pe(o,pe(a,pe(s,c))))})}function Pt(r,t,e,n){return e===void 0&&(e="same"),n===void 0&&(n=!1),Y(function(){var o=pe(It(r,t.filters,[1,1],e),t.bias);return n?Ne(o):o})}function fn(r,t){Object.keys(r).forEach(function(e){t.some(function(n){return n.originalPath===e})||r[e].dispose()})}function xa(r,t){return function(e,n,o,a){var i=lt(r(e*n*o*o),[o,o,e,n]),s=Me(r(n));return t.push({paramPath:a+"/filters"},{paramPath:a+"/bias"}),{filters:i,bias:s}}}function Os(r,t){return function(e,n,o){var a=bn(r(e*n),[e,n]),i=Me(r(n));return t.push({paramPath:o+"/weights"},{paramPath:o+"/bias"}),{weights:a,bias:i}}}var If=function(){function r(t,e,n){this.depthwise_filter=t,this.pointwise_filter=e,this.bias=n}return r}();function Bs(r,t){return function(e,n,o){var a=lt(r(9*e),[3,3,e,1]),i=lt(r(e*n),[1,1,e,n]),s=Me(r(n));return t.push({paramPath:o+"/depthwise_filter"},{paramPath:o+"/pointwise_filter"},{paramPath:o+"/bias"}),new If(a,i,s)}}function Ls(r){return function(t){var e=r(t+"/depthwise_filter",4),n=r(t+"/pointwise_filter",4),o=r(t+"/bias",1);return new If(e,n,o)}}function Dn(r,t){return function(e,n,o){var a=r[e];if(!va(a,n))throw new Error("expected weightMap["+e+"] to be a Tensor"+n+"D, instead have "+a);return t.push({originalPath:e,paramPath:o||e}),a}}function dn(r){var t=r;function e(o){var a=t.slice(0,o);return t=t.slice(o),a}function n(){return t}return{extractWeights:e,getRemainingWeights:n}}function Af(r,t){var e=xa(r,t),n=Bs(r,t);function o(i,s,u,c){c===void 0&&(c=!1);var l=c?e(i,s,3,u+"/conv0"):n(i,s,u+"/conv0"),h=n(s,s,u+"/conv1"),f=n(s,s,u+"/conv2");return{conv0:l,conv1:h,conv2:f}}function a(i,s,u,c){c===void 0&&(c=!1);var l=o(i,s,u,c),h=l.conv0,f=l.conv1,d=l.conv2,p=n(s,s,u+"/conv3");return{conv0:h,conv1:f,conv2:d,conv3:p}}return{extractDenseBlock3Params:o,extractDenseBlock4Params:a}}function fx(r){var t=[],e=dn(r),n=e.extractWeights,o=e.getRemainingWeights,a=Af(n,t).extractDenseBlock4Params,i=a(3,32,"dense0",!0),s=a(32,64,"dense1"),u=a(64,128,"dense2"),c=a(128,256,"dense3");if(o().length!==0)throw new Error("weights remaing after extract: "+o().length);return{paramMappings:t,params:{dense0:i,dense1:s,dense2:u,dense3:c}}}function Df(r){return function(t){var e=r(t+"/filters",4),n=r(t+"/bias",1);return{filters:e,bias:n}}}function Tf(r,t){var e=Dn(r,t),n=Df(e),o=Ls(e);function a(s,u){u===void 0&&(u=!1);var c=u?n(s+"/conv0"):o(s+"/conv0"),l=o(s+"/conv1"),h=o(s+"/conv2");return{conv0:c,conv1:l,conv2:h}}function i(s,u){u===void 0&&(u=!1);var c=u?n(s+"/conv0"):o(s+"/conv0"),l=o(s+"/conv1"),h=o(s+"/conv2"),f=o(s+"/conv3");return{conv0:c,conv1:l,conv2:h,conv3:f}}return{extractDenseBlock3Params:a,extractDenseBlock4Params:i}}function dx(r){var t=[],e=Tf(r,t).extractDenseBlock4Params,n={dense0:e("dense0",!0),dense1:e("dense1"),dense2:e("dense2"),dense3:e("dense3")};return fn(r,t),{params:n,paramMappings:t}}var Nf=function(r){se(t,r);function t(){return r.call(this,"FaceFeatureExtractor")||this}return t.prototype.forwardInput=function(e){var n=this.params;if(!n)throw new Error("FaceFeatureExtractor - load model before inference");return Y(function(){var o=e.toBatchTensor(112,!0),a=[122.782,117.001,104.298],i=to(o,a).div(K(255)),s=vo(i,n.dense0,!0);return s=vo(s,n.dense1),s=vo(s,n.dense2),s=vo(s,n.dense3),s=Zr(s,[7,7],[2,2],"valid"),s})},t.prototype.forward=function(e){return te(this,void 0,void 0,function(){var n;return ne(this,function(o){switch(o.label){case 0:return n=this.forwardInput,[4,qe(e)];case 1:return[2,n.apply(this,[o.sent()])]}})})},t.prototype.getDefaultModelName=function(){return"face_feature_extractor_model"},t.prototype.extractParamsFromWeigthMap=function(e){return dx(e)},t.prototype.extractParams=function(e){return fx(e)},t}(hn);function Kt(r,t){return Y(function(){return pe(fa(r,t.weights),t.bias)})}function px(r,t,e){var n=[],o=dn(r),a=o.extractWeights,i=o.getRemainingWeights,s=Os(a,n),u=s(t,e,"fc");if(i().length!==0)throw new Error("weights remaing after extract: "+i().length);return{paramMappings:n,params:{fc:u}}}function vx(r){var t=[],e=Dn(r,t);function n(a){var i=e(a+"/weights",2),s=e(a+"/bias",1);return{weights:i,bias:s}}var o={fc:n("fc")};return fn(r,t),{params:o,paramMappings:t}}function Ff(r){var t={},e={};return Object.keys(r).forEach(function(n){var o=n.startsWith("fc")?e:t;o[n]=r[n]}),{featureExtractorMap:t,classifierMap:e}}var Pf=function(r){se(t,r);function t(e,n){var o=r.call(this,e)||this;return o._faceFeatureExtractor=n,o}return Object.defineProperty(t.prototype,"faceFeatureExtractor",{get:function(){return this._faceFeatureExtractor},enumerable:!0,configurable:!0}),t.prototype.runNet=function(e){var n=this,o=this.params;if(!o)throw new Error(this._name+" - load model before inference");return Y(function(){var a=e instanceof Xo?n.faceFeatureExtractor.forwardInput(e):e;return Kt(a.as2D(a.shape[0],-1),o.fc)})},t.prototype.dispose=function(e){e===void 0&&(e=!0),this.faceFeatureExtractor.dispose(e),r.prototype.dispose.call(this,e)},t.prototype.loadClassifierParams=function(e){var n=this.extractClassifierParams(e),o=n.params,a=n.paramMappings;this._params=o,this._paramMappings=a},t.prototype.extractClassifierParams=function(e){return px(e,this.getClassifierChannelsIn(),this.getClassifierChannelsOut())},t.prototype.extractParamsFromWeigthMap=function(e){var n=Ff(e),o=n.featureExtractorMap,a=n.classifierMap;return this.faceFeatureExtractor.loadFromWeightMap(o),vx(a)},t.prototype.extractParams=function(e){var n=this.getClassifierChannelsIn(),o=this.getClassifierChannelsOut(),a=o*n+o,i=e.slice(0,e.length-a),s=e.slice(e.length-a);return this.faceFeatureExtractor.extractWeights(i),this.extractClassifierParams(s)},t}(hn),nc=["neutral","happy","sad","angry","fearful","disgusted","surprised"],mx=function(){function r(t){var e=this;if(t.length!==7)throw new Error("FaceExpressions.constructor - expected probabilities.length to be 7, have: "+t.length);nc.forEach(function(n,o){e[n]=t[o]})}return r.prototype.asSortedArray=function(){var t=this;return nc.map(function(e){return{expression:e,probability:t[e]}}).sort(function(e,n){return n.probability-e.probability})},r}(),gx=function(r){se(t,r);function t(e){return e===void 0&&(e=new Nf),r.call(this,"FaceExpressionNet",e)||this}return t.prototype.forwardInput=function(e){var n=this;return Y(function(){return cn(n.runNet(e))})},t.prototype.forward=function(e){return te(this,void 0,void 0,function(){var n;return ne(this,function(o){switch(o.label){case 0:return n=this.forwardInput,[4,qe(e)];case 1:return[2,n.apply(this,[o.sent()])]}})})},t.prototype.predictExpressions=function(e){return te(this,void 0,void 0,function(){var n,o,a,i,s=this;return ne(this,function(u){switch(u.label){case 0:return[4,qe(e)];case 1:return n=u.sent(),[4,this.forwardInput(n)];case 2:return o=u.sent(),[4,Promise.all(ze(o).map(function(c){return te(s,void 0,void 0,function(){var l;return ne(this,function(h){switch(h.label){case 0:return[4,c.data()];case 1:return l=h.sent(),c.dispose(),[2,l]}})})}))];case 3:return a=u.sent(),o.dispose(),i=a.map(function(c){return new mx(c)}),[2,n.isBatchInput?i:i[0]]}})})},t.prototype.getDefaultModelName=function(){return"face_expression_model"},t.prototype.getClassifierChannelsIn=function(){return 256},t.prototype.getClassifierChannelsOut=function(){return 7},t}(Pf);function Mf(r,t){var e={expressions:t};return Object.assign({},r,e)}function Ws(r){return yf(r)&&r.landmarks instanceof vr&&r.unshiftedLandmarks instanceof vr&&r.alignedRect instanceof Wt}function ba(r,t){var e=r.detection.box,n=t.shiftBy(e.x,e.y),o=n.align(),a=r.detection.imageDims,i=new Wt(r.detection.score,o.rescale(a.reverse()),a),s={landmarks:n,unshiftedLandmarks:t,alignedRect:i};return Object.assign({},r,s)}var yx=function(){function r(t){t===void 0&&(t={});var e=t.drawLines,n=e===void 0?!0:e,o=t.drawPoints,a=o===void 0?!0:o,i=t.lineWidth,s=t.lineColor,u=t.pointSize,c=t.pointColor;this.drawLines=n,this.drawPoints=a,this.lineWidth=i||1,this.pointSize=u||2,this.lineColor=s||"rgba(0, 255, 255, 1)",this.pointColor=c||"rgba(255, 0, 255, 1)"}return r}(),xx=function(){function r(t,e){e===void 0&&(e={}),this.faceLandmarks=t,this.options=new yx(e)}return r.prototype.draw=function(t){var e=Sn(t),n=this.options,o=n.drawLines,a=n.drawPoints,i=n.lineWidth,s=n.lineColor,u=n.pointSize,c=n.pointColor;if(o&&this.faceLandmarks instanceof gf&&(e.strokeStyle=s,e.lineWidth=i,Nn(e,this.faceLandmarks.getJawOutline()),Nn(e,this.faceLandmarks.getLeftEyeBrow()),Nn(e,this.faceLandmarks.getRightEyeBrow()),Nn(e,this.faceLandmarks.getNose()),Nn(e,this.faceLandmarks.getLeftEye(),!0),Nn(e,this.faceLandmarks.getRightEye(),!0),Nn(e,this.faceLandmarks.getMouth(),!0)),a){e.strokeStyle=c,e.fillStyle=c;var l=function(h){e.beginPath(),e.arc(h.x,h.y,u,0,2*Math.PI),e.fill()};this.faceLandmarks.positions.forEach(l)}},r}();function bx(r,t){var e=Array.isArray(t)?t:[t];e.forEach(function(n){var o=n instanceof vr?n:Ws(n)?n.landmarks:void 0;if(!o)throw new Error("drawFaceLandmarks - expected faceExpressions to be FaceLandmarks | WithFaceLandmarks<WithFaceDetection<{}>> or array thereof");new xx(o).draw(r)})}function wx(r,t){var e=xa(r,t),n=Bs(r,t);function o(i,s,u){var c=n(i,s,u+"/separable_conv0"),l=n(s,s,u+"/separable_conv1"),h=e(i,s,1,u+"/expansion_conv");return{separable_conv0:c,separable_conv1:l,expansion_conv:h}}function a(i,s){var u=n(i,i,s+"/separable_conv0"),c=n(i,i,s+"/separable_conv1"),l=n(i,i,s+"/separable_conv2");return{separable_conv0:u,separable_conv1:c,separable_conv2:l}}return{extractConvParams:e,extractSeparableConvParams:n,extractReductionBlockParams:o,extractMainBlockParams:a}}function Cx(r,t){var e=[],n=dn(r),o=n.extractWeights,a=n.getRemainingWeights,i=wx(o,e),s=i.extractConvParams,u=i.extractSeparableConvParams,c=i.extractReductionBlockParams,l=i.extractMainBlockParams,h=s(3,32,3,"entry_flow/conv_in"),f=c(32,64,"entry_flow/reduction_block_0"),d=c(64,128,"entry_flow/reduction_block_1"),p={conv_in:h,reduction_block_0:f,reduction_block_1:d},v={};qr(t,0,1).forEach(function(y){v["main_block_"+y]=l(128,"middle_flow/main_block_"+y)});var m=c(128,256,"exit_flow/reduction_block"),g=u(256,512,"exit_flow/separable_conv"),x={reduction_block:m,separable_conv:g};if(a().length!==0)throw new Error("weights remaing after extract: "+a().length);return{paramMappings:e,params:{entry_flow:p,middle_flow:v,exit_flow:x}}}function _x(r,t){var e=Dn(r,t),n=Df(e),o=Ls(e);function a(s){var u=o(s+"/separable_conv0"),c=o(s+"/separable_conv1"),l=n(s+"/expansion_conv");return{separable_conv0:u,separable_conv1:c,expansion_conv:l}}function i(s){var u=o(s+"/separable_conv0"),c=o(s+"/separable_conv1"),l=o(s+"/separable_conv2");return{separable_conv0:u,separable_conv1:c,separable_conv2:l}}return{extractConvParams:n,extractSeparableConvParams:o,extractReductionBlockParams:a,extractMainBlockParams:i}}function Ex(r,t){var e=[],n=_x(r,e),o=n.extractConvParams,a=n.extractSeparableConvParams,i=n.extractReductionBlockParams,s=n.extractMainBlockParams,u=o("entry_flow/conv_in"),c=i("entry_flow/reduction_block_0"),l=i("entry_flow/reduction_block_1"),h={conv_in:u,reduction_block_0:c,reduction_block_1:l},f={};qr(t,0,1).forEach(function(m){f["main_block_"+m]=s("middle_flow/main_block_"+m)});var d=i("exit_flow/reduction_block"),p=a("exit_flow/separable_conv"),v={reduction_block:d,separable_conv:p};return fn(r,e),{params:{entry_flow:h,middle_flow:f,exit_flow:v},paramMappings:e}}function Of(r,t,e){return pe(It(r,t.filters,e,"same"),t.bias)}function ei(r,t,e){e===void 0&&(e=!0);var n=e?Ne(r):r;return n=xt(n,t.separable_conv0,[1,1]),n=xt(Ne(n),t.separable_conv1,[1,1]),n=Ge(n,[3,3],[2,2],"same"),n=pe(n,Of(r,t.expansion_conv,[2,2])),n}function Sx(r,t){var e=xt(Ne(r),t.separable_conv0,[1,1]);return e=xt(Ne(e),t.separable_conv1,[1,1]),e=xt(Ne(e),t.separable_conv2,[1,1]),e=pe(e,r),e}var kx=function(r){se(t,r);function t(e){var n=r.call(this,"TinyXception")||this;return n._numMainBlocks=e,n}return t.prototype.forwardInput=function(e){var n=this,o=this.params;if(!o)throw new Error("TinyXception - load model before inference");return Y(function(){var a=e.toBatchTensor(112,!0),i=[122.782,117.001,104.298],s=to(a,i).div(K(256)),u=Ne(Of(s,o.entry_flow.conv_in,[2,2]));return u=ei(u,o.entry_flow.reduction_block_0,!1),u=ei(u,o.entry_flow.reduction_block_1),qr(n._numMainBlocks,0,1).forEach(function(c){u=Sx(u,o.middle_flow["main_block_"+c])}),u=ei(u,o.exit_flow.reduction_block),u=Ne(xt(u,o.exit_flow.separable_conv,[1,1])),u})},t.prototype.forward=function(e){return te(this,void 0,void 0,function(){var n;return ne(this,function(o){switch(o.label){case 0:return n=this.forwardInput,[4,qe(e)];case 1:return[2,n.apply(this,[o.sent()])]}})})},t.prototype.getDefaultModelName=function(){return"tiny_xception_model"},t.prototype.extractParamsFromWeigthMap=function(e){return Ex(e,this._numMainBlocks)},t.prototype.extractParams=function(e){return Cx(e,this._numMainBlocks)},t}(hn);function Rx(r){var t=[],e=dn(r),n=e.extractWeights,o=e.getRemainingWeights,a=Os(n,t),i=a(512,1,"fc/age"),s=a(512,2,"fc/gender");if(o().length!==0)throw new Error("weights remaing after extract: "+o().length);return{paramMappings:t,params:{fc:{age:i,gender:s}}}}function Ix(r){var t=[],e=Dn(r,t);function n(a){var i=e(a+"/weights",2),s=e(a+"/bias",1);return{weights:i,bias:s}}var o={fc:{age:n("fc/age"),gender:n("fc/gender")}};return fn(r,t),{params:o,paramMappings:t}}var $o;(function(r){r.FEMALE="female",r.MALE="male"})($o||($o={}));var Ax=function(r){se(t,r);function t(e){e===void 0&&(e=new kx(2));var n=r.call(this,"AgeGenderNet")||this;return n._faceFeatureExtractor=e,n}return Object.defineProperty(t.prototype,"faceFeatureExtractor",{get:function(){return this._faceFeatureExtractor},enumerable:!0,configurable:!0}),t.prototype.runNet=function(e){var n=this,o=this.params;if(!o)throw new Error(this._name+" - load model before inference");return Y(function(){var a=e instanceof Xo?n.faceFeatureExtractor.forwardInput(e):e,i=Zr(a,[7,7],[2,2],"valid").as2D(a.shape[0],-1),s=Kt(i,o.fc.age).as1D(),u=Kt(i,o.fc.gender);return{age:s,gender:u}})},t.prototype.forwardInput=function(e){var n=this;return Y(function(){var o=n.runNet(e),a=o.age,i=o.gender;return{age:a,gender:cn(i)}})},t.prototype.forward=function(e){return te(this,void 0,void 0,function(){var n;return ne(this,function(o){switch(o.label){case 0:return n=this.forwardInput,[4,qe(e)];case 1:return[2,n.apply(this,[o.sent()])]}})})},t.prototype.predictAgeAndGender=function(e){return te(this,void 0,void 0,function(){var n,o,a,i,s,u,c=this;return ne(this,function(l){switch(l.label){case 0:return[4,qe(e)];case 1:return n=l.sent(),[4,this.forwardInput(n)];case 2:return o=l.sent(),a=ze(o.age),i=ze(o.gender),s=a.map(function(h,f){return{ageTensor:h,genderTensor:i[f]}}),[4,Promise.all(s.map(function(h){var f=h.ageTensor,d=h.genderTensor;return te(c,void 0,void 0,function(){var p,v,m,g,x;return ne(this,function(y){switch(y.label){case 0:return[4,f.data()];case 1:return p=y.sent()[0],[4,d.data()];case 2:return v=y.sent()[0],m=v>.5,g=m?$o.MALE:$o.FEMALE,x=m?v:1-v,f.dispose(),d.dispose(),[2,{age:p,gender:g,genderProbability:x}]}})})}))];case 3:return u=l.sent(),o.age.dispose(),o.gender.dispose(),[2,n.isBatchInput?u:u[0]]}})})},t.prototype.getDefaultModelName=function(){return"age_gender_model"},t.prototype.dispose=function(e){e===void 0&&(e=!0),this.faceFeatureExtractor.dispose(e),r.prototype.dispose.call(this,e)},t.prototype.loadClassifierParams=function(e){var n=this.extractClassifierParams(e),o=n.params,a=n.paramMappings;this._params=o,this._paramMappings=a},t.prototype.extractClassifierParams=function(e){return Rx(e)},t.prototype.extractParamsFromWeigthMap=function(e){var n=Ff(e),o=n.featureExtractorMap,a=n.classifierMap;return this.faceFeatureExtractor.loadFromWeightMap(o),Ix(a)},t.prototype.extractParams=function(e){var n=1539,o=e.slice(0,e.length-n),a=e.slice(e.length-n);return this.faceFeatureExtractor.extractWeights(o),this.extractClassifierParams(a)},t}(hn),Bf=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.postProcess=function(e,n,o){var a=o.map(function(s){var u=s.width,c=s.height,l=n/Math.max(c,u);return{width:u*l,height:c*l}}),i=a.length;return Y(function(){var s=function(f,d){return bt([Xt([68],f),Xt([68],d)],1).as2D(1,136).as1D()},u=function(f,d){var p=a[f],v=p.width,m=p.height;return d(v,m)?Math.abs(v-m)/2:0},c=function(f){return u(f,function(d,p){return d<p})},l=function(f){return u(f,function(d,p){return p<d})},h=e.mul(Xt([i,136],n)).sub(bt(Array.from(Array(i),function(f,d){return s(c(d),l(d))}))).div(bt(Array.from(Array(i),function(f,d){return s(a[d].width,a[d].height)})));return h})},t.prototype.forwardInput=function(e){var n=this;return Y(function(){var o=n.runNet(e);return n.postProcess(o,e.inputSize,e.inputDimensions.map(function(a){var i=a[0],s=a[1];return{height:i,width:s}}))})},t.prototype.forward=function(e){return te(this,void 0,void 0,function(){var n;return ne(this,function(o){switch(o.label){case 0:return n=this.forwardInput,[4,qe(e)];case 1:return[2,n.apply(this,[o.sent()])]}})})},t.prototype.detectLandmarks=function(e){return te(this,void 0,void 0,function(){var n,o,a,i=this;return ne(this,function(s){switch(s.label){case 0:return[4,qe(e)];case 1:return n=s.sent(),o=Y(function(){return ze(i.forwardInput(n))}),[4,Promise.all(o.map(function(u,c){return te(i,void 0,void 0,function(){var l,h,f,d,p;return ne(this,function(v){switch(v.label){case 0:return f=(h=Array).from,[4,u.data()];case 1:return l=f.apply(h,[v.sent()]),d=l.filter(function(m,g){return $u(g)}),p=l.filter(function(m,g){return!$u(g)}),[2,new gf(Array(68).fill(0).map(function(m,g){return new xe(d[g],p[g])}),{height:n.getInputHeight(c),width:n.getInputWidth(c)})]}})})}))];case 2:return a=s.sent(),o.forEach(function(u){return u.dispose()}),[2,n.isBatchInput?a:a[0]]}})})},t.prototype.getClassifierChannelsOut=function(){return 136},t}(Pf),Lf=function(r){se(t,r);function t(e){return e===void 0&&(e=new Nf),r.call(this,"FaceLandmark68Net",e)||this}return t.prototype.getDefaultModelName=function(){return"face_landmark_68_model"},t.prototype.getClassifierChannelsIn=function(){return 256},t}(Bf);function Dx(r){var t=[],e=Tf(r,t).extractDenseBlock3Params,n={dense0:e("dense0",!0),dense1:e("dense1"),dense2:e("dense2")};return fn(r,t),{params:n,paramMappings:t}}function Tx(r){var t=[],e=dn(r),n=e.extractWeights,o=e.getRemainingWeights,a=Af(n,t).extractDenseBlock3Params,i=a(3,32,"dense0",!0),s=a(32,64,"dense1"),u=a(64,128,"dense2");if(o().length!==0)throw new Error("weights remaing after extract: "+o().length);return{paramMappings:t,params:{dense0:i,dense1:s,dense2:u}}}var Nx=function(r){se(t,r);function t(){return r.call(this,"TinyFaceFeatureExtractor")||this}return t.prototype.forwardInput=function(e){var n=this.params;if(!n)throw new Error("TinyFaceFeatureExtractor - load model before inference");return Y(function(){var o=e.toBatchTensor(112,!0),a=[122.782,117.001,104.298],i=to(o,a).div(K(255)),s=Za(i,n.dense0,!0);return s=Za(s,n.dense1),s=Za(s,n.dense2),s=Zr(s,[14,14],[2,2],"valid"),s})},t.prototype.forward=function(e){return te(this,void 0,void 0,function(){var n;return ne(this,function(o){switch(o.label){case 0:return n=this.forwardInput,[4,qe(e)];case 1:return[2,n.apply(this,[o.sent()])]}})})},t.prototype.getDefaultModelName=function(){return"face_feature_extractor_tiny_model"},t.prototype.extractParamsFromWeigthMap=function(e){return Dx(e)},t.prototype.extractParams=function(e){return Tx(e)},t}(hn),Fx=function(r){se(t,r);function t(e){return e===void 0&&(e=new Nx),r.call(this,"FaceLandmark68TinyNet",e)||this}return t.prototype.getDefaultModelName=function(){return"face_landmark_68_tiny_model"},t.prototype.getClassifierChannelsIn=function(){return 128},t}(Bf);(function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t})(Lf);function Px(r,t){return pe(et(r,t.weights),t.biases)}function Vs(r,t,e,n,o){o===void 0&&(o="same");var a=t.conv,i=a.filters,s=a.bias,u=It(r,i,e,o);return u=pe(u,s),u=Px(u,t.scale),n?Ne(u):u}function Mx(r,t){return Vs(r,t,[1,1],!0)}function Wf(r,t){return Vs(r,t,[1,1],!1)}function Vf(r,t){return Vs(r,t,[2,2],!0,"valid")}function Ox(r,t){function e(s,u,c){var l=r(s),h=l.length/(u*c*c);if(X0(h))throw new Error("depth has to be an integer: "+h+", weights.length: "+l.length+", numFilters: "+u+", filterSize: "+c);return Y(function(){return _n(lt(l,[u,h,c,c]),[2,3,1,0])})}function n(s,u,c,l){var h=e(s,u,c),f=Me(r(u));return t.push({paramPath:l+"/filters"},{paramPath:l+"/bias"}),{filters:h,bias:f}}function o(s,u){var c=Me(r(s)),l=Me(r(s));return t.push({paramPath:u+"/weights"},{paramPath:u+"/biases"}),{weights:c,biases:l}}function a(s,u,c,l){var h=n(s,u,c,l+"/conv"),f=o(u,l+"/scale");return{conv:h,scale:f}}function i(s,u,c,l,h){h===void 0&&(h=!1);var f=a((h?.5:1)*s,u,c,l+"/conv1"),d=a(s,u,c,l+"/conv2");return{conv1:f,conv2:d}}return{extractConvLayerParams:a,extractResidualLayerParams:i}}function Bx(r){var t=dn(r),e=t.extractWeights,n=t.getRemainingWeights,o=[],a=Ox(e,o),i=a.extractConvLayerParams,s=a.extractResidualLayerParams,u=i(4704,32,7,"conv32_down"),c=s(9216,32,3,"conv32_1"),l=s(9216,32,3,"conv32_2"),h=s(9216,32,3,"conv32_3"),f=s(36864,64,3,"conv64_down",!0),d=s(36864,64,3,"conv64_1"),p=s(36864,64,3,"conv64_2"),v=s(36864,64,3,"conv64_3"),m=s(147456,128,3,"conv128_down",!0),g=s(147456,128,3,"conv128_1"),x=s(147456,128,3,"conv128_2"),y=s(589824,256,3,"conv256_down",!0),b=s(589824,256,3,"conv256_1"),w=s(589824,256,3,"conv256_2"),C=s(589824,256,3,"conv256_down_out"),I=Y(function(){return _n(bn(e(256*128),[128,256]),[1,0])});if(o.push({paramPath:"fc"}),n().length!==0)throw new Error("weights remaing after extract: "+n().length);var E={conv32_down:u,conv32_1:c,conv32_2:l,conv32_3:h,conv64_down:f,conv64_1:d,conv64_2:p,conv64_3:v,conv128_down:m,conv128_1:g,conv128_2:x,conv256_down:y,conv256_1:b,conv256_2:w,conv256_down_out:C,fc:I};return{params:E,paramMappings:o}}function Lx(r,t){var e=Dn(r,t);function n(i){var s=e(i+"/scale/weights",1),u=e(i+"/scale/biases",1);return{weights:s,biases:u}}function o(i){var s=e(i+"/conv/filters",4),u=e(i+"/conv/bias",1),c=n(i);return{conv:{filters:s,bias:u},scale:c}}function a(i){return{conv1:o(i+"/conv1"),conv2:o(i+"/conv2")}}return{extractConvLayerParams:o,extractResidualLayerParams:a}}function Wx(r){var t=[],e=Lx(r,t),n=e.extractConvLayerParams,o=e.extractResidualLayerParams,a=n("conv32_down"),i=o("conv32_1"),s=o("conv32_2"),u=o("conv32_3"),c=o("conv64_down"),l=o("conv64_1"),h=o("conv64_2"),f=o("conv64_3"),d=o("conv128_down"),p=o("conv128_1"),v=o("conv128_2"),m=o("conv256_down"),g=o("conv256_1"),x=o("conv256_2"),y=o("conv256_down_out"),b=r.fc;if(t.push({originalPath:"fc",paramPath:"fc"}),!K0(b))throw new Error("expected weightMap[fc] to be a Tensor2D, instead have "+b);var w={conv32_down:a,conv32_1:i,conv32_2:s,conv32_3:u,conv64_down:c,conv64_1:l,conv64_2:h,conv64_3:f,conv128_down:d,conv128_1:p,conv128_2:v,conv256_down:m,conv256_1:g,conv256_2:x,conv256_down_out:y,fc:b};return fn(r,t),{params:w,paramMappings:t}}function Gt(r,t){var e=Mx(r,t.conv1);return e=Wf(e,t.conv2),e=pe(e,r),e=Ne(e),e}function mo(r,t){var e=Vf(r,t.conv1);e=Wf(e,t.conv2);var n=Zr(r,2,2,"valid"),o=Ie(n.shape),a=n.shape[3]!==e.shape[3],i=n.shape[1]!==e.shape[1]||n.shape[2]!==e.shape[2];if(i){var s=Nr(e.shape);s[1]=1;var u=Ie(s);e=Ve([e,u],1);var c=Nr(e.shape);c[2]=1;var l=Ie(c);e=Ve([e,l],2)}return n=a?Ve([n,o],3):n,e=pe(n,e),e=Ne(e),e}var Vx=function(r){se(t,r);function t(){return r.call(this,"FaceRecognitionNet")||this}return t.prototype.forwardInput=function(e){var n=this.params;if(!n)throw new Error("FaceRecognitionNet - load model before inference");return Y(function(){var o=e.toBatchTensor(150,!0).toFloat(),a=[122.782,117.001,104.298],i=to(o,a).div(K(256)),s=Vf(i,n.conv32_down);s=Ge(s,3,2,"valid"),s=Gt(s,n.conv32_1),s=Gt(s,n.conv32_2),s=Gt(s,n.conv32_3),s=mo(s,n.conv64_down),s=Gt(s,n.conv64_1),s=Gt(s,n.conv64_2),s=Gt(s,n.conv64_3),s=mo(s,n.conv128_down),s=Gt(s,n.conv128_1),s=Gt(s,n.conv128_2),s=mo(s,n.conv256_down),s=Gt(s,n.conv256_1),s=Gt(s,n.conv256_2),s=mo(s,n.conv256_down_out);var u=s.mean([1,2]),c=fa(u,n.fc);return c})},t.prototype.forward=function(e){return te(this,void 0,void 0,function(){var n;return ne(this,function(o){switch(o.label){case 0:return n=this.forwardInput,[4,qe(e)];case 1:return[2,n.apply(this,[o.sent()])]}})})},t.prototype.computeFaceDescriptor=function(e){return te(this,void 0,void 0,function(){var n,o,a,i=this;return ne(this,function(s){switch(s.label){case 0:return[4,qe(e)];case 1:return n=s.sent(),o=Y(function(){return ze(i.forwardInput(n))}),[4,Promise.all(o.map(function(u){return u.data()}))];case 2:return a=s.sent(),o.forEach(function(u){return u.dispose()}),[2,n.isBatchInput?a:a[0]]}})})},t.prototype.getDefaultModelName=function(){return"face_recognition_model"},t.prototype.extractParamsFromWeigthMap=function(e){return Wx(e)},t.prototype.extractParams=function(e){return Bx(e)},t}(hn);function zf(r,t){var e={descriptor:t};return Object.assign({},r,e)}function Uf(r,t){var e={age:t};return Object.assign({},r,e)}function Gf(r,t,e){var n={gender:t,genderProbability:e};return Object.assign({},r,n)}var Hf=function(){function r(t){var e=t===void 0?{}:t,n=e.minFaceSize,o=e.scaleFactor,a=e.maxNumScales,i=e.scoreThresholds,s=e.scaleSteps;if(this._name="MtcnnOptions",this._minFaceSize=n||20,this._scaleFactor=o||.709,this._maxNumScales=a||10,this._scoreThresholds=i||[.6,.7,.7],this._scaleSteps=s,typeof this._minFaceSize!="number"||this._minFaceSize<0)throw new Error(this._name+" - expected minFaceSize to be a number > 0");if(typeof this._scaleFactor!="number"||this._scaleFactor<=0||this._scaleFactor>=1)throw new Error(this._name+" - expected scaleFactor to be a number between 0 and 1");if(typeof this._maxNumScales!="number"||this._maxNumScales<0)throw new Error(this._name+" - expected maxNumScales to be a number > 0");if(!Array.isArray(this._scoreThresholds)||this._scoreThresholds.length!==3||this._scoreThresholds.some(function(u){return typeof u!="number"}))throw new Error(this._name+" - expected scoreThresholds to be an array of numbers of length 3");if(this._scaleSteps&&(!Array.isArray(this._scaleSteps)||this._scaleSteps.some(function(u){return typeof u!="number"})))throw new Error(this._name+" - expected scaleSteps to be an array of numbers")}return Object.defineProperty(r.prototype,"minFaceSize",{get:function(){return this._minFaceSize},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"scaleFactor",{get:function(){return this._scaleFactor},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"maxNumScales",{get:function(){return this._maxNumScales},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"scoreThresholds",{get:function(){return this._scoreThresholds},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"scaleSteps",{get:function(){return this._scaleSteps},enumerable:!0,configurable:!0}),r}();function zx(r,t){function e(u,c){var l=lt(r(9*u),[3,3,u,1]),h=Me(r(u)),f=Me(r(u)),d=Me(r(u)),p=Me(r(u));return t.push({paramPath:c+"/filters"},{paramPath:c+"/batch_norm_scale"},{paramPath:c+"/batch_norm_offset"},{paramPath:c+"/batch_norm_mean"},{paramPath:c+"/batch_norm_variance"}),{filters:l,batch_norm_scale:h,batch_norm_offset:f,batch_norm_mean:d,batch_norm_variance:p}}function n(u,c,l,h,f){var d=lt(r(u*c*l*l),[l,l,u,c]),p=Me(r(c));return t.push({paramPath:h+"/filters"},{paramPath:h+"/"+(f?"batch_norm_offset":"bias")}),{filters:d,bias:p}}function o(u,c,l,h){var f=n(u,c,l,h,!0),d=f.filters,p=f.bias;return{filters:d,batch_norm_offset:p}}function a(u,c,l){var h=e(u,l+"/depthwise_conv"),f=o(u,c,1,l+"/pointwise_conv");return{depthwise_conv:h,pointwise_conv:f}}function i(){var u=o(3,32,3,"mobilenetv1/conv_0"),c=a(32,64,"mobilenetv1/conv_1"),l=a(64,128,"mobilenetv1/conv_2"),h=a(128,128,"mobilenetv1/conv_3"),f=a(128,256,"mobilenetv1/conv_4"),d=a(256,256,"mobilenetv1/conv_5"),p=a(256,512,"mobilenetv1/conv_6"),v=a(512,512,"mobilenetv1/conv_7"),m=a(512,512,"mobilenetv1/conv_8"),g=a(512,512,"mobilenetv1/conv_9"),x=a(512,512,"mobilenetv1/conv_10"),y=a(512,512,"mobilenetv1/conv_11"),b=a(512,1024,"mobilenetv1/conv_12"),w=a(1024,1024,"mobilenetv1/conv_13");return{conv_0:u,conv_1:c,conv_2:l,conv_3:h,conv_4:f,conv_5:d,conv_6:p,conv_7:v,conv_8:m,conv_9:g,conv_10:x,conv_11:y,conv_12:b,conv_13:w}}function s(){var u=o(1024,256,1,"prediction_layer/conv_0"),c=o(256,512,3,"prediction_layer/conv_1"),l=o(512,128,1,"prediction_layer/conv_2"),h=o(128,256,3,"prediction_layer/conv_3"),f=o(256,128,1,"prediction_layer/conv_4"),d=o(128,256,3,"prediction_layer/conv_5"),p=o(256,64,1,"prediction_layer/conv_6"),v=o(64,128,3,"prediction_layer/conv_7"),m=n(512,12,1,"prediction_layer/box_predictor_0/box_encoding_predictor"),g=n(512,9,1,"prediction_layer/box_predictor_0/class_predictor"),x=n(1024,24,1,"prediction_layer/box_predictor_1/box_encoding_predictor"),y=n(1024,18,1,"prediction_layer/box_predictor_1/class_predictor"),b=n(512,24,1,"prediction_layer/box_predictor_2/box_encoding_predictor"),w=n(512,18,1,"prediction_layer/box_predictor_2/class_predictor"),C=n(256,24,1,"prediction_layer/box_predictor_3/box_encoding_predictor"),I=n(256,18,1,"prediction_layer/box_predictor_3/class_predictor"),E=n(256,24,1,"prediction_layer/box_predictor_4/box_encoding_predictor"),S=n(256,18,1,"prediction_layer/box_predictor_4/class_predictor"),k=n(128,24,1,"prediction_layer/box_predictor_5/box_encoding_predictor"),T=n(128,18,1,"prediction_layer/box_predictor_5/class_predictor"),A={box_encoding_predictor:m,class_predictor:g},F={box_encoding_predictor:x,class_predictor:y},P={box_encoding_predictor:b,class_predictor:w},B={box_encoding_predictor:C,class_predictor:I},U={box_encoding_predictor:E,class_predictor:S},z={box_encoding_predictor:k,class_predictor:T};return{conv_0:u,conv_1:c,conv_2:l,conv_3:h,conv_4:f,conv_5:d,conv_6:p,conv_7:v,box_predictor_0:A,box_predictor_1:F,box_predictor_2:P,box_predictor_3:B,box_predictor_4:U,box_predictor_5:z}}return{extractMobilenetV1Params:i,extractPredictionLayerParams:s}}function Ux(r){var t=[],e=dn(r),n=e.extractWeights,o=e.getRemainingWeights,a=zx(n,t),i=a.extractMobilenetV1Params,s=a.extractPredictionLayerParams,u=i(),c=s(),l=Qi(n(5118*4),[1,5118,4]),h={extra_dim:l};if(t.push({paramPath:"output_layer/extra_dim"}),o().length!==0)throw new Error("weights remaing after extract: "+o().length);return{params:{mobilenetv1:u,prediction_layer:c,output_layer:h},paramMappings:t}}function Gx(r,t){var e=Dn(r,t);function n(c,l,h){var f=e(c+"/Conv2d_"+l+"_pointwise/weights",4,h+"/filters"),d=e(c+"/Conv2d_"+l+"_pointwise/convolution_bn_offset",1,h+"/batch_norm_offset");return{filters:f,batch_norm_offset:d}}function o(c){var l="mobilenetv1/conv_"+c,h="MobilenetV1/Conv2d_"+c+"_depthwise",f=l+"/depthwise_conv",d=l+"/pointwise_conv",p=e(h+"/depthwise_weights",4,f+"/filters"),v=e(h+"/BatchNorm/gamma",1,f+"/batch_norm_scale"),m=e(h+"/BatchNorm/beta",1,f+"/batch_norm_offset"),g=e(h+"/BatchNorm/moving_mean",1,f+"/batch_norm_mean"),x=e(h+"/BatchNorm/moving_variance",1,f+"/batch_norm_variance");return{depthwise_conv:{filters:p,batch_norm_scale:v,batch_norm_offset:m,batch_norm_mean:g,batch_norm_variance:x},pointwise_conv:n("MobilenetV1",c,d)}}function a(){return{conv_0:n("MobilenetV1",0,"mobilenetv1/conv_0"),conv_1:o(1),conv_2:o(2),conv_3:o(3),conv_4:o(4),conv_5:o(5),conv_6:o(6),conv_7:o(7),conv_8:o(8),conv_9:o(9),conv_10:o(10),conv_11:o(11),conv_12:o(12),conv_13:o(13)}}function i(c,l){var h=e(c+"/weights",4,l+"/filters"),f=e(c+"/biases",1,l+"/bias");return{filters:h,bias:f}}function s(c){var l=i("Prediction/BoxPredictor_"+c+"/BoxEncodingPredictor","prediction_layer/box_predictor_"+c+"/box_encoding_predictor"),h=i("Prediction/BoxPredictor_"+c+"/ClassPredictor","prediction_layer/box_predictor_"+c+"/class_predictor");return{box_encoding_predictor:l,class_predictor:h}}function u(){return{conv_0:n("Prediction",0,"prediction_layer/conv_0"),conv_1:n("Prediction",1,"prediction_layer/conv_1"),conv_2:n("Prediction",2,"prediction_layer/conv_2"),conv_3:n("Prediction",3,"prediction_layer/conv_3"),conv_4:n("Prediction",4,"prediction_layer/conv_4"),conv_5:n("Prediction",5,"prediction_layer/conv_5"),conv_6:n("Prediction",6,"prediction_layer/conv_6"),conv_7:n("Prediction",7,"prediction_layer/conv_7"),box_predictor_0:s(0),box_predictor_1:s(1),box_predictor_2:s(2),box_predictor_3:s(3),box_predictor_4:s(4),box_predictor_5:s(5)}}return{extractMobilenetV1Params:a,extractPredictionLayerParams:u}}function Hx(r){var t=[],e=Gx(r,t),n=e.extractMobilenetV1Params,o=e.extractPredictionLayerParams,a=r["Output/extra_dim"];if(t.push({originalPath:"Output/extra_dim",paramPath:"output_layer/extra_dim"}),!ma(a))throw new Error("expected weightMap['Output/extra_dim'] to be a Tensor3D, instead have "+a);var i={mobilenetv1:n(),prediction_layer:o(),output_layer:{extra_dim:a}};return fn(r,t),{params:i,paramMappings:t}}function Ht(r,t,e){return Y(function(){var n=It(r,t.filters,e,"same");return n=pe(n,t.batch_norm_offset),ps(n,0,6)})}var qx=.0010000000474974513;function jx(r,t,e){return Y(function(){var n=ha(r,t.filters,e,"same");return n=Nh(n,t.batch_norm_mean,t.batch_norm_variance,t.batch_norm_offset,t.batch_norm_scale,qx),ps(n,0,6)})}function Kx(r){return[2,4,6,12].some(function(t){return t===r})?[2,2]:[1,1]}function Xx(r,t){return Y(function(){var e=null,n=Ht(r,t.conv_0,[2,2]),o=[t.conv_1,t.conv_2,t.conv_3,t.conv_4,t.conv_5,t.conv_6,t.conv_7,t.conv_8,t.conv_9,t.conv_10,t.conv_11,t.conv_12,t.conv_13];if(o.forEach(function(a,i){var s=i+1,u=Kx(s);n=jx(n,a.depthwise_conv,u),n=Ht(n,a.pointwise_conv,[1,1]),s===11&&(e=n)}),e===null)throw new Error("mobileNetV1 - output of conv layer 11 is null");return{out:n,conv11:e}})}function $x(r,t,e,n,o){var a=r.shape[0],i=Math.min(e,a),s=t.map(function(l,h){return{score:l,boxIndex:h}}).filter(function(l){return l.score>o}).sort(function(l,h){return h.score-l.score}),u=function(l){return l<=n?1:0},c=[];return s.forEach(function(l){if(!(c.length>=i)){for(var h=l.score,f=c.length-1;f>=0;--f){var d=Yx(r,l.boxIndex,c[f]);if(d!==0&&(l.score*=u(d),l.score<=o))break}h===l.score&&c.push(l.boxIndex)}}),c}function Yx(r,t,e){var n=r.arraySync(),o=Math.min(n[t][0],n[t][2]),a=Math.min(n[t][1],n[t][3]),i=Math.max(n[t][0],n[t][2]),s=Math.max(n[t][1],n[t][3]),u=Math.min(n[e][0],n[e][2]),c=Math.min(n[e][1],n[e][3]),l=Math.max(n[e][0],n[e][2]),h=Math.max(n[e][1],n[e][3]),f=(i-o)*(s-a),d=(l-u)*(h-c);if(f<=0||d<=0)return 0;var p=Math.max(o,u),v=Math.max(a,c),m=Math.min(i,l),g=Math.min(s,h),x=Math.max(m-p,0)*Math.max(g-v,0);return x/(f+d-x)}function Jx(r){var t=ze(_n(r,[1,0])),e=[Ue(t[2],t[0]),Ue(t[3],t[1])],n=[pe(t[0],Tt(e[0],K(2))),pe(t[1],Tt(e[1],K(2)))];return{sizes:e,centers:n}}function Qx(r,t){var e=Jx(r),n=e.sizes,o=e.centers,a=ze(_n(t,[1,0])),i=Tt(et(Ci(Tt(a[2],K(5))),n[0]),K(2)),s=pe(et(Tt(a[0],K(10)),n[0]),o[0]),u=Tt(et(Ci(Tt(a[3],K(5))),n[1]),K(2)),c=pe(et(Tt(a[1],K(10)),n[1]),o[1]);return _n(bt([Ue(s,i),Ue(c,u),pe(s,i),pe(c,u)]),[1,0])}function Zx(r,t,e){return Y(function(){var n=r.shape[0],o=Qx(Nt(ir(e.extra_dim,[n,1,1]),[-1,4]),Nt(r,[-1,4]));o=Nt(o,[n,o.shape[0]/n,4]);var a=Ih(Jt(t,[0,0,1],[-1,-1,-1])),i=Jt(a,[0,0,0],[-1,-1,1]);i=Nt(i,[n,i.shape[1]]);var s=ze(o),u=ze(i);return{boxes:s,scores:u}})}function er(r,t){return Y(function(){var e=r.shape[0],n=Nt(Pt(r,t.box_encoding_predictor),[e,-1,1,4]),o=Nt(Pt(r,t.class_predictor),[e,-1,3]);return{boxPredictionEncoding:n,classPrediction:o}})}function eb(r,t,e){return Y(function(){var n=Ht(r,e.conv_0,[1,1]),o=Ht(n,e.conv_1,[2,2]),a=Ht(o,e.conv_2,[1,1]),i=Ht(a,e.conv_3,[2,2]),s=Ht(i,e.conv_4,[1,1]),u=Ht(s,e.conv_5,[2,2]),c=Ht(u,e.conv_6,[1,1]),l=Ht(c,e.conv_7,[2,2]),h=er(t,e.box_predictor_0),f=er(r,e.box_predictor_1),d=er(o,e.box_predictor_2),p=er(i,e.box_predictor_3),v=er(u,e.box_predictor_4),m=er(l,e.box_predictor_5),g=Ve([h.boxPredictionEncoding,f.boxPredictionEncoding,d.boxPredictionEncoding,p.boxPredictionEncoding,v.boxPredictionEncoding,m.boxPredictionEncoding],1),x=Ve([h.classPrediction,f.classPrediction,d.classPrediction,p.classPrediction,v.classPrediction,m.classPrediction],1);return{boxPredictions:g,classPredictions:x}})}var wa=function(){function r(t){var e=t===void 0?{}:t,n=e.minConfidence,o=e.maxResults;if(this._name="SsdMobilenetv1Options",this._minConfidence=n||.5,this._maxResults=o||100,typeof this._minConfidence!="number"||this._minConfidence<=0||this._minConfidence>=1)throw new Error(this._name+" - expected minConfidence to be a number between 0 and 1");if(typeof this._maxResults!="number")throw new Error(this._name+" - expected maxResults to be a number")}return Object.defineProperty(r.prototype,"minConfidence",{get:function(){return this._minConfidence},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"maxResults",{get:function(){return this._maxResults},enumerable:!0,configurable:!0}),r}(),qf=function(r){se(t,r);function t(){return r.call(this,"SsdMobilenetv1")||this}return t.prototype.forwardInput=function(e){var n=this.params;if(!n)throw new Error("SsdMobilenetv1 - load model before inference");return Y(function(){var o=e.toBatchTensor(512,!1).toFloat(),a=Ue(et(o,K(.007843137718737125)),K(1)),i=Xx(a,n.mobilenetv1),s=eb(i.out,i.conv11,n.prediction_layer),u=s.boxPredictions,c=s.classPredictions;return Zx(u,c,n.output_layer)})},t.prototype.forward=function(e){return te(this,void 0,void 0,function(){var n;return ne(this,function(o){switch(o.label){case 0:return n=this.forwardInput,[4,qe(e)];case 1:return[2,n.apply(this,[o.sent()])]}})})},t.prototype.locateFaces=function(e,n){return n===void 0&&(n={}),te(this,void 0,void 0,function(){var o,a,i,s,u,c,l,h,f,d,p,v,m,g,x,y,b,w,C,I,E;return ne(this,function(S){switch(S.label){case 0:return o=new wa(n),a=o.maxResults,i=o.minConfidence,[4,qe(e)];case 1:for(s=S.sent(),u=this.forwardInput(s),c=u.boxes,l=u.scores,h=c[0],f=l[0],d=1;d<c.length;d++)c[d].dispose(),l[d].dispose();return m=(v=Array).from,[4,f.data()];case 2:return p=m.apply(v,[S.sent()]),g=.5,x=$x(h,p,a,g,i),y=s.getReshapedInputDimensions(0),b=s.inputSize,w=b/y.width,C=b/y.height,I=h.arraySync(),E=x.map(function(k){var T=[Math.max(0,I[k][0]),Math.min(1,I[k][2])].map(function(z){return z*C}),A=T[0],F=T[1],P=[Math.max(0,I[k][1]),Math.min(1,I[k][3])].map(function(z){return z*w}),B=P[0],U=P[1];return new Wt(p[k],new Ts(B,A,U-B,F-A),{height:s.getInputHeight(0),width:s.getInputWidth(0)})}),h.dispose(),f.dispose(),[2,E]}})})},t.prototype.getDefaultModelName=function(){return"ssd_mobilenetv1_model"},t.prototype.extractParamsFromWeigthMap=function(e){return Hx(e)},t.prototype.extractParams=function(e){return Ux(e)},t}(hn);(function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t})(qf);var tb=.4,nb=[new xe(.738768,.874946),new xe(2.42204,2.65704),new xe(4.30971,7.04493),new xe(10.246,4.59428),new xe(12.6868,11.8741)],rb=[new xe(1.603231,2.094468),new xe(6.041143,7.080126),new xe(2.882459,3.518061),new xe(4.266906,5.178857),new xe(9.041765,10.66308)],ob=[117.001,114.697,97.404],ab="tiny_yolov2_model",ib="tiny_yolov2_separable_conv_model",go=function(r){return typeof r=="number"};function sb(r){if(!r)throw new Error("invalid config: "+r);if(typeof r.withSeparableConvs!="boolean")throw new Error("config.withSeparableConvs has to be a boolean, have: "+r.withSeparableConvs);if(!go(r.iouThreshold)||r.iouThreshold<0||r.iouThreshold>1)throw new Error("config.iouThreshold has to be a number between [0, 1], have: "+r.iouThreshold);if(!Array.isArray(r.classes)||!r.classes.length||!r.classes.every(function(t){return typeof t=="string"}))throw new Error("config.classes has to be an array class names: string[], have: "+JSON.stringify(r.classes));if(!Array.isArray(r.anchors)||!r.anchors.length||!r.anchors.map(function(t){return t||{}}).every(function(t){return go(t.x)&&go(t.y)}))throw new Error("config.anchors has to be an array of { x: number, y: number }, have: "+JSON.stringify(r.anchors));if(r.meanRgb&&(!Array.isArray(r.meanRgb)||r.meanRgb.length!==3||!r.meanRgb.every(go)))throw new Error("config.meanRgb has to be an array of shape [number, number, number], have: "+JSON.stringify(r.meanRgb))}function zs(r){return Y(function(){var t=et(r,K(.10000000149011612));return pe(Ne(Ue(r,t)),t)})}function vn(r,t){return Y(function(){var e=qn(r,[[0,0],[1,1],[1,1],[0,0]]);return e=It(e,t.conv.filters,[1,1],"valid"),e=Ue(e,t.bn.sub),e=et(e,t.bn.truediv),e=pe(e,t.conv.bias),zs(e)})}function mn(r,t){return Y(function(){var e=qn(r,[[0,0],[1,1],[1,1],[0,0]]);return e=ys(e,t.depthwise_filter,t.pointwise_filter,[1,1],"valid"),e=pe(e,t.bias),zs(e)})}function ub(r,t){var e=xa(r,t);function n(i,s){var u=Me(r(i)),c=Me(r(i));return t.push({paramPath:s+"/sub"},{paramPath:s+"/truediv"}),{sub:u,truediv:c}}function o(i,s,u){var c=e(i,s,3,u+"/conv"),l=n(s,u+"/bn");return{conv:c,bn:l}}var a=Bs(r,t);return{extractConvParams:e,extractConvWithBatchNormParams:o,extractSeparableConvParams:a}}function cb(r,t,e,n){var o=dn(r),a=o.extractWeights,i=o.getRemainingWeights,s=[],u=ub(a,s),c=u.extractConvParams,l=u.extractConvWithBatchNormParams,h=u.extractSeparableConvParams,f;if(t.withSeparableConvs){var d=n[0],p=n[1],v=n[2],m=n[3],g=n[4],x=n[5],y=n[6],b=n[7],w=n[8],C=t.isFirstLayerConv2d?c(d,p,3,"conv0"):h(d,p,"conv0"),I=h(p,v,"conv1"),E=h(v,m,"conv2"),S=h(m,g,"conv3"),k=h(g,x,"conv4"),T=h(x,y,"conv5"),A=b?h(y,b,"conv6"):void 0,F=w?h(b,w,"conv7"):void 0,P=c(w||b||y,5*e,1,"conv8");f={conv0:C,conv1:I,conv2:E,conv3:S,conv4:k,conv5:T,conv6:A,conv7:F,conv8:P}}else{var d=n[0],p=n[1],v=n[2],m=n[3],g=n[4],x=n[5],y=n[6],b=n[7],w=n[8],C=l(d,p,"conv0"),I=l(p,v,"conv1"),E=l(v,m,"conv2"),S=l(m,g,"conv3"),k=l(g,x,"conv4"),T=l(x,y,"conv5"),A=l(y,b,"conv6"),F=l(b,w,"conv7"),P=c(w,5*e,1,"conv8");f={conv0:C,conv1:I,conv2:E,conv3:S,conv4:k,conv5:T,conv6:A,conv7:F,conv8:P}}if(i().length!==0)throw new Error("weights remaing after extract: "+i().length);return{params:f,paramMappings:s}}function lb(r,t){var e=Dn(r,t);function n(s){var u=e(s+"/sub",1),c=e(s+"/truediv",1);return{sub:u,truediv:c}}function o(s){var u=e(s+"/filters",4),c=e(s+"/bias",1);return{filters:u,bias:c}}function a(s){var u=o(s+"/conv"),c=n(s+"/bn");return{conv:u,bn:c}}var i=Ls(e);return{extractConvParams:o,extractConvWithBatchNormParams:a,extractSeparableConvParams:i}}function hb(r,t){var e=[],n=lb(r,e),o=n.extractConvParams,a=n.extractConvWithBatchNormParams,i=n.extractSeparableConvParams,s;if(t.withSeparableConvs){var u=t.filterSizes&&t.filterSizes.length||9;s={conv0:t.isFirstLayerConv2d?o("conv0"):i("conv0"),conv1:i("conv1"),conv2:i("conv2"),conv3:i("conv3"),conv4:i("conv4"),conv5:i("conv5"),conv6:u>7?i("conv6"):void 0,conv7:u>8?i("conv7"):void 0,conv8:o("conv8")}}else s={conv0:a("conv0"),conv1:a("conv1"),conv2:a("conv2"),conv3:a("conv3"),conv4:a("conv4"),conv5:a("conv5"),conv6:a("conv6"),conv7:a("conv7"),conv8:o("conv8")};return fn(r,e),{params:s,paramMappings:e}}var rc;(function(r){r[r.XS=224]="XS",r[r.SM=320]="SM",r[r.MD=416]="MD",r[r.LG=608]="LG"})(rc||(rc={}));var Us=function(){function r(t){var e=t===void 0?{}:t,n=e.inputSize,o=e.scoreThreshold;if(this._name="TinyYolov2Options",this._inputSize=n||416,this._scoreThreshold=o||.5,typeof this._inputSize!="number"||this._inputSize%32!==0)throw new Error(this._name+" - expected inputSize to be a number divisible by 32");if(typeof this._scoreThreshold!="number"||this._scoreThreshold<=0||this._scoreThreshold>=1)throw new Error(this._name+" - expected scoreThreshold to be a number between 0 and 1")}return Object.defineProperty(r.prototype,"inputSize",{get:function(){return this._inputSize},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"scoreThreshold",{get:function(){return this._scoreThreshold},enumerable:!0,configurable:!0}),r}(),jf=function(r){se(t,r);function t(e){var n=r.call(this,"TinyYolov2")||this;return sb(e),n._config=e,n}return Object.defineProperty(t.prototype,"config",{get:function(){return this._config},enumerable:!0,configurable:!0}),Object.defineProperty(t.prototype,"withClassScores",{get:function(){return this.config.withClassScores||this.config.classes.length>1},enumerable:!0,configurable:!0}),Object.defineProperty(t.prototype,"boxEncodingSize",{get:function(){return 5+(this.withClassScores?this.config.classes.length:0)},enumerable:!0,configurable:!0}),t.prototype.runTinyYolov2=function(e,n){var o=vn(e,n.conv0);return o=Ge(o,[2,2],[2,2],"same"),o=vn(o,n.conv1),o=Ge(o,[2,2],[2,2],"same"),o=vn(o,n.conv2),o=Ge(o,[2,2],[2,2],"same"),o=vn(o,n.conv3),o=Ge(o,[2,2],[2,2],"same"),o=vn(o,n.conv4),o=Ge(o,[2,2],[2,2],"same"),o=vn(o,n.conv5),o=Ge(o,[2,2],[1,1],"same"),o=vn(o,n.conv6),o=vn(o,n.conv7),Pt(o,n.conv8,"valid",!1)},t.prototype.runMobilenet=function(e,n){var o=this.config.isFirstLayerConv2d?zs(Pt(e,n.conv0,"valid",!1)):mn(e,n.conv0);return o=Ge(o,[2,2],[2,2],"same"),o=mn(o,n.conv1),o=Ge(o,[2,2],[2,2],"same"),o=mn(o,n.conv2),o=Ge(o,[2,2],[2,2],"same"),o=mn(o,n.conv3),o=Ge(o,[2,2],[2,2],"same"),o=mn(o,n.conv4),o=Ge(o,[2,2],[2,2],"same"),o=mn(o,n.conv5),o=Ge(o,[2,2],[1,1],"same"),o=n.conv6?mn(o,n.conv6):o,o=n.conv7?mn(o,n.conv7):o,Pt(o,n.conv8,"valid",!1)},t.prototype.forwardInput=function(e,n){var o=this,a=this.params;if(!a)throw new Error("TinyYolov2 - load model before inference");return Y(function(){var i=e.toBatchTensor(n,!1).toFloat();return i=o.config.meanRgb?to(i,o.config.meanRgb):i,i=i.div(K(256)),o.config.withSeparableConvs?o.runMobilenet(i,a):o.runTinyYolov2(i,a)})},t.prototype.forward=function(e,n){return te(this,void 0,void 0,function(){var o;return ne(this,function(a){switch(a.label){case 0:return o=this.forwardInput,[4,qe(e)];case 1:return[4,o.apply(this,[a.sent(),n])];case 2:return[2,a.sent()]}})})},t.prototype.detect=function(e,n){return n===void 0&&(n={}),te(this,void 0,void 0,function(){var o,a,i,s,u,c,l,h,f,d,p,v,m,g,x=this;return ne(this,function(y){switch(y.label){case 0:return o=new Us(n),a=o.inputSize,i=o.scoreThreshold,[4,qe(e)];case 1:return s=y.sent(),[4,this.forwardInput(s,a)];case 2:return u=y.sent(),c=Y(function(){return ze(u)[0].expandDims()}),l={width:s.getInputWidth(0),height:s.getInputHeight(0)},[4,this.extractBoxes(c,s.getReshapedInputDimensions(0),i)];case 3:return h=y.sent(),u.dispose(),c.dispose(),f=h.map(function(b){return b.box}),d=h.map(function(b){return b.score}),p=h.map(function(b){return b.classScore}),v=h.map(function(b){return x.config.classes[b.label]}),m=jr(f.map(function(b){return b.rescale(a)}),d,this.config.iouThreshold,!0),g=m.map(function(b){return new mf(d[b],p[b],v[b],f[b],l)}),[2,g]}})})},t.prototype.getDefaultModelName=function(){return""},t.prototype.extractParamsFromWeigthMap=function(e){return hb(e,this.config)},t.prototype.extractParams=function(e){var n=this.config.filterSizes||t.DEFAULT_FILTER_SIZES,o=n?n.length:void 0;if(o!==7&&o!==8&&o!==9)throw new Error("TinyYolov2 - expected 7 | 8 | 9 convolutional filters, but found "+o+" filterSizes in config");return cb(e,this.config,this.boxEncodingSize,n)},t.prototype.extractBoxes=function(e,n,o){return te(this,void 0,void 0,function(){var a,i,s,u,c,l,h,f,d,p,v,m,g,x,y,b,w,C,I,E,S,k,T,A,F,P,B,U,z,L=this;return ne(this,function(W){switch(W.label){case 0:return a=n.width,i=n.height,s=Math.max(a,i),u=s/a,c=s/i,l=e.shape[1],h=this.config.anchors.length,f=Y(function(){var G=e.reshape([l,l,h,L.boxEncodingSize]),j=G.slice([0,0,0,0],[l,l,h,4]),q=G.slice([0,0,0,4],[l,l,h,1]),X=L.withClassScores?cn(G.slice([0,0,0,5],[l,l,h,L.config.classes.length]),3):K(0);return[j,q,X]}),d=f[0],p=f[1],v=f[2],m=[],[4,p.array()];case 1:return g=W.sent(),[4,d.array()];case 2:x=W.sent(),y=0,W.label=3;case 3:if(!(y<l))return[3,12];b=0,W.label=4;case 4:if(!(b<l))return[3,11];w=0,W.label=5;case 5:return w<h?(C=Qa(g[y][b][w][0]),!o||C>o?(I=(b+Qa(x[y][b][w][0]))/l*u,E=(y+Qa(x[y][b][w][1]))/l*c,S=Math.exp(x[y][b][w][2])*this.config.anchors[w].x/l*u,k=Math.exp(x[y][b][w][3])*this.config.anchors[w].y/l*c,T=I-S/2,A=E-k/2,F={row:y,col:b,anchor:w},this.withClassScores?[4,this.extractPredictedClass(v,F)]:[3,7]):[3,9]):[3,10];case 6:return z=W.sent(),[3,8];case 7:z={classScore:1,label:0},W.label=8;case 8:P=z,B=P.classScore,U=P.label,m.push(Je({box:new ga(T,A,T+S,A+k),score:C,classScore:C*B,label:U},F)),W.label=9;case 9:return w++,[3,5];case 10:return b++,[3,4];case 11:return y++,[3,3];case 12:return d.dispose(),p.dispose(),v.dispose(),[2,m]}})})},t.prototype.extractPredictedClass=function(e,n){return te(this,void 0,void 0,function(){var o,a,i,s;return ne(this,function(u){switch(u.label){case 0:return o=n.row,a=n.col,i=n.anchor,[4,e.array()];case 1:return s=u.sent(),[2,Array(this.config.classes.length).fill(0).map(function(c,l){return s[o][a][i][l]}).map(function(c,l){return{classScore:c,label:l}}).reduce(function(c,l){return c.classScore>l.classScore?c:l})]}})})},t.DEFAULT_FILTER_SIZES=[3,16,32,64,128,256,512,1024,1024],t}(hn),fb=function(r){se(t,r);function t(e){e===void 0&&(e=!0);var n=this,o=Object.assign({},{withSeparableConvs:e,iouThreshold:tb,classes:["face"]},e?{anchors:rb,meanRgb:ob}:{anchors:nb,withClassScores:!0});return n=r.call(this,o)||this,n}return Object.defineProperty(t.prototype,"withSeparableConvs",{get:function(){return this.config.withSeparableConvs},enumerable:!0,configurable:!0}),Object.defineProperty(t.prototype,"anchors",{get:function(){return this.config.anchors},enumerable:!0,configurable:!0}),t.prototype.locateFaces=function(e,n){return te(this,void 0,void 0,function(){var o;return ne(this,function(a){switch(a.label){case 0:return[4,this.detect(e,n)];case 1:return o=a.sent(),[2,o.map(function(i){return new Wt(i.score,i.relativeBox,{width:i.imageWidth,height:i.imageHeight})})]}})})},t.prototype.getDefaultModelName=function(){return this.withSeparableConvs?ib:ab},t.prototype.extractParamsFromWeigthMap=function(e){return r.prototype.extractParamsFromWeigthMap.call(this,e)},t}(jf),Kf=function(r){se(t,r);function t(){var e=r!==null&&r.apply(this,arguments)||this;return e._name="TinyFaceDetectorOptions",e}return t}(Us),no=function(){function r(){}return r.prototype.then=function(t){return te(this,void 0,void 0,function(){var e;return ne(this,function(n){switch(n.label){case 0:return e=t,[4,this.run()];case 1:return[2,e.apply(void 0,[n.sent()])]}})})},r.prototype.run=function(){return te(this,void 0,void 0,function(){return ne(this,function(t){throw new Error("ComposableTask - run is not implemented")})})},r}();function Ca(r,t,e,n,o){return o===void 0&&(o=function(a){var i=a.alignedRect;return i}),te(this,void 0,void 0,function(){var a,i,s,u,c;return ne(this,function(l){switch(l.label){case 0:return a=r.map(function(h){return Ws(h)?o(h):h.detection}),s=n,s?[3,5]:t instanceof Te?[4,Ms(t,a)]:[3,2];case 1:return u=l.sent(),[3,4];case 2:return[4,Ps(t,a)];case 3:u=l.sent(),l.label=4;case 4:s=u,l.label=5;case 5:return i=s,[4,e(i)];case 6:return c=l.sent(),i.forEach(function(h){return h instanceof Te&&h.dispose()}),[2,c]}})})}function Gs(r,t,e,n,o){return te(this,void 0,void 0,function(){var a=this;return ne(this,function(i){return[2,Ca([r],t,function(s){return te(a,void 0,void 0,function(){return ne(this,function(u){return[2,e(s[0])]})})},n,o)]})})}function db(r){return Y(function(){return bt(ze(r,3).reverse(),3)})}var yo=2,Yo=12;function pb(r,t){var e=xa(r,t),n=Os(r,t);function o(c,l){var h=Me(r(c));return t.push({paramPath:l}),h}function a(c,l,h){h===void 0&&(h=!1);var f=e(c[0],c[1],3,l+"/conv1"),d=o(c[1],l+"/prelu1_alpha"),p=e(c[1],c[2],3,l+"/conv2"),v=o(c[2],l+"/prelu2_alpha"),m=e(c[2],c[3],h?2:3,l+"/conv3"),g=o(c[3],l+"/prelu3_alpha");return{conv1:f,prelu1_alpha:d,conv2:p,prelu2_alpha:v,conv3:m,prelu3_alpha:g}}function i(){var c=a([3,10,16,32],"pnet"),l=e(32,2,1,"pnet/conv4_1"),h=e(32,4,1,"pnet/conv4_2");return Je(Je({},c),{conv4_1:l,conv4_2:h})}function s(){var c=a([3,28,48,64],"rnet",!0),l=n(576,128,"rnet/fc1"),h=o(128,"rnet/prelu4_alpha"),f=n(128,2,"rnet/fc2_1"),d=n(128,4,"rnet/fc2_2");return Je(Je({},c),{fc1:l,prelu4_alpha:h,fc2_1:f,fc2_2:d})}function u(){var c=a([3,32,64,64],"onet"),l=e(64,128,2,"onet/conv4"),h=o(128,"onet/prelu4_alpha"),f=n(1152,256,"onet/fc1"),d=o(256,"onet/prelu5_alpha"),p=n(256,2,"onet/fc2_1"),v=n(256,4,"onet/fc2_2"),m=n(256,10,"onet/fc2_3");return Je(Je({},c),{conv4:l,prelu4_alpha:h,fc1:f,prelu5_alpha:d,fc2_1:p,fc2_2:v,fc2_3:m})}return{extractPNetParams:i,extractRNetParams:s,extractONetParams:u}}function vb(r){var t=dn(r),e=t.extractWeights,n=t.getRemainingWeights,o=[],a=pb(e,o),i=a.extractPNetParams,s=a.extractRNetParams,u=a.extractONetParams,c=i(),l=s(),h=u();if(n().length!==0)throw new Error("weights remaing after extract: "+n().length);return{params:{pnet:c,rnet:l,onet:h},paramMappings:o}}function mb(r,t){var e=Dn(r,t);function n(l){var h=e(l+"/weights",4,l+"/filters"),f=e(l+"/bias",1);return{filters:h,bias:f}}function o(l){var h=e(l+"/weights",2),f=e(l+"/bias",1);return{weights:h,bias:f}}function a(l){return e(l,1)}function i(l){var h=n(l+"/conv1"),f=a(l+"/prelu1_alpha"),d=n(l+"/conv2"),p=a(l+"/prelu2_alpha"),v=n(l+"/conv3"),m=a(l+"/prelu3_alpha");return{conv1:h,prelu1_alpha:f,conv2:d,prelu2_alpha:p,conv3:v,prelu3_alpha:m}}function s(){var l=i("pnet"),h=n("pnet/conv4_1"),f=n("pnet/conv4_2");return Je(Je({},l),{conv4_1:h,conv4_2:f})}function u(){var l=i("rnet"),h=o("rnet/fc1"),f=a("rnet/prelu4_alpha"),d=o("rnet/fc2_1"),p=o("rnet/fc2_2");return Je(Je({},l),{fc1:h,prelu4_alpha:f,fc2_1:d,fc2_2:p})}function c(){var l=i("onet"),h=n("onet/conv4"),f=a("onet/prelu4_alpha"),d=o("onet/fc1"),p=a("onet/prelu5_alpha"),v=o("onet/fc2_1"),m=o("onet/fc2_2"),g=o("onet/fc2_3");return Je(Je({},l),{conv4:h,prelu4_alpha:f,fc1:d,prelu5_alpha:p,fc2_1:v,fc2_2:m,fc2_3:g})}return{extractPNetParams:s,extractRNetParams:u,extractONetParams:c}}function gb(r){var t=[],e=mb(r,t),n=e.extractPNetParams,o=e.extractRNetParams,a=e.extractONetParams,i=n(),s=o(),u=a();return fn(r,t),{params:{pnet:i,rnet:s,onet:u},paramMappings:t}}function Di(r,t){var e=t[0],n=t[1];return{height:Math.floor(e*r),width:Math.floor(n*r)}}function yb(r,t,e){for(var n=e[0],o=e[1],a=Yo/r,i=[],s=Math.min(n,o)*a,u=0;s>=12;)i.push(a*Math.pow(t,u)),s=s*t,u+=1;return i}var Hs=function(r){se(t,r);function t(e,n,o,a){return r.call(this,{left:e,top:n,right:o,bottom:a},!0)||this}return t}(En);function Xf(r){return Y(function(){return et(Ue(r,K(127.5)),K(.0078125))})}function hr(r,t){return Y(function(){return pe(Ne(r),et(t,Ho(Ne(Ho(r)))))})}function qs(r,t,e){return e===void 0&&(e=!1),Y(function(){var n=Pt(r,t.conv1,"valid");return n=hr(n,t.prelu1_alpha),n=Ge(n,e?[2,2]:[3,3],[2,2],"same"),n=Pt(n,t.conv2,"valid"),n=hr(n,t.prelu2_alpha),n=e?n:Ge(n,[3,3],[2,2],"valid"),n=Pt(n,t.conv3,"valid"),n=hr(n,t.prelu3_alpha),n})}function xb(r,t){return Y(function(){var e=qs(r,t,!0),n=Pt(e,t.conv4_1,"valid"),o=_t(da(n,3),3),a=cn(Ue(n,o),3),i=Pt(e,t.conv4_2,"valid");return{prob:a,regions:i}})}function bb(r,t){return Y(function(){var e=Di(t,r.shape.slice(1)),n=e.height,o=e.width,a=_s.resizeBilinear(r,[n,o]),i=Xf(a);return _n(i,[0,2,1,3])})}function wb(r,t,e,n){for(var o=[],a=r.arraySync(),i=0;i<r.shape[0];i++)for(var s=0;s<r.shape[1];s++)a[i][s]>=n&&o.push(new xe(s,i));var u=o.map(function(c){var l=new ga(Math.round((c.y*yo+1)/e),Math.round((c.x*yo+1)/e),Math.round((c.y*yo+Yo)/e),Math.round((c.x*yo+Yo)/e)),h=a[c.y][c.x],f=t.arraySync(),d=new Hs(f[c.y][c.x][0],f[c.y][c.x][1],f[c.y][c.x][2],f[c.y][c.x][3]);return{cell:l,score:h,region:d}});return u}function Cb(r,t,e,n,o){o.stage1=[];var a=t.map(function(f){return Y(function(){var d={scale:f},p=bb(r,f),v=Date.now(),m=xb(p,n),g=m.prob,x=m.regions;d.pnet=Date.now()-v;var y=ze(ze(g,3)[1])[0],b=ze(x)[0];return{scoresTensor:y,regionsTensor:b,scale:f,statsForScale:d}})}),i=a.map(function(f){var d=f.scoresTensor,p=f.regionsTensor,v=f.scale,m=f.statsForScale,g=wb(d,p,v,e);if(d.dispose(),p.dispose(),!g.length)return o.stage1.push(m),[];var x=Date.now(),y=jr(g.map(function(b){return b.cell}),g.map(function(b){return b.score}),.5);return m.nms=Date.now()-x,m.numBoxes=y.length,o.stage1.push(m),y.map(function(b){return g[b]})}),s=i.reduce(function(f,d){return f.concat(d)},[]),u=[],c=[];if(s.length>0){var l=Date.now(),h=jr(s.map(function(f){return f.cell}),s.map(function(f){return f.score}),.7);o.stage1_nms=Date.now()-l,c=h.map(function(f){return s[f].score}),u=h.map(function(f){return s[f]}).map(function(f){var d=f.cell,p=f.region;return new ga(d.left+p.left*d.width,d.top+p.top*d.height,d.right+p.right*d.width,d.bottom+p.bottom*d.height).toSquare().round()})}return{boxes:u,scores:c}}function $f(r,t,e){var n=e.width,o=e.height;return te(this,void 0,void 0,function(){var a,i,s,u=this;return ne(this,function(c){switch(c.label){case 0:return a=Sn(r),[4,Promise.all(t.map(function(l){return te(u,void 0,void 0,function(){var h,f,d,p,v,m,g,x;return ne(this,function(y){return h=l.padAtBorders(r.height,r.width),f=h.y,d=h.ey,p=h.x,v=h.ex,m=p-1,g=f-1,x=a.getImageData(m,g,v-m,d-g),[2,rt.isNodejs()?Fs(x):createImageBitmap(x)]})})}))];case 1:return i=c.sent(),s=[],i.forEach(function(l){var h=ya({width:n,height:o}),f=Sn(h);f.drawImage(l,0,0,n,o);for(var d=f.getImageData(0,0,n,o).data,p=[],v=0;v<d.length;v+=4)p.push(d[v+2]),p.push(d[v+1]),p.push(d[v]);s.push(p)}),[2,s.map(function(l){var h=Y(function(){var f=_n(lt(l,[1,n,o,3]),[0,2,1,3]).toFloat();return Xf(f)});return h})]}})})}function _b(r,t){return Y(function(){var e=qs(r,t),n=Nt(e,[e.shape[0],t.fc1.weights.shape[0]]),o=Kt(n,t.fc1),a=hr(o,t.prelu4_alpha),i=Kt(a,t.fc2_1),s=_t(da(i,1),1),u=cn(Ue(i,s),1),c=Kt(a,t.fc2_2),l=ze(u,1)[1];return{scores:l,regions:c}})}function Eb(r,t,e,n,o){return te(this,void 0,void 0,function(){var a,i,s,u,c,l,h,f,d,p,v,m,g,x;return ne(this,function(y){switch(y.label){case 0:return a=Date.now(),[4,$f(r,t,{width:24,height:24})];case 1:return i=y.sent(),o.stage2_extractImagePatches=Date.now()-a,a=Date.now(),s=i.map(function(b){var w=_b(b,n);return b.dispose(),w}),o.stage2_rnet=Date.now()-a,u=s.length>1?Ve(s.map(function(b){return b.scores})):s[0].scores,h=(l=Array).from,[4,u.data()];case 2:return c=h.apply(l,[y.sent()]),u.dispose(),f=c.map(function(b,w){return{score:b,idx:w}}).filter(function(b){return b.score>e}).map(function(b){var w=b.idx;return w}),d=f.map(function(b){return t[b]}),p=f.map(function(b){return c[b]}),v=[],m=[],d.length>0&&(a=Date.now(),g=jr(d,p,.7),o.stage2_nms=Date.now()-a,x=g.map(function(b){var w=s[f[b]].regions.arraySync();return new Hs(w[0][0],w[0][1],w[0][2],w[0][3])}),m=g.map(function(b){return p[b]}),v=g.map(function(b,w){return d[b].calibrate(x[w])})),s.forEach(function(b){b.regions.dispose(),b.scores.dispose()}),[2,{boxes:v,scores:m}]}})})}function Sb(r,t){return Y(function(){var e=qs(r,t);e=Ge(e,[2,2],[2,2],"same"),e=Pt(e,t.conv4,"valid"),e=hr(e,t.prelu4_alpha);var n=Nt(e,[e.shape[0],t.fc1.weights.shape[0]]),o=Kt(n,t.fc1),a=hr(o,t.prelu5_alpha),i=Kt(a,t.fc2_1),s=_t(da(i,1),1),u=cn(Ue(i,s),1),c=Kt(a,t.fc2_2),l=Kt(a,t.fc2_3),h=ze(u,1)[1];return{scores:h,regions:c,points:l}})}function kb(r,t,e,n,o){return te(this,void 0,void 0,function(){var a,i,s,u,c,l,h,f,d,p,v,m,g,x,y;return ne(this,function(b){switch(b.label){case 0:return a=Date.now(),[4,$f(r,t,{width:48,height:48})];case 1:return i=b.sent(),o.stage3_extractImagePatches=Date.now()-a,a=Date.now(),s=i.map(function(w){var C=Sb(w,n);return w.dispose(),C}),o.stage3_onet=Date.now()-a,u=s.length>1?Ve(s.map(function(w){return w.scores})):s[0].scores,h=(l=Array).from,[4,u.data()];case 2:return c=h.apply(l,[b.sent()]),u.dispose(),f=c.map(function(w,C){return{score:w,idx:C}}).filter(function(w){return w.score>e}).map(function(w){var C=w.idx;return C}),d=f.map(function(w){var C=s[w].regions.arraySync();return new Hs(C[0][0],C[0][1],C[0][2],C[0][3])}),p=f.map(function(w,C){return t[w].calibrate(d[C])}),v=f.map(function(w){return c[w]}),m=[],g=[],x=[],p.length>0&&(a=Date.now(),y=jr(p,v,.7,!1),o.stage3_nms=Date.now()-a,m=y.map(function(w){return p[w]}),g=y.map(function(w){return v[w]}),x=y.map(function(w,C){return Array(5).fill(0).map(function(I,E){var S=s[w].points.arraySync();return new xe(S[0][E]*(m[C].width+1)+m[C].left,S[0][E+5]*(m[C].height+1)+m[C].top)})})),s.forEach(function(w){w.regions.dispose(),w.scores.dispose(),w.points.dispose()}),[2,{boxes:m,scores:g,points:x}]}})})}var Rb=function(r){se(t,r);function t(){return r.call(this,"Mtcnn")||this}return t.prototype.load=function(e){return te(this,void 0,void 0,function(){return ne(this,function(n){return console.warn("mtcnn is deprecated and will be removed soon"),[2,r.prototype.load.call(this,e)]})})},t.prototype.loadFromDisk=function(e){return te(this,void 0,void 0,function(){return ne(this,function(n){return console.warn("mtcnn is deprecated and will be removed soon"),[2,r.prototype.loadFromDisk.call(this,e)]})})},t.prototype.forwardInput=function(e,n){return n===void 0&&(n={}),te(this,void 0,void 0,function(){var o,a,i,s,u,c,l,h,f,d,p,v,m,g,x,y,b,w,C,I,E;return ne(this,function(S){switch(S.label){case 0:if(o=this.params,!o)throw new Error("Mtcnn - load model before inference");if(a=e.canvases[0],!a)throw new Error("Mtcnn - inputCanvas is not defined, note that passing tensors into Mtcnn.forwardInput is not supported yet.");return i={},s=Date.now(),u=Y(function(){return db(_t(As.fromPixels(a)).toFloat())}),c=function(k){return u.dispose(),i.total=Date.now()-s,k},l=u.shape.slice(1),h=l[0],f=l[1],d=new Hf(n),p=d.minFaceSize,v=d.scaleFactor,m=d.maxNumScales,g=d.scoreThresholds,x=d.scaleSteps,y=(x||yb(p,v,[h,f])).filter(function(k){var T=Di(k,[h,f]);return Math.min(T.width,T.height)>Yo}).slice(0,m),i.scales=y,i.pyramid=y.map(function(k){return Di(k,[h,f])}),b=Date.now(),[4,Cb(u,y,g[0],o.pnet,i)];case 1:return w=S.sent(),i.total_stage1=Date.now()-b,w.boxes.length?(i.stage2_numInputBoxes=w.boxes.length,b=Date.now(),[4,Eb(a,w.boxes,g[1],o.rnet,i)]):[2,c({results:[],stats:i})];case 2:return C=S.sent(),i.total_stage2=Date.now()-b,C.boxes.length?(i.stage3_numInputBoxes=C.boxes.length,b=Date.now(),[4,kb(a,C.boxes,g[2],o.onet,i)]):[2,c({results:[],stats:i})];case 3:return I=S.sent(),i.total_stage3=Date.now()-b,E=I.boxes.map(function(k,T){return ba(Kr({},new Wt(I.scores[T],new Ts(k.left/f,k.top/h,k.width/f,k.height/h),{height:h,width:f})),new rx(I.points[T].map(function(A){return A.sub(new xe(k.left,k.top)).div(new xe(k.width,k.height))}),{width:k.width,height:k.height}))}),[2,c({results:E,stats:i})]}})})},t.prototype.forward=function(e,n){return n===void 0&&(n={}),te(this,void 0,void 0,function(){var o;return ne(this,function(a){switch(a.label){case 0:return o=this.forwardInput,[4,qe(e)];case 1:return[4,o.apply(this,[a.sent(),n])];case 2:return[2,a.sent().results]}})})},t.prototype.forwardWithStats=function(e,n){return n===void 0&&(n={}),te(this,void 0,void 0,function(){var o;return ne(this,function(a){switch(a.label){case 0:return o=this.forwardInput,[4,qe(e)];case 1:return[2,o.apply(this,[a.sent(),n])]}})})},t.prototype.getDefaultModelName=function(){return"mtcnn_model"},t.prototype.extractParamsFromWeigthMap=function(e){return gb(e)},t.prototype.extractParams=function(e){return vb(e)},t}(hn),Ib=.4,Ab=[new xe(1.603231,2.094468),new xe(6.041143,7.080126),new xe(2.882459,3.518061),new xe(4.266906,5.178857),new xe(9.041765,10.66308)],Db=[117.001,114.697,97.404],Tb=function(r){se(t,r);function t(){var e=this,n={withSeparableConvs:!0,iouThreshold:Ib,classes:["face"],anchors:Ab,meanRgb:Db,isFirstLayerConv2d:!0,filterSizes:[3,16,32,64,128,256,512]};return e=r.call(this,n)||this,e}return Object.defineProperty(t.prototype,"anchors",{get:function(){return this.config.anchors},enumerable:!0,configurable:!0}),t.prototype.locateFaces=function(e,n){return te(this,void 0,void 0,function(){var o;return ne(this,function(a){switch(a.label){case 0:return[4,this.detect(e,n)];case 1:return o=a.sent(),[2,o.map(function(i){return new Wt(i.score,i.relativeBox,{width:i.imageWidth,height:i.imageHeight})})]}})})},t.prototype.getDefaultModelName=function(){return"tiny_face_detector_model"},t.prototype.extractParamsFromWeigthMap=function(e){return r.prototype.extractParamsFromWeigthMap.call(this,e)},t}(jf),tt={ssdMobilenetv1:new qf,tinyFaceDetector:new Tb,tinyYolov2:new fb,mtcnn:new Rb,faceLandmark68Net:new Lf,faceLandmark68TinyNet:new Fx,faceRecognitionNet:new Vx,faceExpressionNet:new gx,ageGenderNet:new Ax},Yf=function(r){se(t,r);function t(e,n,o){var a=r.call(this)||this;return a.parentTask=e,a.input=n,a.extractedFaces=o,a}return t}(no),js=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n,o=this;return ne(this,function(a){switch(a.label){case 0:return[4,this.parentTask];case 1:return e=a.sent(),[4,Ca(e,this.input,function(i){return te(o,void 0,void 0,function(){return ne(this,function(s){switch(s.label){case 0:return[4,Promise.all(i.map(function(u){return tt.faceExpressionNet.predictExpressions(u)}))];case 1:return[2,s.sent()]}})})},this.extractedFaces)];case 2:return n=a.sent(),[2,e.map(function(i,s){return Mf(i,n[s])})]}})})},t.prototype.withAgeAndGender=function(){return new Ys(this,this.input)},t}(Yf),Ks=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n;return ne(this,function(o){switch(o.label){case 0:return[4,this.parentTask];case 1:return e=o.sent(),e?[4,Gs(e,this.input,function(a){return tt.faceExpressionNet.predictExpressions(a)},this.extractedFaces)]:[2];case 2:return n=o.sent(),[2,Mf(e,n)]}})})},t.prototype.withAgeAndGender=function(){return new Js(this,this.input)},t}(Yf),Xs=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.withAgeAndGender=function(){return new Qs(this,this.input)},t.prototype.withFaceDescriptors=function(){return new eu(this,this.input)},t}(js),$s=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.withAgeAndGender=function(){return new Zs(this,this.input)},t.prototype.withFaceDescriptor=function(){return new tu(this,this.input)},t}(Ks),Jf=function(r){se(t,r);function t(e,n,o){var a=r.call(this)||this;return a.parentTask=e,a.input=n,a.extractedFaces=o,a}return t}(no),Ys=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n,o=this;return ne(this,function(a){switch(a.label){case 0:return[4,this.parentTask];case 1:return e=a.sent(),[4,Ca(e,this.input,function(i){return te(o,void 0,void 0,function(){return ne(this,function(s){switch(s.label){case 0:return[4,Promise.all(i.map(function(u){return tt.ageGenderNet.predictAgeAndGender(u)}))];case 1:return[2,s.sent()]}})})},this.extractedFaces)];case 2:return n=a.sent(),[2,e.map(function(i,s){var u=n[s],c=u.age,l=u.gender,h=u.genderProbability;return Uf(Gf(i,l,h),c)})]}})})},t.prototype.withFaceExpressions=function(){return new js(this,this.input)},t}(Jf),Js=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n,o,a,i;return ne(this,function(s){switch(s.label){case 0:return[4,this.parentTask];case 1:return e=s.sent(),e?[4,Gs(e,this.input,function(u){return tt.ageGenderNet.predictAgeAndGender(u)},this.extractedFaces)]:[2];case 2:return n=s.sent(),o=n.age,a=n.gender,i=n.genderProbability,[2,Uf(Gf(e,a,i),o)]}})})},t.prototype.withFaceExpressions=function(){return new Ks(this,this.input)},t}(Jf),Qs=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.withFaceExpressions=function(){return new Xs(this,this.input)},t.prototype.withFaceDescriptors=function(){return new eu(this,this.input)},t}(Ys),Zs=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.withFaceExpressions=function(){return new $s(this,this.input)},t.prototype.withFaceDescriptor=function(){return new tu(this,this.input)},t}(Js),Qf=function(r){se(t,r);function t(e,n){var o=r.call(this)||this;return o.parentTask=e,o.input=n,o}return t}(no),eu=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n;return ne(this,function(o){switch(o.label){case 0:return[4,this.parentTask];case 1:return e=o.sent(),[4,Ca(e,this.input,function(a){return Promise.all(a.map(function(i){return tt.faceRecognitionNet.computeFaceDescriptor(i)}))},null,function(a){return a.landmarks.align(null,{useDlibAlignment:!0})})];case 2:return n=o.sent(),[2,n.map(function(a,i){return zf(e[i],a)})]}})})},t.prototype.withFaceExpressions=function(){return new Xs(this,this.input)},t.prototype.withAgeAndGender=function(){return new Qs(this,this.input)},t}(Qf),tu=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n;return ne(this,function(o){switch(o.label){case 0:return[4,this.parentTask];case 1:return e=o.sent(),e?[4,Gs(e,this.input,function(a){return tt.faceRecognitionNet.computeFaceDescriptor(a)},null,function(a){return a.landmarks.align(null,{useDlibAlignment:!0})})]:[2];case 2:return n=o.sent(),[2,zf(e,n)]}})})},t.prototype.withFaceExpressions=function(){return new $s(this,this.input)},t.prototype.withAgeAndGender=function(){return new Zs(this,this.input)},t}(Qf),Zf=function(r){se(t,r);function t(e,n,o){var a=r.call(this)||this;return a.parentTask=e,a.input=n,a.useTinyLandmarkNet=o,a}return Object.defineProperty(t.prototype,"landmarkNet",{get:function(){return this.useTinyLandmarkNet?tt.faceLandmark68TinyNet:tt.faceLandmark68Net},enumerable:!0,configurable:!0}),t}(no),Nb=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n,o,a,i,s=this;return ne(this,function(u){switch(u.label){case 0:return[4,this.parentTask];case 1:return e=u.sent(),n=e.map(function(c){return c.detection}),this.input instanceof Te?[4,Ms(this.input,n)]:[3,3];case 2:return a=u.sent(),[3,5];case 3:return[4,Ps(this.input,n)];case 4:a=u.sent(),u.label=5;case 5:return o=a,[4,Promise.all(o.map(function(c){return s.landmarkNet.detectLandmarks(c)}))];case 6:return i=u.sent(),o.forEach(function(c){return c instanceof Te&&c.dispose()}),[2,e.map(function(c,l){return ba(c,i[l])})]}})})},t.prototype.withFaceExpressions=function(){return new Xs(this,this.input)},t.prototype.withAgeAndGender=function(){return new Qs(this,this.input)},t.prototype.withFaceDescriptors=function(){return new eu(this,this.input)},t}(Zf),Fb=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n,o,a,i;return ne(this,function(s){switch(s.label){case 0:return[4,this.parentTask];case 1:return e=s.sent(),e?(n=e.detection,this.input instanceof Te?[4,Ms(this.input,[n])]:[3,3]):[2];case 2:return a=s.sent(),[3,5];case 3:return[4,Ps(this.input,[n])];case 4:a=s.sent(),s.label=5;case 5:return o=a,[4,this.landmarkNet.detectLandmarks(o[0])];case 6:return i=s.sent(),o.forEach(function(u){return u instanceof Te&&u.dispose()}),[2,ba(e,i)]}})})},t.prototype.withFaceExpressions=function(){return new $s(this,this.input)},t.prototype.withAgeAndGender=function(){return new Zs(this,this.input)},t.prototype.withFaceDescriptor=function(){return new tu(this,this.input)},t}(Zf),ed=function(r){se(t,r);function t(e,n){n===void 0&&(n=new wa);var o=r.call(this)||this;return o.input=e,o.options=n,o}return t}(no),Pb=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n,o,a;return ne(this,function(i){switch(i.label){case 0:return e=this,n=e.input,o=e.options,o instanceof Hf?[4,tt.mtcnn.forward(n,o)]:[3,2];case 1:return[2,i.sent().map(function(s){return s.detection})];case 2:if(a=o instanceof Kf?function(s){return tt.tinyFaceDetector.locateFaces(s,o)}:o instanceof wa?function(s){return tt.ssdMobilenetv1.locateFaces(s,o)}:o instanceof Us?function(s){return tt.tinyYolov2.locateFaces(s,o)}:null,!a)throw new Error("detectFaces - expected options to be instance of TinyFaceDetectorOptions | SsdMobilenetv1Options | MtcnnOptions | TinyYolov2Options");return[2,a(n)]}})})},t.prototype.runAndExtendWithFaceDetections=function(){var e=this;return new Promise(function(n){return te(e,void 0,void 0,function(){var o;return ne(this,function(a){switch(a.label){case 0:return[4,this.run()];case 1:return o=a.sent(),[2,n(o.map(function(i){return Kr({},i)}))]}})})})},t.prototype.withFaceLandmarks=function(e){return e===void 0&&(e=!1),new Nb(this.runAndExtendWithFaceDetections(),this.input,e)},t.prototype.withFaceExpressions=function(){return new js(this.runAndExtendWithFaceDetections(),this.input)},t.prototype.withAgeAndGender=function(){return new Ys(this.runAndExtendWithFaceDetections(),this.input)},t}(ed),Mb=function(r){se(t,r);function t(){return r!==null&&r.apply(this,arguments)||this}return t.prototype.run=function(){return te(this,void 0,void 0,function(){var e,n;return ne(this,function(o){switch(o.label){case 0:return[4,new Pb(this.input,this.options)];case 1:return e=o.sent(),n=e[0],e.forEach(function(a){a.score>n.score&&(n=a)}),[2,n]}})})},t.prototype.runAndExtendWithFaceDetection=function(){var e=this;return new Promise(function(n){return te(e,void 0,void 0,function(){var o;return ne(this,function(a){switch(a.label){case 0:return[4,this.run()];case 1:return o=a.sent(),[2,n(o?Kr({},o):void 0)]}})})})},t.prototype.withFaceLandmarks=function(e){return e===void 0&&(e=!1),new Fb(this.runAndExtendWithFaceDetection(),this.input,e)},t.prototype.withFaceExpressions=function(){return new Ks(this.runAndExtendWithFaceDetection(),this.input)},t.prototype.withAgeAndGender=function(){return new Js(this.runAndExtendWithFaceDetection(),this.input)},t}(ed);function Ob(r,t){return t===void 0&&(t=new wa),new Mb(r,t)}function Bb(r,t){if(r.length!==t.length)throw new Error("euclideanDistance: arr1.length !== arr2.length");var e=Array.from(r),n=Array.from(t);return Math.sqrt(e.map(function(o,a){return o-n[a]}).reduce(function(o,a){return o+Math.pow(a,2)},0))}(function(){function r(t,e){e===void 0&&(e=.6),this._distanceThreshold=e;var n=Array.isArray(t)?t:[t];if(!n.length)throw new Error("FaceRecognizer.constructor - expected atleast one input");var o=1,a=function(){return"person "+o++};this._labeledDescriptors=n.map(function(i){if(i instanceof po)return i;if(i instanceof Float32Array)return new po(a(),[i]);if(i.descriptor&&i.descriptor instanceof Float32Array)return new po(a(),[i.descriptor]);throw new Error("FaceRecognizer.constructor - expected inputs to be of type LabeledFaceDescriptors | WithFaceDescriptor<any> | Float32Array | Array<LabeledFaceDescriptors | WithFaceDescriptor<any> | Float32Array>")})}return Object.defineProperty(r.prototype,"labeledDescriptors",{get:function(){return this._labeledDescriptors},enumerable:!0,configurable:!0}),Object.defineProperty(r.prototype,"distanceThreshold",{get:function(){return this._distanceThreshold},enumerable:!0,configurable:!0}),r.prototype.computeMeanDistance=function(t,e){return e.map(function(n){return Bb(n,t)}).reduce(function(n,o){return n+o},0)/(e.length||1)},r.prototype.matchDescriptor=function(t){var e=this;return this.labeledDescriptors.map(function(n){var o=n.descriptors,a=n.label;return new Qu(a,e.computeMeanDistance(t,o))}).reduce(function(n,o){return n.distance<o.distance?n:o})},r.prototype.findBestMatch=function(t){var e=this.matchDescriptor(t);return e.distance<this.distanceThreshold?e:new Qu("unknown",e.distance)},r.prototype.toJSON=function(){return{distanceThreshold:this.distanceThreshold,labeledDescriptors:this.labeledDescriptors.map(function(t){return t.toJSON()})}},r.fromJSON=function(t){var e=t.labeledDescriptors.map(function(n){return po.fromJSON(n)});return new r(e,t.distanceThreshold)},r})();function td(r,t){var e=new Ln(t.width,t.height),n=e.width,o=e.height;if(n<=0||o<=0)throw new Error("resizeResults - invalid dimensions: "+JSON.stringify({width:n,height:o}));if(Array.isArray(r))return r.map(function(s){return td(s,{width:n,height:o})});if(Ws(r)){var a=r.detection.forSize(n,o),i=r.unshiftedLandmarks.forSize(a.box.width,a.box.height);return ba(Kr(r,a),i)}return yf(r)?Kr(r,r.detection.forSize(n,o)):r instanceof vr||r instanceof Wt?r.forSize(n,o):r}const nd=dt()({name:"VCardActions",props:Rt(),setup(r,t){let{slots:e}=t;return Ad({VBtn:{variant:"text"}}),Qt(()=>{var n;return $("div",{class:["v-card-actions",r.class],style:r.style},[(n=e.default)==null?void 0:n.call(e)])}),{}}}),Lb=Ui("v-card-subtitle"),rd=Ui("v-card-title");function Wb(r){return{aspectStyles:Ae(()=>{const t=Number(r.aspectRatio);return t?{paddingBottom:String(1/t*100)+"%"}:void 0})}}const od=je({aspectRatio:[String,Number],contentClass:String,inline:Boolean,...Rt(),...Wi()},"VResponsive"),oc=dt()({name:"VResponsive",props:od(),setup(r,t){let{slots:e}=t;const{aspectStyles:n}=Wb(r),{dimensionStyles:o}=Vi(r);return Qt(()=>{var a;return $("div",{class:["v-responsive",{"v-responsive--inline":r.inline},r.class],style:[o.value,r.style]},[$("div",{class:"v-responsive__sizer",style:n.value},null),(a=e.additional)==null?void 0:a.call(e),e.default&&$("div",{class:["v-responsive__content",r.contentClass]},[e.default()])])}),{}}}),ad=je({transition:{type:[Boolean,String,Object],default:"fade-transition",validator:r=>r!==!0}},"transition"),Ar=(r,t)=>{let{slots:e}=t;const{transition:n,disabled:o,...a}=r,{component:i=Io,...s}=typeof n=="object"?n:{};return Zo(i,kt(typeof n=="string"?{name:o?"":n}:s,a,{disabled:o}),e)};function Vb(r,t){if(!wc)return;const e=t.modifiers||{},n=t.value,{handler:o,options:a}=typeof n=="object"?n:{handler:n,options:{}},i=new IntersectionObserver(function(){var h;let s=arguments.length>0&&arguments[0]!==void 0?arguments[0]:[],u=arguments.length>1?arguments[1]:void 0;const c=(h=r._observe)==null?void 0:h[t.instance.$.uid];if(!c)return;const l=s.some(f=>f.isIntersecting);o&&(!e.quiet||c.init)&&(!e.once||l||c.init)&&o(l,s,u),l&&e.once?id(r,t):c.init=!0},a);r._observe=Object(r._observe),r._observe[t.instance.$.uid]={init:!1,observer:i},i.observe(r)}function id(r,t){var n;const e=(n=r._observe)==null?void 0:n[t.instance.$.uid];!e||(e.observer.unobserve(r),delete r._observe[t.instance.$.uid])}const zb={mounted:Vb,unmounted:id},Ub=zb,Gb=je({alt:String,cover:Boolean,eager:Boolean,gradient:String,lazySrc:String,options:{type:Object,default:()=>({root:void 0,rootMargin:void 0,threshold:void 0})},sizes:String,src:{type:[String,Object],default:""},srcset:String,...od(),...Rt(),...ad()},"VImg"),sd=dt()({name:"VImg",directives:{intersect:Ub},props:Gb(),emits:{loadstart:r=>!0,load:r=>!0,error:r=>!0},setup(r,t){let{emit:e,slots:n}=t;const o=Et(""),a=Ce(),i=Et(r.eager?"loading":"idle"),s=Et(),u=Et(),c=Ae(()=>r.src&&typeof r.src=="object"?{src:r.src.src,srcset:r.srcset||r.src.srcset,lazySrc:r.lazySrc||r.src.lazySrc,aspect:Number(r.aspectRatio||r.src.aspect||0)}:{src:r.src,srcset:r.srcset,lazySrc:r.lazySrc,aspect:Number(r.aspectRatio||0)}),l=Ae(()=>c.value.aspect||s.value/u.value||0);nt(()=>r.src,()=>{h(i.value!=="idle")}),nt(l,(E,S)=>{!E&&S&&a.value&&m(a.value)}),Dd(()=>h());function h(E){if(!(r.eager&&E)&&!(wc&&!E&&!r.eager)){if(i.value="loading",c.value.lazySrc){const S=new Image;S.src=c.value.lazySrc,m(S,null)}!c.value.src||mr(()=>{var S,k;if(e("loadstart",((S=a.value)==null?void 0:S.currentSrc)||c.value.src),(k=a.value)!=null&&k.complete){if(a.value.naturalWidth||d(),i.value==="error")return;l.value||m(a.value,null),f()}else l.value||m(a.value),p()})}}function f(){var E;p(),i.value="loaded",e("load",((E=a.value)==null?void 0:E.currentSrc)||c.value.src)}function d(){var E;i.value="error",e("error",((E=a.value)==null?void 0:E.currentSrc)||c.value.src)}function p(){const E=a.value;E&&(o.value=E.currentSrc||E.src)}let v=-1;function m(E){let S=arguments.length>1&&arguments[1]!==void 0?arguments[1]:100;const k=()=>{clearTimeout(v);const{naturalHeight:T,naturalWidth:A}=E;T||A?(s.value=A,u.value=T):!E.complete&&i.value==="loading"&&S!=null?v=window.setTimeout(k,S):(E.currentSrc.endsWith(".svg")||E.currentSrc.startsWith("data:image/svg+xml"))&&(s.value=1,u.value=1)};k()}const g=Ae(()=>({"v-img__img--cover":r.cover,"v-img__img--contain":!r.cover})),x=()=>{var k;if(!c.value.src||i.value==="idle")return null;const E=$("img",{class:["v-img__img",g.value],src:c.value.src,srcset:c.value.srcset,alt:r.alt,sizes:r.sizes,ref:a,onLoad:f,onError:d},null),S=(k=n.sources)==null?void 0:k.call(n);return $(Ar,{transition:r.transition,appear:!0},{default:()=>[Fr(S?$("picture",{class:"v-img__picture"},[S,E]):E,[[Pi,i.value==="loaded"]])]})},y=()=>$(Ar,{transition:r.transition},{default:()=>[c.value.lazySrc&&i.value!=="loaded"&&$("img",{class:["v-img__img","v-img__img--preload",g.value],src:c.value.lazySrc,alt:r.alt},null)]}),b=()=>n.placeholder?$(Ar,{transition:r.transition,appear:!0},{default:()=>[(i.value==="loading"||i.value==="error"&&!n.error)&&$("div",{class:"v-img__placeholder"},[n.placeholder()])]}):null,w=()=>n.error?$(Ar,{transition:r.transition,appear:!0},{default:()=>[i.value==="error"&&$("div",{class:"v-img__error"},[n.error()])]}):null,C=()=>r.gradient?$("div",{class:"v-img__gradient",style:{backgroundImage:`linear-gradient(${r.gradient})`}},null):null,I=Et(!1);{const E=nt(l,S=>{S&&(requestAnimationFrame(()=>{requestAnimationFrame(()=>{I.value=!0})}),E())})}return Qt(()=>{const[E]=oc.filterProps(r);return Fr($(oc,kt({class:["v-img",{"v-img--booting":!I.value},r.class],style:[{width:pt(r.width==="auto"?s.value:r.width)},r.style]},E,{aspectRatio:l.value,"aria-label":r.alt,role:r.alt?"img":void 0}),{additional:()=>$(Cc,null,[$(x,null,null),$(y,null,null),$(C,null,null),$(b,null,null),$(w,null,null)]),default:n.default}),[[Fi("intersect"),{handler:h,options:r.options},null,{once:!0}]])}),{currentSrc:o,image:a,state:i,naturalWidth:s,naturalHeight:u}}}),Hb=je({start:Boolean,end:Boolean,icon:Pr,image:String,...Rt(),...zi(),...Dc(),...jd(),...Xr(),...ea(),...Tc({variant:"flat"})},"VAvatar"),ac=dt()({name:"VAvatar",props:Hb(),setup(r,t){let{slots:e}=t;const{themeClasses:n}=ta(r),{colorClasses:o,colorStyles:a,variantClasses:i}=Nc(r),{densityClasses:s}=Fc(r),{roundedClasses:u}=Pc(r),{sizeClasses:c,sizeStyles:l}=Kd(r);return Qt(()=>$(r.tag,{class:["v-avatar",{"v-avatar--start":r.start,"v-avatar--end":r.end},n.value,o.value,s.value,u.value,c.value,i.value,r.class],style:[a.value,l.value,r.style]},{default:()=>{var h;return[r.image?$(sd,{key:"image",src:r.image,alt:"",cover:!0},null):r.icon?$(Xd,{key:"icon",icon:r.icon},null):(h=e.default)==null?void 0:h.call(e),Mc(!1,"v-avatar")]}})),{}}}),qb=je({appendAvatar:String,appendIcon:Pr,prependAvatar:String,prependIcon:Pr,subtitle:String,title:String,...Rt(),...zi()},"VCardItem"),jb=dt()({name:"VCardItem",props:qb(),setup(r,t){let{slots:e}=t;return Qt(()=>{var c;const n=!!(r.prependAvatar||r.prependIcon),o=!!(n||e.prepend),a=!!(r.appendAvatar||r.appendIcon),i=!!(a||e.append),s=!!(r.title||e.title),u=!!(r.subtitle||e.subtitle);return $("div",{class:["v-card-item",r.class],style:r.style},[o&&$("div",{key:"prepend",class:"v-card-item__prepend"},[e.prepend?$(Ao,{key:"prepend-defaults",disabled:!n,defaults:{VAvatar:{density:r.density,icon:r.prependIcon,image:r.prependAvatar}}},e.prepend):n&&$(ac,{key:"prepend-avatar",density:r.density,icon:r.prependIcon,image:r.prependAvatar},null)]),$("div",{class:"v-card-item__content"},[s&&$(rd,{key:"title"},{default:()=>{var l,h;return[(h=(l=e.title)==null?void 0:l.call(e))!=null?h:r.title]}}),u&&$(Lb,{key:"subtitle"},{default:()=>{var l,h;return[(h=(l=e.subtitle)==null?void 0:l.call(e))!=null?h:r.subtitle]}}),(c=e.default)==null?void 0:c.call(e)]),i&&$("div",{key:"append",class:"v-card-item__append"},[e.append?$(Ao,{key:"append-defaults",disabled:!a,defaults:{VAvatar:{density:r.density,icon:r.appendIcon,image:r.appendAvatar}}},e.append):a&&$(ac,{key:"append-avatar",density:r.density,icon:r.appendIcon,image:r.appendAvatar},null)])])}),{}}}),ud=Ui("v-card-text"),Kb=je({appendAvatar:String,appendIcon:Pr,disabled:Boolean,flat:Boolean,hover:Boolean,image:String,link:{type:Boolean,default:void 0},prependAvatar:String,prependIcon:Pr,ripple:{type:[Boolean,Object],default:!0},subtitle:String,text:String,title:String,...$d(),...Rt(),...zi(),...Wi(),...Yd(),...Jd(),...Qd(),...Zd(),...Dc(),...ep(),...Xr(),...ea(),...Tc({variant:"elevated"})},"VCard"),ic=dt()({name:"VCard",directives:{Ripple:tp},props:Kb(),setup(r,t){let{attrs:e,slots:n}=t;const{themeClasses:o}=ta(r),{borderClasses:a}=np(r),{colorClasses:i,colorStyles:s,variantClasses:u}=Nc(r),{densityClasses:c}=Fc(r),{dimensionStyles:l}=Vi(r),{elevationClasses:h}=rp(r),{loaderClasses:f}=op(r),{locationStyles:d}=ap(r),{positionClasses:p}=ip(r),{roundedClasses:v}=Pc(r),m=sp(r,e),g=Ae(()=>r.link!==!1&&m.isLink.value),x=Ae(()=>!r.disabled&&r.link!==!1&&(r.link||m.isClickable.value));return Qt(()=>{const y=g.value?"a":r.tag,b=!!(n.title||r.title),w=!!(n.subtitle||r.subtitle),C=b||w,I=!!(n.append||r.appendAvatar||r.appendIcon),E=!!(n.prepend||r.prependAvatar||r.prependIcon),S=!!(n.image||r.image),k=C||E||I,T=!!(n.text||r.text);return Fr($(y,{class:["v-card",{"v-card--disabled":r.disabled,"v-card--flat":r.flat,"v-card--hover":r.hover&&!(r.disabled||r.flat),"v-card--link":x.value},o.value,a.value,i.value,c.value,h.value,f.value,p.value,v.value,u.value,r.class],style:[s.value,l.value,d.value,r.style],href:m.href.value,onClick:x.value&&m.navigate,tabindex:r.disabled?-1:void 0},{default:()=>{var A;return[S&&$("div",{key:"image",class:"v-card__image"},[n.image?$(Ao,{key:"image-defaults",disabled:!r.image,defaults:{VImg:{cover:!0,src:r.image}}},n.image):$(sd,{key:"image-img",cover:!0,src:r.image},null)]),$(up,{name:"v-card",active:!!r.loading,color:typeof r.loading=="boolean"?void 0:r.loading},{default:n.loader}),k&&$(jb,{key:"item",prependAvatar:r.prependAvatar,prependIcon:r.prependIcon,title:r.title,subtitle:r.subtitle,appendAvatar:r.appendAvatar,appendIcon:r.appendIcon},{default:n.item,prepend:n.prepend,title:n.title,subtitle:n.subtitle,append:n.append}),T&&$(ud,{key:"text"},{default:()=>{var F,P;return[(P=(F=n.text)==null?void 0:F.call(n))!=null?P:r.text]}}),(A=n.default)==null?void 0:A.call(n),n.actions&&$(nd,null,{default:n.actions}),Mc(x.value,"v-card")]}}),[[Fi("ripple"),x.value&&r.ripple]])}),{}}});const Xb=je({target:Object},"v-dialog-transition"),$b=dt()({name:"VDialogTransition",props:Xb(),setup(r,t){let{slots:e}=t;const n={onBeforeEnter(o){o.style.pointerEvents="none",o.style.visibility="hidden"},async onEnter(o,a){var f;await new Promise(d=>requestAnimationFrame(d)),await new Promise(d=>requestAnimationFrame(d)),o.style.visibility="";const{x:i,y:s,sx:u,sy:c,speed:l}=uc(r.target,o),h=Sr(o,[{transform:`translate(${i}px, ${s}px) scale(${u}, ${c})`,opacity:0},{}],{duration:225*l,easing:mp});(f=sc(o))==null||f.forEach(d=>{Sr(d,[{opacity:0},{opacity:0,offset:.33},{}],{duration:225*2*l,easing:ai})}),h.finished.then(()=>a())},onAfterEnter(o){o.style.removeProperty("pointer-events")},onBeforeLeave(o){o.style.pointerEvents="none"},async onLeave(o,a){var f;await new Promise(d=>requestAnimationFrame(d));const{x:i,y:s,sx:u,sy:c,speed:l}=uc(r.target,o);Sr(o,[{},{transform:`translate(${i}px, ${s}px) scale(${u}, ${c})`,opacity:0}],{duration:125*l,easing:gp}).finished.then(()=>a()),(f=sc(o))==null||f.forEach(d=>{Sr(d,[{},{opacity:0,offset:.2},{opacity:0}],{duration:125*2*l,easing:ai})})},onAfterLeave(o){o.style.removeProperty("pointer-events")}};return()=>r.target?$(Io,kt({name:"dialog-transition"},n,{css:!1}),e):$(Io,{name:"dialog-transition"},e)}});function sc(r){var e;const t=(e=r.querySelector(":scope > .v-card, :scope > .v-sheet, :scope > .v-list"))==null?void 0:e.children;return t&&[...t]}function uc(r,t){const e=r.getBoundingClientRect(),n=Oc(t),[o,a]=getComputedStyle(t).transformOrigin.split(" ").map(g=>parseFloat(g)),[i,s]=getComputedStyle(t).getPropertyValue("--v-overlay-anchor-origin").split(" ");let u=e.left+e.width/2;i==="left"||s==="left"?u-=e.width/2:(i==="right"||s==="right")&&(u+=e.width/2);let c=e.top+e.height/2;i==="top"||s==="top"?c-=e.height/2:(i==="bottom"||s==="bottom")&&(c+=e.height/2);const l=e.width/n.width,h=e.height/n.height,f=Math.max(1,l,h),d=l/f||0,p=h/f||0,v=n.width*n.height/(window.innerWidth*window.innerHeight),m=v>.12?Math.min(1.5,(v-.12)*10+1):1;return{x:u-(o+n.left),y:c-(a+n.top),sx:d,sy:p,speed:m}}function ti(r,t){return{x:r.x+t.x,y:r.y+t.y}}function Yb(r,t){return{x:r.x-t.x,y:r.y-t.y}}function cc(r,t){if(r.side==="top"||r.side==="bottom"){const{side:e,align:n}=r,o=n==="left"?0:n==="center"?t.width/2:n==="right"?t.width:n,a=e==="top"?0:e==="bottom"?t.height:e;return ti({x:o,y:a},t)}else if(r.side==="left"||r.side==="right"){const{side:e,align:n}=r,o=e==="left"?0:e==="right"?t.width:e,a=n==="top"?0:n==="center"?t.height/2:n==="bottom"?t.height:n;return ti({x:o,y:a},t)}return ti({x:t.width/2,y:t.height/2},t)}const cd={static:Zb,connected:t1},Jb=je({locationStrategy:{type:[String,Function],default:"static",validator:r=>typeof r=="function"||r in cd},location:{type:String,default:"bottom"},origin:{type:String,default:"auto"},offset:[Number,String,Array]},"VOverlay-location-strategies");function Qb(r,t){const e=Ce({}),n=Ce();kn&&(Mi(()=>!!(t.isActive.value&&r.locationStrategy),a=>{var i,s;nt(()=>r.locationStrategy,a),$t(()=>{n.value=void 0}),typeof r.locationStrategy=="function"?n.value=(i=r.locationStrategy(t,r,e))==null?void 0:i.updateLocation:n.value=(s=cd[r.locationStrategy](t,r,e))==null?void 0:s.updateLocation}),window.addEventListener("resize",o,{passive:!0}),$t(()=>{window.removeEventListener("resize",o),n.value=void 0}));function o(a){var i;(i=n.value)==null||i.call(n,a)}return{contentStyles:e,updateLocation:n}}function Zb(){}function e1(r,t){t?r.style.removeProperty("left"):r.style.removeProperty("right");const e=Oc(r);return t?e.x+=parseFloat(r.style.right||0):e.x-=parseFloat(r.style.left||0),e.y-=parseFloat(r.style.top||0),e}function t1(r,t,e){bp(r.activatorEl.value)&&Object.assign(e.value,{position:"fixed",top:0,[r.isRtl.value?"right":"left"]:0});const{preferredAnchor:o,preferredOrigin:a}=Td(()=>{const p=au(t.location,r.isRtl.value),v=t.origin==="overlap"?p:t.origin==="auto"?Aa(p):au(t.origin,r.isRtl.value);return p.side===v.side&&p.align===Da(v).align?{preferredAnchor:iu(p),preferredOrigin:iu(v)}:{preferredAnchor:p,preferredOrigin:v}}),[i,s,u,c]=["minWidth","minHeight","maxWidth","maxHeight"].map(p=>Ae(()=>{const v=parseFloat(t[p]);return isNaN(v)?1/0:v})),l=Ae(()=>{if(Array.isArray(t.offset))return t.offset;if(typeof t.offset=="string"){const p=t.offset.split(" ").map(parseFloat);return p.length<2&&p.push(0),p}return typeof t.offset=="number"?[t.offset,0]:[0,0]});let h=!1;const f=new ResizeObserver(()=>{h&&d()});nt([r.activatorEl,r.contentEl],(p,v)=>{let[m,g]=p,[x,y]=v;x&&f.unobserve(x),m&&f.observe(m),y&&f.unobserve(y),g&&f.observe(g)},{immediate:!0}),$t(()=>{f.disconnect()});function d(){if(h=!1,requestAnimationFrame(()=>{requestAnimationFrame(()=>h=!0)}),!r.activatorEl.value||!r.contentEl.value)return;const p=r.activatorEl.value.getBoundingClientRect(),v=e1(r.contentEl.value,r.isRtl.value),m=Do(r.contentEl.value),g=12;m.length||(m.push(document.documentElement),r.contentEl.value.style.top&&r.contentEl.value.style.left||(v.x-=parseFloat(document.documentElement.style.getPropertyValue("--v-body-scroll-x")||0),v.y-=parseFloat(document.documentElement.style.getPropertyValue("--v-body-scroll-y")||0)));const x=m.reduce((T,A)=>{const F=A.getBoundingClientRect(),P=new ar({x:A===document.documentElement?0:F.x,y:A===document.documentElement?0:F.y,width:A.clientWidth,height:A.clientHeight});return T?new ar({x:Math.max(T.left,P.left),y:Math.max(T.top,P.top),width:Math.min(T.right,P.right)-Math.max(T.left,P.left),height:Math.min(T.bottom,P.bottom)-Math.max(T.top,P.top)}):P},void 0);x.x+=g,x.y+=g,x.width-=g*2,x.height-=g*2;let y={anchor:o.value,origin:a.value};function b(T){const A=new ar(v),F=cc(T.anchor,p),P=cc(T.origin,A);let{x:B,y:U}=Yb(F,P);switch(T.anchor.side){case"top":U-=l.value[0];break;case"bottom":U+=l.value[0];break;case"left":B-=l.value[0];break;case"right":B+=l.value[0];break}switch(T.anchor.align){case"top":U-=l.value[1];break;case"bottom":U+=l.value[1];break;case"left":B-=l.value[1];break;case"right":B+=l.value[1];break}return A.x+=B,A.y+=U,A.width=Math.min(A.width,u.value),A.height=Math.min(A.height,c.value),{overflows:uu(A,x),x:B,y:U}}let w=0,C=0;const I={x:0,y:0},E={x:!1,y:!1};let S=-1;for(;!(S++>10);){const{x:T,y:A,overflows:F}=b(y);w+=T,C+=A,v.x+=T,v.y+=A;{const P=su(y.anchor),B=F.x.before||F.x.after,U=F.y.before||F.y.after;let z=!1;if(["x","y"].forEach(L=>{if(L==="x"&&B&&!E.x||L==="y"&&U&&!E.y){const W={anchor:{...y.anchor},origin:{...y.origin}},G=L==="x"?P==="y"?Da:Aa:P==="y"?Aa:Da;W.anchor=G(W.anchor),W.origin=G(W.origin);const{overflows:j}=b(W);(j[L].before<=F[L].before&&j[L].after<=F[L].after||j[L].before+j[L].after<(F[L].before+F[L].after)/2)&&(y=W,z=E[L]=!0)}}),z)continue}F.x.before&&(w+=F.x.before,v.x+=F.x.before),F.x.after&&(w-=F.x.after,v.x-=F.x.after),F.y.before&&(C+=F.y.before,v.y+=F.y.before),F.y.after&&(C-=F.y.after,v.y-=F.y.after);{const P=uu(v,x);I.x=x.width-P.x.before-P.x.after,I.y=x.height-P.y.before-P.y.after,w+=P.x.before,v.x+=P.x.before,C+=P.y.before,v.y+=P.y.before}break}const k=su(y.anchor);return Object.assign(e.value,{"--v-overlay-anchor-origin":`${y.anchor.side} ${y.anchor.align}`,transformOrigin:`${y.origin.side} ${y.origin.align}`,top:pt(ni(C)),left:r.isRtl.value?void 0:pt(ni(w)),right:r.isRtl.value?pt(ni(-w)):void 0,minWidth:pt(k==="y"?Math.min(i.value,p.width):i.value),maxWidth:pt(lc(ou(I.x,i.value===1/0?0:i.value,u.value))),maxHeight:pt(lc(ou(I.y,s.value===1/0?0:s.value,c.value)))}),{available:I,contentBox:v}}return nt(()=>[o.value,a.value,t.offset,t.minWidth,t.minHeight,t.maxWidth,t.maxHeight],()=>d()),mr(()=>{const p=d();if(!p)return;const{available:v,contentBox:m}=p;m.height>v.y&&requestAnimationFrame(()=>{d(),requestAnimationFrame(()=>{d()})})}),{updateLocation:d}}function ni(r){return Math.round(r*devicePixelRatio)/devicePixelRatio}function lc(r){return Math.ceil(r*devicePixelRatio)/devicePixelRatio}let Ti=!0;const Jo=[];function n1(r){!Ti||Jo.length?(Jo.push(r),Ni()):(Ti=!1,r(),Ni())}let hc=-1;function Ni(){cancelAnimationFrame(hc),hc=requestAnimationFrame(()=>{const r=Jo.shift();r&&r(),Jo.length?Ni():Ti=!0})}const Ro={none:null,close:a1,block:i1,reposition:s1},r1=je({scrollStrategy:{type:[String,Function],default:"block",validator:r=>typeof r=="function"||r in Ro}},"VOverlay-scroll-strategies");function o1(r,t){if(!kn)return;let e;Oi(async()=>{e==null||e.stop(),t.isActive.value&&r.scrollStrategy&&(e=_c(),await mr(),e.active&&e.run(()=>{var n;typeof r.scrollStrategy=="function"?r.scrollStrategy(t,r,e):(n=Ro[r.scrollStrategy])==null||n.call(Ro,t,r,e)}))}),$t(()=>{e==null||e.stop()})}function a1(r){var e;function t(n){r.isActive.value=!1}ld((e=r.activatorEl.value)!=null?e:r.contentEl.value,t)}function i1(r,t){var i;const e=(i=r.root.value)==null?void 0:i.offsetParent,n=[...new Set([...Do(r.activatorEl.value,t.contained?e:void 0),...Do(r.contentEl.value,t.contained?e:void 0)])].filter(s=>!s.classList.contains("v-overlay-scroll-blocked")),o=window.innerWidth-document.documentElement.offsetWidth,a=(s=>Gi(s)&&s)(e||document.documentElement);a&&r.root.value.classList.add("v-overlay--scroll-blocked"),n.forEach((s,u)=>{s.style.setProperty("--v-body-scroll-x",pt(-s.scrollLeft)),s.style.setProperty("--v-body-scroll-y",pt(-s.scrollTop)),s!==document.documentElement&&s.style.setProperty("--v-scrollbar-offset",pt(o)),s.classList.add("v-overlay-scroll-blocked")}),$t(()=>{n.forEach((s,u)=>{const c=parseFloat(s.style.getPropertyValue("--v-body-scroll-x")),l=parseFloat(s.style.getPropertyValue("--v-body-scroll-y"));s.style.removeProperty("--v-body-scroll-x"),s.style.removeProperty("--v-body-scroll-y"),s.style.removeProperty("--v-scrollbar-offset"),s.classList.remove("v-overlay-scroll-blocked"),s.scrollLeft=-c,s.scrollTop=-l}),a&&r.root.value.classList.remove("v-overlay--scroll-blocked")})}function s1(r,t,e){let n=!1,o=-1,a=-1;function i(s){n1(()=>{var l,h;const u=performance.now();(h=(l=r.updateLocation).value)==null||h.call(l,s),n=(performance.now()-u)/(1e3/60)>2})}a=(typeof requestIdleCallback>"u"?s=>s():requestIdleCallback)(()=>{e.run(()=>{var s;ld((s=r.activatorEl.value)!=null?s:r.contentEl.value,u=>{n?(cancelAnimationFrame(o),o=requestAnimationFrame(()=>{o=requestAnimationFrame(()=>{i(u)})})):i(u)})})}),$t(()=>{typeof cancelIdleCallback<"u"&&cancelIdleCallback(a),cancelAnimationFrame(o)})}function ld(r,t){const e=[document,...Do(r)];e.forEach(n=>{n.addEventListener("scroll",t,{passive:!0})}),$t(()=>{e.forEach(n=>{n.removeEventListener("scroll",t)})})}const u1=Symbol.for("vuetify:v-menu"),c1=je({closeDelay:[Number,String],openDelay:[Number,String]},"delay");function l1(r,t){const e={},n=o=>()=>{if(!kn)return Promise.resolve(!0);const a=o==="openDelay";return e.closeDelay&&window.clearTimeout(e.closeDelay),delete e.closeDelay,e.openDelay&&window.clearTimeout(e.openDelay),delete e.openDelay,new Promise(i=>{var u;const s=parseInt((u=r[o])!=null?u:0,10);e[o]=window.setTimeout(()=>{t==null||t(a),i(a)},s)})};return{runCloseDelay:n("closeDelay"),runOpenDelay:n("openDelay")}}const h1=je({activator:[String,Object],activatorProps:{type:Object,default:()=>({})},openOnClick:{type:Boolean,default:void 0},openOnHover:Boolean,openOnFocus:{type:Boolean,default:void 0},closeOnContentClick:Boolean,...c1()},"VOverlay-activator");function f1(r,t){let{isActive:e,isTop:n}=t;const o=Ce();let a=!1,i=!1,s=!0;const u=Ae(()=>r.openOnFocus||r.openOnFocus==null&&r.openOnHover),c=Ae(()=>r.openOnClick||r.openOnClick==null&&!r.openOnHover&&!u.value),{runOpenDelay:l,runCloseDelay:h}=l1(r,y=>{y===(r.openOnHover&&a||u.value&&i)&&!(r.openOnHover&&e.value&&!n.value)&&(e.value!==y&&(s=!0),e.value=y)}),f={onClick:y=>{y.stopPropagation(),o.value=y.currentTarget||y.target,e.value=!e.value},onMouseenter:y=>{var b;(b=y.sourceCapabilities)!=null&&b.firesTouchEvents||(a=!0,o.value=y.currentTarget||y.target,l())},onMouseleave:y=>{a=!1,h()},onFocus:y=>{Fd(y.target,":focus-visible")!==!1&&(i=!0,y.stopPropagation(),o.value=y.currentTarget||y.target,l())},onBlur:y=>{i=!1,y.stopPropagation(),h()}},d=Ae(()=>{const y={};return c.value&&(y.onClick=f.onClick),r.openOnHover&&(y.onMouseenter=f.onMouseenter,y.onMouseleave=f.onMouseleave),u.value&&(y.onFocus=f.onFocus,y.onBlur=f.onBlur),y}),p=Ae(()=>{const y={};if(r.openOnHover&&(y.onMouseenter=()=>{a=!0,l()},y.onMouseleave=()=>{a=!1,h()}),u.value&&(y.onFocusin=()=>{i=!0,l()},y.onFocusout=()=>{i=!1,h()}),r.closeOnContentClick){const b=Ec(u1,null);y.onClick=()=>{e.value=!1,b==null||b.closeParents()}}return y}),v=Ae(()=>{const y={};return r.openOnHover&&(y.onMouseenter=()=>{s&&(a=!0,s=!1,l())},y.onMouseleave=()=>{a=!1,h()}),y});nt(n,y=>{y&&(r.openOnHover&&!a&&(!u.value||!i)||u.value&&!i&&(!r.openOnHover||!a))&&(e.value=!1)});const m=Ce();Oi(()=>{!m.value||mr(()=>{o.value=Nd(m.value)})});const g=Bi("useActivator");let x;return nt(()=>!!r.activator,y=>{y&&kn?(x=_c(),x.run(()=>{d1(r,g,{activatorEl:o,activatorEvents:d})})):x&&x.stop()},{flush:"post",immediate:!0}),$t(()=>{x==null||x.stop()}),{activatorEl:o,activatorRef:m,activatorEvents:d,contentEvents:p,scrimEvents:v}}function d1(r,t,e){let{activatorEl:n,activatorEvents:o}=e;nt(()=>r.activator,(u,c)=>{if(c&&u!==c){const l=s(c);l&&i(l)}u&&mr(()=>a())},{immediate:!0}),nt(()=>r.activatorProps,()=>{a()}),$t(()=>{i()});function a(){let u=arguments.length>0&&arguments[0]!==void 0?arguments[0]:s(),c=arguments.length>1&&arguments[1]!==void 0?arguments[1]:r.activatorProps;!u||pp(u,kt(o.value,c))}function i(){let u=arguments.length>0&&arguments[0]!==void 0?arguments[0]:s(),c=arguments.length>1&&arguments[1]!==void 0?arguments[1]:r.activatorProps;!u||vp(u,kt(o.value,c))}function s(){var l,h;let u=arguments.length>0&&arguments[0]!==void 0?arguments[0]:r.activator,c;if(u)if(u==="parent"){let f=(h=(l=t==null?void 0:t.proxy)==null?void 0:l.$el)==null?void 0:h.parentNode;for(;f.hasAttribute("data-no-activator");)f=f.parentNode;c=f}else typeof u=="string"?c=document.querySelector(u):"$el"in u?c=u.$el:c=u;return n.value=(c==null?void 0:c.nodeType)===Node.ELEMENT_NODE?c:null,n.value}}function p1(){if(!kn)return Et(!1);const{ssr:r}=Sc();if(r){const t=Et(!1);return Li(()=>{t.value=!0}),t}else return Et(!0)}const v1=je({eager:Boolean},"lazy");function m1(r,t){const e=Et(!1),n=Ae(()=>e.value||r.eager||t.value);nt(t,()=>e.value=!0);function o(){r.eager||(e.value=!1)}return{isBooted:e,hasContent:n,onAfterLeave:o}}function hd(){const t=Bi("useScopeId").vnode.scopeId;return{scopeId:t?{[t]:""}:void 0}}const fc=Symbol.for("vuetify:stack"),Er=kc([]);function g1(r,t,e){const n=Bi("useStack"),o=!e,a=Ec(fc,void 0),i=kc({activeChildren:new Set});Pd(fc,i);const s=Et(+t.value);Mi(r,()=>{var h;const l=(h=Er.at(-1))==null?void 0:h[1];s.value=l?l+10:+t.value,o&&Er.push([n.uid,s.value]),a==null||a.activeChildren.add(n.uid),$t(()=>{if(o){const f=Md(Er).findIndex(d=>d[0]===n.uid);Er.splice(f,1)}a==null||a.activeChildren.delete(n.uid)})});const u=Et(!0);o&&Oi(()=>{var h;const l=((h=Er.at(-1))==null?void 0:h[0])===n.uid;setTimeout(()=>u.value=l)});const c=Ae(()=>!i.activeChildren.size);return{globalTop:Rc(u),localTop:c,stackStyles:Ae(()=>({zIndex:s.value}))}}function y1(r){return{teleportTarget:Ae(()=>{const e=r.value;if(e===!0||!kn)return;const n=e===!1?document.body:typeof e=="string"?document.querySelector(e):e;if(n==null)return;let o=n.querySelector(":scope > .v-overlay-container");return o||(o=document.createElement("div"),o.className="v-overlay-container",n.appendChild(o)),o})}}function x1(){return!0}function fd(r,t,e){if(!r||dd(r,e)===!1)return!1;const n=Bc(t);if(typeof ShadowRoot<"u"&&n instanceof ShadowRoot&&n.host===r.target)return!1;const o=(typeof e.value=="object"&&e.value.include||(()=>[]))();return o.push(t),!o.some(a=>a==null?void 0:a.contains(r.target))}function dd(r,t){return(typeof t.value=="object"&&t.value.closeConditional||x1)(r)}function b1(r,t,e){const n=typeof e.value=="function"?e.value:e.value.handler;t._clickOutside.lastMousedownWasOutside&&fd(r,t,e)&&setTimeout(()=>{dd(r,e)&&n&&n(r)},0)}function dc(r,t){const e=Bc(r);t(document),typeof ShadowRoot<"u"&&e instanceof ShadowRoot&&t(e)}const w1={mounted(r,t){const e=o=>b1(o,r,t),n=o=>{r._clickOutside.lastMousedownWasOutside=fd(o,r,t)};dc(r,o=>{o.addEventListener("click",e,!0),o.addEventListener("mousedown",n,!0)}),r._clickOutside||(r._clickOutside={lastMousedownWasOutside:!1}),r._clickOutside[t.instance.$.uid]={onClick:e,onMousedown:n}},unmounted(r,t){!r._clickOutside||(dc(r,e=>{var a;if(!e||!((a=r._clickOutside)!=null&&a[t.instance.$.uid]))return;const{onClick:n,onMousedown:o}=r._clickOutside[t.instance.$.uid];e.removeEventListener("click",n,!0),e.removeEventListener("mousedown",o,!0)}),delete r._clickOutside[t.instance.$.uid])}};function C1(r){const{modelValue:t,color:e,...n}=r;return $(Io,{name:"fade-transition",appear:!0},{default:()=>[r.modelValue&&$("div",kt({class:["v-overlay__scrim",r.color.backgroundColorClasses.value],style:r.color.backgroundColorStyles.value},n),null)]})}const pd=je({absolute:Boolean,attach:[Boolean,String,Object],closeOnBack:{type:Boolean,default:!0},contained:Boolean,contentClass:null,contentProps:null,disabled:Boolean,noClickAnimation:Boolean,modelValue:Boolean,persistent:Boolean,scrim:{type:[Boolean,String],default:!0},zIndex:{type:[Number,String],default:2e3},...h1(),...Rt(),...Wi(),...v1(),...Jb(),...r1(),...ea(),...ad()},"VOverlay"),pc=dt()({name:"VOverlay",directives:{ClickOutside:w1},inheritAttrs:!1,props:{_disableGlobalStack:Boolean,...pd()},emits:{"click:outside":r=>!0,"update:modelValue":r=>!0,afterLeave:()=>!0},setup(r,t){let{slots:e,attrs:n,emit:o}=t;const a=Ic(r,"modelValue"),i=Ae({get:()=>a.value,set:W=>{W&&r.disabled||(a.value=W)}}),{teleportTarget:s}=y1(Ae(()=>r.attach||r.contained)),{themeClasses:u}=ta(r),{rtlClasses:c,isRtl:l}=Od(),{hasContent:h,onAfterLeave:f}=m1(r,i),d=cp(Ae(()=>typeof r.scrim=="string"?r.scrim:null)),{globalTop:p,localTop:v,stackStyles:m}=g1(i,Ac(r,"zIndex"),r._disableGlobalStack),{activatorEl:g,activatorRef:x,activatorEvents:y,contentEvents:b,scrimEvents:w}=f1(r,{isActive:i,isTop:v}),{dimensionStyles:C}=Vi(r),I=p1(),{scopeId:E}=hd();nt(()=>r.disabled,W=>{W&&(i.value=!1)});const S=Ce(),k=Ce(),{contentStyles:T,updateLocation:A}=Qb(r,{isRtl:l,contentEl:k,activatorEl:g,isActive:i});o1(r,{root:S,contentEl:k,activatorEl:g,isActive:i,updateLocation:A});function F(W){o("click:outside",W),r.persistent?L():i.value=!1}function P(){return i.value&&p.value}kn&&nt(i,W=>{W?window.addEventListener("keydown",B):window.removeEventListener("keydown",B)},{immediate:!0});function B(W){var G,j;W.key==="Escape"&&p.value&&(r.persistent?L():(i.value=!1,(G=k.value)!=null&&G.contains(document.activeElement)&&((j=g.value)==null||j.focus())))}const U=lp();Mi(()=>r.closeOnBack,()=>{hp(U,W=>{p.value&&i.value?(W(!1),r.persistent?L():i.value=!1):W()})});const z=Ce();nt(()=>i.value&&(r.absolute||r.contained)&&s.value==null,W=>{if(W){const G=yp(S.value);G&&G!==document.scrollingElement&&(z.value=G.scrollTop)}});function L(){r.noClickAnimation||k.value&&Sr(k.value,[{transformOrigin:"center"},{transform:"scale(1.03)"},{transformOrigin:"center"}],{duration:150,easing:ai})}return Qt(()=>{var W;return $(Cc,null,[(W=e.activator)==null?void 0:W.call(e,{isActive:i.value,props:kt({ref:x},y.value,r.activatorProps)}),I.value&&h.value&&$(Bd,{disabled:!s.value,to:s.value},{default:()=>[$("div",kt({class:["v-overlay",{"v-overlay--absolute":r.absolute||r.contained,"v-overlay--active":i.value,"v-overlay--contained":r.contained},u.value,c.value,r.class],style:[m.value,{top:pt(z.value)},r.style],ref:S},E,n),[$(C1,kt({color:d,modelValue:i.value&&!!r.scrim},w.value),null),$(Ar,{appear:!0,persisted:!0,transition:r.transition,target:g.value,onAfterLeave:()=>{f(),o("afterLeave")}},{default:()=>{var G;return[Fr($("div",kt({ref:k,class:["v-overlay__content",r.contentClass],style:[C.value,T.value]},b.value,r.contentProps),[(G=e.default)==null?void 0:G.call(e,{isActive:i})]),[[Pi,i.value],[Fi("click-outside"),{handler:F,closeConditional:P,include:()=>[g.value]}]])]}})])]})])}),{activatorEl:g,animateClick:L,contentEl:k,globalTop:p,localTop:v,updateLocation:A}}}),ri=Symbol("Forwarded refs");function oi(r,t){let e=r;for(;e;){const n=Reflect.getOwnPropertyDescriptor(e,t);if(n)return n;e=Object.getPrototypeOf(e)}}function _1(r){for(var t=arguments.length,e=new Array(t>1?t-1:0),n=1;n<t;n++)e[n-1]=arguments[n];return r[ri]=e,new Proxy(r,{get(o,a){if(Reflect.has(o,a))return Reflect.get(o,a);if(!(typeof a=="symbol"||a.startsWith("__"))){for(const i of e)if(i.value&&Reflect.has(i.value,a)){const s=Reflect.get(i.value,a);return typeof s=="function"?s.bind(i.value):s}}},has(o,a){if(Reflect.has(o,a))return!0;if(typeof a=="symbol"||a.startsWith("__"))return!1;for(const i of e)if(i.value&&Reflect.has(i.value,a))return!0;return!1},set(o,a,i){if(Reflect.has(o,a))return Reflect.set(o,a,i);if(typeof a=="symbol"||a.startsWith("__"))return!1;for(const s of e)if(s.value&&Reflect.has(s.value,a))return Reflect.set(s.value,a,i);return!1},getOwnPropertyDescriptor(o,a){var s,u;const i=Reflect.getOwnPropertyDescriptor(o,a);if(i)return i;if(!(typeof a=="symbol"||a.startsWith("__"))){for(const c of e){if(!c.value)continue;const l=(u=oi(c.value,a))!=null?u:"_"in c.value?oi((s=c.value._)==null?void 0:s.setupState,a):void 0;if(l)return l}for(const c of e){const l=c.value&&c.value[ri];if(!l)continue;const h=l.slice();for(;h.length;){const f=h.shift(),d=oi(f.value,a);if(d)return d;const p=f.value&&f.value[ri];p&&h.push(...p)}}}}})}const E1=je({fullscreen:Boolean,retainFocus:{type:Boolean,default:!0},scrollable:Boolean,...pd({origin:"center center",scrollStrategy:"block",transition:{component:$b},zIndex:2400})},"VDialog"),S1=dt()({name:"VDialog",props:E1(),emits:{"update:modelValue":r=>!0},setup(r,t){let{slots:e}=t;const n=Ic(r,"modelValue"),{scopeId:o}=hd(),a=Ce();function i(u){var h,f;const c=u.relatedTarget,l=u.target;if(c!==l&&((h=a.value)==null?void 0:h.contentEl)&&((f=a.value)==null?void 0:f.globalTop)&&![document,a.value.contentEl].includes(l)&&!a.value.contentEl.contains(l)){const d=Ld(a.value.contentEl);if(!d.length)return;const p=d[0],v=d[d.length-1];c===p?v.focus():p.focus()}}kn&&nt(()=>n.value&&r.retainFocus,u=>{u?document.addEventListener("focusin",i):document.removeEventListener("focusin",i)},{immediate:!0}),nt(n,async u=>{var c,l;await mr(),u?(c=a.value.contentEl)==null||c.focus({preventScroll:!0}):(l=a.value.activatorEl)==null||l.focus({preventScroll:!0})});const s=Ae(()=>kt({"aria-haspopup":"dialog","aria-expanded":String(n.value)},r.activatorProps));return Qt(()=>{const[u]=pc.filterProps(r);return $(pc,kt({ref:a,class:["v-dialog",{"v-dialog--fullscreen":r.fullscreen,"v-dialog--scrollable":r.scrollable},r.class],style:r.style},u,{modelValue:n.value,"onUpdate:modelValue":c=>n.value=c,"aria-modal":"true",activatorProps:s.value,role:"dialog"},o),{activator:e.activator,default:function(){for(var c=arguments.length,l=new Array(c),h=0;h<c;h++)l[h]=arguments[h];return $(Ao,{root:"VDialog"},{default:()=>{var f;return[(f=e.default)==null?void 0:f.call(e,...l)]}})}})}),_1({},a)}});const k1=je({color:String,inset:Boolean,length:[Number,String],thickness:[Number,String],vertical:Boolean,...Rt(),...ea()},"VDivider"),R1=dt()({name:"VDivider",props:k1(),setup(r,t){let{attrs:e}=t;const{themeClasses:n}=ta(r),{textColorClasses:o,textColorStyles:a}=fp(Ac(r,"color")),i=Ae(()=>{const s={};return r.length&&(s[r.vertical?"maxHeight":"maxWidth"]=pt(r.length)),r.thickness&&(s[r.vertical?"borderRightWidth":"borderTopWidth"]=pt(r.thickness)),s});return Qt(()=>$("hr",{class:[{"v-divider":!0,"v-divider--inset":r.inset,"v-divider--vertical":r.vertical},n.value,o.value,r.class],style:[i.value,a.value,r.style],"aria-orientation":!e.role||e.role==="separator"?r.vertical?"vertical":"horizontal":void 0,role:`${e.role||"separator"}`},null)),{}}});const vd=(()=>na.reduce((r,t)=>(r[t]={type:[Boolean,String,Number],default:!1},r),{}))(),md=(()=>na.reduce((r,t)=>{const e="offset"+Qo(t);return r[e]={type:[String,Number],default:null},r},{}))(),gd=(()=>na.reduce((r,t)=>{const e="order"+Qo(t);return r[e]={type:[String,Number],default:null},r},{}))(),vc={col:Object.keys(vd),offset:Object.keys(md),order:Object.keys(gd)};function I1(r,t,e){let n=r;if(!(e==null||e===!1)){if(t){const o=t.replace(r,"");n+=`-${o}`}return r==="col"&&(n="v-"+n),r==="col"&&(e===""||e===!0)||(n+=`-${e}`),n.toLowerCase()}}const A1=["auto","start","end","center","baseline","stretch"],D1=je({cols:{type:[Boolean,String,Number],default:!1},...vd,offset:{type:[String,Number],default:null},...md,order:{type:[String,Number],default:null},...gd,alignSelf:{type:String,default:null,validator:r=>A1.includes(r)},...Rt(),...Xr()},"VCol"),mc=dt()({name:"VCol",props:D1(),setup(r,t){let{slots:e}=t;const n=Ae(()=>{const o=[];let a;for(a in vc)vc[a].forEach(s=>{const u=r[s],c=I1(a,s,u);c&&o.push(c)});const i=o.some(s=>s.startsWith("v-col-"));return o.push({"v-col":!i||!r.cols,[`v-col-${r.cols}`]:r.cols,[`offset-${r.offset}`]:r.offset,[`order-${r.order}`]:r.order,[`align-self-${r.alignSelf}`]:r.alignSelf}),o});return()=>{var o;return Zo(r.tag,{class:[n.value,r.class],style:r.style},(o=e.default)==null?void 0:o.call(e))}}}),nu=["start","end","center"],yd=["space-between","space-around","space-evenly"];function ru(r,t){return na.reduce((e,n)=>{const o=r+Qo(n);return e[o]=t(),e},{})}const T1=[...nu,"baseline","stretch"],xd=r=>T1.includes(r),bd=ru("align",()=>({type:String,default:null,validator:xd})),N1=[...nu,...yd],wd=r=>N1.includes(r),Cd=ru("justify",()=>({type:String,default:null,validator:wd})),F1=[...nu,...yd,"stretch"],_d=r=>F1.includes(r),Ed=ru("alignContent",()=>({type:String,default:null,validator:_d})),gc={align:Object.keys(bd),justify:Object.keys(Cd),alignContent:Object.keys(Ed)},P1={align:"align",justify:"justify",alignContent:"align-content"};function M1(r,t,e){let n=P1[r];if(e!=null){if(t){const o=t.replace(r,"");n+=`-${o}`}return n+=`-${e}`,n.toLowerCase()}}const O1=je({dense:Boolean,noGutters:Boolean,align:{type:String,default:null,validator:xd},...bd,justify:{type:String,default:null,validator:wd},...Cd,alignContent:{type:String,default:null,validator:_d},...Ed,...Rt(),...Xr()},"VRow"),yc=dt()({name:"VRow",props:O1(),setup(r,t){let{slots:e}=t;const n=Ae(()=>{const o=[];let a;for(a in gc)gc[a].forEach(i=>{const s=r[i],u=M1(a,i,s);u&&o.push(u)});return o.push({"v-row--no-gutters":r.noGutters,"v-row--dense":r.dense,[`align-${r.align}`]:r.align,[`justify-${r.justify}`]:r.justify,[`align-content-${r.alignContent}`]:r.alignContent}),o});return()=>{var o;return Zo(r.tag,{class:["v-row",n.value,r.class],style:r.style},(o=e.default)==null?void 0:o.call(e))}}});function B1(){const r=Et(!1);return Li(()=>{window.requestAnimationFrame(()=>{r.value=!0})}),{ssrBootStyles:Ae(()=>r.value?void 0:{transition:"none !important"}),isBooted:Rc(r)}}const L1=je({scrollable:Boolean,...Rt(),...Xr({tag:"main"})},"VMain"),W1=dt()({name:"VMain",props:L1(),setup(r,t){let{slots:e}=t;const{mainStyles:n}=dp(),{ssrBootStyles:o}=B1();return Qt(()=>$(r.tag,{class:["v-main",{"v-main--scrollable":r.scrollable},r.class],style:[n.value,o.value,r.style]},{default:()=>{var a,i;return[r.scrollable?$("div",{class:"v-main__scroller"},[(a=e.default)==null?void 0:a.call(e)]):(i=e.default)==null?void 0:i.call(e)]}})),{}}}),V1={name:"CameraComponent",components:{Vue3Lottie:Wd},setup(){const{mobile:r}=Sc(),t=Vd(),e=Ce(null),n=Ce(null),o=Ce(null),a=Ce(!1),i=Ce(!1),s=Ce(!0),u=localStorage.getItem("selectedCamera"),c=Ce(5),l=Ce(10),h=Ce(!1),f=Ce(!1),d=Ce({x:0,y:0}),p=Ce({x:0,y:0}),v=Ce(10),m=Ce(10),g=Ce(0),x=Ce(!0),y=Ce("K\u1EBFt qu\u1EA3"),b=Ce("");let w=null,C=null;Li(async()=>{await tt.tinyFaceDetector.loadFromUri("/weights"),await tt.faceLandmark68Net.loadFromUri("/weights"),await tt.ssdMobilenetv1.loadFromUri("/weights"),await tt.faceLandmark68TinyNet.loadFromUri("/weights"),B()});const I=()=>{c.value=5,l.value=10,h.value=!1,v.value=10,m.value=10,g.value=0,setTimeout(()=>{z()},500)},E=()=>{m.value=v.value-g.value,m.value<=4&&m.value>=0?b.value="\u{1F61E} R\u1EA5t c\xF3 kh\u1EA3 n\u0103ng b\u1EA1n \u0111ang b\u1ECB kh\xF4 m\u1EAFt nghi\xEAm tr\u1ECDng.":m.value>=5&&m.value<=9?b.value="\u2639\uFE0F M\u1EAFt b\u1EA1n c\xF3 th\u1EC3 \u0111ang b\u1ECB kh\xF4 nh\u1EB9.":m.value===10?b.value="\u{1F604} M\u1EAFt b\u1EA1n r\u1EA5t t\u1ED1t! B\u1EA1n kh\xF4ng b\u1ECB kh\xF4 m\u1EAFt.":m.value<0&&(m.value=0,b.value="\u{1F61E} R\u1EA5t c\xF3 kh\u1EA3 n\u0103ng b\u1EA1n \u0111ang b\u1ECB kh\xF4 m\u1EAFt nghi\xEAm tr\u1ECDng."),x.value=!0},S=()=>{a.value=!0;const q=e.value.videoWidth,X=e.value.videoHeight;e.value.width=q,e.value.height=X,o.value.width=q,o.value.height=X},k=async()=>{a.value&&(await F(),requestAnimationFrame(k))};let T=Date.now(),A=0;const F=async()=>{if(!!o.value&&(A++,A%4===0)){const q=Date.now();if(q-T<100)return;T=q;const X=await Ob(e.value,new Kf).withFaceLandmarks(!0),oe=o.value.getContext("2d");if(oe.fillStyle="black",oe.clearRect(0,0,o.value.width,o.value.height),X){const re=td(X,{width:o.value.width,height:o.value.height}),ie=re.landmarks.getLeftEye(),ce=re.landmarks.getRightEye();if(c.value<=0){h.value==!1&&L(),oe.fillRect(0,0,o.value.width,o.value.height);const de=re.landmarks.getLeftEyeBrow(),le=re.landmarks.getRightEyeBrow();P(oe,e.value,de,le,ie,ce)}else bx(o.value,[re])}}};function P(q,X,oe,re,ie,ce){let le=Math.min(...oe.map(he=>he.x),...ie.map(he=>he.x),...re.map(he=>he.x),...ce.map(he=>he.x))-20,_e=Math.min(...oe.map(he=>he.y),...ie.map(he=>he.y),...re.map(he=>he.y),...ce.map(he=>he.y))-20,me=Math.max(...oe.map(he=>he.x),...ie.map(he=>he.x),...re.map(he=>he.x),...ce.map(he=>he.x))+20,we=Math.max(...oe.map(he=>he.y),...ie.map(he=>he.y),...re.map(he=>he.y),...ce.map(he=>he.y))+20;const ge=me-le,Ee=we-_e,ke=o.value.width/2,Re=o.value.height/2,wt=le+ge/2,Ct=_e+Ee/2,ot=ke-wt,pn=Re-Ct,At=le+ot,Vt=_e+pn;l.value>0&&G(ie,ce)&&(console.log("Blink detected!"),g.value++),q.drawImage(X,le,_e,ge,Ee,At,Vt,ge,Ee)}const B=async()=>{i.value=!0;try{const q={video:!0};u&&(q.video={deviceId:u.deviceId}),C=await navigator.mediaDevices.getUserMedia(q),e.value.srcObject=C,await new Promise(X=>e.value.onloadedmetadata=X),a.value=!0}catch(q){console.error("Error accessing the camera:",q),U()}i.value=!1},U=()=>{confirm("L\u1ED7i camera, B\u1EA1n c\xF3 mu\u1ED1n reload l\u1EA1i tr\xECnh duy\u1EC7t?")&&t.push("/patientauths")},z=()=>{clearInterval(w),s.value=!1,w=setInterval(()=>{c.value>0?c.value--:clearInterval(w)},1e3),k()},L=()=>{clearInterval(w),h.value=!0,w=setInterval(()=>{console.log(l.value),l.value>0?l.value--:clearInterval(w)},1e3)},W=q=>{const X=Math.hypot(q[1].x-q[5].x,q[1].y-q[5].y),oe=Math.hypot(q[2].x-q[4].x,q[2].y-q[4].y),re=Math.hypot(q[0].x-q[3].x,q[0].y-q[3].y);return(X+oe)/(2*re)},G=(q,X)=>{const oe=W(q),re=W(X);return(oe+re)/2<.25?(f.value=!0,d.value={x:(q[0].x+q[3].x)/2,y:(q[1].y+q[5].y)/2},p.value={x:(X[0].x+X[3].x)/2,y:(X[1].y+X[5].y)/2},setTimeout(()=>{f.value=!1},500),!0):!1};return{countdown:c,router:t,videoElement:e,container:n,canvas:o,isCameraOn:a,showOverlay:s,isLoading:i,isTesting:h,countdownTesting:l,isBlinkingStatus:f,leftEyePosition:d,rightEyePosition:p,mobile:r,finalScore:m,showResultDialog:x,resultTitle:y,resultMessage:b,score:v,startDetection:z,onVideoLoaded:S,retryTest:I,getResult:E,moveToAnotherTest:async()=>{const{id:q,datetime:X}=t.currentRoute.value.params;q&&X?(m.value!=null&&await Hd(`checkups/${q}/${X}/blinkScore`,m.value),t.push(`/color-test/${q}/${X}/od`)):t.push("/color-test")}}}},ro=r=>(Ud("data-v-c3bc509c"),r=r(),Gd(),r),z1={ref:"container",class:"video-container"},U1={class:"eye-video-container"},G1={ref:"canvas"},H1={key:0,class:"overlay-camera"},q1=ro(()=>ut("h1",{class:"overlay-title"},"Eye Blink Test",-1)),j1=ro(()=>ut("h2",{class:"overlay-subtitle"}," B\u1EB1ng c\xE1ch \u0111o th\u1EDDi gian m\u1EAFt b\u1EA1n m\u1EDF bao l\xE2u, b\u1EA1n c\xF3 th\u1EC3 ki\u1EC3m tra xem m\xECnh c\xF3 b\u1ECB kh\xF4 m\u1EAFt hay kh\xF4ng. ",-1)),K1=ro(()=>ut("h2",{class:"overlay-subtitle mb-8"}," \u0110\u1EC3 tham gia b\xE0i ki\u1EC3m tra kh\xF4 m\u1EAFt n\xE0y, vui l\xF2ng b\u1EADt camera c\u1EE7a b\u1EA1n ngay b\xE2y gi\u1EDD. ",-1)),X1={key:1,class:"intro-detect"},$1={key:0,class:"overlay-subtitle"},Y1={key:1,class:"overlay-subtitle"},J1={key:2,class:"countbar-section"},Q1=ro(()=>ut("div",{class:"d-flex justify-space-between w-100"},[ut("h2",{class:"overlay-subtitle"},"0s"),ut("h2",{class:"overlay-subtitle"},"10s")],-1)),Z1={class:"countdown-bar-container"},ew={key:3,class:"action-buttons"},tw=ro(()=>ut("div",{class:"text-center mt-3"},"Score",-1)),nw={class:"font-weight-bold text-center text-h3"};function rw(r,t,e,n,o,a){const i=zd("Vue3Lottie");return Ut(),oo(W1,null,{default:at(()=>[$(ic,{flat:"",class:"camera-wrapper"},{default:at(()=>[ut("div",z1,[Fr(ut("div",U1,[ut("video",{ref:"videoElement",onLoadedmetadata:t[0]||(t[0]=(...s)=>n.onVideoLoaded&&n.onVideoLoaded(...s)),autoplay:"",class:"videot"},null,544)],512),[[Pi,n.isCameraOn]]),ut("canvas",G1,null,512),n.showOverlay?(Ut(),$n("div",H1,[$(yc,null,{default:at(()=>[$(mc,{class:"text-center"},{default:at(()=>[q1,j1,K1,$(io,{class:"detect-button",onClick:n.startDetection},{default:at(()=>[Yn("Start")]),_:1},8,["onClick"])]),_:1})]),_:1})])):tn("",!0),!n.showOverlay&&n.countdown<5?(Ut(),$n("div",X1,[$(yc,null,{default:at(()=>[$(mc,{class:"text-center"},{default:at(()=>[n.countdown<5&&n.countdown!=0?(Ut(),$n("h2",$1," Khi b\u1EAFt \u0111\u1EA7u , b\u1EA1n h\xE3y gi\u1EEF y\xEAn m\u1EAFt trong v\xF2ng 10 gi\xE2y ")):tn("",!0),n.countdown<5&&n.countdown!=0?(Ut(),$n("h2",Y1," B\u1EAFt \u0111\u1EA7u nh\u1EADn di\u1EC7n khu v\u1EF1c m\u1EAFt c\u1EE7a b\u1EA1n trong "+ao(n.countdown)+" gi\xE2y ",1)):tn("",!0)]),_:1})]),_:1})])):tn("",!0),n.isTesting?(Ut(),$n("div",J1,[Q1,ut("div",Z1,[ut("div",{class:"countdown-bar",style:Ia({width:`${(10-n.countdownTesting)*10}%`})},null,4)])])):tn("",!0),(10-n.countdownTesting)*10===100?(Ut(),$n("div",ew,[$(io,{class:"btn-retry",onClick:n.retryTest},{default:at(()=>[Yn("Th\u1EED l\u1EA1i")]),_:1},8,["onClick"]),$(io,{class:"btn-get-result",onClick:n.getResult},{default:at(()=>[Yn("Nh\u1EADn k\u1EBFt qu\u1EA3")]),_:1},8,["onClick"])])):tn("",!0),n.isBlinkingStatus?(Ut(),oo(i,{key:4,animationLink:"https://lottie.host/84919d81-006e-43ae-8d7d-c5786da676d2/FRo1gC7x1n.json",height:n.mobile?100:200,width:n.mobile?100:200,style:Ia(`position: absolute; top: ${n.leftEyePosition.y}px; left: ${n.mobile?"5%":"30%"}; z-index: 999;`)},null,8,["height","width","style"])):tn("",!0),n.isBlinkingStatus?(Ut(),oo(i,{key:5,animationLink:"https://lottie.host/84919d81-006e-43ae-8d7d-c5786da676d2/FRo1gC7x1n.json",height:n.mobile?100:200,width:n.mobile?100:200,style:Ia(`position: absolute; top: ${n.rightEyePosition.y}px; right: ${n.mobile?"5%":"30%"}; z-index: 999;`)},null,8,["height","width","style"])):tn("",!0)],512),(10-n.countdownTesting)*10===100?(Ut(),oo(S1,{key:0,modelValue:n.showResultDialog,"onUpdate:modelValue":t[1]||(t[1]=s=>n.showResultDialog=s),"max-width":"800px"},{default:at(()=>[$(ic,{class:"result-card"},{default:at(()=>[$(rd,{class:"headline text-center blue darken-2 white--text"},{default:at(()=>[Yn(ao(n.resultTitle),1)]),_:1}),tw,ut("p",nw,ao(n.finalScore),1),$(ud,{class:"text-center mt-4 text-subtitle-1 font-weight-light"},{default:at(()=>[Yn(ao(n.resultMessage),1)]),_:1}),$(R1),$(nd,{class:"justify-center"},{default:at(()=>[$(io,{large:"",color:"blue darken-2",text:"",onClick:n.moveToAnotherTest},{default:at(()=>[Yn(" Ti\u1EBFp t\u1EE5c ")]),_:1},8,["onClick"])]),_:1})]),_:1})]),_:1},8,["modelValue"])):tn("",!0)]),_:1})]),_:1})}const lw=qd(V1,[["render",rw],["__scopeId","data-v-c3bc509c"]]);export{lw as default};
