# build stage
FROM node:14-alpine as build
WORKDIR /src
COPY . .
# RUN npm install
# RUN chmod +x /src/replaceEnv.sh
# RUN /src/replaceEnv.sh
# RUN npm run build

# production stage
FROM steebchen/nginx-spa:stable as production

# Copy over the self-signed cert and key
# COPY ./keys/selfsigned.crt /etc/ssl/certs/
# COPY ./keys/selfsigned.key /etc/ssl/private/

# Modify the nginx configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Expose both HTTP & HTTPS
EXPOSE 89
EXPOSE 443

COPY --from=build /src/dist /app

CMD ["nginx"]
