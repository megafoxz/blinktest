const path = require("path");
const { app, BrowserWindow, contextBridge } = require("electron");

let win = null;

app.on("ready", function () {
  // Initialize the window to our specified dimensions
  win = new BrowserWindow({
    width: 1200,
    height: 1000,
    // autoHideMenuBar: true,
    // frame: false,
    // fullscreen: true,

    webPreferences: {
      nativeWindowOpen: true,
      nodeIntegration: true,
      contextIsolation: true,
      enableRemoteModule: true,
      sandbox: true,
      worldSafeExecuteJavaScript: true,
      webViewTag: true,
      devTools: true,
      webSecurity: false,

      // contentSecurityPolicy:
      //   "default-src 'self'; script-src 'self' 'nonce-xyz'; style-src 'self' 'unsafe-inline';",
      // preload: path.join(__dirname, "./preload.js"),
    },
  });
  win.webContents.insertCSS("html, body {  overflow: hidden !important; }");
  // win.setMenuBarVisibility(false);
  // win.setMenu(null);

  // Specify entry point to default entry point of vue.js
  win.loadURL("http://localhost:3000");
  // win.loadFile(path.join(__dirname, "./index.html"));

  // Remove window once app is closed
  win.on("closed", function () {
    win = null;
  });

  // contextBridge.exposeInMainWorld("api", {
  //   getWebContentsId: () => mainWindow.webContents.id,
  // });
});
//create the application window if the window variable is null
app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});
//quit the app once closed
app.on("window-all-closed", function () {
  if (process.platform != "darwin") {
    app.quit();
  }
});
