#!/bin/sh

ROOT_DIR=/src

# Replace env vars in JavaScript files
echo "Replacing env constants in JS"
for file in $ROOT_DIR/.env.production;
do
  echo "Processing $file ...";
  sed -i 's|http://localhost:4000/graphql|'${VITE_APP_APOLLO_SERVER_URL}'|g' $file 
done

echo "Done Replacing..."