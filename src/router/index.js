// Composables
import { createRouter, createWebHistory } from "vue-router";
import AuthRoutes from "./AuthRoutes";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/:pathMatch(.*)*",
      component: () => import("@/views/Error404.vue"),
    },
    AuthRoutes,
  ],
});

router.beforeEach((to, from, next) => {
  next();

  // const requiresAuth = to.meta.requiresAuth;
  // if (requiresAuth) {
  //   next({ name: "BlinkDetection" });
  // } else {
  //   next();
  // }
});

export default router;
