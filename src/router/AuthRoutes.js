const AuthRoutes = {
  path: "/",
  component: () => import("@/views/AuthLayout.vue"),
  meta: {
    requiresAuth: false,
  },
  redirect: "/blink",
  children: [
    {
      path: "/blink/:id?/:datetime?",
      name: "BlinkDetection_Onsite",
      component: () =>
        import(
          /* webpackChunkName: "blink" */ "@/components/Onsite/BlinkDetection.vue"
        ),
    },
    {
      path: "/amsler-test/:id?/:datetime?/:position?",
      name: "AmslerTest_Onsite",
      component: () =>
        import(
          /* webpackChunkName: "amsler-test" */ "@/components/Onsite/AmslerTest.vue"
        ),
    },
    {
      path: "/color-test/:id?/:datetime?/:position?",
      name: "ColorTest_Onsite",
      component: () =>
        import(
          /* webpackChunkName: "color-test" */ "@/components/Onsite/ColorTest.vue"
        ),
    },
    {
      path: "/online/blink/:id?/:datetime?",
      name: "BlinkDetection_Online",
      component: () =>
        import(
          /* webpackChunkName: "blink" */ "@/components/Online/BlinkDetection.vue"
        ),
    },
    {
      path: "/online/amsler-test/:id?/:datetime?/:position?",
      name: "AmslerTest_Online",
      component: () =>
        import(
          /* webpackChunkName: "amsler-test" */ "@/components/Online/AmslerTest.vue"
        ),
    },
    {
      path: "/online/color-test/:id?/:datetime?/:position?",
      name: "ColorTest_Online",
      component: () =>
        import(
          /* webpackChunkName: "color-test" */ "@/components/Online/ColorTest.vue"
        ),
    },
    {
      path: "/online/contrast-sensitivity-test/:id?/:datetime?/:position?",
      name: "ContrastTest_Online",
      component: () =>
        import(
          /* webpackChunkName: "color-test" */ "@/components/Online/ContrastSensitivityTest.vue"
        ),
    },
  ],
};

export default AuthRoutes;
