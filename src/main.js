/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from "./App.vue";

// Composables
import { createApp, inject } from "vue";
import VueApexCharts from "vue3-apexcharts";
import Vue3Lottie from "vue3-lottie";
import "vue3-lottie/dist/style.css";

import Toast from "vue-toastification";
import axios from "axios";

// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

// Plugins
import { registerPlugins } from "@/plugins";

const app = createApp(App);

registerPlugins(app);

app.use(VueApexCharts);
app.use(Toast, Vue3Lottie, axios).mount("#app");

// app.mount("#app");
