// import bcrypt from "bcryptjs";
import moment from "moment";
import { database } from "@/plugins/firebase";
import { dbPrefix } from "@/plugins/setting";

export const emailRegex = /\b[\w.-]+@[\w.-]+\.[A-Za-z]{2,}\b/g;

export const phoneRegex =
  /\b(?:\d{3}|\(\d{3}\))[-. ()]?\d{3}[-. ()]?\d{4}(?: *x\d+)?\b/;

// Allow URLs without a protocol, Allow URLs with special characters, Allow URLs with a query string
export const stringUrlRegex =
  /((?:https?|ftp):\/\/[^\s]+)|(www\.[^\s]+\.[^\s]+(?:\/[\w-]+)*(?:\?[\w%&=]+)*)/gi;

export const arrayRegex = /\[(.*)\]/;

export const objectArrayRegex = /^\[(\{.*?\},?\s*)+\]$/;

export const codeRegex = /```[\s\S]*?```/g;

// "?/n" or "?/n/n" or "/n/n" in begining setence
export const newLineOrSpecificChar = /^\s*[\n\uFEFF\xA0"?]+/;

export function flattenParams(params) {
  const flattenedParams = { ...params };
  const keysToFlatten = [
    "complaints",
    "main_complaints",
    "clarified_main_complaints",
    "confirmed_questions",
  ];

  keysToFlatten.forEach((key) => {
    if (Array.isArray(flattenedParams[key])) {
      const flatArray = [];
      flattenedParams[key].forEach((item, index) => {
        if (typeof item === "object" && item !== null) {
          // Assuming the structure is object with numeric keys
          Object.values(item).forEach((subItem) => flatArray.push(subItem));
        } else {
          // Normal array element
          flatArray.push(item);
        }
      });
      flattenedParams[key] = flatArray;
    }
  });

  return flattenedParams;
}

export const formatString = (str) => {
  // This regular expression matches spaces, parentheses, dashes, and underscores.
  const regex = /[\s()\-_]+/g;

  // Replace matched characters with an empty string (effectively removing them).
  const formattedString = str.replace(regex, "");

  return formattedString;
};

export async function getFilesUrl(path) {
  const url = import.meta.env.VITE_APP_FILE_SERVER_URL + "/files?path=" + path;
  return fetch(url)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then((fileDetails) => {
      return fileDetails;
      // Handle the file details as needed
    })
    .catch((error) => {
      console.error(
        "There has been a problem with your fetch operation:",
        error
      );
    });
}

export const calculateAge = (dob) => {
  if (!dob) return 0;

  // Convert dob from 'dd/mm/yyyy' to 'yyyy-mm-dd'
  const parts = dob.split("/");
  const formattedDOB = `${parts[2]}-${parts[1]}-${parts[0]}`;

  const birthDate = new Date(formattedDOB);
  const today = new Date();
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();

  // Adjust the age if the current month is before the birth month
  // Or if it's the birth month but the current day is before the birth day
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }

  return age;
};

export const getGenderColor = (sex) => {
  if (sex === "Male") return "blue";
  if (sex === "Female") return "pink";
  return "grey";
};

export const timeStamp = () => {
  return `${moment().utc().format("YYYYMMDDHHmmss")}`;
};

export const blobToBase64 = (blob) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result.split(",")[1]);
    reader.onerror = reject;
    reader.readAsDataURL(blob);
  });
};

export const parseEyeSymptoms = (output) => {
  const lines = output
    .split("\n")
    .join(" ")
    .split(/Right Eye \(OD\):|Left Eye \(OS\):|Possible diseases:/)
    .map((s) => s.trim());
  const result = { OD: {}, OS: {}, PossibleDiseases: {} };

  // Handle OD section
  if (lines[1] !== "No symptoms reported") {
    let items = lines[1].split(" || ");
    items.forEach((item, idx) => {
      result["OD"][idx + 1] = item;
    });
  } else {
    result["OD"][1] = "No symptoms reported";
  }

  // Handle OS section
  if (lines[2] !== "No symptoms reported") {
    let items = lines[2].split(" || ");
    items.forEach((item, idx) => {
      result["OS"][idx + 1] = item;
    });
  } else {
    result["OS"][1] = "No symptoms reported";
  }

  // Handle PossibleDiseases section
  let diseases = lines[3].split(" || ");
  diseases.forEach((disease, idx) => {
    let [name, percentage] = disease.split(" - ");
    result["PossibleDiseases"][idx + 1] = { disease: name, percentage };
  });

  return result;
};

export const parseParagraphToStepFlow = (text, id, title, description) => {
  text = text.replace(/\\n/g, "\n");

  const regex =
    /(?:Step (\d+): (.*))|(?:- Brief description: (.*))|(?:- Sub-step (\d+): (.*))|(?:- Tip: (.*))/g;
  const nodes = [];
  const links = [];
  const maxLevels = 20;
  const topicCounter = new Array(maxLevels).fill(0);
  const subStepCounter = new Array(maxLevels).fill(0);
  const parentNodes = new Array(maxLevels).fill(null);
  const totalWidth = 15000;
  const totalHeight = 15000;
  const centerX = totalWidth / 2;
  const centerY = totalHeight / 2;
  const levelDistance = 400;
  const nodeDistance = 500;
  let nodeIdCounter = 0;

  let match;
  let prevNodeIndex;
  let currentMainStep = 0;
  let previousMainStepNodeId = null;
  while ((match = regex.exec(text)) !== null) {
    let level;
    let label;

    if (match[1]) {
      level = 1;
      label = match[2].trim();
      topicCounter[level - 1]++;
      prevNodeIndex = nodeIdCounter;
      currentMainStep = topicCounter[level - 1];
      subStepCounter[currentMainStep - 1] = 0;
    } else if (match[3]) {
      if (nodes[prevNodeIndex]) {
        nodes[prevNodeIndex].description = match[3].trim();
      }
    } else if (match[4]) {
      level = 2;
      label = match[5].trim();
      subStepCounter[currentMainStep - 1]++;
      prevNodeIndex = nodeIdCounter;
    } else if (match[6]) {
      if (nodes[prevNodeIndex]) {
        nodes[prevNodeIndex].description = match[6].trim();
      }
    }

    if (level !== undefined) {
      const nodeId = nodeIdCounter++;
      const parentNodeId = parentNodes[level - 1];

      if (level === 1 && previousMainStepNodeId !== null) {
        links.push({
          source: previousMainStepNodeId,
          target: nodeId,
        });
      } else if (level > 1) {
        links.push({
          source: parentNodeId,
          target: nodeId,
          linkType: "dash",
        });
      }

      const x =
        centerX +
        (level === 1
          ? nodeDistance * (topicCounter[level - 1] - 1)
          : nodeDistance * (currentMainStep - 1));
      const y =
        centerY +
        (level === 1
          ? 0
          : (subStepCounter[currentMainStep - 1] % 2 === 0 ? -1 : 1) *
            levelDistance *
            Math.ceil(subStepCounter[currentMainStep - 1] / 2)); // Adjusted positioning for sub-steps
      nodes.push({
        id: nodeId,
        label:
          level == 1 ? `Step ${topicCounter[level - 1]}: ${label}` : `${label}`,
        description: "",
        x,
        y,
        level,
      });

      parentNodes[level] = nodeId;
      if (level === 1) {
        previousMainStepNodeId = nodeId;
      }
    }
  }

  nodes.forEach((node) => {
    const combinedText = node.label + " " + node.description;
    const wordsCount = combinedText.split(/\s+/).length;

    if (node.level == 1) {
      // Make main step nodes bigger
      const width = 430;
      const height = 300;
      node.width = width;
      node.height = height;
    } else {
      // Keep sub-step nodes the same size as before
      const width = 200;
      const height = 160;
      node.width = width;
      node.height = height;
    }
  });

  return [
    {
      id,
      title,
      description,
      content: text,
      nodes,
      links,
    },
  ];
};

export const parseParagraphToMindMap = (text, id, title, description) => {
  // Replace '\\n' with '\n'
  text = text.replace(/\\n/g, "\n");

  const regex =
    /(?:Central Topic: (.*))|(?:- Description: (.*))|(?:Main Topic (\d+): (.*))|(?:- Description: (.*))|(?:- Subtopic (\d+): (.*))|(?:- Description: (.*))|(?:- Sub-subtopic (\d+): (.*))|(?:- Description: (.*))/g;
  const nodes = [];
  const links = [];
  const maxLevels = 20;
  const topicCounter = new Array(maxLevels).fill(0);
  const parentNodes = new Array(maxLevels).fill(null);
  const totalWidth = 15000;
  const totalHeight = 15000;
  const centerX = totalWidth / 2;
  const centerY = totalHeight / 2;
  const levelDistance = 400;
  const nodeDistance = 300;
  let nodeIdCounter = 0;

  let match;
  let prevNodeIndex;
  while ((match = regex.exec(text)) !== null) {
    let level;
    let label;
    let desc;

    if (match[1]) {
      level = 0;
      label = match[1].trim();
      prevNodeIndex = 0;
    } else if (match[2]) {
      nodes[prevNodeIndex].description = match[2].trim();
    } else if (match[3]) {
      level = 1;
      label = match[4].trim();
      topicCounter[level - 1]++;
      prevNodeIndex++;
    } else if (match[5]) {
      nodes[prevNodeIndex].description = match[5].trim();
    } else if (match[6]) {
      level = 2;
      label = match[7].trim();
      topicCounter[level - 1]++;
      prevNodeIndex++;
    } else if (match[8]) {
      nodes[prevNodeIndex].description = match[8].trim();
    } else if (match[9]) {
      level = 3;
      label = match[10].trim();
      topicCounter[level - 1]++;
      prevNodeIndex++;
    } else if (match[11]) {
      nodes[prevNodeIndex].description = match[11].trim();
    }

    if (level !== undefined) {
      const nodeId = nodeIdCounter++;
      const parentNodeId = parentNodes[level - 1];
      const nodeLabelNumber =
        level > 0 ? `${level}.${topicCounter[level - 1]}` : "";

      if (level > 0) {
        links.push({
          source: parentNodeId,
          target: nodeId,
        });
      }

      const x =
        centerX +
        (level === 1
          ? nodeDistance *
            (topicCounter[level - 1] - 1) *
            (parentNodeId % 2 === 0 ? 1 : -1)
          : level > 1
          ? nodeDistance *
              topicCounter[level - 1] *
              (parentNodeId % 2 === 0 ? 1 : -1) +
            (level === 2 ? (parentNodeId % 2 === 0 ? 50 : -50) : 0)
          : 0);
      const y = centerY + levelDistance * level;
      nodes.push({
        id: nodeId,
        label: `${nodeLabelNumber} ${label}`,
        description: "",
        x,
        y,
      });

      parentNodes[level] = nodeId;
    }
  }

  nodes.forEach((node) => {
    const combinedText = node.label + " " + node.description;
    const wordsCount = combinedText.split(/\s+/).length;
    const width = 200 + wordsCount * 10;
    const height = 160 + wordsCount * 5;
    node.width = width;
    node.height = height;
  });

  return [
    {
      id,
      title,
      description,
      content: text,
      nodes,
      links,
    },
  ];
};

export const formatTimeDifference = (time) => {
  const now = moment();
  const duration = moment.duration(now.diff(time));
  const years = duration.years();
  const months = duration.months();
  const days = duration.days();
  const hours = duration.hours();
  const minutes = duration.minutes();

  if (years) return `${years}y`;
  if (months) return `${months}M`;
  if (days) return `${days}d`;
  if (hours) return `${hours}h`;
  return `${minutes}m`;
};

// export const encodeValue = (val) => {
//   const salt = bcrypt.genSaltSync(10);
//   return bcrypt.hashSync(val, salt);
// };

export const getErrorTypeAndMessage = (errorString) => {
  const errorRegex = /openai\.error\.(.*):\s(.*)/;
  const match = errorString.match(errorRegex);
  if (match) {
    return match[1] + " : " + match[2];
  } else {
    return null;
  }
};

export const scrollToBottom = () => {
  console.log(document.body.scrollHeight);
  window.scrollTo(0, document.body.scrollHeight + 500);
};

export const uuidv4 = () => {
  var code = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
    /[xy]/g,
    function (c) {
      var r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    }
  );
  return `${moment().utc().format("YYYYMMDDHHmmss")}__${code}`;
};

export const removeSpecialChars = (sourceString) => {
  return sourceString.replace(/[`~!@#$%^&*()|+=?;:'",<>\{\}\[\]\\\/]/gi, "");
};

export const extractContactInfo = (paragraph) => {
  const phones = [];
  const emails = [];
  const urls = [];
  const codes = [];

  // Replace phone numbers with links and store them in the phones array
  const updatedParagraph = paragraph.replace(phoneRegex, function (match) {
    phones.push(match);
    const updatedPhone = `<a href="tel:${match}" style="color: #7700fe; text-decoration: none; border-bottom: 2px dashed #32557f; padding-bottom: 1px;">${match}</a>`;
    return updatedPhone;
  });

  // Find all email addresses and store them in the emails array
  const updatedParagraphWithEmails = updatedParagraph.replace(
    emailRegex,
    function (match) {
      emails.push(match);
      const updatedEmail = `<a href="mailto:${match}" style="color: #7700fe; text-decoration: none; border-bottom: 2px dashed #32557f; padding-bottom: 1px;">${match}</a>`;
      return updatedEmail;
    }
  );

  // Find all url addresses and store them in the urls array
  const updatedParagraphWithUrls = updatedParagraphWithEmails.replace(
    stringUrlRegex,
    function (match) {
      urls.push(match);
      const updatedUrl = `<a href="${match}" style=" color: #7700fe; text-decoration: none; border-bottom: 2px dashed #32557f; padding-bottom: 1px; ">${match}</a>`;
      return updatedUrl;
    }
  );

  // Find all code blocks and store them in the codes array
  const updatedParagraphWithCodes = updatedParagraphWithUrls.replace(
    codeRegex,
    function (match) {
      codes.push(match);
      const updatedCode = `<code style="background-color: black; color: white; font-size: 14px; padding: 5px; display: block; overflow-x: auto;">${match}</code>`;
      return updatedCode;
    }
  );

  // Return the updated paragraph and the arrays of phones, emails, urls, and codes
  return [updatedParagraphWithCodes, phones, emails, urls, codes];
};

export function setCookie(cname, cvalue, exdays) {
  let d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
  const name = cname;
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return "";
}

export const deleteCookie = (cname) => {
  const name = cname;
  document.cookie = name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
};

export function setLocalStorage(lname, lvalue) {
  localStorage.setItem(lname, lvalue);
  return true;
}

export function removeLocalStorage(lname) {
  localStorage.removeItem(lname);
  return true;
}

export function getLocalStorage(lname) {
  const stringData = localStorage.getItem(lname);
  if (!stringData) {
    return null;
  }
  return stringData;
}

export function parseLocalStorage(lname) {
  const stringData = getLocalStorage(lname);
  if (!stringData) {
    return null;
  }
  return JSON.parse(stringData);
}

export function capitalize(s) {
  if (!s) return null;
  return s[0].toUpperCase() + s.slice(1);
}

export const convertPriceString = (val, isVND = true, forceMinus = false) => {
  if (val == 0) return "0.000" + (isVND ? " VND" : "");
  if (!val || isNaN(val)) return "";

  // if val is actually < 0 => forceMinus = true
  if (val < 0) {
    return convertPriceString(Math.abs(val), isVND, true);
  }

  const valStr = typeof val === "string" ? val : val.toString();
  const valDec = valStr.split(".")[0];

  var result =
    valDec.replace(/./g, function (c, i, a) {
      return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
    }) + (isVND ? " VND" : "");

  return `${forceMinus ? "- " : ""}` + result;
};

export const deconvertPriceString = (str) => {
  if (!str) return 0;
  var isMinus = str.includes("-");
  var numb = str.match(/\d/g);
  numb = numb.join("");
  if (isMinus) {
    numb = "-" + numb;
  }
  return parseInt(numb);
};

export const getNumberFromString = (str) => {
  if (!str) return 0;
  var numb = str.match(/\d/g);
  numb = numb.join("");
  return parseInt(numb);
};

export const formatNumber = (val) => {
  if (!val || isNaN(val)) return "";

  const valStr = typeof val === "string" ? val : val.toString();
  const valDec = valStr.split(",")[0];
  // const valExponent = valStr.split('.')[1] ? ',' + valStr.split('.')[1] : ''

  return valDec.replace(/,/g, function (c, i, a) {
    return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "," + c : c;
  });
};

export const currencyFormat = (num, position, char) => {
  if (num == null) {
    return 0;
  }

  var number = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");

  switch (position) {
    case "head":
      number = char + number;
      break;
    case "end":
      number = number + char;
      break;

    default:
      break;
  }

  return number;
};

/**
 * Convert a vietnamese characters into alphabet characters to compare easily
 * @param {*} input
 * @return string alphabet
 */
export const transliterateChar = (input) => {
  var mappings = {
    ĂÂÀẰẦẢẲẨÃẴẪÁẮẤẠẶẬ: "A",
    ÊÈỀẺỂẼỄÉẾẸỆ: "E",
    ÌỈĨÍỊ: "I",
    ÔƠÒỒỜỎỔỞÕỖỠÓỐỚỌỘỢ: "O",
    ƯÙỪỦỬŨỮÚỨỤỰ: "U",
    ỲỶỸỴ: "Y",
    ăâàằầảẳẩãẵẫáắấạặậ: "a",
    êèềẻểẽễéếẹệ: "e",
    ìỉĩíị: "i",
    ôơòồờỏổởõỗỡóốớọộợ: "o",
    ưùừủửũữúứụự: "u",
    ỳỷỹỵ: "y",
  };
  for (const c of input.split(""))
    for (const mapping in mappings)
      if (mapping.includes(c)) input = input.replaceAll(c, mappings[mapping]);
  return input;
};

export const sleep = (milliseconds = 100) => {
  return new Promise((resolve) => {
    var timeout = setTimeout(() => {
      clearTimeout(timeout);
      resolve();
    }, milliseconds);
  });
};

// convert long string to part of string, between word is space
export const convertLongWordToEasyWord = (stringInput = "", result) => {
  let string = stringInput;
  let resultString = result;

  for (let i = 0; i < string.length; i++) {
    let char = string.charAt(i);
    if (char == " ") {
      i = i + 1;
      char = string.charAt(i).toUpperCase(); // get next char
    }
    if (char == char.toUpperCase()) {
      resultString =
        i == 0 && resultString == "" ? char : `${resultString} ${char}`;
    } else {
      if (i == 0 && resultString == "") {
        resultString = char.toUpperCase();
      } else {
        resultString += `${char}`;
      }
    }
    if (i + 1 == string.length) {
      return resultString;
    }
    let nextString = string.slice(i + 1, string.length);
    return convertLongWordToEasyWord(nextString, resultString);
  }
  return result;
};

export const calculateAverage = (items) => {
  var sum = 0;
  for (var i = 0; i < items.length; i++) {
    sum += parseInt(items[i], 10); //don't forget to add the base
  }
  return sum / items.length;
};

export const getUniqueValues = (items) => {
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  return items.filter(onlyUnique);
};

export const $dbWatcher = async (ref, func) => {
  database.ref(`${dbPrefix}/${ref}`).on("value", (snapshot) => {
    const val = snapshot.val();
    if (!func) return;
    func(val, snapshot.ref);
  });
};

export const $dbSet = async (ref, data) => {
  return new Promise((resolve) => {
    return database.ref(`${dbPrefix}/${ref}`).set(data, (err) => {
      if (err) {
        console.log(err.message);
        return resolve(false);
      }
      return resolve(data);
    });
  });
};

export const $dbPush = (ref, data) => {
  return new Promise((resolve, reject) => {
    const newRef = database.ref(`${dbPrefix}/${ref}`).push(data, (err) => {
      if (err) {
        console.log(err.message);
        reject(err);
      } else {
        resolve(newRef.key);
      }
    });
  });
};

export const $dbGet = (ref) => {
  return new Promise((resolve) => {
    database
      .ref()
      .child(`${dbPrefix}`)
      .child(ref)
      .get()
      .then((snapshot) => {
        if (snapshot.exists()) {
          if (snapshot.val() == true) {
            return resolve("");
          }
          return resolve(snapshot.val());
        } else {
          console.log("No data available");
          return resolve("");
        }
      })
      .catch((error) => {
        console.error(error);
        return resolve("");
      });
  });
};

export const $dbRemove = (ref) => {
  return new Promise((resolve) => {
    database
      .ref(`${dbPrefix}/${ref}`)
      .remove()
      .then(() => {
        return resolve(true);
      });
  });
};
