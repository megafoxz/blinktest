// Styles
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";

// Composables
import { createVuetify } from "vuetify";

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  components: {},
  theme: {
    themes: {
      defaultTheme: "light",
      light: {
        colors: {
          primary: "#fdfffe",
          secondary: "#e3e3e3",
          mindmap_secondary: "#fdfffe",
          assitant_chatbubble: "#e3e3e3",
          question_button: "#9E9E9E",
        },
      },
      dark: {
        colors: {
          primary: "#051723",
          secondary: "#000000",
          mindmap_secondary: "#000000",

          assitant_chatbubble: "#FFFFFF",
          question_button: "#FFFFFF",
          "primary-darken-1": "#051723",
          "secondary-darken-1": "#051723",
        },
      },
    },
  },
});
