import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/database";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBMumAZajdHgN-XwTPb60yh6kbTWJ5Jkys",
  authDomain: "flowbuider.firebaseapp.com",
  databaseURL:
    "https://flowbuider-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "flowbuider",
  storageBucket: "flowbuider.appspot.com",
  messagingSenderId: "1094195448231",
  appId: "1:1094195448231:web:6786840c3e55e2bcfcce82",
  measurementId: "G-HZ44KV6SY4",
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const database = app.database();
const auth = app.auth();
// Google authentication provider
const provider = new firebase.auth.GoogleAuthProvider();
const firestore = app.firestore();

export { app, provider, auth, database, firestore };
