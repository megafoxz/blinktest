// Modify Cookie Name and Local Storage Name
export const ApplicationCookieTokenIDName = "id_token";
export const ApplicationLocalStorageUserDataName = "user_data";
export const DefaultLanguageLocale = "en";
export const dbPrefix = "APICORE";

//Flow Ryo Test
export const DefaultFlowChatQuestion = {
  flowID: "20230605140151__b70edeac-9a42-40a9-8805-2b58fbb22c18",
  title: "Ryo Flow Question 2",
  description: "Ryo Flow Question 2",
  roomID: 1,
  personaID: "20230425082413__bc790452-4abb-4418-ad39-6618a7fa7675",
  personaTitle: "Act as a AI Assistant",
  personaDescription: "",
  defaultGreetingMessage: `Hello there, I am Ryo, your Assistant. How can I assist you today?`,
};

export const DefaultFlowMindmap = {
  flowID: "20230605141006__3fe1d560-6e3b-40ed-827c-1240d2475c9d",
  title: "Ryo MindMap",
  description: "Ryo MindMap",
};

export const DefaultFlowStepFlow = {
  flowID: "20230605141202__b5275ef6-257b-4cdd-a7f1-65c28fccb44f",
  title: "Ryo Step Flow",
  description: "Ryo Step Flow",
};

export const DefaultOPD = {
  flowID: "20230805114252__88bbdbdd-d33c-4829-bb04-768b5ac455e7",
  title: "OPD Flow",
  description: "OPD Flow",
};

export const FundusFlow = (id, name) => {
  return {
    flowID: "20231213012935__3a8c77b0-4c93-43b8-895b-2825a906cc78",
    title: `${id}_${name}`,
    description: "Fundus Image Processing",
  };
};

export const ImageDiagnosis = (id, name) => {
  return {
    flowID: "20231222010632__02f3e20e-e98b-40d6-8786-b5a334fe1887",
    title: `${id}_${name}`,
    description: "Image Eye Diagnosis",
  };
};

export const ImageQualityChecker = (id, name) => {
  return {
    flowID: "20240325044428__bea9e106-ba71-4dd4-a89e-15f5e1f27f21",
    // flowID: "20240401082120__3bce77a9-954d-4753-89b9-d80a9aa83393",
    title: `${id}_${name}`,
    description: "Image Quality Checker",
  };
};
