// src/plugins/contant.js
import plate_1 from "@/assets/images/color_test/8.png";
import plate_2 from "@/assets/images/color_test/12.png";
import plate_3 from "@/assets/images/color_test/29.png";
import plate_4 from "@/assets/images/color_test/5.png";
import plate_5 from "@/assets/images/color_test/15.png";
import plate_6 from "@/assets/images/color_test/74.png";
import plate_7 from "@/assets/images/color_test/42.png";
import plate_8 from "@/assets/images/color_test/45.png";

export const Plates = [
  {
    id: 1,
    rightAnswer: 8,
    images: plate_1,
    answers: [8, 16, 3, 0],
    description_1: "Nhìn bình thường",
    blindAnswer: [3, 0, 16],
    description_2: "Thiếu hụt màu đỏ-xanh lá cây",
  },
  {
    id: 2,
    rightAnswer: 12,
    images: plate_2,
    answers: [3, 4, 12, 22],
    description_1: "Mọi người đều nên thấy số 12",
    blindAnswer: [3, 4, 22],
    description_2: "Mọi người đều nên thấy số 12",
  },
  {
    id: 3,
    rightAnswer: 29,
    images: plate_3,
    answers: [20, 29, 12, 70],
    description_1: "Nhìn bình thường",
    blindAnswer: [70, 12, 20],
    description_2: "Thiếu hụt màu đỏ-xanh lá cây",
  },
  {
    id: 4,
    rightAnswer: 5,
    images: plate_4,
    answers: [3, 44, 5, 11],
    description_1:
      "Số được hiển thị ở trên thực tế là số 5. Hầu hết những người mù màu sẽ không thấy rõ số này.",
    blindAnswer: [3, 44, 11],
    description_2:
      "Số được hiển thị ở trên thực tế là số 5. Hầu hết những người mù màu sẽ không thấy rõ số này.",
  },
  {
    id: 5,
    rightAnswer: 15,
    images: plate_5,
    answers: [15, 17, 32, 66],
    description_1: "Nhìn bình thường",
    blindAnswer: [17, 32, 66],
    description_2: "Những người mù màu đỏ-xanh lá cây thấy số 17.",
  },
  {
    id: 6,
    rightAnswer: 74,
    images: plate_6,
    answers: [74, 4, 21, 88],
    description_1: "Nhìn bình thường",
    blindAnswer: [4, 11, 88],
    description_2: "Những người mù màu đỏ-xanh lục thấy số 21 ",
  },
  {
    id: 7,
    rightAnswer: 42,
    images: plate_7,
    answers: [32, 7, 42, 11],
    description_1: "Nhìn bình thường",
    blindAnswer: [32, 7, 11],
    description_2:
      "Nếu thấy số 2, số 4 mờ thì mù màu đỏ (protanopia) Nếu thấy số 4, số 2 mờ thù mù màu xanh lục (deuteranopia)",
  },
  {
    id: 8,
    rightAnswer: 45,
    images: plate_8,
    answers: [11, 5, 2, 45],
    description_1: "Nhìn thấy số 45: là người có thị giác màu bình thường",
    blindAnswer: [11, 5, 2],
    description_2: "Phần lớn người mù màu không thể nhìn rõ con số này",
  },
];
