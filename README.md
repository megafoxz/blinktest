## Prerequisites

- Git: [Download Git](https://git-scm.com/downloads)
- Docker: [Download Docker](https://www.docker.com/products/docker-desktop)
- Docker Compose: [Download Docker Compose](https://docs.docker.com/compose/install/)

## Instructions
